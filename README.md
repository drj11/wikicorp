# What are the most popular words on Wikipedia?

In typography we may wish to answer questions like,
how frequently does the digraph `kt` appear?

In order to provide a basis for answering these questions,
I have decided to build a corpus of text from Wikipedia.

The initial goal will be to ingest sufficient text
to accurately rank all words that appear more frequently than
1 in 10,000 words.

This repository is the software arm;
it currently also hosts the actual corpus, but
that may change in the future.


## Building and Running

The program `wikicorp` is written in Go, and can be built at the
top-level if you have a working Go environment:

    make

To run it requires a CSV (probably one downloaded from
toolforge, see below).
Which wikipedia API server you use depends on which Wikipedia
pages you want (en for en.wikipedia.org, cy for
cy.wikipedia.org, and so on):

    mkdir crawl
    wikicorp -wiki cy topviews-2021.csv


## Resources

toolforge shows most popular pages:

https://pageviews.toolforge.org/topviews/?project=en.wikipedia.org&platform=all-access&date=last-year&excludes=

And makes the top 500 CSV available as a download.

## Method

From Wikipedia logs, find the most frequently viewed pages.

Accumulate the most popular pages until we have about 1,000,000
words.

Do some sums.

## Data

I scraped the top 500.
I got 14 million words (as delivered HTML).

# END
