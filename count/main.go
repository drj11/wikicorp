package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"regexp"
)

var countp = flag.String("count", " ", "string to count")

func main() {
	err := mainerr()
	if err != nil {
		log.Fatal(err)
	}
}

func mainerr() error {
	flag.Parse()

	var count uint64
	for _, file_name := range flag.Args() {
		r, err := os.Open(file_name)
		if err != nil {
			return err
		}
		count += Count(r, *countp)
	}
	fmt.Println(*countp, count)
	return nil
}

func Count(r io.Reader, k string) uint64 {
	bs, err := ioutil.ReadAll(r)
	if err != nil {
		log.Fatal(err)
	}

	re := regexp.MustCompile(regexp.QuoteMeta(k))
	matches := re.FindAll(bs, -1)
	return uint64(len(matches))
}
