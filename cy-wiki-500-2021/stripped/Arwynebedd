 Data cyffredinol
Mathintegryn pendant, maint corfforol, meintiau deilliadol ISQ, meintiau sgalar, area 
 Ffeiliau perthnasol ar Gomin Wicimedia

Priodwedd meintiol yw arwynebedd (sy'n enw gwrywaidd) a mesuriad y gofod dau ddimensiwn mae’n ei orchuddio. Mesurir arwynebedd, fel arfer, mewn sgwariau e.e. milimetr sgwâr, centimetr sgwâr. Defnyddir y term 'arwyneb yr arwynebedd' am surface area.[1]

      Mae cyfanswm arwynebedd y tri siâp hyn oddeutu 15.57 sgwâr.
Arwynebedd, felly, yw'r lamina planar, yn  y plân, a'r  faint o baent sy’n angenrheidiol i orchuddio’r wyneb ag un gôt.[2] Mae'n analog dau ddimensiwn o hyd cromlin (cysyniad un dimensiwn) neu gyfaint solid (cysyniad tri dimensiwn).
Gellir mesur arwynebedd siâp trwy gymharu'r siâp â sgwariau o faint sefydlog: gweler y diagram.[3] Yn y System Ryngwladol o Unedau (OS), yr uned arwynebedd safonol yw'r metr sgwâr (wedi'i ysgrifennu fel m2), sef arwynebedd sgwâr y mae ei ochrau'n un fetr o hyd.[4] Byddai gan siâp ag arwynebedd o dri metr sgwâr yr un arwynebedd â thri sgwâr o'r fath. Mewn mathemateg, diffinnir sgwâr fel uned sydd ag arwynebedd o un, ac mae arwynebedd unrhyw siâp neu arwyneb arall yn rhif real di-ddimensiwn.

      Mae gan y sgwâr hwn a'r ddisg hon yr un arwynebedd (gweler: sgwario'r cylch).
Mae yna sawl fformiwla adnabyddus ar gyfer arwynebedd siapiau syml fel trionglau, petryalau, a chylchoedd. Gan ddefnyddio'r fformwlâu hyn, gellir dod o hyd i arwynebedd unrhyw bolygon trwy rannu'r polygon yn drionglau.[5] Ar gyfer siap â ffin grom, fel rheol mae angen calcwlws i gyfrifo'r arwynebedd. Yn wir, roedd y broblem o bennu arwynebedd ffigurau plân yn gymhelliant mawr i ddatblygiad hanesyddol calcwlws.[6]
Ar gyfer siâp solet fel sffêr, côn, neu silindr, gelwir arwynebedd ei ffin yn "arwyneb yr arwynebedd".[7][8][9] Cyfrifwyd fformiwlâu ar gyfer arwynebedd siapiau syml gan yr hen Roegiaid, ond fel rheol mae cyfrifo arwynebedd siâp mwy cymhleth yn gofyn am galcwlws aml-newidyn (<i>multivariable</i>).
Mae arwynebedd yn chwarae rhan bwysig mewn mathemateg fodern. Yn ychwanegol at ei bwysigrwydd amlwg mewn geometreg a chalcwlws, mae arwynebedd yn gysylltiedig â'r diffiniad o benderfynyddion mewn algebra llinol, ac mae'n briodwedd sylfaenol arwynebau mewn geometreg wahaniaethol. Mewn dadansoddiad, diffinnir arwynebedd is-set o'r blanau gan ddefnyddio mesur Lebesgue,[10] er nad yw pob is-set yn fesuradwy.[11] Yn gyffredinol, mae arwynebedd mewn mathemateg-uwch yn cael ei ystyried yn achos arbennig o gyfaint ar gyfer rhanbarthau dau-ddimensiwn.[7]
Gellir diffinio arwynebedd trwy ddefnyddio gwirebau, gan ei ddiffinio fel swyddogaeth casgliad o rai ffigurau plân i'r set o rifau real. Gellir profi bod ffwythiant o'r fath yn bodoli.

  Cynnwys
 
1 Unedau

2 Fformiwlau defnyddiol
 
2.1 Arwynebeddau gwrthrychau 2-dimensiwn

2.2 Arwynebedd arwyneb siapiau 3-dimensiwn




3 Diffiniad ffurfiol

4 Unedau
 
4.1 Trawsnewid
 
4.1.1 Unedau nad ydynt yn fetrig




4.2 Unedau eraill gan gynnwys unedau hanesyddol




5 Hanes
 
5.1 Arwynebedd y cylch

5.2 Ardwynebedd triongl

5.3 Arwynebedd pedrochr

5.4 Arwynebedd y polygon cyffredinol

5.5 Arwynebedd a bennir gan ddefnyddio calcwlws




6 Fformiwlâu arwynebedd
 
6.1 Fformiwlâu polygon
 
6.1.1 Petryalau




6.2 Rhestr o fformiwlâu

6.3 Perthynas yr arwynebedd â'r perimedr




7 Cyfeiriadau




Unedau
Mae'r canlynol yn enghreifftiau o unedau arwynebedd:

 metr sgwâr = System Ryngwladol o Unedau

ar = 100 metr sgwâr

hectar = 10,000 metr sgwâr

cilomedr sgwâr = 1,000,000 metr sgwâr

megametr sgwâr = 1012 metr sgwâr

Unedau traddodiadol "Ymerodol", fel y'i diffinir eisoes o'r metr:

 troedfedd sgwâr = 0.09290304 metr sgwâr

llathen sgwâr = 9 troedfedd sgwâr = 0.83612736 metr sgwâr

erw = 43,560 troedfedd sgwâr = 4046.8564224 metr sgwâr

milltir sgwâr = 640 erw = 2.5899881103 cilomedr sgwâr

Fformiwlau defnyddiol
Arwynebeddau gwrthrychau 2-dimensiwn
 Sgwâr neu betryal: h w (lle h yw'r hyd, ac w yw'r lled; mewn achos sgwâr, mae h = w).

Cylch:  (lle r yw'r radiws)

Hirgrwn:  (lle a a b yw'r echelinau lled-fwyaf a lled-leiaf)

Unrhyw bolygon rheolaidd:  (lle mae P = hyd y perimedr, ac a yw hyd apothem y polygon [y pellter o ganolbwynt y polygon i ganolbwynt un o'r ymylon])

Paralelogram:  (Y sail S yw hyd rhyw ymyl, a'r uchder u yw'r pellter rhwng y llinellau sy'n cynnwys yr ymylon sydd â hyd S)

Trapesoid:  (hydoedd yr ymylon paralel yw B a b, ac u yw'r pellter rhwng y llinellau sy'n cynnwys yr ymylon paralel)

Triongl:  (lle mae S yn unrhyw ymyl, ac u yw'r pellter rhwng y llinell sy'n cynnwys S i gornel arall y triongl). Gellir defnyddio'r fforwla hwn os wybyddir yr uchder u. Os gwybyddir hyd y tri ymyl, yna gellir defnyddio'r fformwla canlynol:  (lle a, b, c yw hydoedd yr ymylon, ac  yw hanner hyd ei berimedr)

Mae'r arwynebedd rhwng graff dwy ffwythiant yn hafal i integreiddad un o ffwythiannau f(x), tynnu integreiddiad y ffwythiant arall, g(x).

Yr arwynebedd a amgylchynir gan ffwythiant r = r(θ) â mynegwyd cyfesurynnau polar yw .

Rhoddir yr arwynebedd a amgylchynir gan gromlin barametraidd  â diweddbwyntiau  gan integreiddiad llwybr

  


 neu cyfernod-z

  


Arwynebedd arwyneb siapiau 3-dimensiwn
 Ciwb: , lle h yw hyd unrhyw ymyl

Bocs petryalog: , lle dynoda h, w, ac u hyd, lled, ac uchder y bocs

Sffêr: , π yw gymhareb hyd cylchyn a diamedr cylch, 3.14159..., ac r yw radiws y sffêr

Sylindr: , lle r yw radiws y sail, ac u yw'r uchder

Côn: , lle r yw radiws y sail, ac u yw'r uchder.

Diffiniad ffurfiol
Dull o ddiffinio'r hyn a olygir wrth "arwynebedd" yw trwy wirebau. Gellir diffinio "arwynebedd" fel ffwythiant o gasgliad M o fathau arbennig o ffigurau plân  (a elwir yn setiau mesuradwy) i'r set o rifau real, sy'n bodloni'r priodweddau canlynol:[12]

 Ar gyfer pob S yn M, a(S) ≥ 0.

Os yw S a T yn M yna mae S ∪ T a S ∩ T, a hefyd a(S∪T) = a(S) + a (T) - mae (S∩T).

Os yw S a T yn M gyda S ⊆ T yna mae T - S yn M ac a( T-S ) = a(T) - a(S).

Os yw set S yn M ac S yn gyfath â T yna mae T hefyd yn M ac a(S) = a(T).

Mae pob petryal R yn M. Os oes gan y petryal hyd h a lled o k yna a(R) = hk.

Gadewch i Q fod yn set wedi'i hamgáu rhwng dau ranbarth S a T. Mae rhanbarth cam (step region) yn cael ei ffurfio o undeb meidraidd o betryalau cyfagos sy'n gorffwys ar sylfaen gyffredin, h.y. S ⊆ Q ⊆ T. Os oes rhif unigryw c fel bod a(S) ≤ c ≤ a(T) ar gyfer pob rhanbarth cam S a T o'r math hwn, yna a(Q) = c.

Gellir profi, felly, bod ffwythiant arwynebedd o'r fath yn bodoli.[13]

Unedau
      Pedadrat metr sgwâr wedi'i wneud o bibell PVC.
Mae gan bob uned o hyd uned arwynebedd gyfatebol, sef arwynebedd sgwâr â'r hyd ochr penodol, fel y dywedwyd ar ddechrau'r erthygl hon. Felly gellir mesur arwynebedd mewn metrau sgwâr (m2), centimetrau sgwâr (cm2 ), milimetrau sgwâr (mm2 ), cilometrau sgwâr (km2 ), troedfeddi sgwâr (tr2 ), milltiroedd sgwâr (mi2), ac ati.[14] Yn algebraaidd, gellir meddwl am yr unedau hyn fel sgwariau'r unedau hyd cyfatebol.
Uned arwynebedd SI yw'r 'fetr sgwâr', sy'n cael ei ystyried yn uned sy'n deillio o SI.[15]

Trawsnewid
      Er bod 10 mm mewn 1 cm, ceir 100 mm2 mewn 1 cm2 .
Byddai cyfrifo arwynebedd sgwâr y mae ei hyd a'i led yn 1 fetr fel a ganlyn:
1 metr × 1 metr = 1 m2
ac felly, byddai gan betryal â gwahanol ochrau (dyweder hyd 3 metr a lled 2 fetr) arwynebedd mewn unedau sgwâr y gellir ei gyfrif fel:
3 metr × 2 fetr = 6 m 2 . Mae hyn gyfwerth â 6 miliwn milimetr sgwâr. Ymhlith y trawsnewidadau ymarferol eraill y mae:

 1 cilomedr sgwâr = 1,000,000 metr sgwâr

1 metr sgwâr = 10,000 centimetr sgwâr = 1,000,000 milimetr sgwâr

1 centimetr sgwâr = 100 milimetr sgwâr.

Unedau nad ydynt yn fetrig
Mewn unedau nad ydynt yn fetrig, y trawsnewid rhwng dwy uned sgwâr yw sgwâr y trawsnewid rhwng yr unedau hyd cyfatebol.

 1 troedfedd = 12 modfedd,

ac mae'r berthynas rhwng troedfeddi sgwâr a modfeddi sgwâr yn

 1 troedfedd sgwâr = 144 modfedd sgwâr,

lle mae 144 = 122 = 12 × 12. Yn yr un modd:

 1 llathen sgwâr = 9 troedfedd sgwâr

1 filltir sgwâr = 3,097,600 llath sgwâr = 27,878,400 troedfedd sgwâr

Yn ogystal, mae ffactorau trawsnewidiad eraill yn cynnwys:

 1 fodfedd sgwâr = 6.4516 centimetr sgwâr

1 troedfedd sgwâr = 0.092903 04 metr sgwâr

1 llathen sgwâr = 0.836127 36 metr sgwâr

1 filltir sgwâr = 2.589988 110 336 cilomedr sgwâr

Unedau eraill gan gynnwys unedau hanesyddol
Mae yna sawl uned gyffredin arall ar gyfer arwynebedd. Yr âr oedd yr uned wreiddiol o ran arwynebedd yn y system fetrig, gyda'r gair yn golygu tir wedi'i aredig. Roedd:

 1 âr = 100 metr sgwâr

Er bod yr âr wedi peidio a bodoli o ddydd i ddydd, mae'r hectar yn dal i gael ei ddefnyddio'n gyffredin i fesur tir:[16]

 1 hectar = 100 âr = 10,000 metr sgwâr = 0.01 cilomedr sgwâr

Defnyddir yr erw yn gyffredin hefyd i fesur arwynebedd tir, lle mae

 1 erw = 4,840 llathen sgwâr = 43,560 troedfedd sgwâr.

Mae erw oddeutu 40% o hectar.
Ar y raddfa atomig, mesurir arwynebedd mewn unedau '<i>barn</i>', fel:[14]

 1 'barn' = 10 −28 metr sgwâr.

Defnyddir y mesur hwn yn gyffredin wrth ddisgrifio maes trawsdoriadol rhyngweithio mewn ffiseg niwclear.[17]
Yn India ,

 20 dhurki = 1 dhur

20 dhur = 1 khatha

20 khata = 1 bigha

32 khata = 1 erw

Hanes
Arwynebedd y cylch
Yn y 5g CC, Hippocrates of Chios oedd y cyntaf i ddangos bod arwynebedd disg (sef y rhanbarth wedi'i amgáu gan gylch) yn gymesur â sgwâr ei ddiamedr,[18] ond ni nododd gysondeb y cymesuredd (constant of proportionality). Canfu Eudoxus o Cnidus, hefyd yn y 5g CC, fod arwynebedd disg yn gymesur â'i radiws sgwâr.[19]
Yn dilyn hynny, deliodd Llyfr I o Elfennau Euclid â hafaledd arwynebedd rhwng ffigurau dau-ddimensiwn. Defnyddiodd y mathemategydd Archimedes offer geometreg Ewclidaidd i ddangos bod yr arwynebedd y tu mewn i gylch yn hafal i arwynebedd triongl ongl sgwâr sydd a hyd ei waelod yn hafal i gylchedd y cylch, ac y mae ei uchder yn hafal i radiws y cylch, yn ei lyfr Mesur Cylch. (Y cylchedd yw 2πr, ac mae arwynebedd triongl yn hanner y sylfaen wedi'i luosi gyda'i uchder, sef πr2 ar gyfer y ddisg.) Amcangyfrifodd Archimedes gwerth π gyda'i ddull dyblu, lle arysgrifiodd driongl rheolaidd mewn cylch a nodi ei arwynebedd, yna dyblodd nifer yr ochrau i roi hecsagon rheolaidd, yna dyblu nifer yr ochrau dro ar ôl tro wrth i ardal y polygon agosáu ac yn agosach at arwynebedd y cylch (a gwnaeth yr un peth â pholygonau wedi'u hamgylchu).
Profodd y gwyddonydd o'r Swistir Johann Heinrich Lambert ym 1761 fod π, cymhareb arwynebedd cylch â'i radiws sgwâr, yn afresymol (rhif anghymarebol), sy'n golygu nad yw'n hafal i gyniferydd unrhyw ddau rif cyfan.[20] Ym 1794 profodd y mathemategydd Ffrengig Adrien-Marie Legendre fod π2 yn afresymol; mae hyn hefyd yn profi bod π yn afresymol.[21] Ym 1882, profodd mathemategydd Almaeneg Ferdinand von Lindemann fod π yn drosgynnol, gan gadarnhau rhagdybiaeth a wnaed gan Legendre ac Euler.[20]

Ardwynebedd triongl
Daeth Heron (neu Hero) o Alexandria o hyd i'r hyn a elwir yn fformiwla Heron ar gyfer arwynebedd triongl o ran ei ochrau, a gellir gweld prawf yn ei lyfr, Metrica, a ysgrifennwyd tua 60 CE. Awgrymwyd bod Archimedes  yn gwybod y fformiwla dros ddwy ganrif yn gynharach,[22] a chan fod Metrica yn gasgliad o'r wybodaeth fathemategol sydd ar gael yn yr hen fyd, mae'n bosibl bod y fformiwla yn rhagddyddio'r cyfeiriad a roddir yn y gwaith hwnnw.[23]
Yn 499 mynegodd Aryabhata, mathemategydd a seryddwr o oes glasurol mathemateg Indiaidd, arwynebedd triongl fel hanner y sylfaen yn fwy na'r uchder yn yr Aryabhatiya (adran 2.6).

Arwynebedd pedrochr
Yn y 7g, datblygodd Brahmagupta fformiwla, a elwir bellach yn fformiwla Brahmagupta, ar gyfer arwynebedd pedrochrog cylchol (pedrochr wedi'i arysgrifio mewn cylch) o ran ei ochrau. Yn 1842 daeth mathemategwyr yr Almaen Carl Anton Bretschneider a Karl Georg Christian von Staudt o hyd i fformiwla, a elwir yn fformiwla Bretschneider, ar gyfer arwynebedd unrhyw pedrochr.

Arwynebedd y polygon cyffredinol
Caniataodd datblygiad y cyfesurynnau Cartesaidd gan René Descartes yn yr 17g i Carl Friedrich Gauss,  ddwy ganrif yn ddiweddarach i ddatblygu fformiwla'r syrfëwr ar gyfer arwynebedd unrhyw bolygon â lleoliadau fertig.

Arwynebedd a bennir gan ddefnyddio calcwlws
Roedd datblygu calcwlws integrol ar ddiwedd yr 17g yn darparu offer y gellid eu defnyddio wedi hynny ar gyfer cyfrifo arwynebedd mwy cymhleth, megis arwynebedd elips ac arwyneb gwahanol wrthrychau tri dimensiwn crwm.

Fformiwlâu arwynebedd
Fformiwlâu polygon
Ar gyfer polygon (syml ) nad yw'n hunan-groestori, mae'r cyfesurynnau Gartesaidd  (I = 0, 1, ..., n -1) lle mae'r fertigau n yn hysbys, mae'r arwynebedd yn cael ei roi gan y fformiwla syrfëwr hwn:[24]

 

a pan fo i = n -1, yna mae i+1 yn  cael ei fynegi felmodwlws n ac felly mae'n cyfeirio at 0.

Petryalau
      Mae arwynebedd y petryal hwn yn lw.
Y fformiwla arwynebedd fwyaf sylfaenol yw'r fformiwla ar gyfer arwynebedd petryal. O ystyried petryal gyda hyd l a lled w, y fformiwla ar gyfer yr arwynebedd yw:[25][26]

 A = lw  (petryal).

Hynny yw, arwynebedd y petryal yw'r hyd wedi'i luosi â'r lled. Fel achos arbennig, fel l = w yn achos sgwâr, rhoddir arwynebedd sgwâr â hyd ochr s yn y fformiwla:[7][25][27]

 A = s2  (sgwâr).

Mae'r fformiwla ar gyfer arwynebedd petryal yn dilyn yn uniongyrchol o briodweddau sylfaenol arwynebedd, ac weithiau fe'i cymerir fel diffiniad neu wireb. Ar y llaw arall, os datblygir y geometreg cyn y rhifyddeg, gellir defnyddio'r fformiwla hon i ddiffinio lluosi rhifau real.

Rhestr o fformiwlâu

Rhestr o fformiwlâu cyffredinol ar gyfer arwynebedd:


Siap

Fformiwla

Newidynnau



Petryal







Triongl







Triongl







Triongl  
(Fformiwla Heron)




 



Triongl isosgeles







Triongl rheolaidd  
(equilateral triangle)








Rhombws/Barcud







Paralelogram







Trapesiwm







Hecsagon rheolaidd







Octagon rheolaidd







Polygon rheolaidd  
( sides)









 
 (Perimedr)


 mewngylch radiws
 amgylch




Cylch


( Diamedr)






Sector cylch







Elíps







Integryn









Arwynebedd yr arwyneb





Sffêr  







Ciwboid







Silindr  
(incl. bottom and top)








Côn  
(incl. bottom)








Torws







Arwynebedd y cylchdro

  
(cylchdro o amgylch echelin-x )






Mae'r cyfrifiadau uchod yn dangos sut i ddod o hyd i arwynebedd llawer o siapiau cyffredin.

Perthynas yr arwynebedd â'r perimedr
Mae'r anghydraddoldeb isoperimetrig yn nodi, ar gyfer cromlin gaeedig o hyd L (felly mae gan y rhanbarth y mae'n ei amgáu berimedr L ) ac ar gyfer arwynebedd A y rhanbarth y mae'n ei amgáu,

 

ac mae cydraddoldeb yn dal os a dim ond os yw'r gromlin yn gylch. Felly, y cylch sydd â'r arwynebedd mwyaf o unrhyw ffigur caeedig gyda pherimedr penodol.

Cyfeiriadau

 
  
 Chwiliwch am arwynebedd yn Wiciadur.





