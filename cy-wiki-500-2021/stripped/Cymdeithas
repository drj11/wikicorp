       Y Ddyletswydd Economaidd-gymdeithasol yng Nghymru
Grŵp mawr o unigolion sydd yn rhannu'r un gofod cyhoeddus ac yn rhyngweithio mewn ffordd systematig yw cymdeithas. Yn fras, mae'n cynnwys pob casgliad o bobl sydd yn ymateb i'w gilydd ac yn ffurfio felly cyfundrefn ar sail cydberthynas, gan gynnwys cydweithio, cyfathrebu, a rhaniad llafur; yn yr ystyr hon, gellir defnyddio'r term hefyd ar gymdeithasau anifeiliaid o'r un rywogaeth.[1] O ganlyniad i weithredoedd ac ymatebion yr unigolion â'i gilydd, byddai'r gymdeithas yn ffurfio rhyw fath o drefn yn seiliedig ar arferion, rheolau, a buddiannau cyffredin er mwyn iddynt cyd-fyw yn heddychlon. Y gymdeithas ddynol yw pwnc disgyblaeth cymdeithaseg, sydd yn ymwneud ag ymddygiad cymdeithasol, patrymau o berthnasau a chysylltiadau cymdeithasol, ac agweddau eraill o fywyd cymunedol a diwylliannol.
Diffinnir cymdeithas ddynol unigryw gan boblogaeth sydd yn byw mewn tiriogaeth ddaearyddol benodol, fel rheol gyda diwylliant a ffordd o fyw gyffredin, dan drefn sydd naill ai yn meddu ar sofraniaeth ac yn annibynnol ar gymdeithasau eraill, neu gyda digon o ymreolaeth ac hunangynhaliaeth i'w hystyried yn system gymdeithasol ar wahân. Gwahaniaethir cymdeithas ddynol benodol gan nodweddion strwythurol ei threfn yn ogystal â nodweddion diwylliannol a chymdeithasol y boblogaeth y mae'n ei chynnwys.[2] Yn ôl gwladwriaeth-ganoliaeth, y meddylfryd traddodiadol ym maes gwyddor gwleidyddiaeth, mae'r gwahaniaethau rhwng cymdeithasau dynol yn cyfateb i ffiniau'r cenedl-wladwriaethau: cymdeithas Ffrengig, cymdeithas Almaenig, ac ati. Er bod ysgolheigion mewn meysydd eraill yn aml yn dilyn y patrwm hwn, nid diffiniad digonol mohoni am ei fod yn hepgor cymdeithasau cenhedloedd nad yw'n annibynnol a chymdeithasau ethnig a diwylliannol sydd yn mynd y tu hwnt i ffiniau, er enghraifft cymdeithas Gymreig, cymdeithas Gyrdaidd, a chymdeithas Québecois. Mae twf globaleiddio yn enwedig wedi dangos cyfyngiadau'r hen ddiffiniad gwleidyddol, ac mae rhai Marcswyr wedi mabwysiadu'r term "ffurfiad cymdeithasol" i gyfeirio at gymdeithasau yn nhermau eu datblygiad gwleidyddol ac economaidd, fel y'i ceir gan amlaf y tu mewn i ffiniau penodol.[3]

  Cynnwys
 
1 Gwahaniaethu oddi wrth gysyniadau eraill
 
1.1 Cymuned

1.2 Y wladwriaeth




2 Gweler hefyd

3 Cyfeiriadau




Gwahaniaethu oddi wrth gysyniadau eraill
Cymuned
Grŵp cyfyngedig o bobl yw cymuned, yn ôl y diffiniad traddodiadol, sydd yn byw yn agos i'w gilydd ac yn rhyngweithio wyneb yn wyneb. Gellir ei hystyried yn ffurf fach, leol ar gymdeithas.

Y wladwriaeth
Nid mater hawdd yw llunio'r gwahaniaeth rhwng cymdeithas a'r wladwriaeth. Defnyddir y term cymdeithas sifil i ddisgrifio'r rhannau cydweithredol o gymdeithas sydd ar wahân i'r wladwriaeth, gan gynnwys sefydliadau crefyddol, busnesau a chwmnïau, y cyfryngau torfol, elusennau, carfanau pwyso, undebau llafur a chymdeithasau proffesiynol, clybiau, a mudiadau cyfundrefnol.

Gweler hefyd
 Anthropoleg

Polisi cymdeithasol

Ymddygiad dynol

Cyfeiriadau

 
  
 Chwiliwch am cymdeithas yn Wiciadur.





