 
Data cyffredinol
Enghraifft o'r canlynolcysyniad crefyddol 
Mathcreawdwr 
 Ffeiliau perthnasol ar Gomin Wicimedia

 Mae hon yn erthygl am Dduw yn y crefyddau un duwiol; (gweler hefyd Al-lâh). Am 'duw' mewn cyd-destun amldduwiaeth, gweler Duw (amldduwiaeth), Duwiau Celtaidd a Duwies.

Mewn unduwiaeth, Duw yw'r bod goruchaf, y crëwr, a phrif wrthrych ffydd.[1] Fel arfer, caiff Duw ei gydnabod fel hollalluog, <i>omniscient</i>, hollbresennol a omnibenevolent yn ogystal â bod yn dragwyddol ac yn angenrheidiol. Nid pawb sy'n cytuno bod duw yn bodoli, ac nid oes prawf gwyddonol o'i fodolaeth. Credir ynddo drwy ffydd yn unig.[1][2][3] Credir mai creawdwr y bydysawd yw Duw, neu o leiaf mai ei gynhaliwr yw ef. Mewn crefyddau eraill hen a diweddar, rhai enwadau Hindŵaidd er enghraifft, credir fod y Bod goruchel yn fenywaidd a chyfeirir ati fel Y Dduwies. Nid yw pawb yn credu mewn Duw neu dduwiau. Mae rhai yn amheuwr ond gyda meddwl agored ar y pwnc; gelwir pobl o'r farn hynny'n agnostig. Mae eraill yn gwrthod bodolaeth Duw yn gyfan gwbl; gelwir y rhain hynny'n anffyddwyr.
Mae rhai crefyddau'n disgrifio Duw heb gyfeirio at ryw, tra bod eraill, fel y Gymraeg, yn defnyddio terminoleg sy'n rhyw-benodol hy gyda rhagfarn ar sail rhyw; 'Ef' (gwrywaidd) yw Duw yn y Gymraeg, yn draddodiadol. Mae Duw wedi cael ei genhedlu fel naill ai persono neu'n ddiberson. Mewn theistiaeth, Duw yw crëwr a chynhaliwr y bydysawd, tra mewn deistiaeth, Duw yw crëwr, ond nid cynhaliwr, y bydysawd. Mewn pantheistiaeth, Duw yw'r bydysawd ei hun. Anffyddiaeth yw absenoldeb Dduw neu ddwyfoldeb, tra bod agnosticiaeth yn ystyried bodolaeth Duw yn anhysbys. Mae Duw hefyd wedi ddylunio fel ffynhonnell pob rhwymedigaeth foesol, a'r "gwrthrych mwyaf y gellir ei ddychmygu".[1] Mae llawer o athronwyr nodedig wedi datblygu dadleuon o blaid ac yn erbyn bodolaeth Duw.[4]
Mae pob crefydd monotheistig yn cyfeirio at ei duw gan ddefnyddio gwahanol enwau, gyda rhai'n cyfeirio at syniadau diwylliannol am hunaniaeth a phriodoleddau'r duw. Yn Ateniaeth yr Aifft hynafol, o bosibl y grefydd monotheistig gynharaf a gofnodwyd, gelwid y duwdod hwn yn Aten[5] a chyhoeddwyd mai hi oedd yr un Bod "Goruchaf" gwir "a chreawdwr y bydysawd.[6] 

      Y gair 'Allah' mewn caligraffi Arabeg
      Enw'r Yhwh mewn ysgrifen Hebraeg.
      Map sy'n dangos canran y boblogaeth yn Ewrop sy'n credu mewn Duw (arolwg 2005)
Defnyddir yr enwau Iahwe a Jehofa, lleisiau posib o YHWH, mewn Cristnogaeth. Yn athrawiaeth Gristnogol y Drindod, mae un Duw yn cydfodoli mewn tri "pherson" o'r enw'r Tad, y Mab a'r Ysbryd Glân. Yn Islam, mae'r teitl Duw ("Allah" yn yr iaith Arabeg ) yn aml yn cael ei ddefnyddio fel enw, tra bod Mwslimiaid hefyd yn defnyddio llu o deitlau eraill am Dduw. Mewn Hindŵaeth, mae Brahman yn aml yn cael ei ystyried yn gysyniad monistig o Dduw.[7] Yng nghrefyddau Tsieineaidd caiff Shangdi ei ystyried yn dad yr hil (neu'r 'hynafiad cyntaf') a'r bydysawd. Ymhlith yr enwau eraill ar Dduw mae Baha yn y Ffydd Bahá'í,[8] Waheguru mewn Sikhaeth,[9] Ahura Mazda mewn Zoroastrianiaeth,[10] Hayyi Rabbi ym Mandaeismaeth[11][12] a Sang Hyang Widhi Wasa yn Hindŵaeth Bali.[13]
Yn y Gymraeg, a'r Saesneg, defnyddir llythyren fawr pan ddefnyddir y gair fel enw priod, yn ogystal ag ar gyfer enwau duwiau unigol eraill ee Baha, Lleu, Jehova.[14] O ganlyniad, defnyddir llythyren fach ar gyfer llu o dduwiau, neu pan gaiff ei ddefnyddio i gyfeirio at y syniad generig o ddwyfoldeb.[15][16][17]
Allāh (Arabeg: الله‎) yw'r term Arabeg ac ni cheir enw lluosog; fe'i defnyddir gan Fwslimiaid a Christnogion ac Iddewon sy'n siarad Arabeg i ddynodi "Y Duw", tra bod ʾilāh (Arabeg: إِلَٰه‎; lluosog `āliha آلِهَة) yn derm a ddefnyddir ar gyfer dwyfoldeb (is-dduw) neu dduw yn gyffredinol.[18][19]
Weithiau, rhoddir enw priod i Dduw hefyd mewn  Hindŵaeth undduwiaid (monotheistig), sy'n pwysleisio natur bersonol Duw, gyda chyfeiriadau cynnar at ei enw fel Krishna-Vasudeva yn Bhagavata neu yn ddiweddarach mewn Vishnu a Hari.[20]
Ahura Mazda yw'r enw ar Dduw a ddefnyddir mewn Zoroastrianiaeth. Mae "Mazda", neu'n hytrach y ffurf  Avestanaidd Mazdā-, Mazdå enwol, yn adlewyrchu'r Proto-Iranaidd *Mazdāh (benywaidd). Yn gyffredinol cymerir mai enw iawn yr ysbryd ydyw, ac fel ei gytras Sansgrit medhā, mae'n golygu "deallusrwydd" neu "ddoethineb". Mae'r enwau Avestan a Sansgrit yn adlewyrchu tarddiad Proto-Indo-Iranaidd * mazdhā-, o Proto-Indo-Ewropeaidd mn̩sdʰeh 1, gan olygu'n llythrennol: "trefnu (dʰeh1) meddwl rhywun (*mn̩-s)", ac felly "doeth". [21]

Y term Waheguru (Punjabi: vāhigurū) a ddefnyddir amlaf mewn Siciaeth i gyfeirio at Dduw. Mae'n golygu "Athro bendigedig" yn yr iaith Punjabi. Ystyr Vāhi (benthyciad o'r Perseg Canol) yw "rhyfeddol" a guru (Sansgrit) yn derm sy'n dynodi "athro". Mae rhai hefyd yn disgrifio Waheguru fel profiad o ecstasi sydd y tu hwnt i bob disgrifiad. Mae'r defnydd mwyaf cyffredin o'r gair "Waheguru" yn y cyfarchiad y mae Sikhiaid yn ei ddefnyddio gyda'i gilydd: 
Waheguru Ji Ka Khalsa, Waheguru Ji Ki Fateh 
Wonderful Lord's Khalsa, Victory is to the Wonderful Lord.

Arabeg yw'r gair Baha, sy'n golygu'r Duw "mwyaf"  yn y Ffydd Bahá'í, ar gyfer "Holl-Gogoneddus".
  Cynnwys
 
1 Yn gyffredinol
 
1.1 Un duw

1.2 theistiaeth, deistiaeth, a holl-dduwiaeth




2 Barn pobl nad ydynt yn theist
 
2.1 Agnosticiaeth ac anffyddiaeth

2.2 Anthropomorffiaeth




3 Bodolaeth

4 Cyfeiriadau

5 Llyfryddiaeth




Yn gyffredinol
Mae athroniaeth crefydd yn cydnabod y canlynol fel priodoleddau hanfodol Duw:

 Undduwiaeth (Omnipotence; pŵer diderfyn)

Hollwybodaeth (Omniscience; gwybodaeth ddiderfyn)

Tragwyddoldeb (Nid yw Duw yn rhwym wrth amser; da yw Duw)

Daioni (mae Duw yn gwbl garedig)

Undod (ni ellir rhannu Duw)

Symlrwydd (nid yw Duw yn gyfansawdd)

Anghorfforoldeb (nid yw Duw yn faterol)

Anghyfnewidiolseb (nid yw Duw yn destun newid)

Caeëdigaeth (nid yw Duw yn cael ei effeithio gan ddim)[22]

Nid oes consensws clir ar natur na bodolaeth Duw.[23] Mae'r cysyniadau Abrahamaidd o Dduw yn cynnwys y diffiniad monotheistig o Dduw mewn Iddewiaeth, y farn trinitaraidd gan Gristnogion, a'r cysyniad Islamaidd o Dduw.
Roedd yna hefyd amryw o syniadau o Dduw yn yr hen fyd Groeg-Rufeinig, megis barn Aristotle am symudwr disymud, cysyniad Neoplatonig yr Un a Duw pantheistig Ffiseg Stoic.
Mae'r crefyddau dharmig yn wahanol yn eu barn am y dwyfol: mae safbwyntiau o Dduw o fewn i Hindŵaeth yn amrywio yn ôl rhanbarth, sect, a chast, ac yn amrywio o undduwiaeth i amldduwiaeth. Mae llawer o grefyddau amldduwiol yn rhannu'r syniad o ddwyfoldeb y crëwr, er bod ganddyn nhw enw heblaw "Duw" a heb yr holl rolau eraill a briodolir i Dduw unigol gan grefyddau monotheistig. Weithiau mae Sikhaeth yn cael ei ystyried yn bantheistig am Dduw.
Yn gyffredinol, mae crefyddau Śramaṇa yn rhai nad ydyn nhw'n pwysleisio'r creu, ac maen nhw hefyd yn dal bod bodau dwyfol (o'r enw Devas mewn <a href="./Bwdhaeth" rel="mw:WikiLink" data-linkid="undefined" data-cx="{&quot;userAdded&quot;:true,&quot;adapted&quot;:true}">Bwdhaeth</a> a Jainiaeth) o bwer a hyd oes gyfyngedig. Yn gyffredinol, mae Jainiaeth wedi gwrthod syniadau'r creu, gan ddal bod sylweddau'r enaid (Jīva) heb eu trin a bod amser yn ddi-ddechrau.[24] Yn dibynnu ar ddehongliad a thraddodiad person, gellir tybio bod Bwdhaeth naill ai'n an-ddamcaniaethol, yn draws-ddamcaniaethol, yn bantheistig neu'n amldduwiol. Fodd bynnag, yn gyffredinol mae Bwdhaeth wedi gwrthod barn monotheistig benodol Duw'r Creawdwr. Mae'r Bwdha yntau'n feirniadol o ddamcaniaeth y creu, yn y testunau Bwdhaidd cynnar.[25][26] Hefyd, mae prif athronwyr Bwdhaidd Indiaidd, fel Nagarjuna, Vasubandhu, Dharmakirti a Buddhaghosa, yn beirniadu'r cysyniad o Dduw'r Creewr, yn gyson.[27][28][29]

Un duw
Amldduwiaeth oedd y grefydd Geltaidd, ac roedd y duwiau'n cynnwys Lleu, Rhiannon, Pwyll, Teyrnon ayb. Mae undduwiaid, fodd bynnag, yn credu nad oes ond un duw, a gallant hefyd gredu bod y duw hwn yn cael ei addoli mewn gwahanol grefyddau o dan enwau gwahanol. Pwysleisir yn arbennig y farn bod pob damcaniaethwr yn addoli'r un duw, p'un ai ydynt yn ei adnabod ai peidio, yn Ffydd Bahá, Hindŵaeth [30] a Sikhaeth.[31]
Mewn Cristnogaeth, mae athrawiaeth y Drindod yn disgrifio Duw fel un Duw mewn tri Pherson dwyfol (Duw ei hun yw pob un o'r tri Pherson). Mae'r Drindod Sanctaidd fwyaf yn cynnwys[32] Duw'r Tad, Duw'r Mab (Iesu Grist), a Duw yr Ysbryd Glân. Yn y canrifoedd diwethaf, cafodd y dirgelwch sylfaenol hwn o'r ffydd Gristnogol ei grynhoi hefyd gan y fformiwla Ladin Sancta Trinitas, Unus Deus (Y Drindod Sanctaidd, Un Duw), a adroddwyd yn y Litanias Lauretanas.
Cysyniad mwyaf sylfaenol Islam yw tawhid sy'n golygu "undod" neu "unigrywiaeth". Disgrifir Duw yn y Corân fel: "Ef yw Allah, yr Un a'r Unig un; Allah, y Tragwyddol, Absoliwt; nid oes neb tebyg iddo." Mae Mwslimiaid yn ceryddu athrawiaeth Gristnogol y Drindod a dewiniaeth Iesu, gan ei chymharu â amldduwiaeth. Yn Islam, mae Duw yn drosgynnol ac nid yw'n debyg i unrhyw un o'i greadigaethau mewn unrhyw ffordd. Felly, nid oes gan Fwslimiaid  ddelweddau o Dduw.[33]

theistiaeth, deistiaeth, a holl-dduwiaeth
Mae Theistiaeth yn gyffredinol yn dal: bod Duw yn bodoli ac yn real, yn wrthrychol ac yn annibynnol o feddwl dynol; bod Duw wedi creu ac yn cynnal popeth; fod Duw yn hollalluog ac yn dragwyddol; a bod Duw yn bersonol ac yn rhyngweithio â'r bydysawd trwy brofiadau crefyddol a gweddïau.[34] Mae Theistiaeth yn dal bod Duw yn drosgynnol ac yn barhaol; felly, mae Duw ar yr un pryd yn anfeidrol ac, mewn rhyw ffordd, yn bresennol ym materion y byd.[35] Nid yw pob damcaniaethwr yn cytuno a'r holl osodiadau hyn, ond mae pob un fel arfer yn tanysgrifio i rai ohonynt.[34] Mae diwinyddiaeth Gatholig yn dal bod Duw yn anfeidrol syml ac nad yw'n ddarostyngedig i amser. Mae'r rhan fwyaf o ddamcaniaethwyr yn dal bod Duw yn hollalluog ac yn garedig, er bod y gred hon yn codi cwestiynau am gyfrifoldeb Duw am ddrygioni a dioddefaint yn y byd. Mae Theistiaeth Agored, mewn yn dadlau, oherwydd natur amser, nad yw hollwybodaeth Duw yn golygu y gall y duwdod ragweld y dyfodol. Defnyddir damcaniaeth weithiau i gyfeirio'n gyffredinol at unrhyw gred mewn duw neu dduwiau, hy, undduwiaeth neu amldduwiaeth.[36][37]

      Duw'n Bendithio'r Seithfed Dydd, 1805 paentiad dyfrlliw gan William Blake
      99 enw Allah, mewn sgript Tsieineaidd Sini
Mae Deistiaeth (Deism) yn dal bod Duw yn hollol drosgynnol: mae Duw yn bodoli, ond nid yw'n ymyrryd yn y byd y tu hwnt i'r hyn oedd angen ei greu.[35] Yn y gred hon, nid yw Duw'n anthropomorffaidd, ac nid yw'n ateb gweddïau nac yn cynhyrchu gwyrthiau. O fewn Deistiaeth mae'r gred nad oes gan Dduw ddiddordeb mewn dynoliaeth ac efallai nad yw hyd yn oed yn ymwybodol o ddynoliaeth.
Mae Holl-dduwiaeth yn cyfuno Deistiaeth â chredoau Pantheistiaidd.[38][39][40] Cynigir Holl-dduwiaeth i egluro Deistiaeth pam y byddai Duw yn creu bydysawd ac yna'n ei adael,[41] ac o ran Holl-dduwiaeth, tarddiad a phwrpas y bydysawd.[41][42]

Barn pobl nad ydynt yn theist
Mae barn yr an-theist (a'r anffyddiwr) am Dduw hefyd yn amrywio ee gyda rhai'n osgoi y cysyniad o Dduw, tra'n derbyn ei fod yn arwyddocaol i lawer; mae anffyddwyr eraill yn deall Duw fel symbol o werthoedd a dyheadau dynol. Cyhoeddodd yr anffyddiwr Seisnig o'r 19g Charles Bradlaugh ei fod yn gwrthod dweud "Nid oes Duw", oherwydd "mae'r gair 'Duw' i mi yn swn sy'n cyfleu dim cadarnhad clir nac unigryw"; dywedodd yn fwy penodol ei fod yn anghredu yn y duw Cristnogol. Cynigiodd Stephen Jay Gould ddull o rannu byd athroniaeth i'r hyn a alwodd yn " magisteria nad yw'n gorgyffwrdd" (non-overlapping magisteria; NOMA). Yn y farn hon, mae cwestiynau goruwchnaturiol, fel y rhai sy'n ymwneud â bodolaeth a natur Duw, yn an-empirig ac yn barth real o fewn diwinyddiaeth. Os felly, yna dylid defnyddio dulliau gwyddoniaeth i ateb unrhyw gwestiwn empirig am y byd naturiol, a dylid defnyddio diwinyddiaeth i ateb cwestiynau am yr ystyr eithaf a gwerthoedd moesol. Yn y farn hon, mae diffygunrhyw ôl troed empirig o magisteriwm y goruwchnaturiol i ddigwyddiadau naturiol yn golygu mai gwyddoniaeth yw'r unig chwaraewr yn y byd naturiol.[43]
Barn arall, a ddatblygwyd gan Richard Dawkins, yw bod bodolaeth Duw yn gwestiwn empirig, ar y sail y byddai "bydysawd â duw yn fath hollol wahanol o fydysawd i fydysawd heb Dduw, ac y byddai'n wahaniaeth gwyddonol."[44] Dadleuodd Carl Sagan ei bod yn anodd profi neu wrthbrofi athrawiaeth Creawdwr y Bydysawd ac mai'r unig ddarganfyddiad gwyddonol y gellir ei ddychmygu a allai wrthbrofi bodolaeth Creawdwr (nid Duw o reidrwydd) fyddai'r darganfyddiad bod y bydysawd yn anfeidrol hen.[45]
Noda Stephen Hawking a’i gyd-awdur Leonard Mlodinow yn eu llyfr, The Grand Design (2010) ei bod yn rhesymol gofyn pwy neu beth a greodd y bydysawd, ond os mai Duw yw’r ateb, yna rhaid hefyd gofyn, pwy greodd Duw? Mae'r ddau awdur yn honni fodd bynnag, ei bod hi'n bosibl ateb y cwestiynau hyn o fewn cylch gwyddoniaeth yn unig, a heb alw unrhyw fodau dwyfol.[46]

Agnosticiaeth ac anffyddiaeth
Agnosticiaeth yw'r farn bod gwir werth rhai honiadau - yn enwedig honiadau metaffisegol a chrefyddol megis a yw Duw, y dwyfol neu'r goruwchnaturiol yn bodoli - yn anhysbys ac efallai'n anhysbys.
Anffyddiaeth, yn ei ystyr ehangaf, yw gwrthod credu ym modolaeth duwiau.[47][48] Mewn ystyr culach, anffyddiaeth yn benodol yw'r safbwynt nad oes duwiau, er y gellir ei diffinio fel diffyg cred ym modolaeth unrhyw dduwdodau, yn hytrach na chred gadarnhaol ym mhresenoldeb unrhyw dduwdodau.[49]

Anthropomorffiaeth
Dadleua Pascal Boyer, er bod amrywiaeth eang o gysyniadau goruwchnaturiol i'w cael ledled y byd, yn gyffredinol, mae bodau goruwchnaturiol yn tueddu i ymddwyn yn debyg iawn i bobl. Mae adeiladu duwiau ac ysbrydion fel pobl yn un o nodweddion mwyaf adnabyddus crefydd. Mae'n dyfynnu enghreifftiau o fytholeg Roegaidd, sydd, yn ei farn ef, yn debycach i opera sebon fodern na systemau crefyddol eraill.[50] Mae Bertrand du Castel a Timothy Jurgensen yn dangos bod model esboniadol Boyer yn cyd-fynd ag epistemoleg ffiseg wrth osod endidau na ellir eu gweld yn uniongyrchol fel cyfryngwyr.[51] Mae'r anthropolegydd Stewart Guthrie'n dadlau bod pobl yn taflunio nodweddion dynol i agweddau nad ydynt yn ddynol ar y byd oherwydd ei fod yn gwneud yr agweddau hynny'n fwy cyfarwydd. Awgrymodd Sigmund Freud hefyd fod cysyniad o dduw yn ymestyniad o dad y person.[52]
Yn yr un modd, roedd Émile Durkheim yn un o'r cynharaf i awgrymu bod duwiau'n cynrychioli estyniad o fywyd dynol fel ag i gynnwys bodau goruwchnaturiol. Yn unol â'r rhesymeg hon, mae'r seicolegydd Matt Rossano yn dadlau: pan ddechreuodd bodau dynol fyw mewn grwpiau mwy, efallai eu bod wedi creu duwiau fel ffordd o orfodi moesoldeb. Mewn grwpiau bach, gellir gorfodi moesoldeb gan rymoedd cymdeithasol fel clecs neu enw da. Fodd bynnag, mae'n anoddach o lawer gorfodi moesoldeb gan ddefnyddio grymoedd cymdeithasol mewn grwpiau llawer mwy. Mae Rossano'n nodi, trwy gynnwys duwiau ac ysbrydion byth-wyliadwrus, fod bodau dynol wedi darganfod strategaeth effeithiol ar gyfer atal hunanoldeb ac adeiladu grwpiau mwy cydweithredol.[53]

Bodolaeth
Mae dadleuon ynghylch bodolaeth Duw fel arfer yn cynnwys dadleuon empirig, diddwythol ac anwythol. Cynhwysa sawl gwahanol safbwynt: "Nid yw Duw yn bodoli" (anffyddiaeth gref); "Nid yw Duw bron yn sicr yn bodoli" (anffyddiaeth de facto); "does neb yn gwybod a yw Duw yn bodoli" (agnosticiaeth)  "Mae Duw yn bodoli, ond ni ellir profi na gwrthbrofi hyn" (theistiaeth de facto); a bod "Duw yn bodoli a gellir profi hyn" (theistiaeth gref) [43]
Cynigiwyd dadleuon dirifedi i brofi bodolaeth Duw,[54] ac yn eu plith y mae Y Pum Ffordd (Aquinas), y Ddadl o'r awydd a gynigiwyd gan CS Lewis, a'r Ddadl Ontolegol a luniwyd gan Anselm a René Descartes.[55]

Cyfeiriadau
Troednodiadau
 
Dyfyniadau


 

Llyfryddiaeth
 
 Bunnin, Nicholas; Yu, Jiyuan (2008). The Blackwell Dictionary of Western Philosophy. Blackwells. ISBN 9780470997215.

Pickover, Cliff, The Paradox of God and the Science of Omniscience, Palgrave/St Martin's Press, 2001. ISBN 1-4039-6457-2

Collins, Francis, The Language of God: A Scientist Presents Evidence for Belief, Free Press, 2006. ISBN 0-7432-8639-1

Miles, Jack, God: A Biography, Vintage, 1996. ISBN 0-679-74368-5

Armstrong, Karen, A History of God: The 4,000-Year Quest of Judaism, Christianity and Islam, Ballantine Books, 1994. ISBN 0-434-02456-2

Paul Tillich, Systematic Theology, Vol. 1 (Chicago: University of Chicago Press, 1951). ISBN 0-226-80337-6

Hastings, James Rodney (1925–2003) [1908–26]. Encyclopedia of Religion and Ethics. John A Selbie (arg. Volume 4 of 24 (Behistun (continued) to Bunyan.)). Edinburgh: Kessinger Publishing, LLC. t. 476. ISBN 978-0-7661-3673-1. The encyclopedia will contain articles on all the religions of the world and on all the great systems of ethics. It will aim at containing articles on every religious belief or custom, and on every ethical movement, every philosophical idea, every moral practice.


 
  
 Chwiliwch am duw yn Wiciadur.

 Awdurdod 
 WorldCat

LCCN: sh85055517

GND: 4021662-7

BNF: cb11975724f (data)

NKC: ph117421







