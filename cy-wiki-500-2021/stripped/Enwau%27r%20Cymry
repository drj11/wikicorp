 Mae'r erthygl hon yn trafod enwau'r Cymry gan gynnwys tarddiadau enwau personol ac arferion enwi pobl yng Nghymru ar hyd yr oesau.

  Cynnwys
 
1 Enwau personol
 
1.1 2015




2 Arallenwau

3 Enwau teuluol

4 Ychwanegu ail enw

5 Ymlediad enwau Cymreig tu draw i Gymru
 
5.1 Cyfenwau Americanwyr o dras Affricanaidd




6 Enwau priod menywod

7 Ffynonellau

8 Llyfryddiaeth




Enwau personol
Mae ffynonellau enwau personol yng Nghymru yn cynnwys y canlynol:

 Enwau cyndeidiau  Byddai'n arfer gan deuluoedd ddefnyddio enwau cyndadau ar eu plant; e.e. byddai un o feibion Llywelyn ap Dafydd ap Llywelyn yn cael yr enw Dafydd a'i adnabod wrth yr enw Dafydd ap Llywelyn ap Dafydd. Hyd at ganol y 19g byddai llawer yn arfer patrwm ffurfiol o enwi plant ar ôl eu cyndeidiau.
 Cai'r mab cyntaf enw tad ei dad,

cai'r ail fab enw tad ei fam,

cai'r ferch gyntaf enw mam ei mam,

cai'r ail ferch enw mam ei thad, yna:

cai'r plant eraill eu henwi ar ôl eu rhieni, ewythrod a modrybedd yn eu tro.   Yn eithriad i'r patrwm hwn cai aml i blentyn ei enwi ar ôl perthynas a fu farw, gan gynnwys perthnasau a fu farw'n ifainc, cyn iddynt fagu plant eu hunain.


Tua diwedd y 19g dechreuodd trwch gwerin Cymru roi dau enw personol i'w plant, arfer oedd yn gyffredin ymhlith y boneddigion ers canrifoedd cyn hynny. Dechreuwyd arfer o roi un enw yn perthyn i'r teulu ac ail enw heb gysylltiad â'r teulu i blentyn. Erbyn ail hanner yr 20g nid yw'n arfer cyffredin i roi enw cyndad neu berthynas ar blentyn.

 Enwau Beiblaidd   Roedd yn arfer yn Oes y Saint a hefyd adeg y Diwygiad Protestannaidd i ddewis enw newydd adeg bedydd. O'r Beibl y daeth llawer o'r enwau hyn a hefyd o enwau saint eraill. Cynyddodd nifer yr enwau Beiblaidd o'r 16g ymlaen. Tybir gan haneswyr yn gyffredinol bod enwau Hen Destament fwyaf cyffredin ymhlith Anghydffurfwyr. Mae Rowlands yn The Surnames of Wales [1] yn cysylltu amlder gymharol uchel enwau o'r Hen Destament yn y De a'r ffaith bod yr Annibynwyr a'r Bedyddwyr gryfaf yno. Yn yr un modd mae'n cysylltu amlder gymharol uchel enwau o'r Testament Newydd yn y Gogledd a'r ffaith bod y Methodistiaid gryfaf yn y Gogledd. Roedd y Methodistiaid wedi torri o Eglwys Loegr yn gymharol hwyr. Enwau o'r Testament Newydd a gysylltir ag Eglwys Loegr fel enwau bedydd ac enwau o'r Hen Destament a gysylltir â'r Anghydffurfwyr cynharaf.

Enwau enwogion   Ceir enghreifftiau o enwi ar ôl pob math o enwogion cyfoes a hanesyddol yn ôl ffasiwn yr oes, gan gynnwys:
 saint – Sant Teilo, Sant Tecwyn, Sant Non

brenhinoedd - y brenin Caradog, Heledd chwaer Cynddylan, y Frenhines Elisabeth

arwyr ym mhob maes - y pêl-droediwr (Ryan (Giggs)), y gantores ((Kylie) Minogue).


Yn ogystal â phobl go iawn mae cymeriadau y ceir sôn amdanynt mewn caneuon, straeon, a ffuglen arall hefyd yn ffynhonnell enwau cyntaf.

 Enwau tramor   Ymledai'r defnydd o enwau enwogion ac enwau tramor gyda thwf cysylltiadau rhwng pobloedd megis teithiau'r saint a thwf Cristnogaeth, trwy ymfudo, a thrwy ymlediad ymerodraethau, e.e. Macsen Wledig (Maximus), cadfridog o dan yr Ymerodraeth Rufeinig. Daw'r cyfenw Herbert o dras Normanaidd. Mae Siencyn yn enghraifft o enw o dras Seisnig, Jankin a aeth yn Jenkin, a fabwysiadwyd yn ne Cymru erbyn y 15g a'i gymreigio i'r ffurf Siencyn. Agorwyd ffynhonnell newydd o enwau gyda thwf llythrennedd a llyfrau printiedig yn ystod y 16g. Ceir enghreifftiau o enwi ar ôl enwogion y byd clasurol a chymeriadau o fytholeg Roeg a Rhufain megis Ajax a Julian, yn ogystal ag enwau o'r Beibl ac o blith y saint. Y pryd hwn y cynyddodd y defnydd o enwau Saesneg neu ffurfiau Saesneg yn hytrach na Chymraeg ar enwau Beiblaidd neu Ewropeaidd.

Mae datblygiad dulliau cyfathrebu hefyd wedi hyrwyddo lledaeniad enwau personol dros y byd. Erbyn heddiw mae'r cyfryngau torfol yn dylanwadu ar boblogaeth helaeth o'r byd ar unwaith. Felly nid yw'n syndod cwrdd â phobl o'r un genhedlaeth  ac o'r un enw ond yn hanu o wledydd gwahanol, e.e. merched wedi eu galw'n Marilyn ar ôl Marilyn Monroe a dynion i'u henwid yn George ar ôl George Best. Arddelir enwau Cymraeg gan rai nad ydynt o dras Gymreig megis Dylan ar ôl Dylan Thomas ac yna Bob Dylan.
Mabwysiadwyd enwau tramor gan y Cymry drwy'r oesau. Cyrhaeddodd rhai enwau megis Aeddan yn gynnar iawn gyda mewnfudwyr o'r Iwerddon. Daeth y Normaniaid hwythau ag enwau tebyg i Robert, Roger, Richard ac Edward ganddynt i Gymru. Nid mewnfudwyr oedd unig ffynhonnell enwau tramor chwaith. Ers yr Oesau Canol cai ffasiynau enwau yng Nghymru eu dylanwadu gan ffasiynau yn Ewrop, yn enwedig yn Lloegr. Erbyn yr Oesau Canol ceir enwau personol eithaf poblogaidd yng Nghymru a oedd yn ffasiynol yn Lloegr hefyd. Byddai enw Saesneg ffasiynol yn disodli enw teuluol traddodiadol oedd yn swnio'n debyg iddo; e.e. Lewis yn disodli Llywelyn, Hugh yn disodli Hywel ac Edward yn disodli Iorwerth. Mabwysiadwyd rhai enwau dro ar ôl tro, a ffurf yr enw yn amrywio yn ôl hanes ei daith cyn cyrraedd y Gymraeg, y newid ar yr enw yn ystod y blynyddoedd ers ei fabwysiadu i'r Gymraeg, ac arferion orgraff y Gymraeg pan yr ysgrifennwyd yr enw gyntaf. Mae Dafydd, Dai, Dei, Deio, Deian, a Dewi i gyd yn ffurfiau ar yr enw Hebraeg gwreiddiol. Mae Ioan, John, Ifan/Ifana, Ieuan, Ianto, Siôn, Sioni, Sionyn, Jac a Siencyn i gyd yn ffurfiau ar yr enw Hebraeg. Mae Gwilym, William, Wil, yn ffurfiau ar enw o dras Almaenig.

 Enwau traddodiadol Gymraeg   Ffynonellau llawer o enwau traddodiadol Gymraeg yw:
 mytholeg y Celtiaid neu chwedlau, e.e. Blodeuwedd (a grëwyd o flodau drwy hud yn y Mabinogi), Bedwyr, Branwen, Gwydion, Peredur. Roedd adrodd hanes tarddiad enw ar gymeriad mewn chwedl, e.e. Blodeuwedd, yn rhan bwysig o aml i chwedl.

rhinweddau, e.e. Heddwen, Carys (o Câr)

byd natur, e.e. Haf, Llinos, Eira, Eirlys

llefydd, gan gynnwys afonydd, e.e. Gwynedd/Gwyneth, Arfon, Llifon (afon), Wyre (afon)   Cafwyd adfywiad yn y defnydd o'r enwau hyn yn ystod yr 20g.


Enwau Lladin
 Defnyddir enwau'n tarddu o rifau Lladin i ddynodi trefn geni plentyn.  Felly ceir enghreifftiau o'r 19g a'r 20g o Una (1) ar gyfer y cyntaf-anedig, Secondus (2), Tertia (3), Quarta (4), Quintus (5), Septimus (7), Octavia (8), Decimus (10), Undecima (11).[2]

Cawsai ambell i blentyn ei alw'n Posthuma neu Posthumus pan oedd ei dad wedi marw cyn ei eni.   Mae'r arfer o ddefnyddio enwau Lladin wedi diflannu yn ystod yr 20g.


Enwau cydnabod a chysylltiadau teuluol eraill   Mae enwau cyfeillion, cymdogion a chymwynaswyr teuluol yn ffynhonnell o enwau ar blant. Yr un modd mae afonydd, bryniau a lleoedd eraill yn cael eu defnyddio'n enwau yn amlach nag arfer ar blant i rieni sydd â chysylltiad â'r lle, e.e. Teifi. Mae Catherine Zeta Jones yn rhannu'r enw Zeta gyda pherthynas iddi. Cafodd y berthynas honno ei henwi ar ôl llong y Zeta yr oedd ei thad berchen arni.[3]

2015
Yr enwau mwyaf poblogaidd gafodd eu cofrestru yn 2015 yng Nghymru.
Cafodd Cymru Fyw olwg ar yr ystadegau i ddarganfod pa rai yw'r enwau Cymraeg mwyaf poblogaidd i fechgyn a merched erbyn hyn:
Enwau o darddiad Cymreig yn 2015 gyda'u safle ar y rhestr yn 2014 mewn cromfachau:

 Bechgyn

1) Dylan 163 (1)

2) Osian 127 (3)

3) Harri 120 (2)

4) Jac 97 (6)

5) Rhys 95 (4)

6) Evan 92 (5)

7) Tomos 76 (8)

8) Cai 70 (9)

9) Ioan 68 (10)

10) Morgan 67 (7)[4]

 Menywod

 1) Seren 119 (1)

2) Ffion 105 (4)

3) Erin 101 (3)

4) Megan 91 (2)

5) Mali 64 (5)

6) Nia 48 (-)

7) Alys 47 (7)

8) Carys 44 (-)

9) Efa 43 (-)

10) Cadi 37 (6)

Yn 2014, Elin, Lois a Lili oedd yr enwau poblogaidd eraill yn y 10 uchaf.

Arallenwau
 Gweler hefyd Llysenwau Cymraeg ac Enw barddol.

Yn ogystal ag enw ffurfiol megis Llywelyn ap Dafydd, byddai rhai pobl yn cael eu hadnabod trwy enw disgrifiadol, e.e. Llywelyn Fawr, Hywel Dda, Dafydd Benfras, Dafydd Gam, Iolo ab yr Ynad Goch. Tardd yr enwau hyn o:

 weithredoedd y person megis Hywel Dda

swydd neu waith, e.e. Jones y Gof

enw trigfan, yn enwedig enw ffarm neu dŷ, e.e. Dafydd Penlan, Alun Tŷ'r Ysgol, Dic Penderyn, Catrin o Ferain. Mae'r arfer hwn yn parhau hyd heddiw yng nghefn gwlad. Pe bai rhywun yn symud i ardal arall yna enw'r fro yr hanai ohono fyddai ei arallenw.

briodoledd personol megis Llwyd (a olygai 'sanctaidd' a'r lliw brown yn ogystal â'r lliw llwyd yn yr Oesau Canol), Goch, Gam, Gwyn.

enw ffurfiol yn newid yn enw anwes, anffurfiol megis Guto o Gruffudd, Dai, Dei o Dafydd, Iolo o Iorwerth, Gwen o Gwenllian neu Gwenhwyfar, Megan o Margaret, Betsan o Elisabeth. Erbyn heddiw gwelir llawer o'r enwau hyn yn cael eu defnyddio fel enwau bedydd llawn.

Mae arallenwau a llysenwau yn gyffredin iawn yng Nghymru, yn rhannol oherwydd bod cymaint o Gymru a'r un enw ganddynt, a'r arallenwau yn fodd i wahaniaethu rhyngddynt. Mae llysenwau lliwgar yn rhan amlwg o ddiwylliant ardaloedd diwydiannol a threfol Cymru, e.e. Shoni Ben Pwll, Wil Bron Sythu, Gwilym Lampy (a weithiai yn y lamp-room), Mrs Davies Lampost (a phostyn lamp tu allan i'w thŷ).[5]
Mae defnyddio enw barddol hefyd yn gyffredin yng Nghymru. Tardd nifer helaeth o'r enwau barddol o fro neu drigfan megis Sarnicol, Pantycelyn. Arddelir enwau mwy disgrifiadol gan eraill, megis Hedd Wyn, Crwys (o 'Croes'). Ers diwedd yr 20g  mae'n ffasiwn ymhlith perfformwyr sy'n Gymry Cymraeg i arddel eu henwau cyntaf yn broffesiynol gan hepgor eu cyfenwau.

Enwau teuluol
Yr hen arfer Gymreig o enwi pobl oedd galw rhywun yn ôl ei enw neu ei henw personol ac ychwanegu enw'r tad a'i dad yntau, weithiau am sawl cenhedlaeth gynharach, e.e. Llywelyn ap Dafydd ap Llywelyn, Maredudd ab Owen, Rhiannon ferch Llŷr. Dyma hefyd a fu'r arfer yn y gwledydd Celtaidd eraill ac yng ngwledydd Llychlyn a rhai mannau eraill yn Ewrop.[6] Mae'r arfer wedi parhau hyd heddiw yng ngwlad yr Iâ. Mae'r arfer wedi ei hadfer gan ychydig rai yng Nghymru tua diwedd yr 20g. Ond roedd y system enwi yn ôl y tadau yn cystadlu yng Nghymru â'r drefn yn Lloegr o gadw cyfenw mewn teulu, a'r plant yn etifeddu'r un cyfenw a'u tad.
Daeth yr arfer o ddefnyddio cyfenwau i fod yn Lloegr yn ystod y 12g a'r 13g. Cyrhaeddodd y drefn cyfenwau Gymru gyntaf gyda'r Normaniaid. Ymledodd arfer y goresgynwyr hyn i rai o uchelwyr Cymru trwy briodas a thrwy ddewis. Cyflwynwyd enwau Normanaidd i Gymru trwy briodas yn enwedig enwau yn dechrau â Fitz, sef 'mab' yn Ffrangeg y Normaniaid. Wrth droi o'r hen drefn at y drefn newydd trowyd enw tad neu dad-cu yn gyfenw, e.e. Dafydd ap Rhys → Dafydd Pryse neu Rees, Ifan ab Owen → Ifan Bowen neu Owen. Weithiau fodd bynnag er i rywun hepgor yr ap yn ei enw, byddai'r genhedlaeth nesaf yn cadw at yr hen drefn a mab Dafydd Rees yn cael ei alw'n William Dafydd. Yn aml byddai enw'n cael ei gofnodi mewn sawl gwahanol ddull a sillafiad mewn gwahanol ddogfennau ar hyd oes y person. O bryd i'w gilydd cofnodwyd llysenw yn ogystal ag enw ffurfiol. Cymerai sawl cenhedlaeth i sillafiad enwau sefydlogi. Weithiau byddai'r drefn enw tad yn parhau ar lafar, ac adref yng Nghymru, tra bod y drefn cyfenw yn cael ei ddefnyddio ar bapur, a thra bod person yn aros yn Lloegr.
Mae'r modd y mae tadenwau'n cael eu haddasu'n gyfenwau wedi newid dros y blynyddoedd. Yr arfer cynnar oedd cadw'r ap yn rhan o'r cyfenw, fel yn Bowen. Erbyn y 18g roedd hi'n arferol i hepgor yr ap, fel yn Owen. Roedd yr arfer hwn wedi dechrau mewn rhai teuluoedd tua'r 16g. Ceir enghraifft o hyn yng nghofrestr plwyf Llanbadog lle y cofnodir yn 1606 Jane William, a oedd yn ferch i Williamkin Jenkin, yn hytrach na Jane ferch William.[7]  Ymhen rhai cenedlaethau eto magodd llawer i gyfenw s ar ei ddiwedd (dynoda s y cyflwr genidol yng ngramadeg Saesneg), fel yn Owens. Mae sillafiad enwau hefyd wedi newid dros y blynyddoedd yn ôl ffasiwn orgraff y Gymraeg. Seisnigeiddiwyd rhai enwau Cymraeg wrth eu cofnodi mewn dogfennau Saesneg neu wrth i Gymry ymfudo i Loegr, America ayb. Newidiwyd Rhys i Rees, ac ap Rhys i'r cyfenwau Price, Pryse, Preece, a Breese.
Tyfu'n araf wnaeth y drefn cyfenwau ymhlith y rhai hynny a oedd â'r cysylltiadau agosaf â'r Normaniaid ac wedi hynny'r Saeson. Gwelir cyfenwau yn cael eu mabwysiadu'n gynnar gan uchelwyr Cymru, a chan y rhai a drigent ymhlith Saeson gerllaw'r trefi neu ynddynt, ar y gororau, ac yn ardaloedd trefedigaethol y Normaniaid a'r Saeson, megis De Sir Benfro. Mabwysiadwyd cyfenwau hefyd gan Gymry a aeth i fyw i Loegr. Mae rhai o'r cyfenwau hyn sy'n gysylltiedig â Chymru i'w cael yng nghofnodion Lloegr, rhai ohonynt yn enwau nas arferid yng Nghymru. Gelwid rhai ar ôl eu bröydd e.e. Gwynett o Wynedd. Gelwid rhai yn ôl eu galwedigaeth, e.e. Wace o'r gair gwas. Wedi'r Ddeddf Uno yn 1536 ymledodd dylanwad arferion Seisnig, gan gynnwys y defnydd o gyfenwau, hefyd drwy'r gyfundrefn lywodraethol ac addysgol.
Credir bod y system tadenwau wedi parhau'n hir yng Nghymru yn rhannol oherwydd pwysigrwydd achau yng Nghyfraith Hywel Dda. Yn ogystal â'r cyfreithiau etifeddiaeth dibynnai'r drefn galanas ar wybod achau'r troseddwr a'r un a gafodd gam. Mae'n debyg bod llawer o Gymru rydd yn ymhyfrydu yn eu hachau a bod diddordeb mewn achyddiaeth wedi treiddio i ddiwylliant y Cymry. Mae Rowlands [1] yn dyfynnu o lyfr JE Griffiths [8] enw hen hen hen daid i Lewis Morris Môn (1701-65), sef

David ap William ap David Lloyd ap Thomas ap Dafydd ap Gwilym ap Dafydd Ieuan ap Howel ap Cynfrig ap Iorwerth Fychan ap Iorwerth ap Grono ap Tegerin.

Ni fabwysiadwyd cyfenwau ymhlith trwch gwerin Cymry hyd at y 18g mewn rhai ardaloedd a’r 19g mewn ardaloedd eraill. Cafwyd hwb i'r arfer o ddefnyddio cyfenwau yn 1813 pan ddaeth yn Ddeddf Gwlad y dylid cofrestru bedyddiadau a chladdedigaethau mewn llyfrau ar wahan wedi'u hargraffu'n arbennig ar gyfer hynny. Yn yr un i gofnodi bedyddiadau ceid colofn ar gyfer enw'r plentyn a cholofn ar gyfer cyfenw'r teulu.[9] Rhaid felly oedd dod o hyd i gyfenw i'w roi yn y golofn ar gyfer y cyfenw. Erbyn y 18g a'r 19g yr arfer wrth gymryd cyfenw oedd defnyddio enw'r tad neu'r tad-cu, gan ychwanegu s yn aml at ddiwedd yr enw, e.e. Jones, (John), Williams (William). Weithiau ceir enghreifftiau o deuluoedd o blant yn cael eu cofnodi â chyfenwau yn tarddu o enw'r tad a'r tad-cu am yn ail blentyn.
Fe ellid hefyd defnyddio ffurf Seisnig megis "Williams" i gymryd lle "ap Wiliam", heb iddo fod wedi troi yn gyfenw sefydlog. Er enghraifft, o gofrestr plwyf Beddgelert yn ail hanner y 18g a dechrau'r 19g; yn dechrau gyda Harri ap William, ei fab ef oedd William Parry. Ei fab yntau oedd Robert Williams. Dim ond yn y genhedlaeth nesaf, gyda Richard Williams, yr oedd y cyfenw yn amlwg yn enw teuluol sefydlog.
Gan mai enwau cyndeidiau yw prif ffynhonnell cyfenwau yng Nghymru mae amlder cyfenwau Cymreig yn adlewyrchu i raddau helaeth amlder enwau personol dynion yn y 18g a hanner cyntaf y 19g. Daeth nifer o enwau personol a oedd yn boblogaidd yn Lloegr yn yr 17g hefyd yn ffasiynol yng Nghymru. Hefyd defnyddiwyd enwau Beiblaidd yn aml yn enwau personol o ganlyniad i dwf yr eglwysi Anghydffurfiol ar ddiwedd y 18g a dechrau'r 19g. Golyga hyn oll nad yw dau Gymro sy'n arddel yr un cyfenw o reidrwydd yn perthyn i'w gilydd.
Mae John a Sheila Rowlands wedi archwilio amlder cyfenwau dros Gymru yn y cyfnod 1813-37. Fe ddilyn tabl yn dangos y deg cyfenw a ddigwydd mewn mwy na 2.5% o gofnodion priodasau yng Nghymru yn y cyfnod hwn, yn ôl cofrestri plwyf y cyfnod.


Amlder cyfenwau dros Gymru yn y cyfnod 1813-1837 [1]


Cyfenw
Amlder dros Gymru %



Jones
13.84



Williams
8.91



Davies
7.09



Thomas
5.70



Evans
5.46



Roberts
3.69



Hughes
2.98



Lewis
2.97



Morgan
2.63



Griffiths
2.58



Cyfanswm    
55.85


Gellir cymharu hyn â'r sefyllfa tua'r un adeg yn Lloegr lle'r oedd yr enw mwyaf poblogaidd, Smith, yn digwydd ag amlder o 1.37%.  Mae amlder cyfenwau yn amrywio o ardal i ardal yn ôl poblogrwydd yr enwau personol perthnasol mewn ardal pan fabwysiadwyd cyfenwau yno. Felly er bod rhai enwau'n boblogaidd trwy Gymru gyfan ceir rhai sy'n drwchus mewn rhai ardaloedd a bron yn llwyr absennol o ardaloedd eraill. Mae'r enw Llewelyn ddim ond i'w weld mewn 0.37% o'r cyfenwau dros Gymru gyfan ond yn gymharol drwchus yn y De, yn enwedig yn Sir Benfro ac ym Morgannwg. Fe ddigwydd ar ei amlder uchaf o 2.88% yn Ninas Powis, Morgannwg.
Enwau dros Brydain yn ôl poblogrwydd.
Dyma'r enwau Cymraeg mwyaf cyffredin ym Mhrydain (6 o'r top 10), sylwer bod enwau cymraeg yn eithaf uchel yn y rhestr gan fod enwau Saesneg fel arfer naill ai yn cyfeirio at le neu swydd (fel Smith neu tailor) neu enw+son.
1. Smith 729 862
2. Jones 578 261
3. Taylor 458 268
4. Williams 411 385
6. Davies 316 982
7. Evans 231 844
9. Thomas 220 228
10. Roberts 219 694
12. Lewis  198 193
22. Harris 161 505
28. James 151 855
29. Morgan 150 454
30. Hughes 147 802
31. Edwards 147 540
38. Morris 131 499
54. Davis 108 041
56. Price 105 993
59. Griffiths 104 048
75. Lloyd 85 021
76. Ellis 84 914
77. Richards 83 575
85. Powell 76 973
95. Owen 69 870
144. Jenkins 52 608

 O  http://surname.sofeminine.co.uk/w/surnames/most-common-surnames-in-great-britain.html Archifwyd 2010-02-11 yn y Peiriant Wayback.  2010.

Yn ôl Rowlands [1] gwnaethpwyd arolwg o lyfrau ffôn Cymru o 1959 gan J Wynne Lewis a gyhoeddwyd yn y Western Mail ar 29 Hydref 1994. Cafodd yntau mai'r un deg cyfenw ag yn y cyfnod 1813-37 a ddigwyddai amlaf yng Nghymru o hyd. Digwyddent yn yr un drefn amlder hefyd heblaw bod Evans yn digwydd yn amlach na Thomas, a Lewis yn amlach na Hughes.
Serch mai enw personol ffurfiol rhyw gyndad neu'i gilydd yw sail mwyafrif llethol cyfenwau Cymreig ffurfiwyd ambell i gyfenw o enwau disgrifiadol yn hytrach nag o enwau personol, e.e.

 o briodoleddau: Annwyl, Gough (o Goch), Lloyd (o Llwyd), Gam/Games (o Gam fel yn Dafydd Gam), Gwyn/Gwynne/Wynn, Vaughan (o Fychan, a allai gael ei ddefnyddio i wahaniaethu rhwng tad a mab oedd â'r un enw).

o lefydd: Conway, Kyffin, Mostyn, Pennant (o Ben y Nant), Blayney (o Flaenau) Nash, Carew, Picton. Hefyd ymhlith y rhai a fudodd i Loegr (e.e. porthmyn) ceir cyfenwau yn deillio o fro megis Powis, Radnor, Denby, Flint, Gwinnett, Gower. Ceir enwau plwyfi'n gyfenwau hefyd, megis Dewey o Landdewi.

o waith: Saer/Sare/Sears/Sayer o saer

o enwau merched: Gwenlan o Gwenllian.

Fel arfer y teuluoedd hynny a fabwysiadodd gyfenw'n gymharol gynnar sydd fwyaf tebygol o fod â chyfenw llai cyffredin. Weithiau gwelir cyfenw Cymreig mewn cofnodion cynnar nas arferir yn gyfenw o gwbl erbyn heddiw. Yn amlach na heb mae'r enwau hyn yn tarddu o'r ardaloedd hynny a fu dan ddylanwad Seisnig neu Normanaidd trwm yn ystod yr Oesau Canol, megis y gororau a De Phenfro. Mae'n debygol bod yr arfer Seisnig o fabwysiadu cyfenw disgrifiadol wedi croesi'r ffin ieithyddol a'i fabwysiadu gan y Cymry Cymraeg a drigai yno neu mewn ardaloedd Cymraeg cyfagos.
Mae ymfudwyr i Gymru drwy'r oesau hefyd wedi dod â'u cyfenwau ganddynt, gan ddechrau adeg y Normaniaid a chan barhau hyd heddiw, ac yn enwedig yn sgil y Chwyldro Diwydiannol ac eto yn ystod y chwarter canrif diwethaf. Mae'r ymfudwyr yma wedi ehangu amrywiaeth cyfenwau'r Cymry cryn dipyn. Wedi dweud hynny, bu i ambell i deulu o ymfudwyr cynnar i Gymru fabwysiadu'r system tadenwau Cymreig. Collodd y teuluoedd hyn eu cyfenwau estron wedi rhai cenedlaethau.

Ychwanegu ail enw
Un enw personol a fyddai'n arfer bod gan bawb, a'i ddilyn gan enw'r tad a'r tad-cu, neu gyfenw. Yn ystod y 19g y dechreuodd y ffasiwn o gael dau enw bedydd. Tyfodd yr arfer ymhlith ymfudwyr o Gymru i America gan mai dyma'r arfer yn America. Rheswm arall dros ychwanegu ail enw oedd er mwyn gwahaniaethu rhwng sawl un â'r un enw. Tyfodd yr arfer hwn ar ddiwedd y 19g. Yn aml cymerwyd cyfenw'r fam yn ail enw ac weithiau cysylltid yr ail enw a'r cyfenw gyda chysylltnod megis Jones-Edwards, Parry-Jones.

Ymlediad enwau Cymreig tu draw i Gymru
Fel ag sydd eisoes wedi ei grybwyll mae ymfudwyr o Gymru wedi mynd ag enwau personol a chyfenwau Cymreig ganddynt. Aeth nifer o Gymry i'r Iwerddon yn sgil cyrchoedd y Normaniaid yno a chyrchoedd y 17g. Soniwyd yn barod am enwau Cymreig yn Lloegr, a rhai ohonynt ddim i'w cael yng Nghymru erbyn hyn. Yn nhiroedd Ymerodraeth Prydain gynt ac yn America, pa le bynnag yr ymfudodd Cymry, y ceir enwau Cymreig hyd heddiw.

Cyfenwau Americanwyr o dras Affricanaidd
Mae John Weston yn yr erthygl 'Why do Welsh names seem to be so common within the African American community?'  [10] yn cynnig sawl ffordd y gallai cyfenwau Cymreig fod wedi ymledu ymhlith Americanwyr o dras Affricanaidd. Byddai caethwas, wrth gael ei ryddhau, weithiau yn cymryd enw ei gyn-feistr yn gyfenw, e.e. y cyfenw Gwyn o'r perchennog caethweision James Gwyn o Ogledd Carolina.[10] Ymhlith y caethweision hyn byddai ambell un yn blentyn gordderch i'r perchennog. Felly ymhlith y rhai sy'n dwyn enwau Cymreig heddiw mae rhai sydd â hynafwyr Cymreig hefyd.[11]
Mae Weston hefyd yn awgrymu posibiliadau eraill:

 Mae'n bwrw bod rhai caethweision oedd wedi dianc wedi cymryd cyfenw'r rhai a'u cynorthwyodd i ddianc. Roedd Cymry ymhlith y cynorthwywyr hyn.

Roedd y cyn-gaethweision yn cymryd cyfenwau ar yr un adeg ag oedd llawer o Gymry hefyd yn cymryd cyfenwau am y tro cyntaf. Roedd nifer o enwau personol megis John a David i'w cael yn y ddwy gymuned fel ei gilydd. Cyd-ddigwyddiad yw hi felly bod y ddwy gymuned yn mabwysiadu cyfenwau tua'r un adeg ac yn addasu'r un enwau personol i fod yn gyfenwau, e.e. Jones o John, Davies o David.

Cynigir y posibilrwydd bod rhai caethweision wedi cymryd enw lle yn gyfenw, e.e. swyddi Evans, Floyd a Morgan yn Georgia a'r pentrefi Jones a Montgomery yn Louisiana.

Ond nid oes gwaith ymchwil manwl wedi ei gyflawni i allu profi pa mor gymharol bwysig oedd y ffyrdd gwahanol yma o ledaenu enwau Cymreig ymhlith Americanwyr o dras Affricanaidd.

Enwau priod menywod
Yn draddodiadol ni newidiai enw merch adeg ei phriodas. Cyrraedd o Loegr wnaeth yr arfer i wraig gymryd enw ei gŵr yn gyfenw adeg ei phriodas. Ymledodd yr arfer hwn yn araf drwy Gymru. Roedd rhai yn arfer yr hen draddodiad hyd at ganol y 19g. Yn rhai o'r cofrestri plwyf gwelir y ddau arfer yn cydredeg, e.e. yng nghofrestr Llanynys yn 1677 cofnodir Magdalen vch Rees Wynne alias Magdalen Thelwall (gweddw Thomas Thelwall).
Yn hanner olaf yr 20g dechreuodd rhai menywod gadw at eu cyfenw gwreiddiol wrth briodi.

Ffynonellau
 
↑ 1.0 1.1 1.2 1.3 John & Sheila Rowlands, The Surnames of Wales (Federation of Family History Societies (Publications) Ltd, 1996)


↑ 'Unusual Names', Cylchgrawn Cymdeithas Hanes Teuluoedd Ceredigion 4 Rhif 6 (2006)


↑ Fishlock's Tales, HTV, 2006


↑ [Swyddfa Ystadegau (ONS) drwy wefan Cymru Fyw (BBC)]; adalwyd 3 Medi 2016.


↑ D. Geraint Lewis, Welsh Names (2001, Geddes & Grosset)


↑ P Hanks a F Hodges, A Dictionary of Surnames (1988, Oxford


↑ TJ Morgan a Prys Morgan, Welsh Surnames t14 (Prifysgol Cymru, 1985)


↑ JE Griffiths, Pedigrees of Anglesey and Carnarvonshire Families (1914, Horncastle)


↑ Anthony J Camp, Tracing your Ancestors, t. 74 (W&G Foyle Ltd, 1964)


↑ 10.0 10.1 "teitl". Archifwyd o'r gwreiddiol ar 2005-08-14. Cyrchwyd 2007-04-29.


↑ Roots in Wales, BBC, (2006)



Llyfryddiaeth
 Heini Gruffudd, Enwau Cymraeg i Blant  (2003, Y Lolfa)

Heini Gruffudd, Enwau'r Cymry  (2003, Y Lolfa)

Cyfres Pigion Llafar Gwlad:4. Llysenwau – Casgliad o Lysenwau Cymraeg a gofnodwyd yn y Cylchgrawn Llafar Gwlad, Gol. Myrddin ap Dafydd (1997, Gwasg Carreg Gwalch)

Roy Noble, Roy Noble's Welsh Nicknames, (1998, Western Mail Books)

DL Chamberlain, Welsh Nicknames (1981)

Behind the name - gwefan yn olrhain tarddiad enwau personol Cymreig

The Welsh Patronymic surname system Archifwyd 2007-01-09 yn y Peiriant Wayback. - gwefan yn egluro'r system tadenwau





