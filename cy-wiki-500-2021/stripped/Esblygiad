      Alfred Russel Wallace   Charles Darwin  Y ddau a wyddonydd a gyd-sefydlodd y theori fod canghennau esblygiad rhywogaethau yn tarddu o ddetholiad naturiol.
      Gwaith Haeckel: "Paleontological Tree of Vertebrates (tua 1879)".  Mae esblygiad rhywogaethau dros gyfnod o amser wedi'i gymharu i goeden, gyda nifer o ganghennau'n tarddu o un bonyn. Dyma yw gwraidd ein syniadaeth heddiw.
      1: Mwtadu yn creu amrywiaeth. 2: Mwtadiaid anffafriol yn methu atgenhedlu'n llwyddiannus. 3: Atgenhedlu yn digwydd gan greu mwtadiaeth ffafriol ac anffafriol eto. 4. Mwtadiaid ffafriol yn fwy tebygol o oroesi. 5: ... ac atgenhedlu.
Esblygiad yw'r ddamcaniaeth fod nodweddion etifeddadwy poblogaethau biolegol yn newid ac yn datblygu dros filiynau o flynyddoedd. Mae'r broses yn gyfrifol am y cymhlethdod a'r amrywiaeth enfawr sydd yn nodweddu bywyd fel y'i welir ar y Ddaear heddiw. Gan fod rhai unigolion yn atgenhedlu'n fwy llwyddiannus nag eraill—oherwydd nodweddion sy'n eu galluogi i oroesi'n well, i atynu cymar yn fwy llwyddianus, neu i fanteisio'n well o'u amgylchedd—a gan fod yr unigolion hyn yn tueddu i drawsyrru i'w epil y nodweddion a arweinodd at eu llwyddiant, tueddir i rywogaethau addasu dros amser i'w hamgylchedd. Dyma brif fecaniaeth esblygiad.
Mae ein dealltwriaeth heddiw o fioleg esblygol yn cychwyn gyda chyhoeddiad papurau Alfred Russel Wallace a Charles Darwin ym 1858 a'i phoblogeiddio yn 1859 pan gyhoeddodd Charles Darwin ei On the Origin of Species. Cyplyswyd hyn a gwaith mynach o'r enw Gregor Mendel a'i waith ar blanhigion a'r hyn rydym yn ei alw'n etifeddeg a genynau.[1]
Oddeutu 4 biliwn o flynyddoedd yn ôl, yn dilyn adweithiau cemegol cryf, crewyd moleciwlau a oedd yn medru creu copi ohonynt eu hunain drwy'r broses a elwir yn 'atgynhyrchu'. Hanner biwliwn o flynyddoedd yn ddiweddarach crewyd yr hynafiad sy'n perthyn i bopeth byw. Drwy ddatblygu'r broses o ffotosynthesis, cynaeafwyd golau'r haul gan yr hynafiaid hyn a gynhyrchodd ocsigen moleciwlar (O2) a ymledodd drwy'r  atmosffer,[2] a ffurfiodd yn ei dro darian o haen osôn a amddiffynodd yr hyn a oedd yn brysur ddatblygu oddi tano: bywyd. O'r hynafiaid un-gell yma, datblygwyd y gallu i gell ffurfio oddi fewn i gell arall, gan greu organebau amlgellog sef ewcaryotau (eukaryotes). Drwy arbenigedd, ffurfiwyd mathau gwahanol a thrwy amsugno ymbelydredd electromagnetig uwchfioled, lledodd y bywyd newydd hwn ar draws ac ar hyd wyneb y Ddaear. Mae'r ffosil cyntaf (neu hynaf) a ganfuwyd hyd yma (2015) oddeutu 3.7 biliwn o flynyddoedd oed, mewn tywodfaen yng Ngorllewin yr Ynys Las.[3]

  Cynnwys
 
1 Syniadau newydd Darwin: esblygiad drwy ddetholiad naturiol

2 Syniadau gwyddonol yn newid
 
2.1 Tystiolaeth am Esblygiad

2.2 Astudiaeth genetig




3 Esblygiad bodau dynol: llinell amser

4 Cyfeiriadau

5 Gweler hefyd




Syniadau newydd Darwin: esblygiad drwy ddetholiad naturiol
Charles Darwin a Gregor Mendel yw sylfaenwyr damcaniaeth esblygiad fodern. Cyflwynodd Darwin ei syniadau am ddetholiad naturiol yn 1859. Roedd y syniad yma yn hanfodol bwysig i syniadau Darwin.
Dyma rai o gonglfeini detholiad naturiol:

 Pe bai pob organeb yn atgynhyrchu'n llwyddiannus, yna byddai poblogaeth y rhywogaeth hwnnw yn cynyddu tu hwnt i reolaeth[4]

Mae adnoddau pob amgylchedd yn gyfyngedig (e.e. hyn-a-hyn o goed sydd ar ynys)

Mae organebau yn wahanol, ac nid oes byth ddau organeb yn union yr un fath; mae gan bob rhywogaeth, felly, gyfoeth o amrywiaeth ac mae amrywiaeth hefyd, wrth gwrs, yn bodoli rhwng rhywogaethau.

Nid yw'r rhan fwyaf o'r epil yn goroesi (2% o bysgod sydd yn goroesi o'r epil (neu'r wyau) sydd yn cael ei gynhyrchu.)

Bydd y rhai sy'n goroesi yn atgenhedlu a phasio eu nodweddion i'r genhedlaeth nesaf.

Felly mae'r nodweddion sy'n galluogi'r unigolyn i oroesi yn cael eu cadw yn y boblogaeth.

Dyma rai o'r ffactorau sy'n penderfynu pa organebau sy'n goroesi:

 Ysglyfaethwyr

Afiechyd

Cystadlaeth am fwyd

Cystadlaeth am dir neu (yn enwedig mewn planhigion) am olau ayyb.

Gelwir effaith y ffactorau hyn yn 'Ddetholiad Naturiol'.
Ar y pryd doedd Darwin ddim yn gwybod am gromosomau, ond erbyn heddiw rydym yn gwybod mai cromosomau sydd yn cludo gwybodaeth etifeddol.

Syniadau gwyddonol yn newid
Fel mae gwybodaeth gwyddonol yn newid, oherwydd tystiolaeth neywdd, mae ein syniadau am bethau yn newid. Yn amser Darwin roedd y mwyafrif o bobl yn credu bod y rhywogaethau naturiol sy'n byw ar y ddaear wedi cael eu creu mewn chydig ddyddiau ac eu bod wedi goroesi heb newid ers hynny. Gelwir hyn yn Greadaeth (o'r gair 'creu'). Cafodd Darwin lawer o bobl crefyddol yn gwrthwynebu ei syniadau gwyddonol newydd. Erbyn heddiw, mae'r rhan fwyaf o wyddonwyr yn derbyn mai detholiad naturiol, ynghyd â mecaniaethau esblygiadol eraill, sy'n gyfrifol am amrywiaeth bywyd.

Tystiolaeth am Esblygiad
Trwy astudio ffosilau mewn creigiau o wahanol oedran (fel y gwnaeth Darwin yng Nghwm Idwal) a'r newidiadau yn eu ffurf dros gyfnod o amser down i wybod mwy am amser a pha bryd y crewyd y gwahanol rywogaethau.

Astudiaeth genetig
Gellir edrych ar y genynau sydd mewn gwahanol rhywogaethau ac wedyn eu cymharu e.e. mae dyn a'r tsimpansî yn rhannu 98% o'u genynau.

Esblygiad bodau dynol: llinell amser



Miliwn o flynyddoedd (CP) 
55
17
8
4.4
4
2.8
2.3




Digwyddiad 
Primatau cyntaf yn ymddangos; tebyg i'r Marmoset
Yr epa mawr (neu'r Hominidae) yn hollti oddi wrth hynafiaid y gibon
Y Tsimpansî a pherthnasau eraill bodau dynol yn hollti oddi wrth y gorila.
Traed yr Ardipithecus yn medru 'gafael'.
Australopithecus afarensis yn ymddangos gydag ymennydd o faint y tsimpansî.
LD 350-1 yn ymddangos; efallai mai dyma'r cyntaf yn nheulu'r Homo
Homo habilis yn ymddangos yn Affrica










-





Cyfeiriadau

Gweler hefyd
 Richard Owen

 

  Eginyn erthygl sydd uchod am fioleg. Gallwch helpu Wicipedia drwy ychwanegu ato.
 
  
 Chwiliwch am esblygiad yn Wiciadur.





