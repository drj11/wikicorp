 Gogledd IwerddonTuaisceart Éireann Northern Ireland
ArwyddairQuis separabit?
Mathgwledydd y Deyrnas Unedig 
 
PrifddinasBelffast 
Poblogaeth1,852,168 
Sefydlwyd  8 Mai 1921 

Cylchfa amserUTC±00:00, Europe/Belfast, UTC+01:00 
NawddsantSant Padrig 
Iaith/Ieithoedd   swyddogolSaesneg, Gwyddeleg, Ulster Scots 
Daearyddiaeth
Rhan o'r canlynolIwerddon 
Arwynebedd14,130 ±1 km² 
Yn ffinio gydaGweriniaeth Iwerddon 
Cyfesurynnau54.68°N 6.75°W 
Cod SYGN92000002 
GB-NIR 
Gwleidyddiaeth
Corff gweithredolGweithrediaeth Gogledd Iwerddon 
Corff deddfwriaetholCynulliad Gogledd Iwerddon 
Swydd pennaeth   y LlywodraethPrif Weinidog Gogledd Iwerddon 


Arianpunt sterling 
  Ffeiliau perthnasol ar Comin
 [golygwch ar Wicidata]

Rhanbarth o'r Deyrnas Unedig yw Gogledd Iwerddon (Gwyddeleg: Tuaisceart Éireann, Saesneg: Northern Ireland; Sgoteg Wlster: Norlin Airlann), yng ngogledd-ddwyrain Iwerddon. Mae'n cynnwys chwech o 32 sir ynys Iwerddon - chwech o naw sir talaith Wledd neu Wlster. Mae iddi arwynebedd o 14,139 km² (5,459 milltir sgwâr), ac mae ganddi boblogaeth o 1,810,863 (Cyfrifiad 2011) (1,685,267, Cyfrifiad 2001). Belffast yw'r brifddinas.
Mae Gogledd Iwerddon yn cael ei llywodraethu gan Gynulliad Gogledd Iwerddon, yn Stormont, ac Ysgrifennydd Gwladol Gogledd Iwerddon sy'n atebol i lywodraeth San Steffan.
Fodd bynnag, hyd at 1800, roedd yr ynys gyfan (y Gogledd a'r De) yn un wlad - 'Teyrnas Iwerddon' - hyd nes i Loegr ei huno o dan Ddeddf Uno 1801 i'r hyn a alwyd yn 'Deyrnas Unedig Prydain Fawr ac Iwerddon'. Wedi cread y Dalaith Rydd Wyddelig (Saorstat Eireann) ym 1922, allan o 26 o siroedd Iwerddon, parhaodd chwe sir y gogledd yn rhan o'r Deyrnas Unedig, a chafodd y wladwriaeth ei hail-enwi yn Deyrnas Unedig Prydain Fawr a Gogledd Iwerddon ym 1927.
Yn ei gyfieithiad o un o ganeuon Tommy Makem, canodd Dafydd Iwan am y 'Pedwar Cae' (An Cheathrú Gort Glas). Gogledd Iwerddon ydyw'r pedwerydd cae - y cae sydd yn nwylo Lloegr, "ddaw eto yn rhydd medd hi."

  Cynnwys
 
1 Yr enw
 
1.1 Unoliaethwyr

1.2 Cenedlaetholwyr/Gweriniaethwyr




2 Cyfeiriadau




Yr enw
Mae enw'r rhanbarth ei hun ac rhai enwau lleoedd o'i fewn yn bwnc dadleuol. Mae llawer yn dibynnu ar safbwyntiau gwleidyddol. Yr enghraifft enwocaf efallai yw dinas Derry/Londonderry, gyda'r ffurf Derry (Saesneg) neu Doire/Doire Cholm Chille (Gwyddeleg) yn cael ei defnyddio gan genedlaetholwyr Gwyddelig a Londonderry gan yr Unoliaethwyr.
'Gogledd Iwerddon' yw'r enw swyddogol ond mae nifer o Unoliaethwyr yn a'u cefnogwyr yn dal i defnyddio'r enw Ulster (Ulaidh). Ar y llaw arall, mae cenedlaetholwyr a gweriniaethwyr yn tueddu i gyfeirio at y rhanbarth fel 'Y Gogledd' (The North/North of Ireland) neu'r 'Chwe Sir' (Six Counties.

Unoliaethwyr
 Ulster (Ulaidh) yw'r enw hanesyddol am dalaith Wyddelig Wlster, gyda chwech o'i naw sir yn gorwedd yng Ngogledd Iwerddon. Mae cenedlaetholwyr yn gwrthod defnyddio'r enw i gyfeirio at Ogledd Iwerddon ond mae'r term "Ulster" yn cael ei defnyddio'n aml gan yr Unoliaethwyr a'r wasg Brydeinig i gyfeirio at Ogledd Iwerddon. Fe'i gwelir hefyd mewn enwau fel Radio Ulster, RUC, Ulster Unionist Party ac Ulster Volunteer Force (grwp paramilwrol).

Y Dalaith (The Province / An Chúige) yw'r enw hanesyddol am dalaith Wyddelig Wlster ond fe'i defnyddir gan Unoliaethwyr ac eraill fel term am Ogledd Iwerddon. Yn ôl canllawiau'r BBC, mae "the province" yn dderbyniol ond dydy "Ulster" ddim. Mae'n argymell peidio defnyddio'r gair "Prydeinig" ("British") i gyfeirio at y bobl ond yn hytrach i ddefnyddio'r term niwtral "pobl Gogledd Iwerddon" ("people of Northern Ireland", a'r term "tir mawr" "mainland" wrth gyfeirio at Brydain Fawr mewn perthynas â Gogledd Iwerddon[1] Nid wy'r termau hyn yn dderbyniol gan y gymuned weriniaethol.

Cenedlaetholwyr/Gweriniaethwyr
Defnyddir sawl term gan weriniaethwyr a chenedlaetholwyr i gyfeirio at Ogledd Iwerddon:

 North of Ireland (Tuaisceart na hÉireann) - term sydd ddim yn cyfieithu i'r Gymraeg yn iawn ac sy'n pwysleisio fod y rhanbarth yn rhan o Iwerddon yn hytrach na'r DU.

Gogledd-ddwyrain Iwerddon (North-East Ireland / Oirthuaisceart Éireann). Amrywiad ar yr uchod, llawer llai cyffredin.

Y Chwe Sir (The Six Counties / na Sé Chontae) - term a ddefnyddir gan weriniaethwyr, e.e. Sinn Féin. Cyfeirir at Weriniaeth Iwerddon fel 'y 26 Sir' (Twenty-Six Counties)[2] Dadleuir fod defnyddio enw swyddogol y rhanbarth yn gyfystyr â derbyn ei fod yn perthyn i'r DU.

The Occupied Six Counties.[3]

British-Occupied Ireland. Fel Occupied Six Counties, defnyddir hyn gan weriniaethwyr sy'n gwrthwynebu Cytundeb Dydd Gwener y Groglith (Good Friday Agrrement).[4]

Fourth Green Field (An Cheathrú Gort Glas). O'r gân Four Green Fields gan Tommy Makem.

Cyfeiriadau

  gw • sg • go Gwledydd a thiriogaethau'r Deyrnas Unedig

Gwlad neu ranbarth  
  Yr Alban

 Cymru

 Gogledd Iwerddon

 Lloegr



Tiriogaethau tramor  
 Anguilla

Bermuda

De Georgia ac Ynysoedd Sandwich y De

Gibraltar

Montserrat

Saint Helena, Ascension a Tristan da Cunha

Tiriogaeth Brydeinig yr Antarctig

Tiriogaeth Brydeinig Cefnfor India

Ynysoedd Caiman

Ynysoedd y Falklands

Ynysoedd Pitcairn

Ynysoedd Prydeinig y Wyryf

Ynysoedd Turks a Caicos



Tiriogaethau dibynnol y Goron  
 Jersey

Ynys y Garn

Ynys Manaw



Ardaloedd Canolfan Sofranaidd  
 Akrotiri a Dhekelia








