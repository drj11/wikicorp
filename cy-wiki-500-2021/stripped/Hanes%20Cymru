 Hanes Cymru
 
 Mae'r erthygl hon yn rhan o gyfres ― 
Cyfnodau
 
Cynhanes
 
Cyfnod y Rhufeiniaid
 
Oes y Seintiau
 
Yr Oesoedd Canol
 
Yr Oesoedd Canol Cynnar
 
Oes y Tywysogion
 
Yr Oesoedd Canol Diweddar
 
Cyfnod y Tuduriaid
 
17eg ganrif
 
18fed ganrif
 
19eg ganrif
 
20fed ganrif
 
Y Rhyfel Byd Cyntaf
 
Y cyfnod rhwng y rhyfeloedd
 
Yr Ail Ryfel Byd
 
21ain ganrif
Teyrnasoedd
 
Deheubarth
 
Gwynedd
 
Morgannwg
 
Powys
Yn ôl pwnc
 
Hanes crefyddol
 
Hanes cyfansoddiadol
 
Hanes cyfreithiol
 
Hanes cymdeithasol
 
Hanes demograffig
 
Hanes economaidd
 
Hanes gwleidyddol
 
Hanes LHDT
 
Hanes milwrol
 
Hanes morwrol
 
Hanes tiriogaethol
Hanesyddiaeth
 ― 
  v
t
e



 Am y llyfr gan John Davies, gweler Hanes Cymru (llyfr). Am Gymru gyn-hanesyddol, gweler Cynhanes Cymru.
Er nad yw'n wlad fawr o ran ei maint, mae hanes Cymru yn hir a diddorol. Gellid dadlau fod hanes Cymru fel gwlad, neu egin-wlad, yn dechrau yn ystod cyfnod rheolaeth yr Ymerodraeth Rufeinig. Credir fod Cristnogaeth wedi ymgartrefu yng Nghymru tua diwedd y cyfnod hwnnw ac arferir galw'r cyfnod olynol yn Oes y Seintiau. Yng nghyfnod y seintiau cynnar gwelwyd newidiadau sylfaenol ym Mhrydain o dan bwysau'r goresgyniad Eingl-Sacsonaidd yn ne a dwyrain yr ynys a mewnfudo gan Wyddelod i Gymru a gorllewin yr Alban.
Yn dilyn ymadawiad y Rhufeiniaid, fe dyfodd nifer o freniniaethau  - yng Ngwynedd, Powys, Dyfed a Deheubarth. Roedd gan y brenhinoedd hyn eu heglwysi, eu cyfreithiau a'u llysoedd eu hunain, gyda safon byw a diwylliant uchel. Erbyn yr 8g, pan godwyd Clawdd Offa, roedd tiriogaeth Cymru yn ddiffiniedig a'r Oesoedd Canol wedi dechrau. Yn sgil y goresgyniad Normanaidd newidiodd patrwm llywodraethu Cymru a dechreuodd ei harweinwyr pendefig galw eu hunain yn dywysogion yn hytrach na brenhinoedd; gelwir y cyfnod hwnnw yn "Oes y Tywysogion". Yn yr Oesoedd Canol Diweddar yng Nghymru ar ôl cwymp Llywelyn ap Gruffudd a'i frawd Dafydd ap Gruffudd, cafwyd cyfnod ansefydlog dan reolaeth coron Lloegr. Arweiniodd Owain Glyndŵr wrthryfel llwyddiannus yn erbyn y Saeson ac yn ddiweddarach enillodd Harri Tudur Frwydr Bosworth gan sefydlu cyfnod y Tuduriaid.
Un canlyniad o'r cyfnod hwnnw oedd y Deddfau Uno a roddodd Cymru yn yr un system gyfreithiol a gweinyddol â Lloegr. Ar yr un pryd newidiodd Cymru mewn cenhedlaeth neu ddwy o fod yn wlad Gatholig i fod yn wlad Brotestannaidd. Gwelwyd hefyd adfywiad llenyddol - cyfnod y Dadeni Dysg a'r Beibl Cymraeg - ond troes y bonedd fwyfwy Seisnigaidd a thyfai bwlch rhwng arweinwyr cymdeithas a'r werin. Un canlyniad o hynny oedd y mudiadau crefyddol ymneilltuol a ymledai'n gyflym yn ystod y 17eg ganrif a'r 18fed. Dechreuodd Cymru droi'n wlad ddiwydiannol yn ogystal, ac erbyn diwedd y 18g a dechrau'r 19eg ganrif roedd trefi diwydiannol, chwareli a phyllau glo yn nodwedd amlwg ar fywyd y wlad. Tyfodd llythrennedd ac ymledai'r wasg Gymraeg. Cynyddai'r galw am Ddatgysylltu'r Eglwys Anglicanaidd yng Nghymru ac am hunanlywodraeth ac erbyn diwedd y 19g roedd mudiad Cymru Fydd ar ei anterth.
Cymysg fu ffawd y wlad yn ystod y 20fed ganrif. Ond er gwaethaf y Rhyfel Byd Cyntaf, Dirwasgiad Mawr y 1930au, yr Ail Ryfel Byd a'r dirywiad ieithyddol yn y 1970au a'r 1980au, mae Cymru heddiw yn meddu Cynulliad Cenedlaethol ac ymddengys fod yr iaith Gymraeg yn wynebu dyfodol mwy gobeithiol nag a ddychmygid cenhedlaeth yn ôl.

  Cynnwys
 
1 Cyfnod y Rhufeiniaid yng Nghymru

2 Oes y Seintiau yng Nghymru

3 Yr Oesoedd Canol yng Nghymru
 
3.1 Yr Oesoedd Canol Cynnar 600 - 1067

3.2 Oes y Tywysogion

3.3 Yr Oesoedd Canol Diweddar




4 Cyfnod y Tuduriaid

5 Yr Ail Ganrif ar Bymtheg

6 Y Ddeunawfed Ganrif

7 Y Bedwaredd Ganrif ar Bymtheg

8 Yr Ugeinfed Ganrif

9 21ain ganrif
 
9.1 Datganoli Cymru




10 Cyfeiriadau

11 Llyfryddiaeth




Cyfnod y Rhufeiniaid yng Nghymru
 
  Prif erthygl: Cyfnod y Rhufeiniaid yng Nghymru
Roedd Cymru yn rhan o'r Ymerodraeth Rufeinig am tua 350 o flynyddoedd. Cawsai'r Rhufeiniaid a'u diwylliant effaith sylweddol ar y wlad a'i phobl. Gadawodd y Rhufeiniaid rwydwaith o ffyrdd ar eu hôl a sefydlasant nifer o drefi. Cafodd ei hiaith, Lladin, ddylanwad mawr ar yr iaith Gymraeg wrth iddi ymffurfio o Frythoneg Ddiweddar; mae tua 600 o eiriau Cymraeg yn tarddu o'r cyfnod hwnnw (yn hytrach nag o Ladin yr Oesoedd Canol fel yn achos ieithoedd eraill fel Saesneg). Credir i'r Gristnogaeth gyrraedd Cymru yn y cyfnod Rhufeinig hefyd.
Roedd Cymru'n gartref i lwythau Celtaidd fel y Silwriaid yn y de-ddwyrain a'r Votadini yn y gogledd-orllewin, a'u tiriogaeth yn cyfateb yn fras i'r teyrnasoedd Cymreig cynnar a sefydlwyd ar ôl ymadawiad y Rhufeiniaid.

Oes y Seintiau yng Nghymru
 
  Prif erthygl: Oes y Seintiau yng Nghymru
Mae cloddio archeolegol wedi datgelu rhywfaint am y cyfnod yn dilyn ymadawiad y Rhufeiniaid. Y safle sydd wedi rhoi mwyaf o wybodaeth yw bryngaer Dinas Powys ym Morgannwg, lle'r oedd y trigolion yn amlwg yn perthyn i haen uchaf cymdeithas. Ymhlith y darganfyddiadau roedd darnau crochenwaith o ardal Môr y Canoldir, gwydr o ffynhonnell Diwtonaidd a gwaith metel Celtaidd. Credir fod Dinas Powys yn llys pennaeth neu frenin yn y cyfnod yma. Ceir hefyd dystiolaeth meini ag arysgrifen arnynt. Yng ngogledd Cymru roedd y rhain mewn Lladin, ond yn y de-orllewin a Brycheiniog mae'r arysgrifau mewn Ogam neu'n ddwyieithog. Ymddengys fod Gwyddelod wedi ymsefydlu yma yn y cyfnod wedi i'r Rhufeiniaid adael os nad ynghynt, ac roedd teulu brenhinol Teyrnas Dyfed o dras Wyddelig.
Daeth Cristnogaeth Cymraeg i'r amlwg gyntaf yn ne-ddwyrain Cymru, gyda thystiolaeth o Gristionogion mewn sefydliadau megis Caerwent a Chaerleon yn y cyfnod Rhufeinig. Roedd Sant Dyfrig yn un o'r arweinwyr cyntaf. Cawn wybodaeth am y seintiau yn eu bucheddau, ond yn anffodus mae y rhan fwyaf o'r rhai sydd wedi goroesi wedi eu hysgrifennu ar ôl y goncwest Normanaidd. Ymhlith y seintiau enwocaf mae Dewi Sant, Teilo, Illtud, Cadog a Deiniol. Roedd cysylltiadau agos rhwng Cymru ag Iwerddon a Llydaw.
Ceir rhywfaint o wybodaeth am hanner cyntaf y 6g yng ngwaith Gildas, y De Excidio Britanniae, sy’n bregeth mewn tair rhan yn condemnio pechodau’r Brythoniaid yn ei oes ef ac yn awgrymu mai oherwydd y pechodau hyn roedd y Sacsoniaid wedi goresgyn rhan helaeth o’r ynys. Mae’n condemnio pum teyrn yn arbennig, yn cynnwys Maelgwn Gwynedd, brenin Gwynedd, y mwyaf grymus o'r pump.

Yr Oesoedd Canol yng Nghymru
 
  Prif erthygl: Yr Oesoedd Canol yng Nghymru
 Gweler hefyd: Rheolaeth y Saeson o Gymru

Yr Oesoedd Canol Cynnar 600 - 1067
 
  Prif erthygl: Yr Oesoedd Canol Cynnar yng Nghymru
      Teyrnasoedd Cymru. 
Ar ddechrau'r cyfnod roedd rhai rhannau o Gymru, yn enwedig Powys, yn dod o dan bwysau cynyddol oddi wrth yr Eingl-Sacsoniaid, yn enwedig teyrnas Mercia. Collodd Powys cryn dipyn o'i thiriogaeth, oedd yn arfer ymestyn i'r dwyrain o'r ffin bresennol, gan gynnwys yr hen ganolfan, Pengwern. Efallai fod adeiladu Clawdd Offa, yn draddodiadol gan Offa, brenin Mercia yn yr 8g, yn dynodi ffin wedi ei chytuno.
Y cyntaf i deyrnasu dros ran helaeth o Gymru oedd Rhodri Mawr, yn wreiddiol yn frenin Teyrnas Gwynedd, a daeth yn frenin Powys a Ceredigion hefyd. Pan fu ef farw, rhannwyd ei deyrnas rhwng ei feibion, ond gallodd ei ŵyr, Hywel Dda, ffurfio teyrnas Deheubarth trwy uno teyrnasoedd llai'r de-orllewin, ac erbyn 942 roedd yn frenin ar y rhan fwyaf o Gymru. Yn draddodiadol, cysylltir ef a ffurfio Cyfraith Hywel trwy alw cyfarfod yn Hendy-gwyn ar Daf. Pan fu ef farw yn 950 gallodd ei feibion ddal eu gafael ar Ddeheubarth, ond adfeddiannwyd Gwynedd gan y frenhinlin draddodiadol.
Erbyn hyn roedd y Llychlynwyr yn ymosod at Gymru, yn enwedig y Daniaid yn y cyfnod rhwng 950 a 1000. Dywedir i Godfrey Haroldson gymryd dwy fil o gaethion o Ynys Môn yn 987, a thalodd brenin Gwynedd, Maredudd ab Owain, swm mawr i'r Daniaid i brynu ei bobl yn ôl o gaethiwed.
Gruffydd ap Llywelyn oedd y teyrn nesaf i allu uno'r teyrnasodd Cymreig. Brenin Gwynedd ydoedd yn wreiddiol, ond erbyn 1055 roedd wedi gwneud ei hun yn frenin bron y cyfan o Gymru ac wedi cipio rhannau o Loegr ger y ffin. Yn 1063 gorchfygwyd ef gan Harold Godwinson a'i ladd gan ei ŵyr ei hun. Rhannwyd ei deyrnas unwaith eto, gyda Bleddyn ap Cynfyn a'i frawd Rhiwallon yn dod yn frenhinoedd Gwynedd a Phowys.

Oes y Tywysogion
 
  Prif erthygl: Oes y Tywysogion
Pan orchfygwyd Lloegr gan y Normaniaid yn 1066, y prif deyrn yng Nghymru oedd Bleddyn ap Cynfyn, oedd yn teyrnasu dros Wynedd a Phowys. Yn ne Cymru y cafodd y Normaniaid eu llwyddiannau cynnar, gyda William Fitzosbern, Iarll 1af Henffordd yn cipio Teyrnas Gwent cyn 1070. Erbyn 1074 roedd byddin Iarll Amwythig yn anrheithio Deheubarth.[1]
Pan laddwyd Bleddyn ap Cynfyn yn 1075, bu cyfnod o ryfel cartref yng Nghymru, a roddodd gyfle i'r Normaniaid gipio tiroedd yng ngogledd Cymru. Yn 1081 trefnwyd cyfarfod rhwng Ieirll Caer ac Amwythig a Gruffudd ap Cynan, oedd newydd gipio gorsedd Gwynedd oddi wrth Trahaearn ap Caradog ym Mrwydr Mynydd Carn. Cymerwyd Gruffudd yn garcharor trwy frad, a chadwyd ef yng Nghaer am flynyddoedd, gyda'r Normaniaid yn cipio rhan helaeth o Wynedd.[2] Yn y de, diorseddwyd Iestyn ap Gwrgant, teyrn olaf Morgannwg, tua 1090 gan Robert Fitzhamon, arglwydd Caerloyw, oedd wedi sefydlu arglwyddiaeth yng Nghaerdydd ac a aeth ymlaen i gipio ardal Bro Morgannwg. Lladdwyd Rhys ap Tewdwr, brenin Deheubarth, ym 1093 wrth amddiffyn Brycheiniog rhag y Normaniaid, a chipwyd ei deyrnas a'i rhannu rhwng nifer o arglwyddi Normanaidd.[3] I bob golwg, roedd y goncwest Normanaidd bron yn gyflawn.

Yr Oesoedd Canol Diweddar
 
  Prif erthygl: Yr Oesoedd Canol Diweddar yng Nghymru
Ar ôl i Llywelyn Ein Llyw Olaf, Tywysog Cymru, gael ei fradychu a'i ladd yng Nghilmeri ym 1282 daeth y wlad dan reolaeth Edward I, Brenin Lloegr. Adeiladodd Edward gestyll ar hyd arfordir Cymru mewn cylch haearn o gwmpas y wlad a chafodd ei fab Edward ei arwisgo yn Dywysog Cymru.
Cestyll Edward I a’r Bwrdeisdrefi
Ym mis Mehefin 1287, gwrthryfelodd un o arglwyddi Cymru, Rhys ap Maredudd. Arglwydd o Sir Gaerfyrddin oedd Rhys ac roedd wedi cefnogi Edward yn rhyfeloedd 1276-7 ac 1282-3. Roedd wedi ei siomi na chafodd fwy o wobr am ei gefnogaeth. Yn ogystal, roedd Rhys wedi cael llond bol bod y Sais o Ustus De Cymru yn ymyrryd yn ei waith ef o reoli. Parhaodd y gwrthryfel tan fis Ionawr 1288. Cafodd Rhys ei orchfygu gan fyddin y brenin oedd yn cynnwys llawer o Gymry. Effeithiodd gwrthryfel 1294-5 ar Gymru gyfan. Cafodd ei arwain gan dri arglwydd o Gymry: Madog ap Llywelyn yn y gogledd, Morgan ap Maredudd ym Morgannwg, a Maelgwn ap Rhys yn y de-orllewin. Parhaodd y gwrthryfel o fis Medi 1294 tan haf 1295 pan orchfygwyd y Cymry gan y Saeson.[4]
Yn y 15g cafwyd gwrthryfel Owain Glyndŵr, ond ni lwyddodd i ailsefydlu teyrnas annibynnol ond am gyfnod byr. Yn ddiweddarach yn y ganrif honno cafwyd Rhyfeloedd y Rhosynnau yn Lloegr a effeithiodd yn fawr ar Gymru. Yn 1485 ddaeth Harri Tudur i'r orsedd ar ôl curo Rhisiart III ym Mrwydr Maes Bosworth a dechreuodd cyfnod y Tuduriaid.

Cyfnod y Tuduriaid
 
  Prif erthygl: Cyfnod y Tuduriaid yng Nghymru
      Harri Tudur, y cyntaf o frenhinlin y Tuduriaid
Dechreuodd y cyfnod hwn gyda theyrnasiad Harri Tudur ar goron Lloegr ar ôl iddo ennill Brwydr Bosworth yn 1485, a daeth i ben gyda marwolaeth Elisabeth I yn 1603 a hithau yn ddi-blant.
Roedd yn gyfnod cythryblus yn grefyddol gyda Harri VIII o Loegr yn cweryla gyda'r Pab a sefydlu Eglwys Loegr. Fel adwaith yn erbyn y Diwygiad Protestannaidd cafwyd cyfnod o geisio adfer y ffydd Gatholig gan y frenhines Mari I o Loegr. Ceisiodd ei olynydd Elisabeth I ddilyn polisi cymedrol o oddefgarwch ar y dechrau ond cododd to o Gatholigion milwriaethus. Dyma gyfnod y Gwrthddiwygiad Catholig, cyfnod o erlid pobl fel Rhisiart Gwyn a William Davies. Roedd Owen Lewis, Gruffydd Robert a Morys Clynnog ymysg Cymry Pabyddol eraill y cyfnod. Bu erlid ar y Piwritaniaid hefyd, ar bobl fel John Penry.
Dyma gyfnod Deddf Uno 1536 a hefyd diddymu'r mynachlogydd a chyfieithu'r Beibl cyfan i'r Gymraeg am y tro cyntaf.

Yr Ail Ganrif ar Bymtheg
 
  Prif erthygl: Yr Ail Ganrif ar Bymtheg yng Nghymru
Roedd yr 19g yn gyfnod a ddominyddwyd gan dwf Protestaniaeth a'i henwadau a'r ymrafael am rym gwladol rhwng y frenhiniaeth a'r senedd yn Lloegr a arweiniodd at y Rhyfel Cartref.

Y Ddeunawfed Ganrif
 
  Prif erthygl: Y Ddeunawfed Ganrif yng Nghymru
Roedd y 18g yn gyfnod o barhad o rai agweddau ar fywyd crefyddol a chymdeithasol y ganrif flaenorol ac, ar yr un pryd, yn gyfnod o newidiadau mawr yn y wlad, yn arbennig yn ail hanner y ganrif a osododd Cymru ar lwybr newydd gyda diwydiant yn tyfu'n gyflym a phoblogaeth y trefi'n dechrau cynyddu.

      "Yr Wyddfa o Lyn Nantlle", dyfrlliw gan Richard Wilson
Dyma'r ganrif pan oedd y Diwygiad Methodistaidd mewn bri gyda phobl fel Howel Harris, William Williams Pantycelyn a Daniel Rowland yn arwain trwy deithio'r wlad i bregethu o flaen tyrfaoedd mawr. Cafwyd hefyd ysgolion cylchynol Griffith Jones yn y ganrif hon a dyma gyfnod gwaith Y Gymdeithas er Taenu Gwybodaeth Gristnogol (SPCK), yn ogystal. Erbyn diwedd y ganrif roedd canran sylweddol o'r boblogaeth yn Anghydffurfwyr o ryw fath, ond arhosai nifer yn ffyddlon i'r Eglwys yn ogystal.
Roedd y mwyafrif mawr o'r boblogaeth yn Gymry uniaith Gymraeg o hyd a'r rhan fwyaf yn byw mewn pentrefi a threfi bach cefn gwlad. Roedd caneuon gwerin a barddoniaeth rydd yn boblogaidd, yn arbennig adeg y gwyliau mawr fel y Gwyliau Mabsant. Yn ail hanner y ganrif roedd yr anterliwt ar ei hanterth a phobl yn tyrru i'r llwyfan yn y ffeiriau, yn arbennig yn siroedd y gogledd-ddwyrain. Cafwyd tyfiant mawr yn y nifer o lyfrau a gyhoeddwyd ynghyd â gwawr newyddiaduriaeth yng Nghymru gydag ymddangosiad y cylchgronau cyntaf. Gosodwyd sylfeini'r Eisteddfod Genedlaethol gyda gwaith y Gwyneddigion yn Llundain ac yn y cylchoedd llenyddol cafwyd math o Ddadeni gyda bri ar bopeth Celtaidd ac ail-ddarganfod meistri'r gorffennol fel Dafydd ap Gwilym a'r Gogynfeirdd diolch i waith Goronwy Owen, Ieuan Fardd a Morrisiaid Môn. Dechreuodd teithwyr ymweld â'r wlad a gwerthfawrogi ei thirwedd "gwyllt" - y twristiaid cyntaf - ac ymledodd dylanwad y Mudiad Rhamantaidd ar lenorion ac artistiaid y wlad, fel yr arlunydd enwog Richard Wilson.

Y Bedwaredd Ganrif ar Bymtheg
 
  Prif erthygl: Y Bedwaredd Ganrif ar Bymtheg yng Nghymru
Roedd y 19g yn gyfnod o newid mawr ym mywyd y wlad. Dyma ganrif y Siartwyr a Dic Penderyn, Brad y Llyfrau Gleision a Helyntion Beca.
Cafwyd newidiadau ym myd amaethyddiaeth yn ystod ail hanner y ddeunawfed ganrif, yn arbennig gyda dechrau cau'r tiroedd comin. Erbyn y 19g roedd y tirfeddianwyr yn codi rhenti mwy ar eu tenantiaid, ac roedd gwasgfa ar y ffermwyr oedd yn berchen eu tir i werthu i'r tirfeddianwyr mawr. Roedd cau'r tir comin yn amddifadu'r ffermwyr o dir pori hanfodol. Mewn gair roedd tlodi dybryd yng nghefn gwlad, a hynny pan oedd yna gynnydd yn y boblogaeth.

      Agorwyd Pont y Borth ar Afon Menai yn 1826 (plât Tsieineaidd o'r 1840au)
Roedd y Chwyldro Diwydiannol yng Nghymru ar gynnydd a thirwedd Cymru'n newid. Roedd gwaith ar gael mewn trefi fel Bersham a Brymbo yn y Gogledd-ddwyrain ac yng nghymoedd a threfi De Cymru. Roedd angen cynhyrchu haearn i adeiladu'r peiriannau newydd oedd yn cael eu hadeiladu. Roedd gan Gymru ddigon o haearn a glo hefyd i'r ffwrneisi i weithio'r haearn hwnnw. Felly roedd poblogaeth Cymru ar gerdded o'r ardaloedd gwledig i'r trefi diwydiannol fel Merthyr Tudful a'r Rhondda a oedd yn tyfu yn gyflym. Yn y gogledd agorwyd nifer o chwareli llechi ac ithfaen, mawr a bychain, a thyfodd canolfannau fel Bethesda, Llanberis a Blaenau Ffestiniog yn drefi prysur a ddeuai i chwarae rhan bwysig yn hanes economaidd a diwylliannol y genedl.
Fel bod y bobl yn gallu tramwyo ac i gludo'r glo a'r haearn roedd angen adeiladu ffyrdd, camlesi a rheilffyrdd.
Y 19g oedd canrif fawr y wasg yng Nghymru. Cyhoeddwyd nifer fawr o gylchgronau a phapurau newydd a daeth mwy o lyfrau Cymraeg allan nag erioed.

Yr Ugeinfed Ganrif
 
  Prif erthygl: Yr Ugeinfed Ganrif yng Nghymru
Gellid dadlau bod yr 20g yng Nghymru yn gyfnod a welodd fwy o newid yn y wlad nag yn ystod unrhyw gyfnod arall yn ei hanes.

      Ffurfiwyd Plaid Cymru ym 1925
Yn chwarter cyntaf y ganrif roedd Cymru yn wlad Ryddfrydol o ran ei gwleidyddiaeth. Ond y Blaid Lafur oedd y blaid rymusaf yng Nghymru o'r 1930au ymlaen. Ffurfiwyd Plaid Cymru a Chymdeithas yr Iaith a sefydlwyd Y Swyddfa Gymreig yn 1964 a Chynulliad Cenedlaethol Cymru ym 1999.
Erbyn 1911 roedd 86,000 o bobl yn gweithio ar y rheilffyrdd ac yn nociau mawr y De. Ddechrau'r 20g disodlwyd haearn gan ddur fel y prif fetel a oedd yn cael ei allforio o'r wlad. Roedd 3,700 yn gweithio mewn gwaith copr yn 1911 ac 21,000 mewn gwaith tun. Roeddent yn cynhyrchu 848,000 tunnell o blât tin mewn blwyddyn. Cynhyrchwyd 56.8 miliwn tunnell o lo yn 1913. Roedd Cymru yn allforio traean o holl allforion glo'r byd gan gyflogi 250,000 o ddynion ym meysydd glo'r de a'r gogledd-ddwyrain. Ond cafwyd dirywiad difrifol mewn nifer o'r diwydiannau traddodiadol ar ôl yr Ail Ryfel Byd, yn arbennig yn y meysydd glo. Ym 1913 cyflogid yn y gwaith glo 232,000 o ddynion, ond erbyn 1960 dim ond 106,000 a gyflogid a syrthiasai'r nifer i 30,000 yn unig erbyn 1979. Nid oedd ond un pwll glo ar ôl yng Nghymru erbyn y 1990au. Roedd dirywiad tebyg yn y diwydiant dur ac economi Cymru yn gyffredinol. Roedd Cymru fel nifer o wledydd eraill y gorllewin yn dod yn fwyfwy ddibynnol ar y sector gwasanaeth. Un o ganlyniadau'r dirywiad yn y diwydiant glo ac o ganlyniad esgeuluso diogelwch y tipiau glo oedd trychineb Aberfan, pan lyncwyd ysgol gyfan gan lithriad gwastraff glo gan ladd 144 o blant ac athrawon.
Yn 1911 roedd gan Gymru boblogaeth o 2,400,000 gyda bron i 1,000,000 yn siarad Cymraeg. Dyma'r nifer fwyaf erioed ond eisoes roedd y Cymry Cymraeg yn lleiafrif. Ond lladdwyd nifer o Gymry ifainc yn y Rhyfel Byd Cyntaf, llawer ohonyn nhw'n siaradwyr Cymraeg. Cafodd Dirwasgiad Mawr y 1930au ei effaith hefyd a chredir fod tua 450,000 o bobl wedi ymfudo o'r wlad rhwng 1921 a 1939.
Ar droad y ganrif roedd tua hanner poblogaeth Cymru yn aelodau o eglwys. Yn 1900 roedd gan y Methodistiaid Calfinaidd 158,111 o aelodau, yr Annibynwyr 144,000 a'r Bedyddwyr 106,000. Erbyn diwedd y ganrif darlun o ddirywiad enbyd a geir.

21ain ganrif
 
  Prif erthygl: Yr Unfed Ganrif ar Hugain yng Nghymru
      Adeilad y Senedd yng Nghaerdydd, cartref newydd Cynulliad Cenedlaethol Cymru.
Dangosodd Cyfrifiad 2001 gynydd yn y nifer o siaradwyr Cymraeg, gyda 21% o'r boblogaeth 3 oed a hŷn yn gallu'r iaith, i gymharu â 18.7% ym 1991 ac 19.0% ym 1981. Mae hyn yn gyferbyniad i'r patrwm o leihad yn nifer y siaradwyr trwy gydol yr 20g.[5]
Bu nifer o ddatblygiadau o nod yn y brifddinas wedi agoriad Stadiwm y Mileniwm ym 1999,[6] megis agoriad Canolfan y Mileniwm ym 2004 fel canolfan ar gyfer cynnal digwyddiadau diwylliannol.[7]

Datganoli Cymru
 
  Prif erthygl: Datganoli Cymru
Sefydlwyd Cynulliad Cenedlaethol Cymru (Cynulliad Cenedlaethol Cymru) ym 1999 (o dan Ddeddf Llywodraeth Cymru 1998) gyda’r pŵer i bennu sut y caiff cyllideb llywodraeth ganolog Cymru ei gwario a’i gweinyddu, er bod Senedd y DU wedi cadw’r hawl i osod terfynau ar ei bwerau.[8] Mae llywodraethau’r Deyrnas Unedig a Chymru bron yn ddieithriad yn diffinio Cymru fel gwlad.[9][10]
Dywed Llywodraeth Cymru: "Nid yw Cymru yn Dywysogaeth. Er ein bod wedi ein huno â Lloegr gan dir, a'n bod yn rhan o Brydain Fawr, mae Cymru yn wlad yn ei rhinwedd ei hun."[11]
Cwblhawyd adeilad newydd Senedd Cynulliad Cenedlaethol Cymru ym mis Chwefror 2006 ac agorwyd yn swyddogol ar Ddydd Gŵyl Dewi'r un flwyddyn.[7]
Mae Deddf Llywodraeth Cymru 2006 (p 32) yn Ddeddf gan Senedd y Deyrnas Unedig a ddiwygiodd Gynulliad Cenedlaethol Cymru ac sy’n caniatáu i bwerau pellach gael eu rhoi iddo’n haws. Mae’r Ddeddf yn creu system lywodraethu gyda gweithrediaeth ar wahân wedi’i thynnu o’r ddeddfwrfa ac yn atebol iddi.[12]
Yn dilyn refferendwm llwyddiannus yn 2011 ar ymestyn pwerau deddfu’r Cynulliad Cenedlaethol mae bellach yn gallu gwneud deddfau, a elwir yn Ddeddfau’r Cynulliad, ar bob mater mewn meysydd pwnc datganoledig, heb fod angen cytundeb Senedd y DU.[13]
Yn refferendwm 2016, pleidleisiodd Cymru o blaid gadael yr Undeb Ewropeaidd, er i wahaniaethau demograffig ddod i’r amlwg. Yn ôl Danny Dorling, athro daearyddiaeth ym Mhrifysgol Rhydychen, “Os edrychwch chi ar yr ardaloedd mwy gwirioneddol Gymreig, yn enwedig y rhai Cymraeg eu hiaith, doedden nhw ddim eisiau gadael yr UE.”[14]
Ar ôl Deddf Senedd ac Etholiadau (Cymru) 2020, ailenwyd y Cynulliad Cenedlaethol yn "Senedd Cymru" (yn Gymraeg) ac yn "Senedd Cymru" (yn Saesneg) (y cyfeirir ati hefyd gyda'i gilydd fel y "Senedd"), a ystyriwyd fel adlewyrchiad gwell o bwerau deddfwriaethol ehangach y corff.[15]
Yn 2016, lansiwyd YesCymru. Ymgyrch anwleidyddol dros Gymru annibynnol a gynhaliodd ei rali gyntaf yng Nghaerdydd yn 2019.[16] Dangosodd arolwg barn ym mis Mawrth 2021, record o 39% o gefnogaeth i annibyniaeth i Gymru pan nad yw eithrio yn gwybod.[17]

Cyfeiriadau

Llyfryddiaeth
Ceir llyfryddiaethau mwy penodol yn yr erthyglau ar y cyfnodau unigol. Rhoddir yma ddetholiad o lyfrau ar hanes Cymru yn gyffredinol neu am gyfnodau sylweddol, e.e. Cymru yn y Cyfnod Modern.

 John Davies, Hanes Cymru

 
  
 Mae gan Wicilyfrau gwerslyfr neu lawlyfr ar y pwnc yma:
 Hanes Cymru


   v
t
e
 Cymru 
   v
t
e
 Hanes
 
 Cynhanes

Cyfnod y Rhufeiniaid

Oes y Seintiau

Yr Oesoedd Canol Cynnar

Oes y Tywysogion

Yr Oesoedd Canol Diweddar

Cyfnod y Tuduriaid

Yr 17g

Y 18g

Y 19g

Yr 20g

Yr 21g


 
   v
t
e
   Daearyddiaeth a'r amgylchedd

Cyffredinol 
 Daeareg

Hinsawdd


Rhanbarthau a lleoliadau 
 Cantrefi a chymydau

Cymunedau

Rhanbarthau

Siroedd a dinasoedd

Trefi


Tirffurfiau 
 Afonydd

Llynnoedd

Moroedd, baeau a phentiroedd

Mynyddoedd

Ynysoedd


Bywyd gwyllt 
 Ffawna
 Adar


Fflora


 
   v
t
e
   Gwleidyddiaeth 
Economi

 Gwleidyddiaeth 
 Cenedlaetholdeb

Etholiadau

Heddychaeth

Pleidiau gwleidyddol


 Llywodraeth 
 Senedd Cymru
 Y gyfraith

Etholaethau


Llywodraeth Cymru
 Prif Weinidog Cymru


Llywodraeth leol

Senedd y Deyrnas Unedig
 Etholaethau


Llywodraeth y Deyrnas Unedig
 Prif Weinidog

Swyddfa Cymru

Ysgrifennydd Gwladol



Economi 
 Banciau

Cwmnïau

Diwydiant
 Amaeth

Copr

Glo

Gweithgynhyrchu

Gwlân

Llechi

Pysgota


Hanes economaidd

Twristiaeth


 
   v
t
e
   Demograffeg 
Cymdeithas 
Diwylliant

Demograffeg 
 Crefydd
 Cristnogaeth

Iddewiaeth

Islam


Grwpiau ethnig
 Cymry

Asiaid

Duon


Hanes demograffig

Ieithoedd
 Cymraeg

Y Fro Gymraeg

Saesneg Gymreig



Cymdeithas 
 Addysg

Cludiant

Gofal iechyd
 GIG

Ysbytai


Gwyddoniaeth a thechnoleg

LHDT

Rhywioldeb

Trosedd


Diwylliant 
 Cerddoriaeth
 Cymraeg

Gwerin


Coginiaeth

Y cyfryngau
 Radio

Teledu


Chwaraeon
 Meysydd

Pêl-droed

Rygbi'r undeb

Pêl-fâs


Eisteddfod

Ffasiwn

Ffilm

Hiraeth

Llên
 Llên gwerin Cymru

Llenyddiaeth Gymraeg

Llenyddiaeth Saesneg Cymru


Symbolau cenedlaethol
 Baneri

Cenhinen

Cenhinen Bedr

Dewi Sant

Y Ddraig Goch

Y faner genedlaethol

"Hen Wlad fy Nhadau"



 
 Rhestr pynciau'n ymwneud â Chymru · Categori

  gw • sg • goHanes Ewrop 

Gwladwriaethau sofranaidd  
Albania · Yr Almaen · Andorra · Awstria · Belarws · Gwlad Belg · Bosnia-Hertsegofina · Bwlgaria · Croatia · Denmarc · Y Deyrnas Unedig (Yr Alban • Cymru • Gogledd Iwerddon • Lloegr) · Yr Eidal · Estonia · Y Ffindir · Ffrainc · Gwlad Groeg · Hwngari · Gwlad yr Iâ · Yr Iseldiroedd · Gweriniaeth Iwerddon · Latfia · Liechtenstein · Lithwania · Lwcsembwrg · Gweriniaeth Macedonia · Malta · Moldofa · Monaco · Montenegro · Norwy · Portiwgal · Gwlad Pwyl · Rwmania · San Marino · Sbaen · Serbia · Slofacia · Slofenia · Sweden · Y Swistir · Y Weriniaeth Tsiec · Wcráin



Gwladwriaethau trawsgyfandirol  
Armenia1 · Aserbaijan2 · Cyprus1 · Georgia2 · Casachstan3 · Rwsia3 · Twrci3



Tiriogaethau dibynnol, ardaloedd ymreolaethol, a thiriogaethau eraill  
Abkhazia 2 · Ajaria1 · Akrotiri a Dhekelia · Åland · Azores · Crimea · De Ossetia 2 · Føroyar · Gagauzia · Gibraltar · Gogledd Cyprus1 · Jan Mayen · Jersey · Kosovo · Madeira4 · Nagorno-Karabakh1 · Nakhchivan1 · Svalbard · Transnistria · Ynys y Garn · Yr Ynys Las5 · Ynys Manaw



Dynodir gwlad anghydnabyddedig neu a gydnabyddir yn rhannol gan lythrennau italig. 1 Yn Ne Orllewin Asia yn gyfan gwbwl.  2 Yn rhannol neu'n gyfan gwbwl yn Asia, yn dibynnu ar ddiffiniadau'r ffiniau rhwng Ewrop ac Asia.  3 Gyda'r mwyafrif o'i thir yn Asia.  4 Ar Blât Affrica.  5 Ar Blât Gogledd America.






