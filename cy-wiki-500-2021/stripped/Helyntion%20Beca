  Digriflun o'r London Illustrated News yn dangos Merched Beca yn ymosod ar dollborth
Data cyffredinol
Enghraifft o'r canlynolterfysg 
Cysylltir gydaThomas Rees (Twm Carnabwth), Edward Compton Lloyd Hall, Hugh Williams 
Dechreuwyd13 Mai 1839 
Daeth i ben1843 
Gwladwriaethy Deyrnas Unedig 
RhanbarthCymru 

      Nofel gan T. Llew Jones: Cri'r Dylluan (2005) am helyntion Beca
      Disgriflun a gyhoeddwyd yn y cylchgrawn Punch yn 1843
      Llythyr gan Beca at y Cwnstabliaid (heddweision) a cheidwaid y tollbyrth,  Sir Gaerfyrddin ym 1842
      Adroddiad yn Y Cenhadwr Americanaidd (gol. Robert Everett yn sôn Rebecayddion ger Delaware, Ohio yn 1843.
Gwrthryfel gwerinol yn erbyn y tollau drud a godwyd am deithio ar hyd y ffyrdd tyrpeg oedd Helyntion Beca (hefyd Becca), a barodd o 1839 hyd 1844.
Trawodd Beca gyntaf yn Efail-wen ar yr 13 Mai 1839, gan falu'r dollborth yno. Fe wnaethpwyd yr un peth eto ar y 6 Mehefin yn yr un flwyddyn ac eto ar yr 17 Gorffennaf. Llosgwyd y tolldy y tro hwn. Ni ddarganfuwyd pwy oedd y troeseddwyr, gan fod y rhai a gymerodd ran yn y weithred wedi duo eu gwynebau a gwisgo dillad menywod. Gwelwyd dryllio dros gant o dollbyrth rhwng Ionawr 1843 a gwanwyn 1844 ledled de-orllewin Cymru. Roedd yr helynt ar ei waethaf yn Sir Gaerfyrddin. Ar 6 Gorffennaf 1843, ymosododd oddeutu 200 o bobl ar Dollborth Bolgoed, Pontarddulais. Yr arweinydd oedd crydd lleol o'r enw Daniel Lewis. Fe'i bradychwyd, yn ôl yr hanes, ond osgôdd gael ei ddanfon i Awstralia gan nad oedd tystion ar gael. Ceir cofeb i'r digwyddiad yn y lleoliad hwn - ger Tafarn y Ffynnon heddiw.[1]
'Merched Rebecca' neu 'Ferched Beca' oedd yr enw arnyn nhw ar lafar. Dywed rhai iddynt gael eu henwi ar ôl benyw o'r enw Rebecca, neu Beca Fawr, a oedd wedi benthyg ei dillad iddyn nhw. Y farn gyffredinol, fodd bynnag, yw fod yr enw Rebecca yn gyfeiriad at adnod yn Llyfr Genesis, Pennod 24, adnod 60, lle sonir am fam a brawd Rebecca yn gadael iddi fynd gydag Eliezer, gwas Abraham, i briodi Isaac:

 Ac a fendithiasant Rebbecah, ac a ddywedasant wrthi, "Ein chwaer wyt, bydd di fil fyrddiwn; ac etifedded dy had borth ei gaseion."

Yn aml, roedd ganddi "ddwy chwaer" o'i phopty yn cyd-arwain ac a elwid yn Siarlot a Neli.[2]

  Cynnwys
 
1 Ymosodiadau ar dollbyrth

2 Achosion

3 Pwy oedd Rebeca?

4 Cyfeiriadau

5 Llyfryddiaeth




Ymosodiadau ar dollbyrth
Ymhlith yr ymosodiadau mwyaf nodedig y mae:

 13 Mai 1839: tollborth Efail-wen; ceir cofeb

6 Mehefin: tollborth a tholldy Efail-wen

1842: Tollborth Sanclêr; ceir cerflun yn Heol y Pentref

6 Gorffennaf 1843: tollborth Bolgoed, Pontarddulais; ceir cofeb ger Tafarn y Ffynnon

20 Gorffennaf 1843: tollborth Rhyd Y Pandy, Treforys

7 Medi 1843: tollborth yr Hendy,

Cafwyd ymosodiadau hefyd yn: Hwlffordd, Caerfyrddin, Aberteifi, Cydweli, y Tymbl, Llandysul, Pontarddulais a Chastellnewydd Emlyn.

Achosion
Un o achosion amlwg yr helynt oedd y tollbyrth eu hun, a weinyddwyd gan y Cwmnïau Tyrpeg newydd. Roedd rhaid talu  tollau i deithio ar hyd y prif-ffyrdd hyn. Roedd ddeuddeg Cwmni yn sir Gaerfyrddin, pedwar yn Sir Benfro, dau yn Sir Aberteifi, dau yn Sir Faesyfed, un yn Sir Frycheiniog a deuddeg ym Morgannwg. Roedd nifer uchel y Cwmnïau Tyrpeg yn yr ardal yn golygu bod rhaid i deithwyr talu tollau sawl gwaith (am bob cwmni) i'w gymharu â Lloegr lle nad oedd y fath gor-gystadlu. Roedd rhaid i'r ffermwyr talu tollau wrth yrru'u gwartheg a theithio yn ôl ac ymlaen i'r marchnad, ac wrth ôl calch fel gwrtaith am y pridd.
Nid yr unig rheswm dros yr helyntion oedd y tollbyrth. Roedd tuedd gan werin De-Orllewin Cymru i weinyddu cyfraith eu hun trwy'r traddodiad o'r Ceffyl Pren. Dyma arfer o siomi yn gyhoeddus aelod o'r gymuned sydd wedi achosi cerydd i'w gymdogion neu gydweithio a'r awdurdodau. Byddai dorf gyda'i wynebau'n ddu (fel wnaeth Merched Beca) yn cario ffigur ceffyl at ddrws y person, neu ffigur yn ei ddynwared (a weithiau llosgwyd), neu hyd yn oed y berson ei hun, gan ei fychanu. Yn ôl yr hanesydd David Williams, "ymestyniad o arfer y Ceffyl Pren" oedd terfysgoedd Beca.[3]
Roedd rhesymau eraill am yr helyntion yn cynnwys:

 Tlodi cyffredinol De-Orllewin Cymru a'i dir gymharol anffrwythlon.

Twf y boblogaeth yn rhoi pwysau ar y bobl wrth i ffermydd fynd yn llai (problem a waethygwyd gan system etifeddiaeth Cymru sydd yn rhannu tir rhwng pob perthynas gwrywaidd).

Y dirwasgiad amaethyddol a ddechreuodd yn 1836 (a ddaeth i ben tua'r un amser a gorffennodd y terfysgoedd) a chyfres o gynaeafau gwael.

Baich y degwm, yr arian a dalwyd i'r Eglwys Anglicanaidd gan y ffermwyr. Achosodd anfodlonrwydd ychwanegol gan y ffaith mai anghydffurfwyr oedd rhan fwyaf o'r werin. Gwaethygwyd y baich gan Ddeddf Cymudiad y Degwm (1836) a seilio faint y degwm ar brisiau cnydau y 7 mlynedd cynt. O ganlyniad roedd yn rhaid i'r werin dalu degwm uchel er bod pris y cnydau'n y farchnad wedi disgyn[4].

Y diffyg cymorth i'r tlawd yn dilyn Deddf Gwella Cyfraith y Tlodion (1834) wnaeth diddymu cymorth allanol a gorfodi i'r tlawd  weithio yn y Tlotai. Ymosododd Merched Beca ar dloty Caerfyrddin yn 1843.[5]

Yr atgasedd at y tirfeddianwyr a'r rhent uchel roeddent yn ei godi. Roedd nifer ohonynt yn landlordiaid absennol, a gwaethygwyd y gwrthdaro gan wahaniaethau mewn iaith (nid oedd y rhan fwyaf yn siarad Cymraeg) a chrefydd Anglicaniaid oedd gan y rhan fwyaf, i'w gymharu a'r werin Anghydffurfiol, Gymraeg). Gweinyddwyd nifer o'r Gwmnïau Tyrpeg atgas gan yr un tirfeddianwyr, a hwy oedd yn Ynadon yn y Llysoedd hefyd! Roedd y gyfraith a'r achosion llys Saesneg (nas deallwyd gan y werin uniaith Cymraeg) yn rheswm arall am yr anfodlonrwydd. Bygythiwyd tirfeddianwyr ymysg parchedigion Anglicanaidd gan lythyron o ferched Beca.

Pwy oedd Rebeca?
Sonnir am Twm Carnabwth (Thomas Rees) fel un o'u harweinwyr. Roedd yn gymeriad cyfeillgar, lliwgar, ac yn ŵr crefyddol iawn. Roedd yn adroddwr pwnc gwych ond hefyd roedd yn enwog fel paffiwr (bocsiwr) mewn ffeiriau yn Sir Benfro, Aberteifi a Chaerfyddin. Cafodd Twm ei gladdu yng Nghapel y Bedyddwyr, Mynachlog Ddu.
Fodd bynnag, mae'n ddigon posib mai trefnwyr yr ymgyrch oedd dau ddyn arbennig: y Bargyfreithiwr Lloyd Hall a'r cyfreithiwr Hugh Williams.[2]
Un arweinydd lleol, o Bontarddulais oedd Daniel Lewis (taid y darlledwr Wynford Vaughan Thomas), er iddo osgoi cael ei erlyn gan nad oedd tystion.

Cyfeiriadau

Llyfryddiaeth
 David Williams, The Rebecca Riots: A Study in Agrarian Discontent (Caerdydd: Gwasg Prifysgol Cymru, 1955); wedi'i gyfieithu gan Beryl Thomas fel Helyntion Beca (Caerdydd: Gwasg Prifysgol Cymru, 1974)

Pat Molloy And they blessed Rebecca: An Account of the Welsh Toll-Gate Riots, 1839–1844 (Gwasg Gomer, 1983)

Catrin Stevens, Terfysg Beca / The Rebecca Riots (Gwasg Gomer, 2007)





