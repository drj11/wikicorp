 


Llydaw (Breizh, Bretagne, Bertaèyn)



   (Baner Llydaw)



  



Hysbysrwydd
Rhanbarth Llydaw
Liger-Iwerydd



Prifddinas:
Roazhon (Rennes)
Naoned (Nantes)



Poblogaeth (2003): 
Dwysedd:


2 972 700 o drigolion 
107 trigolyn/km²


1 134 266 o drigolion 
166 trigolyn/km²




Ardal:
27 208 km²
6 815 km²



Llywydd y Cyngor:

Pierrick Massiot

Patrick Mareschal



Départements:

Arfordir Armor (22) Îl-a-Gwilun (35) Môr Bychan (56) Penn-ar-Bed (29)

Liger-Iwerydd (44)


 Erthygl am y wlad Geltaidd hanesyddol yw hon. Gweler hefyd Llydaw (gwahaniaethu).

Un o'r gwledydd Celtaidd, yng ngweriniaeth Ffrainc, yw Llydaw (Cymraeg Canol: Brytaen neu Brytaen Fechan[1], Llydaweg: Breizh, Ffrangeg: Bretagne). Fe'i rhannwyd rhwng dau ranbarth (régions) Ffrengig gan lywodraeth Vichy Ffrainc yn ystod yr Ail Ryfel Byd, sef Bretagne a Rhanbarth Bröydd Liger. Yn y naill mae pedwar o bum département y wlad; yn y llall y mae'r pumed (Liger-Iwerydd), ynghyd â départements sy'n rhan o fröydd eraill.
Yn 2006, amcangyfrifwyd fod poblogaeth Llydaw tua 4.3 miliwn. O'r rhain, roedd 72% yn byw yn region Bretagne, a 28% yn Pays-de-la-Loire.

  Cynnwys
 
1 Hanes

2 Ieithoedd

3 Rhaniadau gweinyddol
 
3.1 Esgobaethau

3.2 Bröydd Traddodiadol

3.3 Cymunedau




4 "Gwell marw na ’maeddu"

5 Gweler hefyd

6 Dolen allanol




Hanes
 
  Prif erthygl: Hanes Llydaw
      Map Llydaw
Fe ymfudodd llawer o hynafiaid y Llydawyr o Ynys Prydain ar ôl ymadawiad y Rhufeiniaid yn 410 OC. Yn y 9g, llwyddodd Nevenoe (Nominoë yn Ffrangeg) i gyfuno Llydaw oll yn un deyrnas.
Yn Rhyfel Olyniaeth Llydaw, rhwng 1341 a 1364, fe wrthdarodd cynghreiriaid Lloegr yn erbyn cynghreiriaid Ffrainc. Daeth annibyniaeth Llydaw i ben drwy Ddeddf Uno yn 1532, ond roedd ganddi rywfaint o ymreolaeth o fewn Ffrainc tan 1789. Gwrthryfel yn erbyn y Chwyldro Ffrengig oedd y Chouanted, a gefnogwyd gan y Saeson.
Datblygodd y mudiad cenedlaethol Llydewig modern tua diwedd y 19g a dechrau'r 20g. Pan orchfygwyd Ffrainc gan Yr Almaen yn yr Ail Ryfel Byd, bu hollt yn y mudiad cenedlaethol. Roedd rhai cenedlaetholwyr Llydewig yn amlwg yn y gwrthwynebiad arfog i'r Almaenwyr, tra dewisodd eraill megis Roparz Hemon gydweithio gyda'r Almaenwyr yn y gobaith o ennill annibyniaeth i Lydaw. Yn Rhagfyr 1943 llofruddiwyd yr Abbé Perrot, cenedlaetholwr Llydewig amlwg, gan aelod o'r Blaid Gomiwnyddol, gan haeru ei fod yn cydweithio a'r Almaenwyr. Bu raid i eraill, megis Roparz Hemon, ffoi i Gymru ac Iwerddon ar ddiwedd y rhyfel, a bu adwaith cryf ar ran llywodraeth Ffrainc yn erbyn yr iaith a'r diwylliant Llydewig.
Pan rannwyd Ffrainc yn ranbarthau gweinyddol, nid oedd rhanbarth Bretagne ond yn cynnwys pedwar allan o'r pum departement oedd yn draddodiadol yn rhan o Lydaw. Ni chynhwyswyd y rhan a neilltuwyd i Loire-Atlantique sy'n cynnwys Nantes, un o ddwy brifddinas draddodiadol Llydaw ac mae hyn yn destyn llosg gan Lydawyr. Bu dirywiad mawr yn sefyllfa'r iaith Lydaweg ers 1945; mewn llawer o ardaloedd lle siaredid yr iaith, magwyd plant a aned ers y cyfnod yma yn uniaith Ffrangeg. Ers y 1970au bu cynnydd mewn diddordeb yn iaith a diwylliant Llydaw, yn arbennig mewn cerddoriaeth, lle daeth Alan Stivell yn adnabyddus. Mae mudiad Diwan wedi sefydlu ysgolion Llydaweg i geisio achub yr iaith.
Ar 16 Mawrth 1978, drylliwyd y llong Amoco Cadiz gerllaw porthladd bychan Portsall yn Ploudalmézeau. Collwyd rhan helaeth o'i llwyth o olew i'r môr, gan greu difrod mawr ar draethau gogleddol Llydaw.

Ieithoedd
Ers yr Oesoedd Canol, mae gwahanaeth eglur rhwng Llydaw Isel (yn yr Orllewin: Breizh-Izel neu Goueled-Breizh; Basse-Bretagne) a Llydaw Uchel (yn y Dwyrain: Breizh-Uhel neu Gorre-Breizh; Haute-Bretagne neu Pays Gallo). Mae'r mwyafrif o siaradwyr Llydaweg yn Llydaw Isel, lle mae trefi Kemper (Quimper), Brest, an Oriant (Lorient), a Gwened (Vannes). Yn Llydaw Uchel, fodd bynnag, roedd y werin yn siarad Gallo, ac yma y mae'r ddwy ddinas fawr (Naoned a Roazhon) a llawer o drefi eraill, er enghraifft Sant Maloù (Saint-Malo), Sant Nazer (Saint-Nazaire) a Sant Brïeg (Saint-Brieuc). Mae'r "ffin" ddiwyllianol hon yn ymestyn o Sant Brïeg i dre Gwened.

Rhaniadau gweinyddol
Esgobaethau
Ceir naw "esgobaeth" neu ranbarth hanesyddol yn Llydaw. Dyma'r naw esgobaeth a gynrychiolir gan y llinellau du a gwyn ar faner Llydaw:

      Naw hen esgobaeth Llydaw
 Bro Gerne

Bro Ddol

Bro Leon

Bro Naoned

Bro Roazhon

Bro Sant Brïeg

Bro Sant Maloù

Bro Dreger

Bro Wened

Bröydd Traddodiadol
Rhennir Llydaw yn naw "Bro" traddodiadol sy'n dilyn ffiniau'r esgobaethau:

 Bro-Gerne

Bro-Zol

Bro-Leon

Bro-Naoned

Bro-Roazhon

Bro-Sant-Brieg

Bro Sant-Maloù

Bro-Dreger

Bro-Wened

Cymunedau
 
  Prif erthygl: Rhestr cymunedau Llydaw
O fewn pob department ceir cymuned: kumunioù (Llydaweg) a communes (Ffrangeg) i "gymuned".

"Gwell marw na ’maeddu"
 Au cours d’une chevauchée, le Roi Breton Conan Meriadec apercu dans les roseaux d’une ruisseau aux abords fangeux une forme blanche qui allait et venait.[2]

Wrth i'r Brenin Conan farchogaeth heibio afon fechan un diwrnod cafodd sylw Conan Meriadec, Brenin Llydaw, ei dynnu at anifail bach lliw eira yn yr hesg yn mynd yn ôl a blaen wrth geisio croesi’r afon. Daliodd Conan ei geffyl yn ôl i edrych, ac fe welodd y creadur yn ceisio croesi ar frigyn rhy wan i’w gynnal - ac ymddangosai’n llawn ofn y byddai’n disgyn i’r llaid. Cododd y creadur ei lygaid fel petai’n dyheu am gymorth. “Paham y mae cymaint o ofn ar y creadur” meddai Conan wrth ei gydymaith. “Fy mrenin” meddai, “ermin yw hwn. Ni chafodd ei frifo - mae arno ofn baeddu ei ffwr gwyn difrycheulyd yn y baw wrth groesi’r afonig”. “O Ryfeddod o Burdeb” meddai Meriadec, “mae anrhydedd yn galw arnaf i’w achub a’i amddiffyn”.
Fel petai’r carlwm wedi deall y sgwrs rhwng y ddau ddyn ac wedi gwerthfawrogi daioni Conan, rhedodd ar hyd y gangen yr estynnodd y brenin ato, a fe guddiodd ym mhlygiadau ei fantell. Dan deimlad a chan fwytho’r creadur bach dywedodd Conan “Fel hyn mae hi am fod, well gen ti farw na chael dy ddifwyno” "Kentoc’h mervel eget bezan saotret. O hyn ymlaen dyma fydd arwyddair y Llydäwyr, a thithau ermin fach fydd yr ymgnawdoliad byw ohono".

Gweler hefyd
 Rhestr o adar Llydaw

Théodore Hersart de la Villemarqué

Dolen allanol
 Cymdeithas Cymru-Llydaw Archifwyd 2004-12-27 yn y Peiriant Wayback.

  gw • sg • go Y Gwledydd Celtaidd

  
  Yr Alban

 Cernyw

 Cymru

 Galisia

 Iwerddon

 Llydaw

 Manaw



Gweler hefyd: Y Celtiaid


 
  
 Chwiliwch am Llydaw yn Wiciadur.

 
↑ "Geiriadur Prifysgol Cymru". geiriadur.ac.uk. Cyrchwyd 2021-05-01.


↑ Youenn Kermorgen







