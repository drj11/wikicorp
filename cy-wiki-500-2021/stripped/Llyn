       Lake yn Bariloche (Yr Ariannin)
      Llyn Titicaca, De America, o'r gofod
      Fideo o lynnoedd ac afonydd Cymru a sut y mae deg ohonynt yn cael eu gwarchod gan Gyfoeth Naturiol Cymru.
 Os ydych wedi cyrraedd yma wrth chwilio am y penrhyn a rhanbarth yng ngogledd-orllewin Cymru, gweler Llŷn (gwahaniaethu).

Corff sylweddol o ddŵr sy'n gorwedd mewn pant ar wyneb y tir yw llyn; neu mewn Cymraeg cynnar: llwch sy'n perthyn yn agos i'r gair Gaeleg loch. Fel rheol mae afonydd yn llifo i mewn ac allan o lynnoedd, er eithriadau lle nad oes all-lif na mewnlif iddynt.
Mae'r rhan fwyaf o lynnoedd yn llynnoedd dŵr croyw, ond ceir rhai sy'n llynnoedd dŵr hallt, er enghraifft Great Salt Lake yn Utah, UDA. Mae rhai llynnoedd mawr yn cael eu cyfrif fel moroedd, e.e. Môr Caspia a'r Môr Marw.
Ceir llynnoedd artiffisial hefyd, wedi'u creu gan amlaf er mwyn cael ffynhonnell ddŵr neu ar gyfer cynlluniau trydan hydro. Gan amlaf  (megis Llyn Celyn) fe'i creir trwy godi argae ar afon.

  Cynnwys
 
1 Llynnoedd nodedig
 
1.1 Y llynnoedd mwyaf (arwynebedd) yn ôl cyfandir




2 Llynnoedd yng Nghymru
 
2.1 Aberthu

2.2 Anghenfilod

2.3 Gorlifoedd

2.4 Y Tylwyth Teg




3 Gweler hefyd

4 Cyfeiriadau




Llynnoedd nodedig
 Y llyn mwyaf yn y byd o ran arwynebedd ei wyneb yw Môr Caspia ( 394,299 km²).

Y llyn dyfnaf yw Llyn Baikal yn Siberia, gyda dyfnder o 1,637 m (5,371 tr.); dyma lyn dŵr croyw mwyaf y byd o ran maint ei ddŵr.

Y llyn hynaf yn y byd yw Llyn Baikal, ac yn nesaf iddo Llyn Tanganyika (rhwng Tansanïa, y Congo, Sambia a Bwrwndi).

Y llyn uchaf yn y byd yw pwll dienw ar Ojos del Salado at 6390m,[1] ac mae Pwll Lhagba yn Tibet at 6,368 m yn ail.[2]

Y llyn uchaf yn y byd y gellir ei fordwyo ar longau masnachol yw Llyn Titicaca yn Bolifia at 3,812 m. Mae hefyd y llyn dŵr croyw mwyaf (a'r ail yn gyffredinol) yn Ne America.

Y llyn isaf yn y byd yw'r Môr Marw sy'n ffinio ag Israel, Gwlad Iorddonen a'r Lan Orllewinol at 418 m (1,371 tr) is lefel y môr. Mae hefyd yn un o'r llynnoedd mwyaf hallt yn y byd.

Y llyn dŵr croyw mwyaf o ran arwynebedd, a'r trydydd mwyaf o ran maint, yw Llyn Superior gyda arwynebedd o 82,414 km². Fodd bynnag, mae Llyn Huron a Llyn Michigan yn ffurfio un system hydrolegol gydag arwynebedd o 117,350 km², y cyfeirir ato weithiau fel Llyn Michigan-Huron. Mae'r rhain i gyd yn rhan o'r Llynnoedd Mawr yng Ngogledd America.

Yr ynys fwyaf mewn llyn dŵr croyw yw Ynys Manitoulin yn Llyn Huron, gyda arwynebedd o 2,766 km². Llyn Manitou, ar yr ynys honno, yw'r llyn mwyaf ar ynys mewn llyn dŵr croyw.

Y llyn mwyaf a leolir ar ynys yw Llyn Nettilling ar Ynys Baffin.

Y llyn mwyaf yn y byd sy'n draenio'n naturiol i ddau gyfeiriad yw Llyn Wollaston.

Lleolir Llyn Toba ar ynys Sumatra ar yr hyn sydd efallai'r caldera atgyfodol mwyaf ar y Ddaear.

Y llyn mwyaf o fewn ffiniau unrhyw un ddinas yw Llyn Wanapitei yn ninas Greater Sudbury, Ontario, Canada. Cyn 2001, apn newidiwyd y ffiniau, Llyn Ramsey oedd y mwyaf, hefyd yn Sudbury.

Llyn Enriquillo yn Ngweriniaeth Dominica yw'r unig llyn dŵr hallt yn y byd lle ceir crocodilod.

Y llynnoedd mwyaf (arwynebedd) yn ôl cyfandir
 Affrica - Llyn Victoria-Nyanza, y llyn dŵr croyw ail fwyaf yn y byd. Mae'n un o Lynnoedd Mawr Affrica.

Antarctica - Llyn Vostok (is-rewlifol)

Asia - Môr Caspia, y llyn mwyaf ar y Ddaear

Awstralia - Llyn Eyre

Ewrop - Llyn Ladoga, yn cael ei dilyn gan Llyn Onega, ill dau yng ngogledd-orllewin Rwsia.

Gogledd America - Llyn Superior

De America - Llyn Titicaca (mae Llyn Maracaibo yn Feneswela yn fwy ond nis ystyrir yn wir llyn bellach am fod sianel yn ei gysylltu â'r môr).

Llynnoedd yng Nghymru
Mae llynnoedd yng Nghymru, yn ogystal â'r môr, afonydd a ffynhonnau, yn gyforiog o goelion a straeon gwerin dyfrllyd – lawer ohonynt (efallai?) yn atgof niwlog am hen draddodiadau a defodau cyn-Gristnogol Celtaidd.
Roedd dŵr yn elfen holl bwysig ym mywydau pobol; yn rhan mor hanfodol o drefn natur ac eto'n llawn gwrthgyferbyniadau – yn ffynhonnell bywyd a bwyd, yn iachau a phuro, ond hefyd yn chwalu a lladd. Gallai fod yn dryloyw ac eto'n llawn dirgelwch yn nyfnder anweladwy llyn, môr neu bwll a phan yn llonydd yn adlewyrchu goleuni duw'r haul a llun y sawl a edrychai iddo.

Aberthu
Ystyrid bob corff o ddŵr yn sanctaidd ac yn borth i'r arall-fyd lle preswyliai ac y gellid cysylltu â'r duwiau a bodau goruwchnaturiol eraill. Dyma pam yr aberthid cymaint o drysorau i lynnoedd ar draws y byd Celtaidd. Tystia'r Groegwr Strabo fel y byddai llwyth Celtaidd y Volcae Tectosages yn aberthu ingotiau o aur ac arian i lyn sanctaidd ger Toulouse yn yr ail ganrif CC ac na feiddiai neb amharu â'r safle cyn i'r Rhufeiniaid anwar geisio codi'r cyfoeth o waelod y llyn yn 106CC! Disgrifia Gregory o Tours yn y canol-oesoedd ŵyl baganaidd 3 niwrnod ger llyn Gévaudan yn y Cevennes, ple aberthai'r bobl fwyd, dillad ac anifeiliaid i'r llyn.
Mae'r dystiolaeth archeolegol am aberthu i lynnoedd yn sylweddol. Un o'r safleoedd enwocaf a chyfoethocaf yw La Téne yn y Swisdir lle cafwyd olion llwyfan pren mewn mawnog ar ymyl bae bychan ar lan ddwyreinol Llyn Neuchâtel. Oddiarno, yn y ddwy ganrif CC derbyniodd duw'r llyn gannoedd o dlysau, picellau, cleddyfau a thariannau yn ogystal â chŵn, gwartheg, moch, ceffylau a phobl. Ar sail arbenigrwydd a cheinder yr addurniadau ar lawer o'r ebyrth metal hyn y diffiniwyd teip ag arddull addurniadol un o gyfnodau amlycaf y gelfyddyd glasurol Geltaidd.
Yng Nghymru canfyddwyd celc o waith metel yn Llyn Mawr, Morgannwg, yn dyddio o tua 600CC. Yn eu mysg roedd addurniadau harnais a cherbyd ceffyl; bwyelli, crymannau a dau grochan efydd – rhai ohonynt yn arddull addurniadol Hallstat, oedd yn blaenori'r La Ténne. Yn Llyn Cerrig Bach ym Môn cafwyd casgliad o arfau, crochannau, cadwen gaethweision a darnau o gerbyd rhyfel, oll wedi eu haberthu rhwng yr ail ganrif CC a'r ganrif gynta OC.

Anghenfilod
Parhaodd y cof am aberthu i lynnoedd yn hir iawn ac mae straeon gwerin yn gyffredin am anghenfil sy'n preswylio yn y dyfnder dyfrllyd ac yn mynnu aberth blynyddol – merch ifanc fel arfer – neu bydd yn gadael y llyn ac anrheithio'r wlad oddiamgylch. Ond wrth lwc, fel arfer, fe ddaw arwr i ymladd a difa'r anghenfil ac achub y ferch!
Un o'r enghreifftiau enwocaf o anghenfil o'r fath, pe cai ferch ifanc neu beidio(!), oedd Yr Afanc a cheir sawl fersiwn o'r stori am y peth erchyll hwn. Yn Nyffryn Conwy, lle y llechai mewn pwll mawr yn yr afon Conwy – Llyn yr Afanc ger Betws y Coed – fe achosai orlifoedd enbyd yn y dyffryn bob hyn a hyn. Ond llwyddwyd i'w raffu a'i lusgo gan yr Ychain Bannog i Lyn Cwm Ffynnon yng nghesail y Glyder Fawr, ple na allai wneud cymaint o ddifrod. Yn Llyn Syfaddon ger Aberhonddu, Hu Gadarn a'i haliodd o'r llyn, tra yn Llyn Barfog ger Aberdyfi y Brenin Arthur a'i farch fu'n gyfrifol.
A beth am Tegid – sy'n anghenfil mwy modern, efallai – a driga yn Llyn Tegid? Fel Loch Ness yn yr Alban mae Llyn Tegid angen anghenfil yndoes?!

Gorlifoedd
Priodolir tarddiad amryw o lynoedd i orlif pan esgeuluswyd roi'r caead yn ei ôl ar rhyw ffynnon neu'i gilydd. Enghraifft o hyn yw stori Llyn Llech Owain yn Sir Gaerfyrddin a ffurfwyd pan anghofiodd Owain roi'r llech oedd yn gaead ar ffynnon ar y Mynydd Mawr yn ei hôl. Wrth farchogi ymaith digwyddodd edrych dros ei ysgwydd a gweld bod y ffynnon yn brysur orlifo'r wlad. Onibai iddo lwyddo i garlamu ei geffyl rownd y llyn newydd i'w atal rhag tyfu'n fwy, byddai'r wlad i gyd, a phawb a drigai ynddi, wedi boddi.
Ceir straeon tebyg am Ffynnon Grasi yn gorlifo a chreu Llyn Glasfryn yn Eifionydd; Ffynnon Gywer yn creu Llyn Tegid ac mae fersiwn gynnar o stori Cantre'r Gwaelod yn son am anffawd Mererid, ceidwades y ffynnon, yn methu ail-gaeadu'r ffynnon sanctaidd nes i'r cantref cyfan gael ei foddi.
Os dielid am esgeuluso ail-gaeadu ffynnon ceir hefyd ddial am greulondeb a chamwri. Mae stori Tyno Helig ar lannau'r Fenai, pan glywir y llais ysbrydol yn gwaeddi “Daw dial! Daw dial! Daw dial!” yn rhybudd erchyll y boddir y drwgweithredwr a'i holl ddisgynyddion rhyw ddydd. Cysylltir straeon tebyg hefyd â Llyn Llynclys rhwng Croesoswallt a Llanymynaich, Llyn Syfaddon a Llyn Tegid.

Y Tylwyth Teg
Ond, o'r cyfan, efallai mai straeon am lynnoedd yn breswylfeydd i'r Tylwyth Teg sydd fwyaf adnabyddus ac am fugail yn hudo rhyw Dylwythes Deg i'r tir i'w briodi. Chwedl Llyn y Fan yw'r enghraifft orau o hyn a sut y gosodwyd amodau ar Rhiwallon fyddai'n sicrhau y byddai ei wraig yn aros yn ein byd ni. Pan dorwyd yr amodau aeth y ferch yn ôl i'r llyn gyda'i holl eiddo a'i gwartheg. Ond ni allasi gymeryd eu tri mab, na chwaith yr holl wybodaeth am feddyginiaethau yr oedd wedi ei ddysgu iddynt. Felly, am bod y fferm bellach yn “bancrypt(!)” fe drodd Rhiwallon a'i feibion yn feddygon gan ddod yn adnabyddus o hynny ymlaen fel teulu enwog Meddygon Myddfai.
Ceir straeon tebyg, neu o leia'n cynnwys rhai elfennau o'r stori hon, yn gysylltiedig â Llyn Nelferch neu Lyn y Forwyn ym Morgannwg; a Llynnau Barfog ac Arenig ym Meirionnydd a Llyn y Dywarchen a Llun Dwythwch yn Eryri.
Ond yn ogystal â'r Tylwyth Teg a'u hanifeiliaid yn dod i'n byd ni ceir enghraifft, yn stori Llyn Cwm Llwch ger Aberhonddu am ddrws cyfrin fyddai'n agor bob Calan Mai i wlad y Tylwyth Teg. Gwarchodid y porth a'r llyn gan gorach blïn mewn côt goch, a chawn disgrifiad o breswylfa'r Tylwyth ar ynys hud, fyddai'n anweledig fel arfer – sy'n ein hatgoffa, mewn cyswllt arall, o Ynys y Meirw, Afallon neu Dir Na-nog ym môr y gorllewin. Dyma'r porth i'r arall-fyd yn sicr.

Gweler hefyd
 Rhestr llynnoedd Cymru

Llynnoedd Ewrop

Rhestr o lynnoedd mwyaf y byd

Cyfeiriadau

  Mae'r erthygl hon yn cynnwys testun a sgwennwyd ac a briodolir i Twm Elias ac a uwchlwythwyd ar Wicipedia gan Defnyddiwr:Twm Elias. Cyhoeddwyd y gwaith yn gyntaf yn : Gwyddoniadur Cymru (Gwasg y Brifysgol).
 
 

 
  
 Chwiliwch am llyn yn Wiciadur.

  
 Mae gan Gomin Wikimedia gyfryngau sy'n berthnasol i:
 Lakes






