 Roald Dahl

Ganwyd13 Medi 1916  Caerdydd 
Bedyddiwyd1 Rhagfyr 1916 
Bu farw23 Tachwedd 1990  Rhydychen 
Man preswylLlandaf, Gipsy House 
Dinasyddiaeth Cymru
Alma mater  Ysgol Repton
Ysgol y Gadeirlan, Llandaf 

Galwedigaethsgriptiwr, nofelydd, hunangofiannydd, bardd, awdur storiau byrion, awdur plant, ysgrifennwr, awdur, peilot awyren ymladd 
Adnabyddus amCharlie a'r Ffatri Siocled, James a'r Eirinen Wlanog Enfawr, Mr Cadno Campus, Matilda, Y Gwrachod, Y Twits, Yr CMM, The Gremlins, Moddion Rhyfeddol George, Danny Pencampwr y Byd, Charlie a'r Esgynnydd Mawr Gwydr, Esio Trot 
Arddullllenyddiaeth arswyd, cyfriniaeth, ffantasi, llenyddiaeth plant 
Prif ddylanwadCharles Dickens, C. S. Forester, Frederick Marryat, Jonas Lie, Rudyard Kipling, William Makepeace Thackeray 
TadHarald Dahl 
MamSofie Magdalene Hesselberg 
PriodPatricia Neal, Felicity Dahl 
PlantOlivia Dahl, Tessa Dahl, Ophelia Dahl, Theo Dahl, Lucy Dahl 
Gwobr/auGwobr Edgar, Gwobr Zilveren Griffel, Gwobr World Fantasy am Gyflawniad Gydol Oes, Gwobr y Brwsh Paent Aur, Gwobr Rheithgor Plant Iseldiroedd, Gwobr Rheithgor Plant Iseldiroedd, Gwobr Rheithgor Plant Iseldiroedd, Gwobr Edgar 
Gwefanhttps://www.roalddahl.com 

Awdur nofelau a storïau byrion, a llenor ar gyfer y sgrîn, oedd Roald Dahl (13 Medi 1916 – 23 Tachwedd 1990). Roedd yn enwog am ei lyfrau ar gyfer oedolion a phlant yn arbennig.
Ganwyd Dahl yng Nghaerdydd i rieni o Norwy. Cafodd ei fedyddio yn yr Eglwys Norwyaidd ar bwys y bae. Mynychodd Ysgol Repton yn Swydd Derby. Daeth i'r amlwg yn yr 1940au gyda'i waith ar gyfer plant ac oedolion, gan ddod yn un o awduron mwyaf poblogaidd y byd. Datgelwyd yn y 1980au iddo weithio fel asiant ddirgel ar gyfer MI6, Gwasanaeth Cudd-wybodaeth Dramor Prydain, gan wasanaethu yn yr Unol Daleithiau i hybu diddordebau Prydain ac i frwydro yn erbyn symudiad "America First", gan weithio ar y cyd gydag Ian Fleming a David Ogilvy.[1] Mae'r llyfr hanesyddol, "The Irregulars" gan Jennet Conant (2008, Simon and Schuster) yn disgrifio'r cyfnod hwn ym mywyd Dahl a'i gyfoeswyr.
Mae llyfrau mwyaf poblogaidd Dahl yn cynnwys Y Twits, Charlie a'r Ffatri Siocled, James a'r Eirinen Wlanog Enfawr, Matilda, Y Gwrachod a Yr CMM.

  Cynnwys
 
1 Teulu

2 Diwrnod Roald Dahl

3 Llenyddiaeth plant

4 Rhestr Gweithiau
 
4.1 Gwaith ar gyfer Plant
 
4.1.1 Storïau Plant

4.1.2 Barddoniaeth Plant




4.2 Ffuglen ar gyfer Oedolion
 
4.2.1 Nofelau

4.2.2 Casgliadau Storïau Byrion




4.3 Ffeithiol

4.4 Dramâu

4.5 Sgriptiau Ffilm

4.6 Teledu




5 Cyfeiriadau




Teulu
Pan oedd yn bedwar mis oed, cafodd Theo Dahl anafiadau drwg pan drawodd tacsi ei bram. Fe ddioddefodd o hydrocephalus, ac fel canlyniad, daeth Dahl i ymwneud gyda datblygiad y falf "Wade-Dahl-Till" (neu'r falf WDT), dyfais i leddfu'r cyflwr.[2][3]
Priododd Dahl yr actores Americanaidd,  Patricia Neal, ar 2 Gorffennaf 1953 yn Eglwys y Drindod, Dinas Efrog Newydd. Parhaodd y briodas 30 mlynedd a chawsont bump o blant: Olivia (a fu farw o frech-wen encephalitis, yn saith oed), Tessa, Theo, Ophelia, a Lucy. Cysegrwyd y llyfr The BFG i Olivia.
Yn 1965, dioddefodd Neal dri ebychiad aneurysm ymenyddol tra'n feichiog gyda'u pumed plentyn, Lucy; fe gymerodd Dahl reolaeth o'i adferiad, ac fe ailddysgodd sut i gerdded a siarad yn y diwedd.[4] Ar ôl priodas gythryblus, cawsant ysgariad yn 1983, ac fe ail-briododd Dahl Felicity ("Liccy") d'Abreu Crosland (ganwyd 12 Rhagfyr 1938), a oedd 22 mlynedd yn iau nag ef.
Mae Ophelia Dahl yn gyfarwyddwr ac yn gyd-sefydlydd (gyda Paul Farmer) cymdeithas ddi-elw Partners in Health, sy'n darparu gofal iechyd i rai o gymunedau tlotaf y byd. Mae Lucy Dahl yn llenor ar gyfer y sgrin yn Los Angeles. Mae merch Tessa, Sophie Dahl (a ysbrydolodd Dahl i greu cymeriad Sophie, prif gymeriad Y CMM) yn fodel ac yn awdures, mae hi'n cofio Roald Dahl fel "dyn anodd iawn – cryf iawn, dominyddol ... ddim yn annhebyg i tad y chwiorydd Mitford yn rhuo o gwmpas y tŷ yn dweud ei farn yn swnllyd, gan wahardd rhai bechgyn rhag ymweld â'r tŷ."[5] Mae un o nifer o wyrion Dahl yn mynychu'r Royal Palm Academy.[6]

Diwrnod Roald Dahl
Yn ddiweddar, mae diwrnod pen-blwydd Dahl, 13 Medi, wedi cael ei ddathlu yn eang fel Diwrnod Roald Dahl.[7][8]
Ar benwythnos 17/18 Medi 2016 cynhaliwyd cyfres o ddigwyddiadau yng Nghaerdydd i ddathlu canmlwyddiant geni Dahl. Daeth torf o filoedd i weld y digwyddiadau 'annisgwyl' oedd wedi eu seilio ar waith llenyddol yr awdur, a gynhyrchwyd ar y cyd rhwng National Theatre Wales a Chanolfan Mileniwm Cymru.
[9]

Llenyddiaeth plant
Roedd mam Dahl yn arfer adrodd straeon iddo ef a'i chwiorydd am greaduriaid mytholegol Norwyaidd, ac mae nifer o'i lyfrau'n cyfeirio at y rhain neu wedi cael eu hysbrydoli gan y straeon, megis y cewri yn The BFG. Darlunwyd nifer o'i lyfrau gan Quentin Blake.

Rhestr Gweithiau
Gwaith ar gyfer Plant
Storïau Plant
      Yr CMM  (1982)
 The Gremlins (1943)

James a'r Eirinen Wlanog Enfawr (Saesneg: James and the Giant Peach) (1961) — Ffilm: James and the Giant Peach (animeiddio) (1996)

Charlie a'r Ffatri Siocled (Saesneg: Charlie and the Chocolate Factory) (1964) — Ffilmiau: Willy Wonka & the Chocolate Factory (1971) a Charlie and the Chocolate Factory (2005)

Y Bys Hud (Saesneg: The Magic Finger) (1966)

Mr Cadno Campus (Saesneg: Fantastic Mr Fox) (1970) — Ffilm: Fantastic Mr. Fox (animeiddio)

Charlie a'r Esgynnydd Mawr Gwydr (Saesneg: Charlie and the Great Glass Elevator) (1972) Dilyniant i Charlie and the Chocolate Factory.

Danny Pencampwr y Byd (Saesneg: Danny the Champion of the World) (1975) — Ffilm: Danny the Champion of the World (ffilm teledu) (1989)

The Wonderful Story of Henry Sugar and Six More (1977)

Y Crocodeil Anferthol – (1) cyfieithydd Emily Huws; (2) cyfieithydd Elin Meek (Saesneg: The Enormous Crocodile) (1978)

Y Twits (Saesneg: The Twits) (1980)

Moddion Rhyfeddol George (Saesneg: George's Marvelous Medicine) (1981)

Yr CMM (Saesneg: The BFG) (1982) — Ffilm: The BFG (animeiddio) (1989)

Y Gwrachod (Saesneg: The Witches) (1983) — Ffilm: The Witches (1990)

Jiráff, a'r Pelican a Fi (Saesneg: The Giraffe and the Pelly and Me) (1985)

Matilda (1988) — Ffilm: ''Matilda (1996)

Nab Wrc (Saesneg: Esio Trot) (1989)

The Minpins (1991)

The Vicar of Nibbleswicke (1991)

Barddoniaeth Plant
      Pastai Odl  (Rhyme Stew) (1989)
 Ffi Ffai Ffiaidd (Saesneg: Revolting Rhymes) (1982)

Ych-A-Fi (Saesneg: Dirty Beasts) (1983)

Pastai Odl (Saesneg: Rhyme Stew) (1989)

Ffuglen ar gyfer Oedolion
Nofelau
 Sometime Never: A Fable for Supermen (1948)

My Uncle Oswald (1979)

Casgliadau Storïau Byrion
 Over To You: Ten Stories of Flyers and Flying (1946)

Someone Like You (1953)

Kiss Kiss (1960)

Twenty-Nine Kisses from Roald Dahl (1969)

Tales of the Unexpected (1979)

Switch Bitch (1974) ISBN 0-1400-4179-6

More Tales of the Unexpected (1980)

The Best of Roald Dahl (1978)

Roald Dahl's Book of Ghost Stories (1983). Golygwyd gyda rhagair gan Dahl.

Ah, Sweet Mystery of Life: The Country Stories of Roald Dahl (1989)

The Collected Short Stories of Dahl (1991)

Two Fables (1986). "Princess and the Poacher" a "Princess Mammalia".

The Great Automatic Grammatizator (1997). (Adnabyddir yn yr Unol Daleithiau dan yr enw The Umbrella Man and Other Stories).

Skin And Other Stories (2002)

Roald Dahl: Collected Stories (2006)

Ffeithiol
 The Mildenhall Treasure (1946, 1977, 1999)

Boy – Tales of Childhood (1984) Atgofion hyd oedran 16, gan ganolbwyntio yn arbenig ar addysgu ym Mhrydain yn nechrau'r 20g.

Going Solo (1986) Dilyniant iw hunanatgofiant, ynddi mae'n mynd i weithio i Shell ac yn gwario amser yn gweithio yn Tanzania cyn ymuno â ymdrech y rhyfel gan fod yn un o'r peilotau cynghreiriol olaf i adael Gwlad Groeg yn ystod y goresgyniad Almaeneg.

Measles, a Dangerous Illness (1986)[10]

Memories with Food at Gipsy House (1991)

Roald Dahl's Guide to Railway Safety (1991)

My Year (1993)

The Roald Dahl Ominibus (1993)

Dramâu
 The Honeys (1955) Cynhyrchwyd yn Theatr Longacre ar Broadway.

Sgriptiau Ffilm
 36 Hours (1965)

You Only Live Twice (1967)

Chitty Chitty Bang Bang (1968)

The Night Digger (1971)

Willy Wonka & the Chocolate Factory (1971)

Teledu
 Way Out (1961) Cyfres Arswyd a gynhyrchwyd gan David Susskind

Alfred Hitchcock Presents ysgrifennwyd penodau gan Roald Dahl:
 Alfred Hitchcock Presents: "Lamb to the Slaughter" (1958)

Alfred Hitchcock Presents: "Dip in the Pool" (1958)

Alfred Hitchcock Presents: "Poison" (1958)

Alfred Hitchcock Presents: "Man from the South" (1960)

Alfred Hitchcock Presents: "Mrs. Bixby and the Colonel's Coat" (1960)

Alfred Hitchcock Presents: "The Landlady" (1961)


Tales of the Unexpected (1979-88), ysgrifennwyd a chyflwynwyd penodau gan  Roald Dahl

Cyfeiriadau

 Awdurdod 
 WorldCat

VIAF: 108159131

LCCN: n79055236

ISNI: 0000 0001 2146 8188

GND: 118630962

SELIBR: 238061

SUDOC: 030357055

BNF: cb118984388 (data)

BIBSYS: 90064099

ULAN: 500283614

NLA: 35032995

NDL: 00437111

NKC: jn19990001595

The SBN id CFIV035247 is not valid.

BNE: XX912544

CiNii: DA00159788

ODNB: Bywgraffiadur Cenedlaethol Geiriadur Rhydychen (Mynediad llyfrgelloedd cyhoeddus)







