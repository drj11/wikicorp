  
Mae  Wsbeceg yn iaith Twrcaidd  a hi yw iaith gyntaf ac iaith swyddogol cenedlaethol  Wsbecistan. Siaredir Wsbeceg gan tua 32 miliwn o siaradwyr brodorol yn Wsbecistan ei hun ac mewn mannau eraill yng Nghanolbarth Asia.
Perthynnai Wsbeceg i gangen Dwyrain Twrceg, neu Karluk, cangen o Deulu ieithyddol Twrceg. Mae dylanwadau allanol yn cynnwys Perseg, Arabeg a Rwsieg. Mae un o'r gwahaniaethau amlycaf rhwng Wsbeceg ac ieithoedd Twrceg eraill  yw gwled yn nhalgrynnu'r llafariad /ɑ/ i /ɒ/, nodwedd â ddylanwadwyd gan Perseg.

  Cynnwys
 
1 Enw

2 Hanes

3 Systemau ysgrifennu

4 Gramadeg
 
4.1 Ffonoleg
 
4.1.1 Llafariaid

4.1.2 Cytseiniaid




4.2 Morffoleg a cystrawen




5 Nifer o siaradwyr

6 Benthyciadiau geiriol

7 Tafodieithoedd

8 Gweler hefyd

9 Cyfeiriadau

10 Ffynonellau

11 Dolenni allanol




Enw
Yn ei hiaith ei hun, sillafir Wsbeceg fel o'zbek tili neu oʻzbekcha, ac yn sgript Arabeg, اوزبیک تیلیاوزبیک تیلی a اوزبیکچهاوزبیکچه.oʻzbek tili

Hanes
Mwyaf tebyg bu i siaradwyr Twrceg sefydlu ym masanau'r afonnydd yr Amu Darya, Syr Darya a'r Zarafshan o gwmpas o leiaf 600-700 CE, gan yn raddol ddisodli neu gymathu siaradwyr ieithoedd dwyreiniol Iraneg a arferai fyw'n flaenorol yn Sogdia, Bactria a Khwarezm. Brenhinllyn Twrceg gyntaf y rhanbarth oedd Khanaeth Kara-Khanid yn y 9fed–12g,[1] a oedd yn gydffederasiwn rhwng Karlukiaid, Chigiliaid, y Yaghma a llwythau eraill.[2]
Gellir ystyried Wsbeceg yn ddisgynnydd uniongyrchol neu'n ffurf ddiweddarach o Cibak, iaith ddylanwadol ar ddatblygiad llenyddol Twrceg Ganolbarth Asia yn  ystod teyrnasiad Cibak Khan, Timur (Tamerlane), a llinach Timurid (gan gynnwys y llywodraethwyr y Mughal cynnar o'r India). Fe feistrolwyd yr iaith gan Ali-Shir Nava'i yn y 15fed a'r 16g. Nava'i oedd  cynrychiolydd mwyaf o lenyddiaeth iaith Cibak. Cyfrannodd yn sylweddol tuag at ddatblygiad yr iaith Cibak a'i ddisgynnydd uniongyrchol, Wsbeceg ac fe gaiff ei ystyried yn eang i fod yn y sylfaenydd o lenyddiaeth Wsbeceg. Yn y bôn yn seiliedig ar yr amrywiad Karlukaidd o ieithoedd Twrceg, mae Cibak yn cynnwys nifer fawr o  fenthygiadau ieithyddol Perseg ac Arabeg. Erbyn y 19g anaml iawn eu'i ddefnyddwyd ar gyfer cyfansoddiadau llenyddol, ond dim tan yn gynnar yn ystod yr 20g bu iddi ddiflannu'n llwyr.[3][4][5]
Bu i'r term Wsbeceg fel y'i cymhwysir i iaith olygu gwahanol bethau ar wahanol adegau. Cyn 1921 ystyriwyd fod "Wsbeceg" a "Sart" i fod yn wahanol dafodieithoedd:

 "Wsbeceg" oedd amrywiad o gytgord-lafariad o Kipchak  a siaradwyd gan disgynyddion y chai a gyrrhaeddodd o Transoxiana gyda Muhammad Shaybani yn y 16g, a oedd yn byw'n bennaf o amgylch Bukhara a Samarkand, er i'r Dwrceg a siaradwyd yn Tashkent  hefyd yn gytgord-lafariad.  Fe ellir ei alw'n Hen Wsbeceg ac fe'i hystyrir i fod yn gysylltiedig â'r grŵp penodol hyn o bobl.

Roedd "Sart" yn ddafodiaith Karlukaidd a siaradwyd gan boblogaethau hŷn Twrceg a sefydlwyd mewn rhanbarth yn Nyffryn Fergana a Rhanbarth Qashqadaryo, ac mewn rhai rhannau o beth yw nawr yn Rhanbarth Samarqand; roedd yn cynnwys cymysgedd trymach o Berseg ac Arabeg, ac nid oedd ganddynt gytgord-llafariad. Fe ddaeth yn iaith Wsbeceg safonol a thafodiaith swyddogol  Wsbecistan.

Yn Khanaeth Khiva, siaradwyd  ffurf hynod o Oguz Twrcegaidd o Dwrceg Karluk gan y Sarts. Ar ôl i'r gyfundrefn Sofietaidd ddiddymu'r term 'Sart' fel difrïol,  dyfarnwyd byddai holl boblogaeth Twrceg Twrcestan yn cael eu hadnabod fel Wsbeciaid, er nad oedd i lawer dreftadaeth lwythol Wsbeceg.
Fodd bynnag, nid yr "Wsbeceg" cyn-chwyldroadol oedd yr iaith ysgrifenedig safonol a ddewiswyd ar gyfer y weriniaeth newydd yn 1924, er gwaethaf protestiadau'r Bolsieficiaid Wsbecaidd megis Fayzulla Khodzhayev, ond yn hytrach yr iaith Sart o ranbarth Samarkand. Dadlai  Edward A. Allworth bod hyn yn "ystumio hanes llenyddol y rhanbarth yn ofnadwy" a  ddefnyddwyd hyn i roi awduron megis Ali-Shir Nava'i o'r 15g hunaniaeth Wsbecaidd.[6] Mae'r dair dafodieithoedd yn parhau i fodoli o fewn Wsbeceg lafar fodern.

Systemau ysgrifennu
      Ysgrifen o 1911 yn yr Uwyddor Arabeg Uyghuraidd.
Mae Wsbeceg wedi cael ei hysgrifennu mewn amrywiaeth o sgriptiau drwy gydol hanes:

 Cyn-1928: wyddor Yaña imlâ yn seiliedig ar Arabeg a ddefnyddwyd gan lythrenogion, sef tua 3.7% o Uzbeks ar y pryd.[7]
 1880au: ceisiodd cenhadon Rwsiaidd ddefnyddio Syrilig ar gyfer Wsbeceg.


1928-1940: defnyddwyd Yañalif a oedd yn seiliedig ar Ladin yn swyddogol.

1940-1992: defnyddwyd y sgript Syrilig yn swyddogol.

  Delwedd:Students in Tashkent 1943.jpg   Myfyrwyr yn Tashkent yn astudio siart drosi Syrilig-Lladin,1943.Ers 1992: sgript Yañalif sydd yn seiliedig ar Ladin yw'r sgript swyddogol yn Wsbecistan.

Er gwaethaf statws swyddogol yn y sgript Lladin yn Wsbecistan, mae'r ddefnydd o Syrilig yn parhau i fod yn eang, yn enwedig mewn hysbysebion ac arwyddion. Mewn papurau newydd, gall sgriptiau fod yn gymysg, gyda phenawdau mewn Lladin ac erthyglau mewn Syrilig.[8] Nid yw'r sgript Arabeg yn cael ei ddefnyddio bellach yn Wsbecistan ac eithrio'n symbolaidd mewn rhai testunau neu ar gyfer astudiaethau academaidd o Chagatai (Hen Wsbeceg).
Yn rhanbarth gorllewin Tseiniaidd Xinjiang, lle mae poblogaeth leiafrifol Wsbeceg, mae'r Arabeg yn parhau iw chael ei defnyddio.
Yn Affganistan, mae'r orgraff Arabeg draddodiadol yn parahau iw gael ei ddefnyddio.

Gramadeg
Ffonoleg
Llafariaid
Mae i Wsbeceg safonol chwe llafariad ffonem:[9]



Blaen

 ôl



Agos

yr wyf yn

u



Canol

e

o



Agored

æ

ɒ


Cytseiniaid


Tafodol

Deintyddol

Alfeolaidd

Palatal

Velar

Uvular

Glottal



Trwynol

m

n

ŋ



Plosive/Affricate

di-lais

p

t

(ts)

tʃ

k

q

(ʔ)



lleisiodd

b

d

dʒ

ɡ



Fricative

di-lais

ɸ

s

ʃ

χ

h



lleisiodd

z

(ʒ)

ʁ



Approximant

l

j

w



Rhotic

r


Morffoleg a cystrawen
Fel iaith Twrceg, mae i Wsbeceg  lythrennau gweigion (null subject), cyflynol a does iddi unrhyw erthyglau na dim enwau dosbarthol "noun classes") (rhywedd neu fel arall). Trefn geiriau yw goddrych-gwrthrych–ferf (GGF) (SOV). Fel arfer geiriau llymsain (h. y. y sillaf olaf sy'n cael eu pwysleisio) sydd yn Wsbeceg, ond nid oes pwyslais ar rai terfyniadau a geirynnau atodol.
Mewn Wsbeceg, mae dau brif gategori o eiriau:

 enwol (sy'n cyfateb i enwau, rhagenwau, ansoddeiriau, a rhai adferfau)

berfol (sy'n cyfateb i ferfau a'r rhai adferfau)

 Berfau

Mae Wsbeceg yn defnyddio'r atodiadau llafar canlynol:



Atodiaidau

Swyddogaeth

Enghraifft

Cyfieithu



-moq

ferfenw

kelmoq

i ddod



-di

amser gorffennol y ferf

keldi

daeth



-ing

hanfodol

keling!

dod!



-sa

yn amodol

kelsa

byddai dod


Caiff amserau presennol a'r dyfodol eu mynegu gydag atodiadau - a -ac y.

 Bannodau

Cymerir enwau yr atodiad -us  fel bannod amhenodol. Caifff enwau diatodiadol eu deall fel bannod benodol.

 Rhagenwau



Rhagenw

Cyfieithu



dynion

Yr wyf yn



biz

rydym



aaa

chi (anffurfiol unigol)



siz

chi (ffurfiol unigol a lluosog)



u

ef/hi/ef


Nifer o siaradwyr
Mae amcangyfrifon o nifer y siaradwyr Wsbeceg yn amrywio'n fawr. Mae un amcan yn y gwyddoniadur Swedeg y Nationalencyklopedin yn amcangyfrif y nifer o siaradwyr brodorol i fod yn 30 miliwn,[10]
ac yn y CIA World  Factboook  mae'r amcangyfrif yn 25 miliwn. Amcangyfrifir ffynonellau eraill i'r nifer fod yn 21 miliwn yn Wsbecistan,[11] 3.4 miliwn yn Affganistan,[12] 900,000 yn Tajicistan,[13] 800,000 yng Nghyrgystan,[14] 500,000 yng Nghasacstan,[15] 300,000 yn Nhwrcmenistan,[16] a 300,000 yn Rwsia.[17]

Benthyciadiau geiriol
Mae dylanwad Islam, ac felly drwy estyniad, yn Arabeg, yn amlwg mewn geiriau benthyg Wsbeceg. Mae iddi hefyd ddylanwad weddilliol Rwsieg, o'r adeg pan oedd yr Wsbeciaid o dan reolaeth Ymerodraeth Rwsia â'r Undeb Sofietaidd. Yn bwysicaf oll, mae geirfa, ieithwedd ac ynganiad Wsbeceg wedi ei ddylanwadu'n drwm gan Perseg drwy ei gwreiddiau hanesyddol.

Tafodieithoedd
Mae i Wsbeceg lawer o dafodieithoedd, yn amrywio'n  eang o ranbarth i ranbarth. Fodd bynnag, mae tafodiaith gyffredin a ddefnyddir yn y cyfryngau torfol ac yn y rhan fwyaf o ddeunyddiau printiedig. Ymhlith y tafodieithoedd mwyaf-eang yw tafodiaith Tashkent, tafodiaith Wsbeceg, tafodiaith Ferghana, tafodiaith Khorezm dafodiaith, tafodiaith Chimkent-Twrcestan, a tahfodiaith Swrcandaria.

Gweler hefyd
 Iaith Chagatai

Iaith Wsbeceg Ddeheuol

Cyfeiriadau

Ffynonellau
Dolenni allanol
 Trawsnewidwyr

 Wsbeceg Syrilig–Lladin /

Wsbeceg Syrilig-Lladin ysgrifen a gwefan /

Wsbeceg-Lladin Syrilig ysgrifen a gwefan /

 Geiriaduron

 Geiriadur Iaith Wsbeceg Cyfrol i (А—Р) Archifwyd 2012-10-18 yn y Peiriant Wayback. (Tashkent, 1981)

Geiriadur Iaith Wsbeceg, Cyfrol II (С—Ҳ) Archifwyd 2012-10-18 yn y Peiriant Wayback. (Tashkent, 1981)

Geiriadur ar-lein Saesneg-Wsbeceg ac Wsbeceg-Saesneg

Geiriadur ar-lein Saesneg-Wsbeceg ac Wsbeceg-Saesneg

Geiriadur ar-lein Rwseg-Wsbeceg ac Wsbeceg-Rwseg

Geiriadur Wsbeceg<>Twrceg r Archifwyd 2015-01-14 yn y Peiriant Wayback. ( Prifysgol Pamukkale)

Ole Olufsen: "MA Vocabulary of the Dialect of  Bokhara" [1] (København 1905)

 Gramadeg ac orgraff

 Introduction to the Uzbek Language g Archifwyd 2006-04-09 yn y Peiriant Wayback., Mark Dickens

Principal Orthographic Rules For The Uzbek Languagef Orthographic h Archifwyd 2012-02-09 yn y Peiriant Wayback., cyfieithiad o Uzbekistan Cabinet y Gweinidog Penderfyniad Rhif 339, awst 24, 1995

Uzbek alphabet r, Omniglot

 Dysgu/deunyddiau addysgu

 Ona tilli uz, a website about Uzbek

Uzbek language materials u Archifwyd 2013-01-26 yn y Peiriant Wayback., Uz-Translations





