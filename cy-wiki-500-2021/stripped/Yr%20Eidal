 yr Eidal
Repubblica Italiana

Mathgwladwriaeth sofran, gwlad 
Enwyd ar ôlItalia 
 
PrifddinasRhufain 
Poblogaeth60,317,000 
Sefydlwyd  18 Mehefin 1946 (1946–) 

AnthemIl Canto degli Italiani 
Pennaeth llywodraethGiorgia Meloni 
Cylchfa amserEwrop/Rhufain 
NawddsantFfransis o Assisi, Catrin o Siena 
Iaith/Ieithoedd   swyddogolEidaleg 
Nifer a laddwyd633,133 
Daearyddiaeth
Rhan o'r canlynolEwrop, yr Undeb Ewropeaidd, European Economic Area 
Arwynebedd301,338 km² 
GerllawMôr Adria, Môr Tirrenia, Môr Ionia, Môr Liguria, Y Môr Canoldir 
Yn ffinio gydaSan Marino, y Fatican, Ffrainc, Y Swistir, Awstria, Slofenia, Malta 
Cyfesurynnau42.5°N 12.5°E 
Gwleidyddiaeth
Corff gweithredolLlywodraeth yr Eidal 
Corff deddfwriaetholSenedd yr Eidal 
Swydd pennaeth   y wladwriaethArlywyddion yr Eidal 
Pennaeth y wladwriaethSergio Mattarella 
Swydd pennaeth   y LlywodraethPrif Weinidog yr Eidal 
Pennaeth y LlywodraethGiorgia Meloni 


ArianEwro 
Canran y diwaith12 ±1 % 
Cyfartaledd plant1.39 
Mynegai Datblygiad Dynol0.895 
  Ffeiliau perthnasol ar Comin
 [golygwch ar Wicidata]

Gwlad yn ne Ewrop yw Gweriniaeth yr Eidal neu'r Eidal (Eidaleg: Italia). Mae'r rhan fwyaf o'r wlad yn benrhyn mawr siâp esgid uchel gyda nifer o ynysoedd ym Môr y Canoldir: Sisili a Sardegna ydyw'r mwyaf. Yn y gogledd mae mynyddoedd yr Alpau. Ceir môr ar dair ochr i'r Eidal, ond yn y gogledd mae'n ffinio ar Ffrainc, y Swistir, Awstria, a Slofenia. Y tu mewn i'r Eidal mae dwy wladwriaeth fach: San Marino a Dinas y Fatican.
Heddiw, mae gan yr Eidal un o'r economïau mwyaf datblygedig yn y byd o ran CMC,[1][2][3] a hi yw'r wythfed economi fwyaf y byd yn ôl CMC enwol (y trydydd yn yr Undeb Ewropeaidd ), y chweched 'cyfoeth cenedlaethol' mwyaf ac yn ei banc canolog mae'r 3edd storfa fwyaf o aur wrth-gefnr. Fe'i rhestrir yn uchel iawn o ran disgwyliad oes ei phobl, ansawdd bywyd, gofal iechyd,[4] ac addysg. Mae'r wlad yn chwarae rhan amlwg mewn materion economaidd, milwrol, diwylliannol a diplomyddol rhanbarthol a byd-eang; mae'n bwer rhanbarthol[5][6] ac yn bwer mawr,[7][8] a ganddi hi mae'r wythfed byddin mwyaf pwerus y byd. Mae'r Eidal yn aelod sefydlol a blaenllaw o'r Undeb Ewropeaidd ac yn aelod o nifer o sefydliadau rhyngwladol, gan gynnwys y Cenhedloedd Unedig, NATO, yr OECD, y Sefydliad Diogelwch a Chydweithrediad yn Ewrop, Sefydliad Masnach y Byd, y Grŵp o Saith, y G20, Undeb Gwledydd y Môr Canoldir, Cyngor Ewrop, Uno er Consensws, Ardal Schengen a llawer mwy. Ganwyd llawer o ddyfeiswyr ac arlunwyr mawr y byd yn yr hyn rydym yn ei adnabod heddiw fel Yr Eidal. ,Bu'r wlad yn ganolfan fyd-eang yn y maesydd canlynol: celf, cerddoriaeth, llenyddiaeth, athroniaeth, gwyddoniaeth, technoleg, a ffasiwn ers amser maith, ac mae wedi dylanwadu a chyfrannu'n fawr at feysydd amrywiol gan gynnwys y sinema, bwyd, chwaraeon, y gyfraith, bancio a busnes.[9] Gan yr Eidal mae'r nifer fwyaf o Safleoedd Treftadaeth y Byd (58 yn 2020), a hi yw'r bumed wlad yr ymwelir â hi fwyaf gan dwristiaeth.

  Cynnwys
 
1 Hanes
 
1.1 Crynodeb

1.2 Yr hanes yn llawnach




2 Daearyddiaeth
 
2.1 Demograffeg

2.2 Rhanbarthau

2.3 Dinasoedd




3 Gwleidyddiaeth
 
3.1 Ieithoedd




4 Yr amgylchedd
 
4.1 Hinsawdd

4.2 Milwrol




5 Economi

6 Diwylliant
 
6.1 Llenyddiaeth

6.2 Arlunio

6.3 Cerddoriaeth




7 Cyfeiriadau

8 Dolenni allanol




Hanes
 
  Prif erthygl: Hanes yr Eidal
Crynodeb
Mae hanes yr Eidal yn mynd yn ôl i gyfnod cynnar iawn, er mai yn gymharol ddiweddar yr unwyd yr Eidal i greu'r wladwriaeth fodern.
Daw'r enw Italia o'r hen enw am bobloedd a thiriogaeth de yr Eidal. Yn y rhan yma, roedd nifer o bobloedd wahanol, megis yr Etrwsciaid, Samnitiaid, Umbriaid a Sabiniaid. Yn y gogledd, ymsefydlodd llwythau Celtaidd o gwmpas dyffryn afon Po.
Yn rhan ddeheuol yr orynys ac ar ynys Sisili, ymsefydlodd Groegiaid rhwng 800 a 600 CC, a gelwid y rhan yma yn Magna Graecia ("Groeg Fawr") mewn Lladin. Yn y gogledd, yr Etrwsciaid oedd y grym mwyaf yn y cyfnod cynnar. Yn y 4 CC gorchfygwyd hwy gan y Rhufeiniaid, ac yn y 3 CC gorchfygodd Rhufain y Groegiaid yn y de hefyd.
Bu cyfres o ryfeloedd, y Rhyfeloedd Pwnig, rhwng Rhufain a Carthago; yn ystod yr Ail Ryfel Pwnig bu'r cadfridog Carthaginaidd Hannibal yn ymgyrchu yn yr Eidal am flynyddoedd. Er iddo ennill nifer o fuddugoliaethau ysgubol dros y Rhufeiniaid, bu raid iddo encilio o'r Eidal yn y diewedd. Ar ddiwedd y Trydydd Rhyfel Pwnig yn 146 CC, dinistriwyd Carthago.
Tyfodd Ymerodraeth Rhufain yn gyflym yn y cyfnod canlynol; concrwyd Gâl gan Iŵl Cesar rhwng 60 a 50 CC. Daeth ei fab mabwysiedig, Augustus, yn ymerawdwr cyntaf Rhufain. Daeth yr ymerodraeth yn y gorllewin i ben yn y 5g, a meddiannwyd yr Eidal gan bobloedd Almaenig megis yr Ostrogothiaid a'r Lombardiaid. Ffurfiwyd nifer o wladwriaethau.
Dim ond yn y 19g yr ad-unwyd yr Eidal, gyda Giuseppe Garibaldi yn un o'r prif ysgogwyr. Sefydlwyd Teyrnas yr Eidal yn 1861. Daeth Benito Mussolini, arweinydd plaid y Ffasgwyr yn Brif Weinidog yn 1922. Pan ddechreuodd yr Ail Ryfel Byd, daeth Mussolini a'r Eidal i mewn i'r rhyfel ar ochr yr Almaen. Diswyddwyd ef yn 1943, a'i saethu yn 1945.
Ar 2 Mehefin 1946, cafwyd pleidlais mewn refferendwm i ddileu'r frenhiniaeth ac i sefydlu Gweriniaeth yr Eidal. Mabwysiadwyd y cyfansoddiad newydd ar 1 Ionawr 1948.

Yr hanes yn llawnach
Oherwydd ei lleoliad daearyddol canolog yn Ne Ewrop a'r Môr Canoldir, yn hanesyddol bu'r Eidal yn gartref i fyrdd o bobl a diwylliannau gwahanol dros sawl mileniwm. Heddiw, mae'r rhan fwyaf yn bobloedd Italaidd Indo-Ewropeaidd a nhw a roddodd eu henw i'r penrhyn, gan ddechrau o'r oes glasurol (rhwng yr 8g CC a 6g ÔC). Y sefydlwyr cyntaf yn yr ardal roedd y Phoeniciaid a'r Carthaginiaid  a sefydlodd drefedigaethau yar ynysoedd yr Eidal, yn bennaf.[10] Sefydlodd y Groegiaid aneddiadau yn Magna Graecia, yn Ne'r Eidal, a bu'r Etrwsciaid a'r Celtiaid yn byw yng nghanol a gogledd yr Eidal, yn y drefn honno. Ffurfiodd llwyth Italaidd o'r enw'r Latins y Deyrnas Rufeinig yn yr 8g CC, a ddaeth yn weriniaeth (Gweriniaeth Rhufain) yn y pen draw, gyda llywodraeth o'r enw Senedd a'r Bobl . Aeth y Weriniaeth ati iorchfygu a chymhathu ei chymdogion ar benrhyn yr Eidal, gan orchyfygu rhannau o Ewrop, Gogledd Affrica ac Asia. Erbyn y ganrif gyntaf CC, daeth yr Ymerodraeth Rufeinig i'r amlwg fel y pŵer cryfaf ym masn y Môr Canoldir, a gynhwysai Pax Romana, cyfnod o fwy na 200 mlynedd lle datblygodd cyfraith, technoleg, economi, celf, a llenyddiaeth yr Eidal.[11][12]
Yn ystod yr Oesoedd Canol Cynnar, bu cwymp yr Ymerodraeth Rufeinig Orllewinol a goresgynwyd y tiroedd hyn gan bobloedd eraill, ond erbyn yr 11g cododd nifer o ddinas-wladwriaethau a gweriniaethau morwrol cystadleuol, yn bennaf yn rhanbarthau gogleddol a chanolog yr Eidal, gan gynnwys Fenis, Milan, Fflorens, Genoa, Pisa, Lucca, Cremona, Siena, Città di Castello, Perugia. Ffynnodd yr ardal oherwydd masnach a bancio, gan osod y sylfaen ar gyfer cyfalafiaeth fodern.[13] Gwasanaethodd y taleithiau annibynnol hyn yn bennaf fel prif hybiau masnachu Ewrop gydag Asia a'r Dwyrain Agos, yn aml yn mwynhau mwy o ddemocratiaeth na'r brenhiniaethau ffiwdal mwy a oedd i''w gweld ledled Ewrop. Fodd bynnag, roedd rhan o ganol yr Eidal dan reolaeth y Gwladwriaethau Pabaidd, tra bod De'r Eidal wedi aros yn ffiwdal i raddau helaeth tan y 19g, yn rhannol o ganlyniad i olyniaeth Fysantaidd, Arabaidd, Normanaidd, Angevin, Aragoniaid ac eraill.[14]
Dechreuodd y Dadeni Dysg yn yr Eidal, gan ledaenu i weddill Ewrop, a chafwyd diddordeb newydd mewn dyneiddiaeth, <a href="./Gwyddoniaeth" rel="mw:WikiLink" data-linkid="undefined" data-cx="{&quot;userAdded&quot;:true,&quot;adapted&quot;:true}">gwyddoniaeth</a>, mordwyo a chelf. Ffynnodd diwylliant yr Eidal, gan gynhyrchu ysgolheigion, artistiaid a polymathiaid enwog. Yn ystod yr Oesoedd Canol, darganfu fforwyr Eidalaidd lwybrau newydd i'r Dwyrain Pell a'r Byd Newydd. Serch hynny, gwanhaodd pŵer masnachol a gwleidyddol yr Eidal yn sylweddol pan agorwyd llwybrau masnach a oedd yn osgoi Môr y Canoldir.[15] Erbyn y 15g a'r 16g roedd yr Eidal yn dameidiog, yn wleidyddol, a chafodd ei gorchfygu a'i rhannu ymhellach ymhlith nifer o bwerau Ewropeaidd tramor dros y canrifoedd.
Erbyn canol y 19g, roedd cenedlaetholdeb yr Eidal yn ei anterth, ac felly hefyd y galwadau am annibyniaeth er mwyn gostwng rheolaeth dramor, a chafwyd cyfnod o gynnwrf gwleidyddol chwyldroadol. Ar ôl canrifoedd o dra-arglwyddiaethu tramor a rhaniad gwleidyddol, unwyd yr Eidal bron yn gyfan gwbl ym 1861 yn dilyn rhyfel o annibyniaeth, gan sefydlu Teyrnas yr Eidal fel pŵer mawr.[16] O ddiwedd y 19g i ddechrau'r 20g, diwydiannwyd yr Eidal yn gyflym, yn y gogledd yn bennaf, a ganwyd ymerodraeth drefedigaethol,[17] tra bod y de yn parhau i fod yn dlawd i raddau helaeth ac wedi'i eithrio o ddiwydiant, gan ysgogi diaspora mawr a dylanwadol.[18] Er gwaethaf ei bod yn un o'r pedwar prif bŵer perthynol yn y Rhyfel Byd Cyntaf, aeth yr Eidal i gyfnod o argyfwng economaidd a chythrwfl cymdeithasol, gan arwain at gynnydd unbennaeth ffasgaidd yr Eidal ym 1922. Yn yr Ail Ryfel Byd ochrodd gyda'r Almaen ac yn filwrol, fe'u trechwyd, ac arweiniodd hyn at Ryfel Cartref yr Eidal. Yn dilyn rhyddhau'r Eidal, diddymodd y wlad eu brenhiniaeth, sefydlwyd Gweriniaeth ddemocrataidd, mwynhawyd ffyniant economaidd, a daeth yr Eidal yn wlad ddatblygedig iawn.[1]

Daearyddiaeth
 
  Prif erthygl: Daearyddiaeth yr Eidal
Gwlad yn ne Ewrop sy'n ymestyn allan fel gorynys hir i ganol Môr y Canoldir yw'r Eidal. Mae hefyd yn cynnwys nifer o ynysoedd; y mwyaf yw Sisili a Sardinia. Mae'n ffinio ar y Swistir, Ffrainc, Awstria,  Slofenia, ac mae San Marino a Dinas y Fatican yn cael eu hamgylchynu gan yr Eidal.
Gwlad fynyddig yw'r Eidal. Yn y gogledd, ceir yr Alpau, sy'n ffurfio ffîn ogleddol y wlad. Y copa uchaf yw Monte Bianco (Ffrangeg: Mont Blanc), 4,807.5 medr o uchder, ar y ffîn rhwng yr Eidal a Ffrainc. Mynydd adnabyddus arall yw'r Matterhorn (Cervino mewn Eidaleg, ar y ffîn rhwng yr Eidal a'r Swistir. Ymhellach tua'r de, mae mynyddoedd yr Apenninau yn ymestyn o'r gogledd i'r de ar hyd yr orynys. Ceir y copa uchaf yn yr Apenninau Canolog, y Gran Sasso d'Italia (2,912 m). Ceir nifer o losgfynyddoedd byw yn yr Eidal: Etna, Vulcano, Stromboli a Vesuvius.
Afon fwyaf yr Eidal yw Afon Po, sy'n tarddu yn yr Alpau Cottaidd ac yn llifo tua'r dwyrain am 652 km (405 milltir) i'r Môr Adriatig ar hyd gwastadedd eang. Y llyn mwyaf yw Llyn Garda yn y gogledd.

Demograffeg
 
  Prif erthygl: Demograffeg yr Eidal
Ar 31 Rhagfyr 2006 roedd poblogaeth yr Eidal yn 59,131,287. Roedd 30,412,846 o'r rhain yn ferched a 28,718,441 yn ddynion.

Rhanbarthau
 
  Prif erthygl: Rhanbarthau'r Eidal
Mae'r Eidal wedi'i rhannu yn 20 o ranbarthau (regioni, unigol regione).

 




Rhanbarth
Prifddinas



1
Abruzzo
L'Aquila



2
Valle d'Aosta
Aosta



3
Puglia
Bari



4
Basilicata
Potenza



5
Calabria
Catanzaro



6
Campania
Napoli



7
Emilia-Romagna
Bologna



8
Friuli-Venezia Giulia
Trieste



9
Lazio
Rhufain



10
Liguria
Genova



11
Lombardia
Milan



12
Marche
Ancona



13
Molise
Campobasso



14
Piemonte
Torino



15
Sardinia
Cagliari



16
Sisili
Palermo



17
Trentino-Alto Adige
Trento



18
Toscana
Fflorens



19
Umbria
Perugia



20
Veneto
Fenis









Dinasoedd
      Milan
Dinasoedd mwyaf yr Eidal, gydag ystadegau poblogaeth 2012, yw:




Dinas
Poblogaeth (2012)



Rhufain
2,641,930



Milan
1,245,956



Napoli
960,521



Torino
872,158



Palermo
655,604



Genova
583,089



Bologna
373,010



Fflorens
362,389



Bari
314,258



Catania
293,112


Gwleidyddiaeth
 
  Prif erthygl: Gwleidyddiaeth yr Eidal
Ar 8 Mai, 2008, daeth Silvio Berlusconi yn Brif Weinidog yr Eidal am y trydydd tro, fel olynydd i Romano Prodi. Roedd Berlusconi hefyd yn Brif Weinidog o 1994 hyd 1995 ac o 2001 hyd 2006. Ymddiswyddodd mewn cywilydd ar 16 Tachwedd 2011 oherwydd diffyg arian y wlad a phenodwyd Mario Monti yn ei le.

 Etholiad cyffredinol yr Eidal, 2006

Etholiad cyffredinol yr Eidal, 2008

Etholiad arlywyddol yr Eidal, 2006

Arlywyddion yr Eidal

Prif Weinidogion yr Eidal

Ieithoedd
Eidaleg yw iaith swyddogol yr Eidal. Yn nhalaith Bolzano-Bozen, mae'r mwyafrif o'r boblogaeth yn siarad Almaeneg fel iaith gyntaf, ac mae Almaeneg yn iaith swyddogol yno ar y cyd ag Eidaleg. Mae Ffrangeg yn gyd-iaith swyddogol yn Val d'Aosta.

Yr amgylchedd
Yr Eidal oedd y wlad gyntaf i ymelwa ar ynni geothermol i gynhyrchu trydan.[19] Mae'r graddiant geothermol uchel sy'n ffurfio rhan o'r penrhyn yn ei gwneud yn bosibl y gellir ei hecsbloetio hefyd mewn rhanbarthau eraill; nododd ymchwil a wnaed yn y 1960au a'r 1970au feysydd geothermol posibl yn Lazio a Thysgani, yn ogystal ag yn y rhan fwyaf o'r ynysoedd folcanig. [19]

      Parciau cenedlaethol a rhanbarthol yn yr Eidal
Ar ôl ei thwf diwydiannol cyflym, cymerodd yr Eidal amser hir i wynebu ei phroblemau amgylcheddol. Mae bellach yn safle 84 yn y byd am gynaliadwyedd ecolegol.[20] Ceir ynddi barciau cenedlaethol sy'n gorchuddio tua 5% o'r wlad,[21] tra bod cyfanswm yr arwynebedd a warchodir gan barciau cenedlaethol, parciau rhanbarthol a gwarchodfeydd natur yn gorchuddio tua 10.5% o diriogaeth yr Eidal,[22] ac mae 12% o'r arfordiroedd yn ardaloedd morol gwarchodedig.[23]
Yn y 2010au, daeth yr Eidal yn un o gynhyrchwyr mwyaf blaenllaw y byd o ynni adnewyddadwy, a'r pedwerydd mwyaf y byd o ran ei chapasiti ynni solar wedi'i osod[24][25] a'r chweched mwyaf o gapasiti ynni gwynt yn 2010.[26] Darparodd ynni adnewyddadwy tua 37% o ddefnydd ynni'r Eidal yn 2020, o'i gymharu a dros hanner yng Nghymru yn yr un cyfnod.[27] Mae llygredd aer yn parhau i fod yn broblem ddifrifol, yn enwedig yn y gogledd diwydiannol, gan gyrraedd y degfed lefel uchaf ledled y byd o allyriadau carbon deuocsid diwydiannol yn y 1990au.[28] Yr Eidal oedd y deuddegfed cynhyrchydd carbon deuocsid mwyaf yn 2020.
Mae llawer o gyrsiau dŵr a darnau arfordirol wedi’u halogi gan weithgarwch diwydiannol ac amaethyddol, ac oherwydd bod lefelau dŵr yn codi, mae Fenis wedi bod dan ddŵr yn rheolaidd yn ystod y blynyddoedd diwethaf. Nid yw gwastraff o weithgarwch diwydiannol bob amser yn cael ei waredu trwy ddulliau cyfreithiol ac mae wedi arwain at effeithiau iechyd parhaol ar drigolion yr ardaloedd yr effeithiwyd arnynt, fel yn achos trychineb Seveso. Mae'r wlad hefyd wedi gweithredu sawl adweithydd niwclear rhwng 1963 a 1990 ond, ar ôl trychineb Chernobyl a refferendwm ar y mater daeth y rhaglen niwclear i ben, penderfyniad a gafodd ei wyrdroi gan y llywodraeth yn 2008, gan gynllunio i adeiladu hyd at bedair gorsaf ynni niwclear gyda thechnoleg Ffrangeg. Cafodd hyn ei wyrdroi ei daro gan refferendwm yn dilyn damwain niwclear Fukushima.[29]
Mae datgoedwigo, adeiladu anghyfreithlon a pholisïau rheoli tir gwael wedi arwain at erydiad sylweddol ar draws rhanbarthau mynyddig yr Eidal, gan arwain at drychinebau ecolegol mawr fel llifogydd Argae Vajont 1963, Sarno yn 1998[30] a llithriadau llaid Messina yn 2009. Roedd gan y wlad sgôr cymedrig Mynegai Uniondeb Tirwedd Coedwig 2019 o 3.65/10, gan ei gosod yn safle 142 yn fyd-eang allan o 172 o wledydd.[31]

Hinsawdd
      Map dosbarthiad hinsawdd Köppen-Geiger o'r Eidal [32]
Mae hinsawdd yr Eidal yn cael ei ddylanwadu gan ddŵr y Môr Canoldir sy'n amgylchynu'r Eidal ar bob ochr oddigerth i'r gogledd. Mae'r moroedd hyn yn gronfa o wres a lleithder i'r Eidal. O fewn y parth tymherus deheuol, maen nhw'n pennu hinsawdd y Canoldir gyda gwahaniaethau lleol oherwydd geomorffoleg y diriogaeth.[33]
Oherwydd hyd  y penrhyn (Gog - De)a'r gefnwlad fynyddig yn bennaf, mae hinsawdd yr Eidal yn amrywiol iawn. Yn y rhan fwyaf o'r rhanbarthau mewndirol gogleddol a chanolog, mae'r hinsawdd yn amrywio o is-drofannol llaith i gyfandirol llaith a chefnforol. Mae hinsawdd rhanbarth daearyddol dyffryn Po yn is-drofannol llaith yn bennaf, gyda gaeafau oer a hafau poeth.[34][35] Mae ardaloedd arfordirol Liguria, Toscana a'r rhan fwyaf o'r De yn gyffredinol yn cyd-fynd â hinsawdd arferol Môr y Canoldir (dosbarthiad hinsawdd Köppen).
Mae tywydd yr arfordir yn wahanol i'r rhai y tu mewn i'r wlad, yn enwedig yn ystod misoedd y gaeaf pan fo'n oerach ar y mynyddoedd, yn wlyb, ac yn aml yn eira. Mae gan y rhanbarthau arfordirol aeafau mwyn a hafau poeth a sych yn gyffredinol; mae dyffrynnoedd yr iseldir yn boeth yn yr haf. Gall tymheredd cyfartalog y gaeaf amrywio o tua 0 °C (32 °F)   yn yr Alpau i 12 °C (54 °F) yn Sisili, felly mae tymheredd cyfartalog yr haf yn amrywio o 20 °C (68 °F) i dros 25 °C (77 °F).
Gall gaeafau amrywio’n fawr ar draws y wlad gyda chyfnodau o oerni, niwl ac eira yn y gogledd ac amodau mwynach a brafiach yn y de. Ceir hafau poeth drwy'r Eidal benbaladr, ac eithrio ar y mynyddoedd, yn enwedig yn y de. Gall ardaloedd gogleddol a chanolog brofi stormydd mellt a tharanau o bryd i'w gilydd o'r gwanwyn i'r hydref.[36]
Cefnogodd yr Eidal ymdrechion rhyngwladol i ailadeiladu a sefydlogi Irac, ond roedd wedi tynnu ei fintai filwrol o ryw 3,200 o filwyr yn ôl erbyn 2006, gan gynnal dim ond gweithredwyr dyngarol a phersonél sifil eraill. Ym mis Awst 2006 anfonodd yr Eidal tua 2,450 o filwyr yn Libanus ar gyfer cenhadaeth cadw heddwch y Cenhedloedd Unedig UNIFIL .  Yr Eidal yw un o arianwyr mwyaf Awdurdod Cenedlaethol Palestina, gan gyfrannu €60 miliwn yn 2013 yn unig. [37]

Milwrol
      Arfbais herodrol Lluoedd Arfog yr Eidal
Mae Byddin yr Eidal, y Llynges, yr Awyrlu a'r Carabinieri gyda'i gilydd yn ffurfio Lluoedd Arfog yr Eidal, dan orchymyn yr Uchel Gyngor Amddiffyn, dan lywyddiaeth Llywydd yr Eidal, fel y'i sefydlwyd gan erthygl 87 o Gyfansoddiad yr Eidal. Yn ôl erthygl 78, mae gan y Senedd yr awdurdod i ddatgan cyflwr o ryfel a breinio’r pwerau angenrheidiol yn y Llywodraeth.
Er nad yw'n gangen o'r lluoedd arfog, mae gan y Guardia di Finanza (y "Gwarchodlu Ariannol") statws milwrol ac fe'i trefnir yn eitha milwrol.[38] Ers 2005, mae'r gwasanaeth milwrol yn wirfoddol.[39] Yn 2010, roedd gan fyddin yr Eidal 293,202 o bersonél ar ddyletswydd weithredol,[40] gyda 114,778 ohonynt yn Carabinieri.[41] Fel rhan o strategaeth rhannu niwclear NATO mae'r Eidal hefyd yn cynnal 90 o fomiau niwclear B61 yr Unol Daleithiau, sydd wedi'u lleoli yng nghanolfannau awyr Ghedi ac Aviano.[42]
Cerbydau ymladd mwyaf adnabyddus byddin yr Eidalyw cerbyd ymladd milwyr traed Dardo, dinistriwr tanciau Centauro a thanc Ariete, ac ymhlith ei awyrennau ceir yr hofrennydd ymosod Mangusta, yn ystod y blynyddoedd diwethaf a ddefnyddiwyd yn nheithiau'r UE, NATO a'r Cenhedloedd Unedig. Mae ganddo hefyd lawer o gerbydau arfog Leopard 1 a M113. Fe'i ffurfiwyd yn 1946 o'r hyn a oedd ar ôl o'r Regio Esercito ("y Fyddin Frenhinol", a sefydlwyd ar achlysur cyhoeddi Teyrnas yr Eidal, 1861 ) ar ôl yr Ail Ryfel Byd, pan ddaeth yr Eidal yn weriniaeth yn dilyn refferendwm.

Economi
Mae gan yr Eidal economi gymysg a  datblygedig [43] fawr, sy'n safle trydydd mwyaf yn Ardal yr Ewro a'r wythfed-fwyaf yn y byd.[44] Yn un o sylfaenwyr y G7, Ardal yr Ewro a'r OECD, fe'i hystyrir yn un o genhedloedd mwyaf diwydiannol y byd ac yn wlad flaenllaw ym myd masnach ac allforion y byd.[45][46][47] Hi oedd yr 8fed wlad o ran ansawdd bywyd uchaf yn y byd yn 2005 a'r 26ain ar y Mynegai Datblygiad Dynol. Mae'r wlad yn adnabyddus am ei busnesau creadigol ac arloesol,[48] sector amaethyddol mawr a chystadleuol [49] (y cynhyrchwr gwin mwya'r byd), [50] ac am ei sector geir dylanwadol ac o ansawdd uchel, peiriannau, bwyd, diwydiant dylunio a ffasiwn.[51][52][53]
Yr Eidal yw chweched wlad gweithgynhyrchu fwya'r byd, a nodweddir gan nifer llai o gorfforaethau rhyngwladol byd-eang. Mae wedi cynhyrchu sector gweithgynhyrchu sy'n canolbwyntio'n aml ar allforio marchnad arbenigol a chynhyrchion moethus, os yw ar un ochr yn llai abl i gystadlu ar y swm, ar yr ochr arall mae'n fwy nag abl i gystadlu mewn safon.[54] Yr Eidal oedd degfed allforiwr mwya'r byd yn 2019 ac mae ei chysylltiadau agos â gwledydd eraill yr Undeb Ewropeaidd yn gymorth sylweddol. Ei phartneriaid allforio mwyaf yn 2019 oedd yr Almaen (12%), Ffrainc (11%), a'r Unol Daleithiau (10%).[55]

      Mae Milan yn ganolfan ariannol fyd-eang ac yn brifddinas ffasiwn y byd.
Mae'r diwydiant modurol yn rhan sylweddol o sector gweithgynhyrchu'r Eidal, gyda dros 144,000 o gwmnïau a bron i 485,000 o bobl yn cael eu cyflogi yn yn 2015,[56] gyda chyfraniad o 8.5% i CMC yr Eidal.[57] Fiat Chrysler Automobiles neu FCA oedd y seithfed gwneuthurwr ceir mwyaf yn y byd yn y 2010au.[58] Mae gan y wlad ystod eang o gynhyrchion clodwiw, o geir dinas gryno i supercars moethus fel Maserati, Lamborghini, a Ferrari.[59] Banca Monte dei Paschi di Siena yw banc hynaf neu ail hynaf y byd sydd wedi weithredu'n ddi-dor, yn dibynnu ar y diffiniad.[60] Mae gan yr Eidal sector gydweithredol cryf hefyd, gyda'r gyfran fwyaf o'r boblogaeth (4.5%) yn cael ei chyflogi gan gwmniau cydweithredol.[61]

      Mae'r Eidal yn rhan o undeb ariannol, Ardal yr Ewro (glas tywyll) ac o farchnad sengl yr UE.
Mae rhaniad cynyddol rhwng y Gogledd a'r De yn ffactor mawr mewn gwendid economaidd-gymdeithasol.[62] Gellir ei nodi gan y gwahaniaeth enfawr mewn incwm ystadegol rhwng y rhanbarthau gogleddol cyfoethog a thlodi'r de a'r bwrdeistrefi.[63] Mae'r dalaith gyfoethocaf, Alto Adige-South Tyrol, yn ennill 152% o'r CMC cenedlaethol y pen, tra bod y rhanbarth tlotaf, Calabria, yn 61%.[64] Mae'r gyfradd ddiweithdra (11.1%) ychydig yn uwch na chyfartaledd Ardal yr Ewro,[65] sef 6.6% yn y Gogledd a 19.2% yn y De.[66] Mae’r gyfradd ddiweithdra ymhlith pobl ifanc (31.7% ym mis Mawrth 2018) yn hynod o uchel o gymharu â safonau’r UE.[67]

Diwylliant
 
  Prif erthygl: Diwylliant yr Eidal
      Hunanbortread gan Leonardo da Vinci
Oherwydd na unwyd yr Eidal fel un wladwriaeth hyd yn gymharol ddiweddar, bu amrywiaeth mawr mewn diwylliant. Yn yr Eidal y dechreuodd y Dadeni yn Ewrop.

Llenyddiaeth
Gosodwyd sylfeini yr iaith Eidaleg lenyddol fodern gan Dante Alighieri o Fflorens. Ei waith enwocaf yw'r Divina Commedia, a ystyrir yn un o brif gampweithiau Ewrop yn y Canol Oesoedd. Llenorion amlwg eraill yw Giovanni Boccaccio, Giacomo Leopardi, Alessandro Manzoni, Torquato Tasso, Ludovico Ariosto, a Petrarch. Ymysg llenorion diweddar yr Eidal, enillwyd Gwobr Nobel am Lenyddiaeth gan Giosuè Carducci (1906), Grazia Deledda (1926), Luigi Pirandello (1936), Salvatore Quasimodo (1959), Eugenio Montale (1975) a Dario Fo (1997).
Ymysg athronwyr amlwg yr Eidal mae Giordano Bruno, Marsilio Ficino, Niccolò Machiavelli a Giambattista Vico.

Arlunio
Yn y Canol Oesoedd a chyfnod y Dadeni, roedd arlunwyr yr Eidal yn enwog trwy Ewrop. Ymysg yr arlunwyr a cherrflunwyr enwocaf, mae Michelangelo, Leonardo da Vinci, Donatello, Botticelli, Fra Angelico, Tintoretto, Caravaggio, Bernini, Titian a Raffael.

Cerddoriaeth
Bu cerddoriaeth yn elfen bwysig iawn yn niwylliant yr eidal o gyfnod cynnar. Yn yr Eidal y dyfeisiwyd opera, ac mae'n parhau'n boblogaidd iawn hyd heddiw. Y tŷ opera enwocaf yw La Scala yn Milan.
Ymysg cyfansoddwyr enwog yr Eidal mae Alessandro Scarlatti, Arcangelo Corelli,  Antonio Vivaldi, Niccolò Paganini, Gioachino Rossini, Giuseppe Verdi a Giacomo Puccini. Canwr enwocaf yr Eidal yn y cyfnod diweddar oedd Luciano Pavarotti.

Cyfeiriadau

Dolenni allanol
 (Eidaleg) Arlywydd yr Eidal

(Eidaleg) Senedd yr Eidal

(Eidaleg) Gwefan swyddogol y Llywodraeth

 
  
 Chwiliwch am Yr Eidal yn Wiciadur.

  gw • sg • go Aelod-wladwriaethau yr Undeb Ewropeaidd

  
  Yr Almaen

 Awstria

 Gwlad Belg

 Bwlgaria

 Croatia

 Cyprus

 Denmarc

 Yr Eidal

 Estonia

 Y Ffindir

 Ffrainc

 Gwlad Groeg

 Hwngari

 Yr Iseldiroedd

 Gweriniaeth Iwerddon

 Latfia

 Lithwania

 Lwcsembwrg

 Malta

 Portiwgal

 Gwlad Pwyl

 Rwmania

 Sbaen

 Slofacia

 Slofenia

 Sweden

 Y Weriniaeth Tsiec




  gw • sg • goGwledydd y Môr Canoldir

  
  Yr Aifft

 Albania

 Algeria

 Bosnia a Hercegovina

 Croatia

 Cyprus

 Yr Eidal

 Ffrainc

 Gwlad Groeg

 Israel

 Libanus

 Libia

 Malta

 Monaco

 Montenegro

 Moroco

 Palesteina

 Sbaen

 Slofenia

 Syria

 Tiwnisia

 Twrci




  gw • sg • go Sefydliad Cytundeb Gogledd yr Iwerydd (NATO)

Aelodau  
  Albania

 Yr Almaen

 Bwlgaria

 Canada

 Croatia

 Denmarc

 Y Deyrnas Unedig

 Yr Eidal

 Estonia

 Ffrainc

 Y Weriniaeth Tsiec

 Hwngari

 Gwlad Belg

 Gwlad Groeg

 Gwlad yr Iâ

 Yr Iseldiroedd

 Latfia

 Lithwania

 Lwcsembwrg

 Montenegro

 Norwy

 Portiwgal

 Gwlad Pwyl

 Rwmania

 Sbaen

 Slofacia

 Slofenia

 Twrci

 Unol Daleithiau America



Ymgeiswyr 
 
  Bosnia a Hercegovina

 Georgia

 Gogledd Macedonia

 Wcráin





  gw • sg • goG8

Aelodau arhosol  
  Yr Almaen

 Canada

 Y Deyrnas Unedig

 Yr Eidal

 Ffrainc

 Japan

 Rwsia

 Unol Daleithiau America



Cynrychiolaethau ychwanegol  Yr Undeb Ewropeaidd






