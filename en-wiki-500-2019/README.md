# en-wiki-500-2019

`en-wiki-500-2019`
is a corpus based on
the 500 most-viewed pages on en.wikipedia.org (English Wikipedia)
in the year 2019.

The `words` file contains all the words.
It has about 4 million words, about 130,000 unique words
(about 20,000 of which only differ by case).

## Method

`topviews-2019.csv` is the CSV downloaded from the Topviews
toolforge page:
https://pageviews.toolforge.org/topviews/?project=en.wikipedia.org&platform=all-access&date=last-year&excludes=

In this case downloaded on 2020-07-16T10:35Z.

Those pages have been crawled using the `wikicorp` program
(currently at top-level of the `wikicorp` repository, but that
may change).
This produces JSON from an API and that JSON has been written
into the `crawl` directory.

The JSON files contain HTML which is stripped of its HTML tags
(fairly crudely), and placed in the `stripped` directory.
The `strip` program is in the `strip` directory.
The exact details of what this program does are likely to change.

The stripped files and tokenised using the program `split`
(in the `split` directory) and placed in the file tokens.
Each token is output followed by `\n`.
That means that where
`\n` appears in the (stripped) input, it will appear as two
blank lines in the `tokens` file.
Most downstream processing is expected to discard blank lines,
but those programs that do care can (carefully) recover the
exact token stream.

The `words` file is produced by:

    grep '[[:alpha:]]' tokens > words

effectively claiming that a _word_ is a token with at least one
alphabetic character.
This eliminates whitespace tokens,
tokens that are entirely numbers, and tokens that are entirely puncutation.
