  Award ceremony presented by the Academy of Motion Picture Arts & Sciences for achievement in filmmaking in 2018



91st Academy Awards
 Official poster
DateFebruary 24, 2019
Site  Dolby Theatre
Hollywood, Los Angeles, California, U.S.

Preshow hosts 
 Ashley Graham

Maria Menounos

Elaine Welteroth

Billy Porter

Ryan Seacrest


Produced byDonna Gigliotti Glenn Weiss
Directed byGlenn Weiss
Highlights
Best PictureGreen Book
Most awardsBohemian Rhapsody (4)
Most nominationsThe Favourite and Roma (10)
TV in the United States
NetworkABC
Duration3 hours, 23 minutes[1]
Ratings29.6 million[2] 20.6% (Nielsen ratings)[3]

 ← 90th

Academy Awards

92nd →



The 91st Academy Awards ceremony, presented by the Academy of Motion Picture Arts and Sciences (AMPAS), honored the best films of 2018, and took place at the Dolby Theatre in Hollywood, Los Angeles, California. The ceremony was held on February 24, 2019. During the ceremony, AMPAS presented Academy Awards (commonly referred to as Oscars) in 24 categories. The ceremony was televised in the United States by the American Broadcasting Company (ABC), produced by Donna Gigliotti and Glenn Weiss, with Weiss also serving as director.[4] It was the first ceremony in three decades, since the 61st Academy Awards in 1989, to be conducted without a host.
In related events, the Academy held its 10th Annual Governors Awards ceremony at the Grand Ballroom of the Hollywood and Highland Center on November 18, 2018.[5] The Academy Scientific and Technical Awards were presented by host David Oyelowo on February 9, 2019, in a ceremony at the Beverly Wilshire Hotel in Beverly Hills.[6]
Green Book won three awards, including Best Picture, although Bohemian Rhapsody led the ceremony with four awards, including Best Actor for Rami Malek's portrayal of Freddie Mercury. Roma and Black Panther also received three awards apiece, with the former winning Best Director for Alfonso Cuarón and becoming the first Mexican submission to win Best Foreign Language Film. Olivia Colman was awarded Best Actress for portraying Anne, Queen of Great Britain in The Favourite.[7] With a U.S. viewership of 29.6 million, it marked a 12% increase over the 2018 ceremony, but still ranks amongst the least-watched ceremonies.[8][9][10]

  Contents
 
1 Winners and nominees
 
1.1 Awards

1.2 Governors Awards
 
1.2.1 Academy Honorary Awards

1.2.2 Irving G. Thalberg Memorial Award







2 Films that received multiple awards and nominations

3 Presenters and performers
 
3.1 Presenters

3.2 Performers




4 Ceremony information
 
4.1 Box office performance of nominated films

4.2 Host selection

4.3 Ratings and reception




5 In Memoriam

6 See also

7 Notes

8 References

9 External links




Winners and nominees
 
      Peter Farrelly, Best Picture and Best Original Screenplay co-winner
      Alfonso Cuarón, Best Director, Best Foreign Language Film, and Best Cinematography winner
      Rami Malek, Best Actor winner
      Olivia Colman, Best Actress winner
      Mahershala Ali, Best Supporting Actor winner
      Regina King, Best Supporting Actress winner
      Spike Lee, Best Adapted Screenplay co-winner
      Jaime Ray Newman, Best Live Action Short Film co-winner
      Domee Shi, Best Animated Short Film co-winner
      Lady Gaga, Best Original Song co-winner
      Mark Ronson, Best Original Song co-winner
      Ruth E. Carter, Best Costume Design winner
      John Ottman, Best Film Editing winner

The nominees for the 91st Academy Awards were announced on January 22, 2019, at 5:20 a.m. PST (13:20 UTC), at the Academy's Samuel Goldwyn Theater in Beverly Hills, by actors Kumail Nanjiani and Tracee Ellis Ross.[11][12]	

Awards
Winners are listed first, highlighted in boldface, and indicated with a double dagger ().[13]





 Best Picture
 Green Book – Jim Burke, Charles B. Wessler, Brian Currie, Peter Farrelly and Nick Vallelonga
 Black Panther – Kevin Feige

BlacKkKlansman – Sean McKittrick, Jason Blum, Raymond Mansfield, Jordan Peele and Spike Lee

Bohemian Rhapsody – Graham King

The Favourite – Ceci Dempsey, Ed Guiney, Lee Magiday and Yorgos Lanthimos

Roma – Gabriela Rodríguez and Alfonso Cuarón

A Star Is Born – Bill Gerber, Bradley Cooper and Lynette Howell Taylor

Vice – Dede Gardner, Jeremy Kleiner, Adam McKay and Kevin Messick




 Best Director
 Alfonso Cuarón – Roma
 Spike Lee – BlacKkKlansman

Paweł Pawlikowski – Cold War

Yorgos Lanthimos – The Favourite

Adam McKay – Vice






 Best Actor
 Rami Malek – Bohemian Rhapsody as Freddie Mercury
 Christian Bale – Vice as Dick Cheney

Bradley Cooper – A Star Is Born as Jackson "Jack" Maine

Willem Dafoe – At Eternity's Gate as Vincent van Gogh

Viggo Mortensen – Green Book as Tony Vallelonga




 Best Actress
  Olivia Colman – The Favourite as Queen Anne
 Yalitza Aparicio – Roma as Cleodegaria "Cleo" Gutierrez

Glenn Close – The Wife as Joan Castleman

Lady Gaga – A Star Is Born as Ally Maine

Melissa McCarthy – Can You Ever Forgive Me? as Lee Israel






 Best Supporting Actor
 Mahershala Ali – Green Book as Don Shirley
 Adam Driver – BlacKkKlansman as Philip "Flip" Zimmerman

Sam Elliott – A Star Is Born as Bobby Maine

Richard E. Grant – Can You Ever Forgive Me? as Jack Hock

Sam Rockwell – Vice as George W. Bush




 Best Supporting Actress
 Regina King – If Beale Street Could Talk as Sharon Rivers
 Amy Adams – Vice as Lynne Cheney

Marina de Tavira – Roma as Sofía

Emma Stone – The Favourite as Abigail Masham

Rachel Weisz – The Favourite as Sarah Churchill






 Best Original Screenplay
 Green Book – Nick Vallelonga, Brian Currie and Peter Farrelly
 The Favourite – Deborah Davis and Tony McNamara

First Reformed – Paul Schrader

Roma – Alfonso Cuarón

Vice – Adam McKay




 Best Adapted Screenplay
 BlacKkKlansman – Charlie Wachtel, David Rabinowitz, Kevin Willmott and Spike Lee based on the book by Ron Stallworth
 The Ballad of Buster Scruggs – Joel Coen and Ethan Coen; All Gold Canyon is based on a story by Jack London; The Gal Who Got Rattled is inspired by a story by Stewart Edward White.

Can You Ever Forgive Me? – Nicole Holofcener and Jeff Whitty based on the memoir by Lee Israel

If Beale Street Could Talk – Barry Jenkins based on the book by James Baldwin

A Star Is Born – Eric Roth, Bradley Cooper and Will Fetters based on the 1954 screenplay by Moss Hart and 1976 screenplay by Joan Didion, John Gregory Dunne and Frank Pierson; based on a story by Robert Carson and William A. Wellman






 Best Animated Feature Film
 Spider-Man: Into the Spider-Verse – Bob Persichetti, Peter Ramsey, Rodney Rothman, Phil Lord and Christopher Miller
 Incredibles 2 – Brad Bird, John Walker and Nicole Paradis Grindle

Isle of Dogs – Wes Anderson, Scott Rudin, Steven Rales and Jeremy Dawson

Mirai – Mamoru Hosoda and Yūichirō Saitō

Ralph Breaks the Internet – Rich Moore, Phil Johnston and Clark Spencer




 Best Foreign Language Film
 Roma (Mexico) in Spanish and Mixtec – Directed by Alfonso Cuarón
 Capernaum (Lebanon) in Arabic – Directed by Nadine Labaki

Cold War (Poland) in Polish and French – Directed by Paweł Pawlikowski

Never Look Away (Germany) in German – Directed by Florian Henckel von Donnersmarck

Shoplifters (Japan) in Japanese – Directed by Hirokazu Kore-eda






 Best Documentary – Feature
 Free Solo – Elizabeth Chai Vasarhelyi, Jimmy Chin, Evan Hayes and Shannon Dill
 Hale County This Morning, This Evening – RaMell Ross, Joslyn Barnes and Su Kim

Minding the Gap – Bing Liu and Diane Quon

Of Fathers and Sons – Talal Derki, Ansgar Frerich, Eva Kemme and Tobias N. Siebert

RBG – Betsy West and Julie Cohen




 Best Documentary – Short Subject
 Period. End of Sentence. – Rayka Zehtabchi and Melissa Berton
 Black Sheep – Ed Perkins and Jonathan Chinn

End Game – Rob Epstein and Jeffrey Friedman

Lifeboat – Skye Fitzgerald and Bryn Mooser

A Night at the Garden – Marshall Curry






 Best Live Action Short Film
 Skin – Guy Nattiv and Jaime Ray Newman
 Detainment – Vincent Lambe and Darren Mahon

Fauve – Jérémy Comte and Maria Gracia Turgeon

Marguerite – Marianne Farley and Marie-Hélène Panisset

Mother – Rodrigo Sorogoyen and María del Puy Alvarado




 Best Animated Short Film
 Bao – Domee Shi and Becky Neiman-Cobb
 Animal Behaviour – Alison Snowden and David Fine

Late Afternoon – Louise Bagnall and Nuria González Blanco

One Small Step – Andrew Chesworth and Bobby Pontillas

Weekends – Trevor Jimenez






 Best Original Score
 Black Panther – Ludwig Göransson
 BlacKkKlansman – Terence Blanchard

If Beale Street Could Talk – Nicholas Britell

Isle of Dogs – Alexandre Desplat

Mary Poppins Returns – Marc Shaiman




 Best Original Song
 "Shallow" from A Star Is Born – Music and Lyrics by Lady Gaga, Mark Ronson, Anthony Rossomando and Andrew Wyatt
 "All the Stars" from Black Panther – Music by Mark "Sounwave" Spears, Kendrick Lamar and Anthony "Top Dawg" Tiffith; Lyrics by Kendrick Lamar, Anthony "Top Dawg" Tiffith and SZA

"I'll Fight" from RBG – Music and Lyrics by Diane Warren

"The Place Where Lost Things Go" from Mary Poppins Returns – Music by Marc Shaiman; Lyrics by Marc Shaiman and Scott Wittman

"When a Cowboy Trades His Spurs for Wings" from The Ballad of Buster Scruggs – Music and Lyrics by David Rawlings and Gillian Welch






 Best Sound Editing
 Bohemian Rhapsody – John Warhurst and Nina Hartstone
 Black Panther – Benjamin A. Burtt and Steve Boeddeker

First Man – Ai-Ling Lee and Mildred Iatrou Morgan

A Quiet Place – Ethan Van der Ryn and Erik Aadahl

Roma – Sergio Díaz and Skip Lievsay




 Best Sound Mixing
 Bohemian Rhapsody – Paul Massey, Tim Cavagin and John Casali
 Black Panther – Steve Boeddeker, Brandon Proctor and Peter J. Devlin

First Man – Jon Taylor, Frank A. Montaño, Ai-Ling Lee and Mary H. Ellis

Roma – Skip Lievsay, Craig Henighan and José Antonio García

A Star Is Born – Tom Ozanich, Dean Zupancic, Jason Ruder and Steve Morrow






 Best Production Design
 Black Panther – Production Design: Hannah Beachler; Set Decoration: Jay Hart
 The Favourite – Production Design: Fiona Crombie; Set Decoration: Alice Felton

First Man – Production Design: Nathan Crowley; Set Decoration: Kathy Lucas

Mary Poppins Returns – Production Design: John Myhre; Set Decoration: Gordon Sim

Roma – Production Design: Eugenio Caballero; Set Decoration: Bárbara Enríquez




 Best Cinematography
 Roma – Alfonso Cuarón
 Cold War – Łukasz Żal

The Favourite – Robbie Ryan

Never Look Away – Caleb Deschanel

A Star Is Born – Matthew Libatique






 Best Makeup and Hairstyling
 Vice – Greg Cannom, Kate Biscoe and Patricia Dehaney
 Border – Göran Lundström and Pamela Goldammer

Mary Queen of Scots – Jenny Shircore, Marc Pilcher and Jessica Brooks




 Best Costume Design
 Black Panther – Ruth E. Carter
 The Ballad of Buster Scruggs – Mary Zophres

The Favourite – Sandy Powell

Mary Poppins Returns – Sandy Powell

Mary Queen of Scots – Alexandra Byrne






 Best Film Editing
 Bohemian Rhapsody – John Ottman
 BlacKkKlansman – Barry Alexander Brown

The Favourite – Yorgos Mavropsaridis

Green Book – Patrick J. Don Vito

Vice – Hank Corwin




 Best Visual Effects
 First Man – Paul Lambert, Ian Hunter, Tristan Myles and J. D. Schwalm
 Avengers: Infinity War – Dan DeLeeuw, Kelly Port, Russell Earl and Dan Sudick

Christopher Robin – Christopher Lawrence, Michael Eames, Theo Jones and Chris Corbould

Ready Player One – Roger Guyett, Grady Cofer, Matthew E. Butler and David Shirk

Solo: A Star Wars Story – Rob Bredow, Patrick Tubach, Neal Scanlan and Dominic Tuohy




Governors Awards
 Main articles: Governors Awards and Irving G. Thalberg Memorial Award
The Academy held its 10th annual Governors Awards ceremony on November 18, 2018, where the following awards were presented:[14]

Academy Honorary Awards
 Main article: Academy Honorary Award
 Cicely Tyson – American actress[15]

Lalo Schifrin – Argentine-American composer[16]

Marvin Levy – American publicist[17]

Irving G. Thalberg Memorial Award
 Main article: Irving G. Thalberg Memorial Award
 Kathleen Kennedy – American producer[18]

Frank Marshall – American producer[19]

Films that received multiple awards and nominations
At the 91st Academy Awards, 52 films received 121 nominations. Of these, 15 films won 24 Academy Awards Merit.

 

Films with multiple nominations


Nominations

Film



10

The Favourite



Roma



8

A Star Is Born



Vice



7

Black Panther



6

BlacKkKlansman



5

Bohemian Rhapsody



Green Book



4

First Man



Mary Poppins Returns



3

The Ballad of Buster Scruggs



Can You Ever Forgive Me?



Cold War



If Beale Street Could Talk



2

Isle of Dogs



Mary Queen of Scots



Never Look Away



RBG


 

Films with multiple awards


Wins

Film



4

Bohemian Rhapsody



3

Black Panther



Green Book



Roma


 
Presenters and performers
The following individuals, listed in order of appearance, presented awards or performed musical performances.[20][21][22][23][24][25][26]

Presenters



Name(s)
Role



Randy Thomas
Announcer



 
 Tina Fey

Amy Poehler

Maya Rudolph



Presenters: Best Supporting Actress



 
 Helen Mirren

Jason Momoa



Presenters: Best Documentary Feature



Tom Morello
Presenter: Vice as Best Picture nominee



 
 Elsie Fisher

Stephan James



Presenters: Best Makeup and Hairstyling



 
 Brian Tyree Henry

Melissa McCarthy



Presenters: Best Costume Design



 
 Chris Evans

Jennifer Lopez



Presenters: Best Production Design



Tyler Perry
Presenter: Best Cinematography



Emilia Clarke
Presenter: "I'll Fight" as Best Original Song nominee



Serena Williams
Presenter: A Star Is Born as Best Picture nominee



 
 Danai Gurira

James McAvoy



Presenters: Best Sound Editing and Best Sound Mixing



Queen Latifah
Presenter: The Favourite as Best Picture nominee



 
 Javier Bardem

Angela Bassett



Presenters: Best Foreign Language Film



Keegan-Michael Key
Presenter: "The Place Where Lost Things Go" as Best Original Song nominee



Trevor Noah
Presenter: Black Panther as Best Picture nominee



Michael Keaton
Presenter: Best Film Editing



 
 Daniel Craig

Charlize Theron



Presenters: Best Supporting Actor



Laura Dern
Presenter: Academy Museum of Motion Pictures segment



 
 Pharrell Williams

Michelle Yeoh



Presenters: Best Animated Feature Film



Kacey Musgraves
Presenter: "When a Cowboy Trades His Spurs for Wings" as Best Original Song nominee



 
 Dana Carvey

Mike Myers



Presenters: Bohemian Rhapsody as Best Picture nominee



 
 Awkwafina

John Mulaney



Presenters: Best Animated Short Film and Best Documentary Short Subject



 
 José Andrés

Diego Luna



Presenters: Roma as Best Picture nominee



 
 Sarah Paulson

Paul Rudd



Presenters: Best Visual Effects



 
 KiKi Layne

Krysten Ritter



Presenters: Best Live Action Short Film



 
 Samuel L. Jackson

Brie Larson



Presenters: Best Original Screenplay and Best Adapted Screenplay



 
 Michael B. Jordan

Tessa Thompson



Presenters: Best Original Score



 
 Chadwick Boseman

Constance Wu



Presenters: Best Original Song



John Bailey (AMPAS president)

Presenter: In Memoriam tribute



Barbra Streisand
Presenter: BlacKkKlansman as Best Picture nominee



 
 Allison Janney

Gary Oldman



Presenters: Best Actor



 
 John Lewis

Amandla Stenberg



Presenters: Green Book as Best Picture nominee



 
 Frances McDormand

Sam Rockwell



Presenters: Best Actress



Guillermo del Toro
Presenter: Best Director



Julia Roberts
Presenter: Best Picture


Performers



Name(s)
Role
Performed



Rickey Minor
Musical arranger and conductor
Orchestral



Queen + Adam Lambert
Performers
"We Will Rock You" and "We Are the Champions"



Jennifer Hudson
Performer
"I'll Fight" from RBG



Bette Midler
Performer
"The Place Where Lost Things Go" from Mary Poppins Returns



 
 David Rawlings

Gillian Welch



Performers
"When a Cowboy Trades His Spurs for Wings" from The Ballad of Buster Scruggs



 
 Bradley Cooper

Lady Gaga



Performers
"Shallow" from A Star Is Born



Los Angeles Philharmonic
Performers
"Leaving Home" during the annual In Memoriam tribute



Ceremony information
Due to the last two ceremonies' mixed receptions and declining ratings (with the previous ceremony being the least viewed in the history of the Academy Awards), producers Michael De Luca and Jennifer Todd declined to helm the 2019 awards, and were replaced by Donna Gigliotti and Glenn Weiss.[27][28]
In August 2018, the Academy announced plans to add a new category honoring achievement in "Popular Film". The proposal was met with wide criticism, as the award's implied focus on blockbuster films was considered to be demeaning towards artistic films and other non-mainstream pictures (with the award's title suggesting that such films were not "popular"), that it could diminish the chance for critically successful mainstream films to also be nominated for Best Picture (such as, in particular, Black Panther, although the Academy stated that a single film could be nominated in both categories),[29] and for being a ploy to boost ratings.[30][31][32][33] The Academy announced the following month that it would postpone the new category in order to seek additional input.[34] Academy president John Bailey admitted that the proposed category was intended to help improve viewership, and noted that the concept of a separate award for commercial film dates back to the 1st Academy Awards (which had separate categories for "Outstanding Picture" and "Best Unique and Artistic Picture").[35]
In January 2019, it was reported that as part of an effort to shorten the ceremony, only two of the nominees for Best Original Song ("All the Stars" and "Shallow") would be performed live.[36] After a negative reaction from audiences and industry musicians, including Lin-Manuel Miranda and members of the music branch,[37] the Academy backtracked and announced that all five songs would be performed. However, "All the Stars" was not performed, with Variety citing "logistics and timing" issues with its performers.[38][39]
The following month, the Academy announced that the presentation of the awards for Best Cinematography, Best Live Action Short Film, Best Film Editing, and Best Makeup and Hairstyling—would occur during commercial breaks. They said that these presentations would be streamed so viewers could watch them live online, and that the winners' acceptance speeches would be replayed later in the broadcast. The decision received extensive backlash from audiences, and from filmmakers including Guillermo del Toro, Christopher Nolan, Martin Scorsese, Quentin Tarantino, Damien Chazelle, Spike Lee, Joe Dante and Alfonso Cuarón (the latter of whom was nominated and won in one of the aforementioned categories).[40][41] Four days later, the Academy reversed the decision and announced that all 24 categories would be presented live.[42]
ABC scheduled a "sneak peek" of its new dramedy Whiskey Cavalier after the ceremony and late local news, ahead of its premiere the following Wednesday.[43]

Box office performance of nominated films

North American Box Office Gross for Best Picture Nominees[44]


Film

Pre-nomination   (before January 22)

Post-nomination   (January 22 – February 24)

Post-awards   (after February 24)

Total



Black Panther

$700.1 million

N/A

N/A

$700.1 million



Bohemian Rhapsody

$202.5 million

$10.6 million

$3.1 million

$216.2 million



A Star Is Born

$204.8 million

$6 million

$4.4 million

$215.2 million



Green Book

$42.5 million

$27.2 million

$15.2 million

$84.8 million



BlacKkKlansman

$48.5 million

$806,590

N/A

$49.3 million



Vice

$39.5 million

$7.6 million

$719,876

$47.8 million



The Favourite

$23 million

$9.1 million

$2.1 million

$34.4 million



Roma[a]

N/A

N/A

N/A

N/A



Total:

$1.278 billion

$60.8 million

$25.6 million

$1.348 billion



Average:

$157.6 million

$8.7 million

$3.7 million

$192.6 million


At the time of the nominations announcement on January 22, 2019, the combined North American box office gross of seven of the eight[a] Best Picture nominees was $1.261 billion, the highest total for Best Picture nominees since the 83rd Academy Awards in 2011.[45][46] The average per-film gross was $157 million, although only three films (Black Panther, A Star Is Born and Bohemian Rhapsody) had actually made over $50 million before the announcement.
Thirty-two nominations went to 12 films on the list of the year's 50 top-grossing movies. Only Black Panther (1st), Incredibles 2 (3rd), Bohemian Rhapsody (12th), A Star Is Born (13th), Ralph Breaks the Internet (14th), Spider-Man: Into the Spider-Verse (18th) and Green Book (46th) were nominated for Best Picture, Best Animated Feature or any of the directing, acting or screenwriting awards. The other top 50 box-office hits that earned nominations were Avengers: Infinity War (2nd), Solo: A Star Wars Story (10th), A Quiet Place (15th), Mary Poppins Returns (19th), Ready Player One (24th), and Christopher Robin (34th).

Host selection
The 90th Academy Awards in 2018 had the lowest Nielsen ratings of all time, with less than half of the 57.25 million viewers of the 70th in 1998. In October 2018, the Academy asked Dwayne Johnson to host the 91st ceremony, believing that the popularity of Hollywood's highest-paid actor would help increase the audience. Johnson immediately began planning what he described as an "audience first" show, but could not change his schedule of filming Hobbs & Shaw and Jumanji: The Next Level.[47]
After considering using one host for each of the three hours,[47] on December 4, 2018, the Academy announced that Kevin Hart would host the ceremony.[48] A controversy emerged when past jokes and comments made by Hart were found to contain anti-gay slurs and language; Hart withdrew from hosting duties on December 6, saying he did not want to be a "distraction" to the ceremony.[49][50] Previous Oscar hosts such as Seth MacFarlane, Ellen DeGeneres, Neil Patrick Harris, Chris Rock, and Jimmy Kimmel expressed no interest in hosting the show.[51]
On January 9, 2019, it was reported that the Academy planned to hold the ceremony without a host, and instead have selected presenters introduce segments and awards. No replacement host was announced, and it became the first ceremony without a designated host since the 61st Academy Awards in 1989.[52] In place of an opening monologue, the ceremony opened with a performance of "We Will Rock You" and "We Are the Champions" by Queen and Adam Lambert, representing nominated film Bohemian Rhapsody. The new format also led to a ceremony that was shorter than previous years, only lasting 3 hours and 23 minutes. Citing its success, it was announced that the 92nd Academy Awards in 2020 would continue using a hostless format.[53][54]

Ratings and reception
Media publications responded more positively to the show than those in recent years, with many praising the no-host approach.[55] On review aggregator website Rotten Tomatoes, the show holds an approval rating of 70% based on 40 critics, and summarized, "Strong musical performances, a steady, somewhat sluggish pace, and a few genuinely surprising moments helped the hostless 91st Oscars create an entertainingly efficient -- if not entirely satisfying -- ceremony."[56]
The American telecast drew 29.6 million U.S. viewers, a 12% increase in viewership over the 2018 ceremony (which are the lowest-rated Academy Awards to date). The show also drew a 7.7 rating for the 18–49 demographic.[57][58]

In Memoriam
The annual In Memoriam segment was introduced by the President of the Academy John Bailey with Gustavo Dudamel conducting the Los Angeles Philharmonic on the notes of the "Leaving Home" theme from Superman by John Williams.[59]
The segment paid tribute to the following 50 artists:

 
 Susan Anspach – actress

Ermanno Olmi – director, writer

Richard Greenberg – title designer, visual effects

John N. Carter – film editor

John Morris – composer

Bernardo Bertolucci – director, writer

Michel Legrand – composer

Margot Kidder – actress

Alixe Gordin – casting director

Neil Simon – writer

Richard H. Kline – cinematographer

Vittorio Taviani – director, writer

Elizabeth Sung – actress

Françoise Bonnot – film editor

Burt Reynolds – actor, director

Kitty O'Neil – stunt performer

Pablo Ferro – title designer, graphic artist

Samuel Hadida – producer, distributor, executive

Raymond Chow – producer, executive

Pierre Rissient – festival selector, publicist, distributor, producer

Anne V. Coates – film editor

Paul Bloch – publicist

Shinobu Hashimoto – writer

Richard Marks – film editor

Stéphane Audran – actress

Robby Müller – cinematographer

Craig Zadan – producer

Barbara Harris – actress

Claude Lanzmann – documentarian, director

Martin Bregman – producer, manager

Nelson Pereira dos Santos – director

Will Vinton – animator

Miloš Forman – director

Witold Sobociński – cinematographer

Daniel C. Striepeke – make-up artist

Penny Marshall – director, producer, actress

Isao Takahata – animation director

Stephen Vaughan – still photographer

Stan Lee – comic book writer, executive producer

William Goldman – writer

John M. Dwyer – set decorator

Tab Hunter – actor

Yvonne Blake – costume designer

Nicolas Roeg – director, cinematographer

James Karen – actor

Gregg Rudloff – sound mixer

Gloria Katz – writer, producer

Bruno Ganz – actor

Audrey Wells – writer, director

Albert Finney – actor


See also
 
 
Film portal

 46th Annie Awards

72nd British Academy Film Awards

44th César Awards

24th Critics' Choice Awards

46th Daytime Emmy Awards

31st European Film Awards

76th Golden Globe Awards

39th Golden Raspberry Awards

22nd Hollywood Film Awards

34th Independent Spirit Awards

71st Primetime Emmy Awards

23rd Satellite Awards

45th Saturn Awards

25th Screen Actors Guild Awards

73rd Tony Awards

List of submissions to the 91st Academy Awards for Best Foreign Language Film

Notes

References

External links
Official websites

 Academy Awards official website

The Academy of Motion Picture Arts and Sciences official website

91st Annual Academy Awards of Merit for Achievements During 2018 – 91st Oscars Rules

Oscar's Channel at YouTube (run by the Academy of Motion Picture Arts and Sciences)

News resources

 Oscars 2018 at BBC News

Oscars 2019 at The Guardian

   v
t
e
 Academy Awards
 
 Academy of Motion Picture Arts and Sciences (AMPAS)

 Records
 most wins per ceremony


Oscar season

Oscar speech

Oscar bait

Governors Awards

Nicholl Fellowships in Screenwriting

Pre-show


Awards of Merit 
 Best Picture

Director

Actor

Actress

Supporting Actor

Supporting Actress

Adapted Screenplay

Original Screenplay

Animated Feature

Documentary Feature

International Feature Film

Animated Short Film

Documentary Short Subject

Live Action Short Film

Cinematography

Costume Design

Film Editing

Makeup and Hairstyling

Original Score

Original Song

Production Design

Sound

Visual Effects

Proposed awards 
 Popular Film


 

Special awards Governors Awards 
 Academy Honorary Award

Irving G. Thalberg Memorial Award

Jean Hersholt Humanitarian Award

Special Achievement Academy Award


Academy Scientific and Technical Awards 
 Academy Award of Merit (non-competitive)

Scientific and Engineering Award

Technical Achievement Award

John A. Bonner Medal of Commendation

Gordon E. Sawyer Award


Student Awards 
 Student Academy Award


 
Former awards Merit Awards 
 Assistant Director

Dance Direction

Sound Editing

Story


Special Awards 
 Academy Juvenile Award


 
Ceremonies‡ 
 1927/28

1928/29

1929/30

1930/31

1931/32

1932/33

1934

1935

1936

1937

1938

1939

1940

1941

1942

1943

1944

1945

1946

1947

1948

1949

1950

1951

1952

1953

1954

1955

1956

1957

1958

1959

1960

1961

1962

1963

1964

1965

1966

1967

1968

1969

1970

1971

1972

1973

1974

1975

1976

1977

1978

1979

1980

1981

1982

1983

1984

1985

1986

1987

1988

1989

1990

1991

1992

1993

1994

1995

1996

1997

1998

1999

2000

2001

2002

2003

2004

2005

2006

2007

2008

2009

2010

2011

2012

2013

2014

2015

2016

2017

2018

2019

2020/21


 
 ‡ Dates and years listed for each ceremony were the eligibility period of film release in Los Angeles County. For the first five ceremonies, the eligibility period was done on a seasonal basis, from August to July. For the 6th ceremony, held in 1934, the eligibility period lasted from August 1, 1932, to December 31, 1933. From the 7th ceremony, held in 1935, through the 92nd ceremony, held in 2020, the period of eligibility became the full previous calendar year from January 1 to December 31. For the 93rd ceremony, scheduled to be held in 2021, the eligibility period will last from January 1, 2020, to February 28, 2021.


 
  Book

 Category







