  American actor, comedian, screenwriter, and producer


 Adam Sandler
 Sandler in 2018
Born Adam Richard Sandler  (1966-09-09) September 9, 1966 (age 53)  New York City, New York, U.S.
Alma materNew York University
Occupation 
 Actor

comedian

screenwriter

producer

singer-songwriter


Years active1987–present[1]
Political partyRepublican
Spouse(s) Jacqueline Titone (m. 2003)
Children2
Comedy career
MediumStand-up, film, television
Websiteadamsandler.com


Adam Richard Sandler (born September 9, 1966) is an American actor, comedian, screenwriter, film producer, and singer-songwriter. After becoming a Saturday Night Live cast member, he went on to star in many Hollywood feature films, which have combined grossed over $2 billion at the box office.[2][3]
Sandler is best known for his comedic roles including Billy Madison (1995), Happy Gilmore (1996), The Waterboy (1998), The Wedding Singer (1998), Big Daddy (1999), Mr. Deeds (2002), 50 First Dates (2004), The Longest Yard (2005), Click (2006), Grown Ups (2010), Just Go with It (2011), Grown Ups 2 (2013), Blended (2014) and Murder Mystery (2019). He also voices Dracula in the Hotel Transylvania franchise (2012–present).
Some of his films, such as the widely panned Jack and Jill, have been heavily criticized, culminating in a shared second place in the number of Golden Raspberry Awards (three) and Raspberry Award nominations (11), in both cases second only to Sylvester Stallone.
Though he is associated predominantly with broad comedies, Sandler has consistently earned critical praise for his occasional dramatic performances in films such as Punch-Drunk Love (2002), Spanglish (2004), Reign Over Me (2007), Funny People (2009), The Meyerowitz Stories (New and Selected) (2017), and Uncut Gems (2019).[4]

  Contents
 
1 Early life

2 Career
 
2.1 Acting career

2.2 Happy Madison Productions




3 Public image

4 Personal life

5 Filmography

6 Discography
 
6.1 Albums

6.2 Singles




7 Awards and nominations

8 References

9 External links




Early life
Sandler was born in Brooklyn, New York, on September 9, 1966,[5][6][7] to Judith "Judy" (née Levine), a nursery school teacher, and Stanley Sandler, an electrical engineer.[5] His family is Jewish and descends from Russian Jewish immigrants on both sides.[8][9][10][11][12][13] His patrilineal great-grandfather, Joseph Jacob Sandler, was born in  Pagėgiai Municipality, Tauragė County, Lithuania.[14] Sandler grew up in Manchester, New Hampshire, after moving there at the age of six.[15] He attended Manchester Central High School. As a teen, Sandler was in BBYO, a Jewish youth group. Sandler graduated from New York University's Tisch School of the Arts in 1988.[16]

Career
Acting career
      Sandler at 2002 Cannes Film Festival
Early in his career, in 1987, Sandler played Theo Huxtable's friend, Smitty, in The Cosby Show and the Stud Boy or Trivia Delinquent in the MTV game show Remote Control. After his film debut Going Overboard in 1989, Sandler performed in comedy clubs, having first taken the stage at his brother's urging when he was 17. He was discovered by comedian Dennis Miller, who caught Sandler's act in Los Angeles and recommended him to Saturday Night Live producer Lorne Michaels. Sandler was hired as a writer for SNL in 1990 and became a featured player the following year, making a name for himself by performing amusing original songs on the show, including "The Thanksgiving Song" and "The Chanukah Song".[6] Sandler told Conan O'Brien on The Tonight Show that NBC fired him and Chris Farley from the show in 1995, and played this up in his return to the show as a host in 2019.[17][18]
In 1993, Adam Sandler appeared in the film Coneheads with Chris Farley, David Spade, Dan Aykroyd, Phil Hartman, and Jane Curtin. In 1994, he co-starred in Airheads with Brendan Fraser and Steve Buscemi. He starred in Billy Madison (1995) playing a grown man repeating grades 1–12 to earn back his father's respect and the right to inherit his father's multimillion-dollar hotel empire. The film was successful at the box office despite negative reviews. He followed this film with Bulletproof (1996), and the financially successful comedies Happy Gilmore (1996) and The Wedding Singer (1998). He was initially cast in the bachelor party–themed comedy/thriller Very Bad Things (1998) but had to back out due to his involvement in The Waterboy (1998), one of his first hits.
Although his earliest films did not receive favorable critical attention, he started to receive more positive reviews, beginning with Punch-Drunk Love in 2002. Roger Ebert's review of Punch-Drunk Love concluded that Sandler had been wasted in earlier films with poorly written scripts and characters with no development.[19] Sandler has moved outside the genre of slapstick comedy to take on more serious roles, such as the aforementioned Punch-Drunk Love, for which he was nominated for a Golden Globe, and Mike Binder's Reign Over Me (2007), a drama about a man who loses his entire family during the September 11 attacks, and then struggles to rekindle a friendship with his old college roommate (Don Cheadle).
He starred alongside friend Kevin James in the film I Now Pronounce You Chuck and Larry (2007), and headlined You Don't Mess with the Zohan (2008). The latter film was written by Sandler, Judd Apatow, and Robert Smigel, and directed by Dennis Dugan.

      Sandler in Berlin in 2009
Sandler starred along with Keri Russell and English comedian Russell Brand in Adam Shankman's fantasy film Bedtime Stories (2008), as a stressed hotel maintenance worker whose bedtime stories he reads to his niece and nephew begin to come true. It marked as Sandler's first family film and first film under the Walt Disney banner.[20]
In 2009, Sandler starred in Judd Apatow's third directorial feature Funny People. The film was released on July 31, 2009.[21] Following the release of Funny People, it, along with Punch-Drunk Love were cited in the June 2010 announcement that Sandler was one of 135 people (including 20 actors) invited to join the Academy of Motion Picture Arts and Sciences.[22]
Sandler appeared in Grown Ups, alongside Kevin James, Chris Rock, Rob Schneider, and David Spade. Sandler and Dickie Roberts scribe Fred Wolf wrote the script and Dennis Dugan directed the film.[23]
Sandler starred with Jennifer Aniston in the 2011 romantic comedy film Just Go with It. Sandler also voiced a capuchin monkey in Kevin James's Zookeeper, released on July 8, 2011. In 2012, he starred in That's My Boy, as a man who fathered a son (Andy Samberg) with his teacher in high school.
Sandler starred with Drew Barrymore in the Warner Bros. romantic comedy Blended, which was filmed in South Africa, and was released on May 23, 2014.
In 2013, he guest starred in the Disney Channel Original Series Jessie as himself. He and Cameron Boyce previously worked together in Grown Ups and Grown Ups 2. The episode is titled "Punched Dumped Love". Sandler co-starred in the drama film Men, Women & Children (2014), directed by Jason Reitman.[24][25] He was considered for the voice of Rocket Raccoon in Marvel's Guardians of the Galaxy but Bradley Cooper was cast instead.[26]
Sandler's recent comedy films, including Grown Ups and Grown Ups 2, have received strongly negative reviews.[27] In reviewing the latter, critic Mark Olsen of The Los Angeles Times remarked that Sandler had become the antithesis of Judd Apatow; he was instead "the white Tyler Perry: smart enough to know better, savvy enough to do it anyway, lazy enough not to care."[28]
In 2015, Sandler released his last theatrical film Pixels, based on French director Patrick Jean's 2010 short film of the same name, before transitioning into a distribution deal with Netflix. His first original film for Netflix was the Western comedy film The Ridiculous 6. Despite being universally panned by critics,[29] on January 6, 2016, it was announced by Netflix that the film had been viewed more times in 30 days than any other movie in Netflix history.[30] He released three additional films on Netflix between 2016 and 2018: The Do-Over, Sandy Wexler, The Week Of and Murder Mystery.
He returned to dramatic territory in 2017 with The Meyerowitz Stories, with his performance receiving favorable notices from critics.[31]
On May 4, 2019, Sandler made his first appearance as host of SNL, ending the episode with a tribute to his friend and fellow former cast member Chris Farley.[32]
In December 2019, Sandler starred in the crime thriller drama Uncut Gems, directed by the Safdie brothers.[33] The movie and Sandler's acting received acclaim and many end of year awards from critics, who noted this role as a career best for Sandler.[34][35]

Happy Madison Productions
      Sandler at a press conference for Click in 2005
Sandler formed his film production company, Happy Madison Productions,[36] in 1999, first producing fellow SNL alumnus Rob Schneider's film Deuce Bigalow: Male Gigolo. The company has produced most of Sandler's subsequent films to date. The majority of the company's films have received negative reviews from critics, with three considered to be among the worst ever made[37] yet most have performed well at box office.
Others who frequently appear in Sandler films include David Spade, Kevin James, Steve Buscemi, Chris Rock, John Turturro, Peter Dante, Allen Covert, Jonathan Loughran, and Jon Lovitz.
In 2014, Netflix announced a four-movie deal with Adam Sandler and Happy Madison Productions.[38] In January 2020, Netflix announced a new four-movie deal worth up to $275 million dollars.[39]
The company is located on the Sony/Columbia Pictures lot in Culver City, California.

Public image
Sandler has been referenced multiple times in various media, including in the TV shows The Simpsons in the episode "Monty Can't Buy Me Love",[40] in Family Guy in the episode "Stew-Roids",[41] and in South Park in the episode "You're Getting Old".[42] He was also referenced in the video game Half-Life: Opposing Force.[43] The HBO series Animals episode "The Trial" features a mock court case to decide whether Sandler or Jim Carrey is a better comedian.

Personal life
On  June 22, 2003, Sandler married model-actress Jacqueline Titone.[44] Titone had converted to Sandler's religion, Judaism.[45] She makes frequent appearances in her husband's films. The couple have two daughters, Sadie born in May 2006,[46] and Sunny born in November 2008,[47] who also make frequent appearances in his films.
Sandler is a supporter of the Republican Party.[48] In 2007, Sandler made a $1 million donation to the Boys & Girls Clubs of America in his hometown, Manchester, New Hampshire.[49] The same year, he donated $2,100 to former New York City mayor Rudy Giuliani's presidential campaign.

Filmography
This is a partial list of Sandler's film and television work. For the complete list, see Adam Sandler filmography.

 
 Going Overboard (1989)

Saturday Night Live (1990–1995)

Coneheads (1993)

Airheads (1994)

Billy Madison (1995)

Happy Gilmore (1996)

The Wedding Singer (1998)

The Waterboy (1998)

Big Daddy (1999)

Little Nicky (2000)

Punch-Drunk Love (2002)

Mr. Deeds (2002)

Anger Management (2003)

50 First Dates (2004)

The Longest Yard (2005)

Click (2006)

Reign Over Me (2007)

I Now Pronounce You Chuck and Larry (2007)

You Don't Mess with the Zohan (2008)

Bedtime Stories (2008)

Funny People (2009)

Grown Ups (2010)

Just Go with It (2011)

Jack and Jill (2011)

Hotel Transylvania (2012)

Grown Ups 2 (2013)

Blended (2014)

Pixels (2015)

Hotel Transylvania 2 (2015)

The Ridiculous 6 (2015)

The Meyerowitz Stories (New and Selected) (2017)

Hotel Transylvania 3: Summer Vacation (2018)

Murder Mystery (2019)

Uncut Gems (2019)


Discography
      Sandler's handprints and shoeprints in front of Grauman's Chinese Theatre, 2008
Albums



Year

Title

Certification



1993

They're All Gonna Laugh at You!

2× Platinum[50]



1996

What the Hell Happened to Me?

2× Platinum[50]



1997

What's Your Name?

Gold[50]



1999

Stan and Judy's Kid

Gold[50]



2004

Shhh...Don't Tell





"The Peeper" was made into a flash cartoon, launched over the 1999 Labor Day weekend as a promotion for Stan and Judy's Kid and was watched by over 1 million users during that period, one of the most watched video clips on the internet at the time.[51]
In 2009 Sandler contributed the Neil Young cover "Like a Hurricane" to Covered, A Revolution in Sound as part of Warner Brothers 50th Anniversary celebrations;[52] the song was performed on the David Letterman Show with a band that included, among others, Waddy Wachtel,[53] who has appeared with Sandler on a number of occasions.[54]

Singles




Title

Year

Peak chart positions

Certifications




Hot 100 [55]

Adult Pop [56]

Mod. Rock [57]



"The Chanukah Song"

1996

80
28
25


 RIAA: Gold[58]




"The Thanksgiving Song"

1997

—
40
29




Awards and nominations
 Main article: List of awards and nominations received by Adam Sandler
References

External links



Wikiquote has quotations related to: Adam Sandler





Wikimedia Commons has media related to Adam Sandler.


 Official website

Adam Sandler on IMDb



   v
t
e
 Adam Sandler
Studio albums 
 They're All Gonna Laugh at You! (1993)

What the Hell Happened to Me? (1996)

What's Your Name? (1997)

Stan and Judy's Kid (1999)

Shhh...Don't Tell (2004)


Songs 
 "The Thanksgiving Song"

"The Chanukah Song"


Skits 
 "Gay Robot"


Related 
 Filmography

List of awards and nominations received by Adam Sandler

Happy Madison Productions



  Awards for Adam Sandler
 
   v
t
e
 Boston Society of Film Critics Award for Best Actor
 
 Robert De Niro (1980)

Burt Lancaster (1981)

Dustin Hoffman (1982)

Eric Roberts (1983)

Haing S. Ngor (1984)

Jack Nicholson (1985)

Bob Hoskins (1986)

Albert Brooks (1987)

Daniel Day-Lewis (1988)

Daniel Day-Lewis (1989)

Jeremy Irons (1990)

Nick Nolte (1991)

Denzel Washington (1992)

Daniel Day-Lewis (1993)

Albert Finney (1994)

Nicolas Cage (1995)

Geoffrey Rush (1996)

Al Pacino (1997)

Brendan Gleeson (1998)

Jim Carrey (1999)

Colin Farrell (2000)

Brian Cox / Denzel Washington (2001)

Adrien Brody (2002)

Bill Murray (2003)

Jamie Foxx (2004)

Philip Seymour Hoffman (2005)

Forest Whitaker (2006)

Frank Langella (2007)

Sean Penn / Mickey Rourke (2008)

Jeremy Renner (2009)

Jesse Eisenberg (2010)

Brad Pitt (2011)

Daniel Day-Lewis (2012)

Chiwetel Ejiofor (2013)

Michael Keaton (2014)

Paul Dano / Leonardo DiCaprio (2015)

Casey Affleck (2016)

Daniel Kaluuya (2017)

John C. Reilly (2018)

Adam Sandler (2019)



   v
t
e
 Golden Raspberry Award for Worst Screenplay
1980–2000 
 Can't Stop the Music – Bronte Woodard and Allan Carr (1980)

Mommie Dearest – Frank Yablans, Frank Perry, Tracy Hotchner and Robert Getchell (1981)

Inchon – Robin Moore and Laird Koenig (1982)

The Lonely Lady – John Kershaw, Shawn Randall and Ellen Shephard (1983)

Bolero – John Derek (1984)

Rambo: First Blood Part II – Sylvester Stallone, James Cameron and Kevin Jarre (1985)

Howard the Duck – Willard Huyck and Gloria Katz (1986)

Leonard Part 6 – Jonathan Reynolds and Bill Cosby (1987)

Cocktail – Heywood Gould (1988)

Harlem Nights – Eddie Murphy (1989)

The Adventures of Ford Fairlane – Daniel Waters, James Cappe & David Arnott (1990)

Hudson Hawk – Steven E. de Souza, Daniel Waters, Bruce Willis and Robert Kraft (1991)

Stop! Or My Mom Will Shoot – Blake Snyder, William Osborne and William Davies – (1992)

Indecent Proposal – Amy Holden Jones (1993)

The Flintstones – Jim Jennewein, Steven E. de Souza, Tom S. Parker and various others  (1994)

Showgirls – Joe Eszterhas (1995)

Striptease – Andrew Bergman (1996)

The Postman – Eric Roth and Brian Helgeland (1997)

An Alan Smithee Film: Burn Hollywood Burn – Joe Eszterhas (1998)

Wild Wild West – Jim Thomas, John Thomas, S. S. Wilson, Brent Maddock, Jeffrey Price and Peter S. Seaman (1999)

Battlefield Earth – Corey Mandell and J. David Shapiro (2000)


2001–present 
 Freddy Got Fingered – Tom Green & Derek Harvie (2001)

Star Wars: Episode II – Attack of the Clones – George Lucas and Jonathan Hales (2002)

Gigli – Martin Brest (2003)

Catwoman – Theresa Rebeck, John Brancato, Michael Ferris and John Rogers (2004)

Dirty Love – Jenny McCarthy (2005)

Basic Instinct 2 – Leora Barish and Henry Bean (2006)

I Know Who Killed Me – Jeffrey Hammond (2007)

The Love Guru – Mike Myers & Graham Gordy  (2008)

Transformers: Revenge of the Fallen – Ehren Kruger, Alex Kurtzman and Roberto Orci (2009)

The Last Airbender – M. Night Shyamalan (2010)

Jack and Jill – Steve Koren and Adam Sandler, story by Ben Zook (2011)

That's My Boy – David Caspe (2012)

Movie 43 – Steve Baker, Ricky Blitt, Will Carlough, Tobias Carlson, Jacob Fleisher, Patrik Forsberg, Will Graham, James Gunn, Claes Kjellstrom, Jack Kukoda, Bob Odenkirk, Bill O'Malley, Matthew Alec Portenoy, Greg Pritikin, Rocky Russo, Olle Sarri, Elizabeth Wright Shapiro, Jeremy Sosenko, Jonathan van Tulleken and Jonas Wittenmark (2013)

Saving Christmas – Darren Doane and Cheston Hervey (2014)

Fifty Shades of Grey - Kelly Marcel (2015)

Batman v Superman: Dawn of Justice – Chris Terrio and David S. Goyer (2016)

The Emoji Movie – Tony Leondis, Eric Siegel and Mike White (2017)

Fifty Shades Freed – Niall Leonard (2018)

Cats – Lee Hall and Tom Hooper (2019)



   v
t
e
 Golden Raspberry Award for Worst Screen Combo
Worst Screen Couple   (1994–2009) 
 Tom Cruise and Brad Pitt – Interview with the Vampire / Sylvester Stallone and Sharon Stone – The Specialist (1994)

Any combination of two people (or two body parts) – Showgirls (1995)

Demi Moore and Burt Reynolds – Striptease (1996)

Dennis Rodman and Jean-Claude Van Damme – Double Team (1997)

Leonardo DiCaprio and Leonardo DiCaprio (as twins) – The Man in the Iron Mask (1998)

Kevin Kline and Will Smith – Wild Wild West (1999)

John Travolta and anyone sharing the screen with him – Battlefield Earth (2000)

Tom Green and any animal he abuses – Freddy Got Fingered (2001)

Adriano Giannini and Madonna – Swept Away (2002)

Ben Affleck and Jennifer Lopez – Gigli (2003)

George W. Bush and either Condoleezza Rice or his pet goat – Fahrenheit 9/11 (2004)

Will Ferrell and Nicole Kidman – Bewitched (2005)

Shawn Wayans and either Kerry Washington or Marlon Wayans – Little Man (2006)

Lindsay Lohan and Lindsay Lohan (as twins) – I Know Who Killed Me (2007)

Paris Hilton and either Christine Lakin or Joel David Moore – The Hottie and the Nottie (2008)

Sandra Bullock and Bradley Cooper – All About Steve (2009)


Worst Screen Couple/Worst Screen Ensemble   (2010) 
 The entire cast of Sex and the City 2 (2010)


Worst Screen Couple   (2011–2012) 
 Adam Sandler and either Katie Holmes, Al Pacino or Adam Sandler – Jack and Jill (2011)

Mackenzie Foy and Taylor Lautner – The Twilight Saga: Breaking Dawn – Part 2 (2012)


Worst Screen Ensemble   (2011–2012) 
 The entire cast of Jack and Jill (2011)

The entire cast of The Twilight Saga: Breaking Dawn – Part 2 (2012)


Worst Screen Combo   (2013–present) 
 Jaden Smith and Will Smith on planet nepotism – After Earth (2013)

Kirk Cameron and his ego – Saving Christmas (2014)

Jamie Dornan and Dakota Johnson – Fifty Shades of Grey (2015)

Ben Affleck and Henry Cavill – Batman v Superman: Dawn of Justice (2016)

Any two obnoxious Emojis – The Emoji Movie (2017)

Donald Trump and "His Self Perpetuating Pettiness" – Death of a Nation and Fahrenheit 11/9 (2018)

Any two half-feline/half-human hairballs – Cats (2019)



   v
t
e
 Independent Spirit Award for Best Male Lead
 
 M. Emmet Walsh (1985)

James Woods (1986)

Dennis Quaid (1987)

Edward James Olmos (1988)

Matt Dillon (1989)

Danny Glover (1990)

River Phoenix (1991)

Harvey Keitel (1992)

Jeff Bridges (1993)

Samuel L. Jackson (1994)

Sean Penn (1995)

William H. Macy (1996)

Robert Duvall (1997)

Ian McKellen (1998)

Richard Farnsworth (1999)

Javier Bardem (2000)

Tom Wilkinson (2001)

Derek Luke (2002)

Bill Murray (2003)

Paul Giamatti (2004)

Philip Seymour Hoffman (2005)

Ryan Gosling (2006)

Philip Seymour Hoffman (2007)

Mickey Rourke (2008)

Jeff Bridges (2009)

James Franco (2010)

Jean Dujardin (2011)

John Hawkes (2012)

Matthew McConaughey (2013)

Michael Keaton (2014)

Abraham Attah (2015)

Casey Affleck (2016)

Timothée Chalamet (2017)

Ethan Hawke (2018)

Adam Sandler (2019)



   v
t
e
 MTV Movie Award for Best Comedic Performance
 
 Billy Crystal (1992)

Robin Williams (1993)

Robin Williams (1994)

Jim Carrey (1995)

Jim Carrey (1996)

Jim Carrey (1997)

Jim Carrey (1998)

Adam Sandler (1999)

Adam Sandler (2000)

Ben Stiller (2001)

Reese Witherspoon (2002)

Mike Myers (2003)

Jack Black (2004)

Dustin Hoffman (2005)

Steve Carell (2006)

Sacha Baron Cohen (2007)

Johnny Depp (2008)

Jim Carrey (2009)

Zach Galifianakis (2010)

Emma Stone (2011)

Melissa McCarthy (2012)

Jonah Hill (2014)

Channing Tatum (2015)

Ryan Reynolds (2016)

Lil Rel Howery (2017)

Tiffany Haddish (2018)

Dan Levy (2019)



   v
t
e
 MTV Movie Award for Best Fight
     
1996: Adam Sandler vs. Bob Barker – Happy Gilmore 
1997: Fairuza Balk vs. Robin Tunney – The Craft 
1998: Will Smith vs. Cockroach – Men in Black 
1999: Ben Stiller vs. Puffy the Dog – There's Something About Mary 
2000: Keanu Reeves vs. Laurence Fishburne – The Matrix 
2001: Zhang Ziyi vs. Entire bar – Crouching Tiger, Hidden Dragon 
2002: Jackie Chan and Chris Tucker vs. Hong Kong gang – Rush Hour 2 
2003: Yoda vs. Christopher Lee – Star Wars: Episode II – Attack of the Clones 
2004: Uma Thurman vs. Chiaki Kuriyama – Kill Bill: Volume 1 
2005: Uma Thurman vs. Daryl Hannah – Kill Bill: Volume 2 
2006: Angelina Jolie vs. Brad Pitt – Mr. & Mrs. Smith 
2007: Gerard Butler vs. Robert Maillet – 300 

 
2008: Sean Faris vs. Cam Gigandet – Never Back Down 
2009: Robert Pattinson vs. Cam Gigandet – Twilight 
2010: Beyoncé Knowles vs. Ali Larter – Obsessed 
2011: Robert Pattinson vs. Bryce Dallas Howard and Xavier Samuel – The Twilight Saga: Eclipse 
2012: Jennifer Lawrence and Josh Hutcherson vs. Alexander Ludwig – The Hunger Games 
2013: Robert Downey Jr., Chris Evans, Mark Ruffalo, Chris Hemsworth, Scarlett Johansson and Jeremy Renner vs. Tom Hiddleston – The Avengers 
2014: Orlando Bloom and Evangeline Lilly vs. Orcs – The Hobbit: The Desolation of Smaug 
2015: Dylan O'Brien vs. Will Poulter – The Maze Runner 
2016: Ryan Reynolds vs. Ed Skrein – Deadpool 
2018: Gal Gadot vs. German soldiers – Wonder Woman 
2019: Brie Larson vs. Gemma Chan – Captain Marvel 




   v
t
e
 MTV Movie Award for Best Kiss
 
 Anna Chlumsky & Macaulay Culkin in My Girl (1992)

Christian Slater & Marisa Tomei in Untamed Heart (1993)

Demi Moore & Woody Harrelson in Indecent Proposal (1994)

Jim Carrey & Lauren Holly in Dumb and Dumber (1995)

Natasha Henstridge & Anthony Guidera in Species (1996)

Will Smith & Vivica A. Fox in Independence Day (1997)

Adam Sandler & Drew Barrymore in The Wedding Singer (1998)

Gwyneth Paltrow & Joseph Fiennes in Shakespeare in Love (1999)

Sarah Michelle Gellar & Selma Blair in Cruel Intentions (2000)

Julia Stiles & Sean Patrick Thomas in Save the Last Dance (2001)

Jason Biggs & Seann William Scott in American Pie 2 (2002)

Tobey Maguire & Kirsten Dunst in Spider-Man (2003)

Owen Wilson, Carmen Electra & Amy Smart in Starsky & Hutch (2004)

Ryan Gosling & Rachel McAdams in The Notebook (2005)

Heath Ledger & Jake Gyllenhaal in Brokeback Mountain (2006)

Will Ferrell & Sacha Baron Cohen in Talladega Nights: The Ballad of Ricky Bobby (2007)

Briana Evigan & Robert Hoffman in Step Up 2: The Streets (2008)

Robert Pattinson & Kristen Stewart in Twilight (2009)

Robert Pattinson & Kristen Stewart in The Twilight Saga: New Moon (2010)

Robert Pattinson & Kristen Stewart in The Twilight Saga: Eclipse (2011)

Robert Pattinson & Kristen Stewart in The Twilight Saga: Breaking Dawn - Part 1 (2012)

Jennifer Lawrence & Bradley Cooper in Silver Linings Playbook (2013)

Emma Roberts, Jennifer Aniston & Will Poulter in We're the Millers (2014)

Ansel Elgort & Shailene Woodley in The Fault in Our Stars (2015)

Rebel Wilson & Adam DeVine in Pitch Perfect 2 (2016)

Ashton Sanders & Jharrel Jerome in Moonlight (2017)

Nick Robinson & Keiynan Lonsdale in Love, Simon (2018)

Noah Centineo & Lana Condor in To All the Boys I've Loved Before (2019)



   v
t
e
 MTV Movie Award for Best On-Screen Team
Best On-Screen Duo (1992–2000, 2013–2017) 
 Dana Carvey & Mike Myers (1992)

Mel Gibson & Danny Glover (1993)

Harrison Ford & Tommy Lee Jones (1994)

Sandra Bullock & Keanu Reeves (1995)

Chris Farley & David Spade (1996)

Nicolas Cage & Sean Connery (1997)

John Travolta & Nicolas Cage (1998)

Jackie Chan & Chris Tucker (1999)

Mike Myers & Verne Troyer (2000)

Mark Wahlberg & Seth MacFarlane (2013)

Vin Diesel & Paul Walker (2014)

Zac Efron & Dave Franco (2015)

Hugh Jackman & Dafne Keen (2017)


Best On-Screen Team (2001–2006, 2018) 
 Drew Barrymore, Cameron Diaz & Lucy Liu (2001)

Vin Diesel & Paul Walker (2002)

Sean Astin, Andy Serkis & Elijah Wood (2003)

Adam Sandler & Drew Barrymore (2004)

Lindsay Lohan, Rachel McAdams, Lacey Chabert & Amanda Seyfried (2005)

Vince Vaughn & Owen Wilson (2006)

Finn Wolfhard, Sophia Lillis, Jaeden Martell, Jack Dylan Grazer, Wyatt Oleff, Jeremy Ray Taylor & Chosen Jacobs (2018)


Best Cast (2012) 
 Daniel Radcliffe, Rupert Grint, Emma Watson & Tom Felton (2012)



   v
t
e
 National Board of Review Award for Best Actor
 
 Ray Milland (1945)

Laurence Olivier (1946)

Michael Redgrave (1947)

Walter Huston (1948)

Ralph Richardson (1949)

Alec Guinness (1950)

Richard Basehart (1951)

Ralph Richardson (1952)

James Mason (1953)

Bing Crosby (1954)

Ernest Borgnine (1955)

Yul Brynner (1956)

Alec Guinness (1957)

Spencer Tracy (1958)

Victor Sjöström (1959)

Robert Mitchum (1960)

Albert Finney (1961)

Jason Robards (1962)

Rex Harrison (1963)

Anthony Quinn (1964)

Lee Marvin (1965)

Paul Scofield (1966)

Peter Finch (1967)

Cliff Robertson (1968)

Peter O'Toole (1969)

George C. Scott (1970)

Gene Hackman (1971)

Peter O'Toole (1972)

Al Pacino / Robert Ryan (1973)

Gene Hackman (1974)

Jack Nicholson (1975)

David Carradine (1976)

John Travolta (1977)

Jon Voight / Laurence Olivier (1978)

Peter Sellers (1979)

Robert De Niro (1980)

Henry Fonda (1981)

Ben Kingsley (1982)

Tom Conti (1983)

Victor Banerjee (1984)

William Hurt / Raul Julia (1985)

Paul Newman (1986)

Michael Douglas (1987)

Gene Hackman (1988)

Morgan Freeman (1989)

Robert De Niro / Robin Williams (1990)

Warren Beatty (1991)

Jack Lemmon (1992)

Anthony Hopkins (1993)

Tom Hanks (1994)

Nicolas Cage (1995)

Tom Cruise (1996)

Jack Nicholson (1997)

Ian McKellen (1998)

Russell Crowe (1999)

Javier Bardem (2000)

Billy Bob Thornton (2001)

Campbell Scott (2002)

Sean Penn (2003)

Jamie Foxx (2004)

Philip Seymour Hoffman (2005)

Forest Whitaker (2006)

George Clooney (2007)

Clint Eastwood (2008)

George Clooney / Morgan Freeman (2009)

Jesse Eisenberg (2010)

George Clooney (2011)

Bradley Cooper (2012)

Bruce Dern (2013)

Michael Keaton / Oscar Isaac (2014)

Matt Damon (2015)

Casey Affleck (2016)

Tom Hanks (2017)

Viggo Mortensen (2018)

Adam Sandler (2019)



   v
t
e
 Teen Choice Awards for Choice Hissy Fit
 
 Sandra Bullock (1999)

Lisa Kudrow (2000)

Jim Carrey (2001)

Ben Stiller (2002)

Adam Sandler (2003)

Lindsay Lohan (2004)	

Jon Heder (2005)

Keira Knightley (2006)

Ryan Seacrest (2007)

None (2008)

Miley Cyrus (2009)

Miley Cyrus (2010)

Ed Helms (2011)

Charlize Theron (2012)

Taylor Lautner (2013)

Jonah Hill (2014)

Anna Kendrick (2015)

Ryan Reynolds (2016)

Madelaine Petsch (2017)

Madelaine Petsch (2018)


 DreamWorks Animation


   v
t
e
 Happy Madison Productions
Films 
 Deuce Bigalow: Male Gigolo (1999)

Little Nicky (2000)

The Animal (2001)

Joe Dirt (2001)

Mr. Deeds (2002)

The Master of Disguise (2002)

Eight Crazy Nights (2002)

The Hot Chick (2002)

Anger Management (2003)

Dickie Roberts: Former Child Star (2003)

50 First Dates (2004)

The Longest Yard (2005)

Deuce Bigalow: European Gigolo (2005)

Grandma's Boy (2006)

The Benchwarmers (2006)

Click (2006)

I Now Pronounce You Chuck & Larry (2007)

Reign Over Me (2007)

Strange Wilderness (2008)

The House Bunny (2008)

You Don't Mess with the Zohan (2008)

Bedtime Stories (2008)

Paul Blart: Mall Cop (2009)

The Shortcut (2009)

Funny People (2009)

Grown Ups (2010)

Just Go with It (2011)

Zookeeper (2011)

Bucky Larson: Born to Be a Star (2011)

Jack and Jill (2011)

That's My Boy (2012)

Here Comes the Boom (2012)

Grown Ups 2 (2013)

Blended (2014)

Paul Blart: Mall Cop 2 (2015)

Pixels (2015)

Joe Dirt 2: Beautiful Loser (2015)

The Ridiculous 6 (2015)

The Do-Over (2016)

Sandy Wexler (2017)

The Week Of (2018)

Father of the Year (2018)

Murder Mystery (2019)

The Wrong Missy (2020)

Hubie Halloween (TBA)


Television series 
 Rules of Engagement (2007–13)

The Gong Show with Dave Attell (2008)

Nick Swardson's Pretend Time (2010–11)

Breaking In (2011–12)

The Goldbergs (2013–present)

Imaginary Mary (2017)

Schooled (2019–20)


Key people 
 Adam Sandler

Allen Covert

Jack Giarraputo

Tim Herlihy

Heather Parry


 
 Sony Pictures Entertainment



 Authority control  
 BIBSYS: 99040427

BNE: XX1096629

BNF: cb140122139 (data)

GND: 135123283

ISNI: 0000 0001 0815 3325

LCCN: no96017226

MusicBrainz: dfc6d0b8-3d0b-4b33-9b3f-dff972e25594

NKC: xx0060655

NLA: 42104189

NLI: 004330502

NTA: 166276952

SUDOC: 07726326X

Trove: 1456818

VIAF: 85692003

 WorldCat Identities: lccn-no96017226







