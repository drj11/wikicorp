 

 American professional wrestling promotion
All Elite Wrestling, LLC
 Trade nameAll Elite Wrestling
 TypePrivate
IndustryProfessional wrestling
FoundedJanuary 1, 2019; 17 months ago (2019-01-01)
FoundersTony Khan[1][2][3]
Headquarters1 TIAA Bank Field Dr,  Jacksonville, Florida, United States
 Area servedWorldwide
 Key people 
 Tony Khan (President and CEO)

Cody Rhodes (Executive Vice President)

Matt Jackson (Executive Vice President)

Nick Jackson (Executive Vice President)

Kenny Omega (Executive Vice President)

Brandi Rhodes (Chief Brand Officer)


Products 
 Live events

Merchandise

Music

Pay-per-view

Publishing

Television


ServicesLicensing
OwnerShahid Khan Tony Khan
Websitewww.allelitewrestling.com 

All Elite Wrestling, LLC (AEW)[4] is an American professional wrestling promotion founded in 2019.[1] Its inception was announced by entrepreneurs Shahid Khan and his son Tony, with the former acting as the promotion's lead investor, and the latter being the founder, president and CEO of the company. Professional wrestlers Cody Rhodes as well as Matt and Nick Jackson, collectively known as The Elite, are the promotion's inaugural contracted talents. The three are serving as both in-ring performers and executive vice presidents alongside Kenny Omega, fellow professional wrestler and co-founder of The Elite.
Since October 2, 2019,[5] AEW has produced a two-hour weekly TV show, AEW Dynamite,[6] airing live on TNT in the United States.[7][8] CBS Sports described AEW as "the first company with major financial backing to take a swing at beginning to compete with WWE on a major level in nearly two decades".[9]

  Contents
 
1 History
 
1.1 Background

1.2 Formation




2 Programming
 
2.1 Events




3 Personnel

4 Championships and accomplishments
 
4.1 Championships

4.2 Accomplishments




5 References

6 External links




History
Background
 Main article: All In (professional wrestling event)
In May 2017, professional wrestling journalist Dave Meltzer made a comment that the American professional wrestling promotion Ring of Honor (ROH) could not sell 10,000 tickets for a wrestling event.[10] The comment was responded to by professional wrestlers Cody Rhodes and The Young Bucks (the tag team of Matt Jackson and Nick Jackson), who were then top stars signed to ROH and good friends both inside and outside of professional wrestling as part of the group Bullet Club (and later, The Elite).[11] They promoted and held an independent professional wrestling event called All In in September 2018, featuring wrestlers from ROH as well as other promotions. The event sold out in 30 minutes and had the largest audience in attendance for a professional wrestling show in America held and organized by promoters not affiliated with WWE or World Championship Wrestling (WCW) since 1993. The event was attended by 11,263 people.[12][13][14][15] The event was acclaimed, and it led to much online speculation that Cody and The Young Bucks would expand their ambitions and create their own professional wrestling promotion or do a second All In event. People in the television industry were also very impressed with the show.[16][17]

Formation
      Nick Jackson (left) and Matt Jackson (right) teamed with Cody Rhodes to help start the promotion.      Cody Rhodes
On November 5, 2018, several trademarks were filed in Jacksonville, Florida, that indicated the launch of All Elite Wrestling.[18] Names filed for trademark included: All Elite Wrestling, AEW All Out, All Out, AEW, Double or Nothing, Tuesday Night Dynamite, AEW Double or Nothing, and several logos.[19][20][21] In December 2018, Cody, The Young Bucks, and several other wrestlers left ROH.[22][23] The official announcement of AEW's creation came at midnight Pacific Time on January 1, 2019 in an episode of Being the Elite, a YouTube web series created by and featuring The Elite. Also announced in the episode was Double or Nothing, AEW's inaugural event and sequel to All In.[24][25][26] On January 2, 2019, Cody and The Young Bucks officially signed with the promotion as competitors as well as serving as AEW's co-Executive Vice Presidents, while entrepreneur, football executive, and longtime wrestling fan Tony Khan was announced as the president of the company.[27][28] Tony and his father, Shahid, were reportedly backing the promotion as lead investors.[29][30] The Khans are billionaires and part of the ownership group of the Jacksonville Jaguars and Fulham F.C.[31]
Cody's wife, Brandi Rhodes, was announced as the company's Chief brand officer on January 3, 2019.[32][33] On January 8, 2019, the company held its inaugural press conference on the forecourt of the TIAA Bank Field, where they announced talents that were going to perform as part of the promotion, including former ROH wrestlers the team of SoCal Uncensored (Christopher Daniels, Scorpio Sky, and Frankie Kazarian), "Hangman" Adam Page; independent wrestlers Dr. Britt Baker, Joey Janela, and former WWE wrestlers Pac, and Chris Jericho.[34] They also announced a working relationship with Chinese professional wrestling promotion Oriental Wrestling Entertainment (OWE).[35][36] On February 7, 2019, the group held a press conference where tickets were released for Double or Nothing. Other big announcements included Kenny Omega joining as a competitor and the company's fourth co-Executive Vice President, as well as the signings of Lucha Brothers (Pentagón Jr. and Rey Fénix), Best Friends (Trent Beretta and Chuck Taylor), and a partnership with Mexican promotion Lucha Libre AAA Worldwide (AAA).[37]

Programming
 See also: List of AEW Dynamite special episodes and Wednesday Night Wars
On May 8, 2019, AEW reached a new media rights deal with British media company ITV plc to broadcast AEW shows on ITV4 with pay-per-views being broadcast on ITV Box Office starting with Double or Nothing on May 25, 2019.[38] On May 15, 2019, AEW and WarnerMedia announced a deal for a weekly prime-time show airing live on TNT beginning on October 2, 2019—the network that formerly had broadcast WCW's Monday Nitro during the Monday Night Wars (1995–2001). CBS Sports describes AEW as "the first company with major financial backing to take a swing at beginning to compete with WWE on a major level in nearly two decades".[9]
On May 25, 2019, AEW produced their first ever pay-per-view (PPV), Double or Nothing. It took place at the MGM Grand Garden Arena, and saw the debut of Jon Moxley. The event earned positive reviews from critics including Canadian Online Explorer, CBS Sports, ESPN, Pro Wrestling Torch, Pro Wrestling Dot Net and WrestleView, with the last three matches earning the most praise.[39][40][41][42][43][44] Dave Meltzer of the Wrestling Observer Newsletter wrote that "after just one show", AEW is already "the hottest non-WWE force in the U.S. pro wrestling business in more than 20 years".[45] On September 19, 2019, TNT's website listed AEW show's as AEW Dynamite, a one-hour preview show was also scheduled for October 1 at 8 PM.[46]
AEW releases a "Road to..." and "Countdown to..." series of videos on its official YouTube channel prior to Dynamite and pay-per-view events. The videos consist of interviews, video packages, and backstage segments. The series is used to hype pre-existing matches, as well as create new rivalries.[47]
In November 2019, AEW announced Bash at the Beach, a nine-day series of events, featuring two episodes of Dynamite, including one aboard Chris Jericho's Rock 'N' Wrestling Rager at Sea.[48]
On January 15, 2020, WarnerMedia and AEW announced a contract extension for Dynamite on TNT through 2023 and that AEW would be launching an upcoming second weekly show.[49]




Program
Original release
Original network
Notes



AEW Dynamite
October 2, 2019–present
TNT
AEW's flagship program.



AEW Dark
October 8, 2019–present
YouTube
Features untelevised matches held during Dynamite tapings on AEW's YouTube channel.



Events
 Main article: List of All Elite Wrestling pay-per-view events
AEW pay-per-view events are available via B/R Live in the United States and Canada, and via FITE TV internationally. Additionally, AEW PPVs are also available via traditional PPV outlets in the United States and Canada and are carried by all major satellite providers.[50][51]
On February 19, 2020, AEW reached a new media rights deal with German media company Sky Deutschland (which previously broadcast WWE and Impact Wrestling shows) to broadcast AEW pay-per-views on Sky Select Event.[52]

Personnel
 Main article: List of All Elite Wrestling personnel
Championships and accomplishments
Championships



Championship

Current champion(s)

Reign

Date won

Days held

Location

Notes



AEW World Championship



Jon Moxley

1

February 29, 2020

121

Chicago, Illinois

Defeated Chris Jericho at Revolution.[38]



AEW TNT Championship



Cody

1

May 23, 2020

37

Jacksonville, Florida

Defeated Lance Archer in a tournament final to become the inaugural champion at Double or Nothing.[53]



AEW Women's World Championship



Hikaru Shida

1

May 23, 2020

37

Jacksonville, Florida

Defeated Nyla Rose in a No Disqualification and No Countout match at Double or Nothing.



AEW World Tag Team Championship

 

Kenny Omega and "Hangman" Adam Page

1

January 21, 2020

160

Nassau, Bahamas

Defeated SoCal Uncensored (Frankie Kazarian and Scorpio Sky) aboard Norwegian Pearl at Chris Jericho's Rock 'N' Wrestling Rager at Sea Part Deux: Second Wave. Aired on tape delay as part of the January 22, 2020 episode of Dynamite.


Accomplishments



Accomplishment
Latest winner
Date won
Location
Notes

Ref.



Casino Battle Royale (men)

Adam Page

May 25, 2019

Paradise, Nevada

Last eliminated MJF at Double or Nothing to win, earning a match for the inaugural AEW World Championship at All Out.





Casino Battle Royale (women)

Nyla Rose

August 31, 2019

Hoffman Estates, Illinois

Last eliminated Britt Baker at All Out to win, earning a match for the inaugural AEW Women's World Championship on the debut episode of Dynamite.





Dynamite Diamond Ring

MJF

November 27, 2019

Hoffman Estates, Illinois

The two winners of the Dynamite Dozen Battle Royale faced each other in a singles match for the Dynamite Diamond Ring. MJF won the battle royal alongside Adam Page and then defeated Page on Dynamite to win the ring.

[38]



Casino Ladder Match

Brian Cage

May 23, 2020

Jacksonville, Florida

Defeated Frankie Kazarian, Scorpio Sky, Kip Sabian, Darby Allin, Orange Cassidy, Colt Cabana, Joey Janela, and Luchasaurus at Double or Nothing to win, earning a future AEW World Championship match.

[54]


References

External links
 Official website

   v
t
e
 All Elite Wrestling
Personnel Male wrestlers 
 Adam Page

Alex Reynolds

Angelico

Anthony Ogogo

Austin Gunn

Billy Gunn

The Blade

Brandon Cutler

Brian Cage

Brodie Lee

The Butcher

Cash Wheeler

Chris Jericho

Christopher Daniels

Chuck Taylor

Cima

Cody

Colt Cabana

Darby Allin

Dax Harwood

Dustin Rhodes

Evil Uno

Frankie Kazarian

Isiah Kassidy

Jack Evans

Jake Hager

Jimmy Havoc

Joey Janela

John Silver

Jon Moxley

Jungle Boy

Kenny Omega

Kip Sabian

Lance Archer

Luchasaurus

Luther

Marko Stunt

Marq Quen

Matt Jackson

Matt Hardy

Michael Nakazawa

MJF

Nick Jackson

Orange Cassidy

Ortiz

Pac

Pentagon Jr.

Peter Avalon

QT Marshall

Rey Fenix

Ricky Starks

Sammy Guevara

Santana

Scorpio Sky

Shawn Spears

Stu Grayson

Trent

Wardlow


Female wrestlers 
 Allie

Anna Jay

Awesome Kong

Bea Priestley

Big Swole

Brandi Rhodes

Dr. Britt Baker D.M.D.

Emi Sakura

Hikaru Shida

Kris Statlander

Leva Bates

Mel

Nyla Rose

Penelope Ford

Riho

Sadie Gibbs

Shanna

Yuka Sakazaki


Other on-air personnel 
 Arn Anderson

Jake Roberts

Rebel

Taz

Tully Blanchard


Stables and tag teams 
 Best Friends

The Brotherhood

The Butcher and The Blade

The Dark Order
 Alex Reynolds and John Silver

Evil Uno and Stu Grayson


The Elite
 The Young Bucks


FTR

Gunn Club

The Hybrid 2

The Inner Circle
 Santana and Ortiz


Jurassic Express

Lucha Brothers

The Natural Nightmares

Private Party

SoCal Uncensored


Referees 
 Earl Hebner


Broadcast team 
 Alex Marvez

Dasha Gonzalez

Excalibur

Jenn Decker

Jim Ross

Justin Roberts

Lexy Nair

Tony Schiavone


Backstage personnel 
 B.J. Whitmer

Dean Malenko

Jerry Lynn

Nick Mondo


Corporate staff 
 Shahid Khan

Tony Khan


 
Programming Weekly 
 Dynamite

Dark


Television specials 
 Bash at the Beach

Fyter Fest (2020)

Blood and Guts


Pay-per-view events 
 Double or Nothing
 2019

2020

2021


Fyter Fest
 2019


Fight for the Fallen

All Out
 2019

2020


Full Gear

Revolution


 
Championships 
 World

TNT

Women's

Tag Team


Partnerships 
 Lucha Libre AAA Worldwide

Oriental Wrestling Entertainment


Related 
 All In

Chris Jericho's Rock 'N' Wrestling Rager at Sea

Pro Wrestling Tees

Starrcast

Wednesday Night Wars


 
  Category



  Links to related articles
 
   v
t
e
 Lucha Libre AAA Worldwide
Active Championships Primary 
 Mega
 reigns



Secondary 
 Latin American (reigns)

Cruiserweight (reigns)

Mini-Estrella (reigns)


Women's Division 
 Reina de Reinas (reigns)


Tag Team Division 
 World Tag (reigns)

Mixed Tag (reigns)


Trios Division 
 Trios (reigns)


 
Inactive 
 Americas Heavyweight (1996–2004)

Americas Trios (1996–1997)

Campeón de Campeones (1996–2001)

Fusión (2012–2014)

Mascot Tag (2002–2009)

Northern Tag (2004-2010)

IWC World Heavyweight (1993–2007)


Tournaments 
 Alas de Oro

Copa Antonio Peña

Rey de Reyes

Lucha Capital

Lucha Libre World Cup


Miscellaneous 
 Roster

Hall of Fame

Alas de Oro

Copa Antonio Peña

Rey de Reyes

Lucha Libre World Cup

TripleSEM


Merchandise 
 Lucha Libre AAA: Héroes del Ring


Personnel 
 Roster

Alumni


Partnerships Current 
 All Elite Wrestling

Impact Wrestling

International Wrestling Revolution Group

Lucha Libre Elite

Major League Wrestling

Pro Wrestling Noah


Former 
 Aro Lucha

International Wrestling Syndicate

Global Force Wrestling

Lucha Underground

The Crash Lucha Libre

World Championship Wrestling

World Wrestling Council

World Wrestling Federation


 

   v
t
e
 Oriental Wrestling Entertainment
Roster 
 Cima

El Lindaman

T-Hawk


Partnerships 
 All Elite Wrestling

The Crash Lucha Libre



   v
t
e
 Professional wrestling in the United States
  Active promotions
National promotions 
 All Elite Wrestling

Impact Wrestling

Major League Wrestling

National Wrestling Alliance

Ring of Honor

WWE


Notable television programs 
 AEW Dynamite

Impact!

MLW Fusion

Ring of Honor Wrestling

WWE NXT

WWE Raw

WWE SmackDown


Notable independent promotions  
 AAW Wrestling

All Pro Wrestling

Championship Wrestling from Hollywood

Chaotic Wrestling

Combat Zone Wrestling

CWF Mid-Atlantic

East Coast Wrestling Association

Empire Wrestling Federation

Evolve

Full Impact Pro

Funking Conservatory

Game Changer Wrestling

Global Force Wrestling

Hoodslam

House of Glory

House of Hardcore

Independent Wrestling Association Mid-South

Independent Wrestling Federation

Innovate Wrestling

Jersey All Pro Wrestling

Juggalo Championship Wrestling

Kaiju Big Battel

Keystone State Wrestling Alliance

Main Event Championship Wrestling

MCW Pro Wrestling

Mason-Dixon Wrestling

Millennium Wrestling Federation

National Wrestling League/House of Pain Wrestling Federation

New England Championship Wrestling

Northeast Wrestling

NOVA Pro Wrestling

Ohio Valley Wrestling 

OMEGA Championship Wrestling

Pro Wrestling America

Pro Wrestling Guerrilla

Premiere Wrestling Xperience

Reality of Wrestling

Resistance Pro Wrestling

Ring Warriors

Southern States Wrestling

Style Battle

Top Rope Promotions

Ultra Championship Wrestling-Zero

United Wrestling Coalition

West Coast Wrestling Connection

World League Wrestling

World Xtreme Wrestling

Xtreme Wrestling Center


Women's promotions: 
 Shimmer Women Athletes

Shine Wrestling

Women Superstars Uncensored

Women of Wrestling


 
Governing bodies and interpromotional alliances 
 Allied Independent Wrestling Federations

Pro Wrestling International

United Wrestling Network

WWNLive


Puerto Rican Promotions 
 IWA Puerto Rico

WWC Lucha Libre

La Liga Wrestling


 
  Defunct promotions
National promotions 
 American Wrestling Association

Extreme Championship Wrestling

Pro Wrestling USA

United States Wrestling Association

Universal Wrestling Federation (Herb Abrams)

World Championship Wrestling (Jim Crockett Promotions)

World Class Championship Wrestling


Notable programs 
 AWA All-Star Wrestling

AWA Championship Wrestling

ECW Hardcore TV

ECW on TNN

ECW on Sci-Fi/Syfy

Lucha Underground

UWF Fury Hour

WCW Monday Nitro

WCW Pro

WCW Saturday Night

WCW Thunder

WCW WorldWide

Wrestling Society X


Notable former independent promotions and wrestling territories  
 50th State Big Time Wrestling

American Wrestling Federation

Assault Championship Wrestling

Big Time Wrestling (Detroit)

Big Time Wrestling (San Francisco)

Capitol Wrestling Corporation

Century Wrestling Alliance

Championship Wrestling from Florida

Chikara

Continental Championship Wrestling

Continental Wrestling Association

Deep South Wrestling

Dragon Gate USA

Extreme Rising

Family Wrestling Entertainment

Fred Kohler Enterprises

Front Row Wrestling

Future of Wrestling

Georgia Championship Wrestling

Global Wrestling Federation

Hardcore Homecoming

Heart of America Sports Attractions/Central States Wrestling

Heartland Wrestling Association

Houston Wrestling

Incredibly Strange Wrestling

Independent Professional Wrestling Alliance

International Championship Wrestling

International World Class Championship Wrestling

IWF Promotions

Memphis Wrestling

Memphis Championship Wrestling

Metro Pro Wrestling

Mid-Eastern Wrestling Federation

National Wrestling Federation

NWA Mid-America

NWA San Francisco

NWA Wildside

Pacific Northwest Wrestling/Portland Wrestling

Phoenix Championship Wrestling

Pro-Pain Pro Wrestling

Smoky Mountain Wrestling

South Atlantic Pro Wrestling

Southern Championship Wrestling (Georgia)

Southwest Championship Wrestling

St. Louis Wrestling Club

Steel City Wrestling

Texas All-Star Wrestling

Texas Wrestling Academy

Turnbuckle Championship Wrestling

Ultimate Pro Wrestling

Universal Wrestling Federation (Bill Watts)/Mid-South Wrestling

Warriors 4 Christ Wrestling

Western States Sports

Windy City Pro Wrestling

World Wide Wrestling Alliance

World Wrestling Alliance

World Wrestling Association

Worldwide Wrestling Associates

World Wrestling Network

Wrestling Association of Championship Krushers

Xcitement Wrestling Federation

Xtreme Pro Wrestling


Women's promotions: 
 ChickFight

Gorgeous Ladies of Wrestling

Ladies Major League Wrestling

Ladies Professional Wrestling Association

Naked Women's Wrestling League

Professional Girl Wrestling Association

Powerful Women of Wrestling

Rise Wrestling

World Women's Wrestling

Wrestlicious


 
Governing bodies and interpromotional alliances 
 American Wrestling Affiliates

Global Professional Wrestling Alliance

National Wrestling Association

Pro Wrestling USA

Wrestling Superstars Live


 

   v
t
e
 Turner Sports broadcast properties (including TNT, TBS, TruTV and WPCH-TV)
American football: 
 NFL on TNT*

College Football on TBS*

Alliance of American Football*

    
Auto racing: 
 NASCAR on TNT*

NASCAR on TBS*


Baseball: 
 Major League Baseball on TBS

Braves on Peachtree TV*

Braves TBS Baseball*


Basketball: 
 NBA on TNT (commentators)

NCAA March Madness on TBS/TNT/truTV (commentators)

NBA on TBS*


Beach volleyball: 
 NCAA Beach Volleyball Championship*


Boxing: 
 Friday Night Knockout*


Golf (TNT): 
 The Open Championship*

PGA Championship*

PGA Grand Slam of Golf*

Presidents Cup*


Soccer 
 UEFA Champions League

UEFA Europa League

UEFA Super Cup

WUSA

1990 FIFA World Cup


Olympics (TNT): 
 1992 Winter Olympics*

1994 Winter Olympics*

1998 Winter Olympics*


Tennis (TNT): 
 Wimbledon*


Professional wrestling: 
 World Championship Wrestling*
 Nitro

Thunder

Saturday Night


All Elite Wrestling
 Dynamite



eSports: 
 ELEAGUE


Pan American Games: 
 1991*


 
 *Former properties

See also: United States sports broadcasting lists

Turner Broadcasting System




  Florida portal
United States portal





