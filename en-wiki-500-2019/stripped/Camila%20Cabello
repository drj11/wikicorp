 

 Cuban-American singer, songwriter


 This article uses Spanish naming customs: the first or paternal family name is Cabello and the second or maternal family name is Estrabao.
 Camila Cabello
 Cabello at the 2019 American Music Awards
Born Karla Camila Cabello Estrabao  (1997-03-03) March 3, 1997 (age 23)  Cojímar, Havana, Cuba[1]
CitizenshipCuban (1997–present) American (2008–present)
Occupation 
 Singer

songwriter

actress


Years active2012–present
Home townMiami, Florida, U.S.
AwardsFull list
Musical career
Genres 
 Pop

R&B


InstrumentsVocals
Labels 
 Epic

Syco


Associated acts 
 Fifth Harmony

Shawn Mendes



Websitecamilacabello.com

Karla Camila Cabello Estrabao (/kəˈmiːlə kəˈbeɪoʊ/; Spanish: [kaˈmila kaˈβeʎo]; born March 3, 1997)[3] is a Cuban-American singer and songwriter. She rose to prominence as a member of the girl group Fifth Harmony, formed on The X Factor USA in 2012, signing a joint record deal with Syco Music and Epic Records. While in Fifth Harmony, Cabello began to establish herself as a solo artist with the release of the collaborations "I Know What You Did Last Summer" with Shawn Mendes, and "Bad Things" with Machine Gun Kelly, the latter reaching number four on the US Billboard Hot 100. After leaving the group in late 2016, Cabello released several other collaborations, including "Hey Ma" by Pitbull and J Balvin for The Fate of the Furious soundtrack, and her debut solo single "Crying in the Club".
Cabello's debut studio album, Camila (2018), reached number one on the Billboard 200 chart. The Latin music-influenced pop album was well received by critics, and received a Platinum certification by the Recording Industry Association of America (RIAA). Its lead single "Havana" topped the charts in several countries including the US and UK, and follow-up single "Never Be the Same" reached the top 10 in multiple countries. Cabello's 2019 duet with Mendes, "Señorita", became her second single to top the Billboard Hot 100. Her second studio album, Romance (2019), peaked at number three on the Billboard 200 chart and number one in Canada. Cabello is set to star in an upcoming film adaptation of Cinderella (2021), for Sony Pictures.
Cabello has amassed billions of streams on music platforms. She has the best-selling digital single of 2018 according to International Federation of the Phonographic Industry (IFPI). Cabello has won many awards throughout her career, including two Latin Grammy Awards, five American Music Awards, and one Billboard Music Award. She has also received three Grammy Award nominations.

  Contents
 
1 Early life

2 Career
 
2.1 2012–2016: The X Factor and Fifth Harmony

2.2 2017–present: Camila and Romance




3 Artistry

4 Philanthropy

5 Awards and nominations

6 Personal life

7 Discography

8 Tours
 
8.1 Headlining

8.2 Opening acts




9 Filmography
 
9.1 Film

9.2 Television

9.3 Web




10 References

11 External links




Early life
Cabello was born in Havana, Cuba[4][5] to Sinuhe Estrabao and Alejandro Cabello, and grew up in the town of Cojímar in East Havana. Her father was born in Mexico City and is a Mexican who moved to Cuba. She has a younger sister named Sofia.[6] For most of her early life, Cabello and her family moved back and forth between Havana and Mexico City. When Cabello was five, she relocated to Miami, Florida, in the United States, with her mother; her father was unable to obtain a visa at the time, and joined the family approximately 18 months later.[4][7][8][9] Cabello acquired American citizenship in 2008.[10] She attended Miami Palmetto High School but left in the 2012–2013 school year, while she was in 9th grade, to pursue her singing career. She later earned her high school diploma.[11]

Career
2012–2016: The X Factor and Fifth Harmony
      Cabello performing at the 93.3 FLZ Jingle Ball in Tampa, Florida with Fifth Harmony in 2013
Camila Cabello auditioned for The X Factor in Greensboro, North Carolina with Aretha Franklin's "Respect",[12][13] however, her audition was not aired because the series did not get the rights for the song. After elimination during the "bootcamp" portion of the process in Miami, Florida, Cabello was called back to the stage along with other contestants Ally Brooke, Normani, Lauren Jauregui, and Dinah Jane to form the girl group that would later become known as Fifth Harmony.[14] After finishing in third place on the show they signed a joint deal with Syco Music, owned by Simon Cowell, and Epic Records, L.A. Reid's record label.[15][16]
The group released the EP Better Together (2013) along with the studio albums Reflection (2015) and 7/27 (2016). The latter two generated the singles "Worth It" and "Work from Home", respectively, which reached the top 10 in several international charts.[17] From 2013 through the end of 2016, Cabello performed in various Fifth Harmony tours.
In November 2015, Cabello collaborated with Canadian singer Shawn Mendes on a duet titled "I Know What You Did Last Summer", a song they wrote together.[18][19] The single charted at number 20 in the US and 18 in Canada,[20] and was certified platinum by the Recording Industry Association of America (RIAA).[21] On October 14, 2016, American rapper Machine Gun Kelly released a joint single with Cabello called "Bad Things",[22] which reached a peak of number four on the US Billboard Hot 100.[23] Time included her on "The 25 Most Influential Teens of 2016" list.[24]
On December 18, 2016, the group announced Cabello's departure, with both sides giving contradictory explanations of the circumstances for her exit.[25][26][27] She appeared in a previously taped performance with the group on Dick Clark's New Year's Rockin' Eve.[28] Writing about Cabello's time in the group, a Billboard journalist noted it is "rather uncommon for someone to stand out in a collective as much as Cabello has over the past years."[29]

2017–present: Camila and Romance
On January 25, 2017, "Love Incredible", a collaboration with Norwegian DJ Cashmere Cat, leaked online.[30] The official version of the song was released on February 16, and later featured on Cashmere's debut studio album, 9.[31] Cabello recorded "Hey Ma" with Pitbull and J Balvin for The Fate of the Furious soundtrack. The Spanish version of the single and its music video were released on March 10, 2017, and the English version was released on April 6.[32][33] The singer was also featured on a collaboration with Major Lazer, Travis Scott and Quavo, for the song "Know No Better".[34] In May 2017, Cabello announced the future release of her first studio album, at the time titled The Hurting. The Healing. The Loving., which she described as "the story of my journey from darkness into light, from a time when I was lost to a time when I found myself again."[35] Her debut solo single "Crying in the Club" was released on May 19, 2017,[36] followed by a performance at the 2017 Billboard Music Awards.[37] The single peaked at number 47 in the United States.[38] She joined Bruno Mars' 24K Magic World Tour as an opening act for several of its dates,[39] and also partnered with clothing brand Guess as the face for their 2017 Fall campaign.[40]
New writing and recording sessions for her album were influenced by the success of her single "Havana" featuring Young Thug, which postponed its original release date.[41] Upon its release, the single reached number one in Australia, Canada, the United Kingdom, Ireland, France, Hungary and the United States.[42] It also spent seven weeks atop the US Mainstream Top 40 airplay chart.[43] The song became Spotify's most-streamed song ever by a solo female artist in June 2018, with over 888 million streams at the time.[44] Titled Camila, her debut album is a pop record containing Latin-influenced songs and ballads.[45] Camila was released on January 12, 2018 and debuted at number one in the United States with 119,000 album-equivalent units, including 65,000 from pure album sales.[46][47] The album was eventually certified platinum in the country.[48] "Real Friends" and "Never Be the Same" were released in the same day on December 7, 2017;[49][50] the latter becoming her third top 10 entry on the Hot 100.[51] "Havana" and "Never Be the Same" made Cabello the first artist to top the Mainstream Top 40 and Adult Top 40 airplay charts with the first two singles from a debut studio album.[52] She later won a MTV Video Music Award for Video of the Year for "Havana".[53]

      Cabello accepting the Best Video award at the 2018 MTV Europe Music Awards
In April 2018, Cabello embarked on the Never Be the Same Tour, her first as a solo artist. She was featured in "Sangria Wine", a song she recorded with Pharrell Williams. Cabello released the song live during her headlining tour.[54] In May 2018, Cabello made a cameo appearance in Maroon 5's music video for "Girls Like You" featuring Cardi B.[55]
In the same month, she began performing as the opening act for American singer-songwriter Taylor Swift in her Reputation Stadium Tour in between the European leg of the Never Be the Same Tour.[56][57] She headlined an arena for the first time on July 31, 2018 at the Mohegan Sun Arena.[58] Cabello was featured in the remix version of "Beautiful", a song from American singer Bazzi. The song was released on August 2.[59] On October 9, 2018, Cabello released the video single, "Consequences", having first surprised 12 of her biggest fans in advance, with a "Most Amazing Mystery Gift & Personal Letter".[60][61]
In October 2018, Cabello announced she would start working on new music in the new year after the holidays.[62][63] In December 2018 she was nominated for two Grammys: Best Pop Solo Performance for a live version of "Havana" and Best Pop Vocal Album for Camila.[64][65] Her performance of "Havana" with guests Ricky Martin, J Balvin and Young Thug at the start of the ceremony made her the first female Latin artist to open the show.[66] In April 2019, it was announced that Cabello would star in an upcoming film adaptation of Cinderella, directed by Kay Cannon for Sony Pictures.[67]
On June 21, 2019, Cabello released "Señorita" with Canadian singer Shawn Mendes, along with the music video.[68] The song debuted at number 2 on the US Billboard Hot 100 chart and marks Mendes' and Cabello's second collaboration, following "I Know What You Did Last Summer" released in 2015.[69][70] In August, "Señorita" climbed to the number one position, making it Cabello's second single to top the Hot 100 chart.[71] It earned them a nomination for the Grammy Award for Best Pop Duo/Group Performance.[citation needed] According to the IFPI, "Señorita" was the third best-selling song of 2019 globally.[72] She also recorded the song "South of the Border" by Ed Sheeran along with American rapper Cardi B, released in July 2019.[73]
On September 1, 2019, Cabello posted a clip on Instagram, teasing the release of her second studio Romance.[74] Two days later, she announced the first two singles from the album: "Liar" and "Shameless", which were released on September 5,[75] followed by "Cry for Me" and "Easy" in October 2019.[76][77] Romance was released on December 6, 2019, and will be supported by the Romance Tour in 2020.[78] "Living Proof" was released with the pre-orders of the album on November 15, 2019.[79] "My Oh My" featuring DaBaby entered the top 20 on the Hot 100.
In March 2020, Cabello participated in iHeart Media's Living Room Concert for America, a benefit to raise awareness and funds for the COVID-19 pandemic.[80]

Artistry
Cabello possesses a mezzo-soprano vocal range.[81] She grew up listening to artists such as Alejandro Fernández and Celia Cruz.[82] Her debut studio album is a pop and R&B record, influenced by Latin music.[83][84] For the album, Cabello incorporated elements of reggaeton, hip hop, and dancehall,[84][85][86] and took inspiration from contemporary Latin artists such as Calle 13 and J Balvin, as well as from the songwriting of Taylor Swift and Ed Sheeran.[87] Her sophomore album was inspired by the "big sounds" of the 80s and Queen.[88] She has also cited Michael Jackson, Rihanna, Shakira, Alejandro Sanz, Beyoncé, John Mayer, and Eminem as influences.[89][90][91][92]

Philanthropy
In February 2016, Cabello announced she had partnered with Save the Children to design a limited-edition "Love Only" T-shirt to help raise awareness of issues involving girls' equal access to education, health care and opportunities to succeed.[93] In June 2016, Cabello, producer Benny Blanco, and members of the nonprofit arts organization OMG Everywhere helped to create the charity single "Power in Me".[94] Cabello has also partnered with the Children's Health Fund, a non-profit dedicated to providing health care to low-income families with children.[95]
On April 3, 2017, Cabello performed at Zedd's WELCOME! Fundraising Concert which raised money for ACLU.[96] Cabello sang to patients at UCLA Mattel Children's Hospital on May 8, 2017.[97] In late 2017, she joined Lin-Manuel Miranda and multiple other Latin artists on the song "Almost Like Praying" for Puerto Rico hurricane relief.[98] Cabello also announced she was donating all proceeds of "Havana" to the ACLU for DREAMers.[99]
Cabello donated portions of proceeds from VIP sale packages to the Children's Health Fund while on the 2018 Never Be the Same tour.[100] On July 13, 2018, she performed a concert in San Juan and donated a portion of the concert's proceeds to Hurricane Maria Relief Fund.[101] On July 16, 2018, Cabello and Ryan Seacrest visited Children's Hospital of Philadelphia, signing autographs and taking pictures with patients.[102] In November 2018, Cabello became an ambassador for Save the Children.[103]
In March 2019, Cabello announced she donated 10K to a GoFundMe campaign for a homeless immigrant.[104] In September 2019, Cabello pledged to raise $250,000 for Save the Children organization.[105] In October 2019, Cabello performed at the We Can Survive concert which donates to breast cancer.[106] On October 22, 2019, Cabello appeared with Kate Middleton and Prince William at Kensington Palace in support of the finalists for the BBC Radio 1 Teen Heroes Awards.[107]

Awards and nominations
 Main article: List of awards and nominations received by Camila Cabello
 See also: List of awards and nominations received by Fifth Harmony
Among her awards, Cabello has won two Latin Grammy Awards, four American Music Awards, a Billboard Music Award,[108] five MTV Europe Music Awards,[109] two iHeartRadio Music Awards,[110] four MTV Video Music Awards (including one for Video of the Year),[111] three iHeartRadio Much Music Video Awards,[112][113] and a Billboard Women in Music award for Breakthrough Artist.[114]

Personal life
Cabello was in a relationship with dating coach and writer Matthew Hussey, whom she met on the set of Today. They dated from February 2018 to June 2019.[115] Since July 2019, she has been dating Canadian singer Shawn Mendes. The relationship has garnered controversy, as both were accused of attempting to form a relationship for publicity,[116][117] while Mendes denied it stating it is "definitely not a publicity stunt".[116] Cabello has also opened up about having anxiety[118][119] and obsessive–compulsive disorder.[120][121] Cabello currently resides in a Mediterranean-style villa in the Hollywood Hills neighborhood of Los Angeles, California.[2]
In December 2019, Tumblr posts she reblogged between 2012 and 2013 surfaced on Twitter, containing racial slurs and derogatory language including the N-word, and mocking of Chris Brown assaulting Rihanna in 2009. Her Tumblr account was deactivated soon after the discovery and Cabello issued an apology stating she was "uneducated and ignorant" when younger, and that she was deeply embarrassed she ever used "horrible and hurtful" language. In her statement, she added that "those mistakes don't represent" her and that she "only [stands] and [has] ever stood for love and inclusivity".[122][123]

Discography
 Main article: Camila Cabello discography
 See also: Fifth Harmony discography
 Camila (2018)

Romance (2019)

Tours
 See also: Fifth Harmony § Tours
Headlining
 Never Be the Same Tour (2018)

The Romance Tour (2020)

Opening acts
 Bruno Mars – 24K Magic World Tour (2017)

Taylor Swift – Reputation Stadium Tour (2018)

Filmography
Film



Year

Title

Role

Notes



2015

Taylor Swift: The 1989 World Tour Live

Herself

Concert film



2018

Taylor Swift: Reputation Stadium Tour

Herself

Concert film



2021

Cinderella

Cinderella

On hold



Television



Year

Title

Role

Notes



2012–2013

The X Factor

Herself

Contestant: season 2 Guest: 1 episode in season 3



2014

Faking It

Herself

Episode: "The Ecstasy and the Agony"



2015

Barbie: Life in the Dreamhouse

Herself

Episode: "Sisters' Fun Day"



2016

The Ride

Herself

Episode: "Fifth Harmony"



2017

One Love Manchester

Herself

Television special



2018

Dancing on Ice[124]

Herself

Guest: 1 episode in series 10



2019

Saturday Night Live

Herself

Musical guest: episode 3 of season 45



2020

Saturday Night Takeaway

Herself

Guest Announcer: Season 16 Episode 1



iHeart Living Room Concert for America

Herself

Concert special



One World: Together at Home

Herself

Television special



Web



Year

Title

Role

Notes



2018

Camila Cabello: Made in Miami

Herself

Short documentary film



King of the Golden Sun

Addison Jones

2 episodes



2020

Dear Class of 2020

Herself

Web television special



Priceless Experiences at Home[125]

Herself

1 episode



References

External links



Wikimedia Commons has media related to Camila Cabello.


 Official website

Camila Cabello on IMDb

  biography portal
Pop music portal

   v
t
e
 Camila Cabello
 
 Awards and nominations

Discography


Studio albums 
 Camila

Romance


Singles 
 "I Know What You Did Last Summer"

"Bad Things"

"Crying in the Club"

"Havana"

"Never Be the Same"

"Sangria Wine"

"Consequences"

"Mi Persona Favorita"

"Señorita"

"Liar"

"Shameless"

"Living Proof"

"My Oh My"


Featured singles 
 "Love Incredible"

"Hey Ma"

"Know No Better"

"Almost Like Praying"

"Beautiful"

"Find U Again"

"South of the Border"


Promotional singles 
 "I Have Questions"

"OMG"

"Real Friends"

"Cry for Me"

"Easy"


Other songs 
 "She Loves Control"

"First Man"


Concert tours 
 Never Be the Same Tour

The Romance Tour


Related articles 
 Fifth Harmony


 
  Book

 Category



   v
t
e
 Fifth Harmony
 
 Ally Brooke

Dinah Jane

Lauren Jauregui

Normani

Camila Cabello


Studio albums 
 Reflection

7/27

Fifth Harmony


EPs 
 Better Together


Singles 
 "Miss Movin' On"

"Boss"

"Sledgehammer"

"Worth It"

"I'm in Love with a Monster"

"Work from Home"

"All in My Head (Flex)"

"That's My Girl"

"Down"

"He Like That"

"Por Favor"


Promotional singles 
 "Me & My Girls"

"The Life"

"Write on Me"

"Angel"

"Deliver"

"Can You See"

"Don't Say You Love Me"


Featured songs 
 "Sin Contrato"


Concert tours 
 The Worst Kept Secret Tour

The Reflection Tour

The 7/27 Tour

PSA Tour


Related articles 
 Discography

Songs recorded

Awards and nominations


 
  Category



   v
t
e
 Latin Grammy Award for Record of the Year
 
 "Corazón Espinado" by Santana (Rodney Holmes, Tony Lindsay, Karl Perazzo, Raul Rekow, Benny Rietveld, Carlos Santana, Chester D. Thompson) featuring Maná (Fher Olvera, Alex González, Juan Calleros, Sergio Vallín) (2000)

"El Alma al Aire" - Alejandro Sanz (2001)

"Y Sólo Se Me Ocurre Amarte" by Alejandro Sanz (2002)

"Es Por Ti" by Juanes (2003)

"No Es lo Mismo" by Alejandro Sanz (2004)

"Tu No Tienes Alma" by Alejandro Sanz (2005)

"La Tortura" by Shakira and Alejandro Sanz (2006)

"La Llave de Mi Corazón" by Juan Luis Guerra (2007)

"Me Enamora" by Juanes (2008)

"No Hay Nadie Como Tú" by Calle 13 (Residente, Visitante) featuring Café Tacuba (Rubén Albarrán, Emmanuel del Real, Enrique Rangel, Joselo Rangel) (2009)

"Mientes" by Camila (2010)

"Latinoamérica" by Calle 13 (Residente, Visitante) featuring Totó la Momposina, Susana Baca & Maria Rita (2011)

"¡Corre!" by Jesse & Joy (2012)

"Vivir Mi Vida" by Marc Anthony (2013)

"Universos Paralelos" by Jorge Drexler featuring Ana Tijoux (2014)

"Hasta la Raíz" by Natalia Lafourcade (2015)

"La Bicicleta" by Carlos Vives & Shakira (2016)

"Despacito" by Luis Fonsi & Daddy Yankee (2017)

"Telefonía" by Jorge Drexler (2018)

"Mi Persona Favorita" by Alejandro Sanz & Camila Cabello (2019)



 Authority control  
 GND: 1153226499

LCCN: no2017083085

MusicBrainz: 01b8b5bf-06cb-45da-85fb-61ada72fcd69

VIAF: 3599149919435306650002

 WorldCat Identities: lccn-no2017083085







