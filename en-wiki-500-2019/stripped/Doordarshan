  This article is about the Indian public service broadcaster. For the terrestrial television channel, see DD National. For the 2020 film, see Doordarshan (film).
 India's autonomous public service broadcaster



Doordarshan
TypeBroadcast, satellite television, Internet, OTT
Country India
AvailabilityNationwide
MottoSatyam Shivam Sundaram
HeadquartersNew Delhi, Delhi
OwnerPrasar Bharati
 Key peopleShashi Sekhar vempati, CEO
 Launch date15 September 1959; 60 years ago (1959-09-15)
 Former namesAll India Radio
 Picture format576i (4:3 SDTV) 1080i (16:9 HDTV)
 Official websitewww.doordarshan.gov.in

      Doordarshan Bhawan, Mandi House, Copernicus Marg, Delhi
Doordarshan (abbreviated in English as DD) is an autonomous[1] public service broadcaster founded by the Government of India, owned by the Broadcasting Ministry of India and one of Prasar Bharati's two divisions.[2] One of India's largest broadcasting organisations in studio and transmitter infrastructure, it was established on 15 September 1959.[3] Doordarshan, which also broadcasts on digital terrestrial transmitters, provides television, radio, online and mobile service throughout metropolitan and regional India and overseas through the Indian Network and Radio India.

  Contents
 
1 History
 
1.1 Beginnings

1.2 Nationwide transmission




2 Channels

3 Channel list
 
3.1 National channels

3.2 Regional channels

3.3 State Network

3.4 UT Network

3.5 Closed or renamed channels




4 International broadcasting

5 Criticism

6 See also

7 References

8 External links




History
Beginnings
The channel began modestly as an experimental broadcaster in Delhi on 14 September 1959, with a small transmitter and a makeshift studio. Regular daily transmission started in 1965 as part of All India Radio, with a five-minute news bulletin read by Pratima Puri. Salma Sultan joined Doordarshan in 1967, and became a news anchor.
Krishi Darshan debuted on Doordarshan on 26 January 1967, and is Indian television's longest running program.[4]
Television service was extended to Bombay (now Mumbai) and Amritsar in 1972. Until 1975 only seven Indian cities had television service, and Doordarshan was the country's sole television provider.
Television service was separated from radio on 1 April 1976.[5] The All India Radio and Doordarshan were placed under the management of separate directors-general in New Delhi. In 1982, Doordarshan became a national broadcaster.

Nationwide transmission
National telecasts (DD National) were introduced in 1982. Colour television began in India with the live telecast of the Independence Day speech by Prime Minister Indira Gandhi on 15 August of that year, followed by the colour telecast of the 1982 Asian Games in Delhi.[6][7] 
Live telecasts of the opening and closing ceremonies of the 2012 Summer Olympics were broadcast on its national channel, and DD Sports provided round-the-clock coverage.[8]
On 17 November 2014, Doordarshan director-general Vijayalaxmi Chhabra introduced a pink-and-purple colour scheme and a new slogan: Desh Ka Apna Channel ("The country's own channel").[9] Doordarshan transmitted over a network of nearly 1,400 terrestrial transmitters in 2017, with 46 studios producing TV programmes.[10] After the introduction of private channels Doordarshan is struggling to keep its position in the television space.[11] Currently Doordarshan is trying to improve its studios and programmes while its primary aim is to serve the country.[12]

Channels
Doordarshan operates 46 Studios, it operates 21 channels: two all-India channels (DD National and DD News),[13] 17 regional satellite channels, 11 state networks, an international channel (DD India), a sports channel (DD Sports), DD Bharati, DD Urdu and DD Kisan. On DD National (DD-1), regional and local programs are carried on a time-sharing basis for terrestrial broadcasting only. DD News, launched on 3 November 2003 and replacing DD Metro (formerly known as the DD-2 entertainment channel), provides 24-hour news. These channels are relayed by all terrestrial transmitters in India. The regional-language satellite channels have two components: a regional service for a particular state (relayed by all terrestrial transmitters in the state), and additional programs in the regional language available through cable operators and DTH operators. DD Sports broadcasts sporting events of national and international importance. It is the only sports channel which telecasts rural sports such as kho-kho and kabbadi.
A new regional channel, DD Arunprabha (a 24/7 satellite television channel focusing on the North Eastern region) was scheduled to begin on 15 February 2018;[14][15] however, its launch was placed on hold.[16] DD Arunprabha was launched on 9 February 2019.[17]
On 9 March 2019, Prasar Bharati brought 11 more State DD Channels on the Satellite footprint of India through DD Free Dish. This includes five channels for Northeastern states. This will go a long way in strengthening regional cultures and fulfilling people's aspirations. These are – 
DD Chhattisgarh, DD Goa, DD Haryana, DD Himachal Pradesh, DD Jharkhand, DD Manipur, DD Meghalaya, DD Mizoram, DD Nagaland, DD Tripura and DD Uttarakhand.[18][19]
On 13 April 2020, DD Retro[20] was launched by Prasar Bharati which will show old classic hindi serials of Doordarshan.

Channel list
National channels


Channel

Programming

Language



DD National

General Entertainment

Hindi and English



DD News

News

Hindi



DD India

News

English



DD Sports

Sports

Hindi and English



DD Bharati

Art and Cultural Infotainment

Hindi and English



DD Kisan

Agricultural Infotainment

Hindi



DD Urdu

Infotainment

Urdu



DD Retro

Old Classic General Entertainment

Hindi


Regional channels


Channel

Language

Region



DD Arunprabha

Hindi and English

Arunachal Pradesh



DD Bangla

Bengali

West Bengal



DD Bihar

Hindi and Bihari

Bihar



DD Chandana

Kannada

Karnataka



DD Girnar

Gujarati

Gujarat



DD Kashir

Kashmiri and Urdu

Jammu and Kashmir



DD Madhya Pradesh

Hindi

Madhya Pradesh



DD Malayalam

Malayalam

Kerala



DD Assam 

Assamese, Hindi and English

Assam



DD Odia

Odia

Odisha



DD Podhigai

Tamil

Tamil Nadu



DD Punjabi

Punjabi

Punjab



DD Rajasthan

Hindi and Rajasthani

Rajasthan



DD Sahyadri

Marathi

Maharashtra



DD Saptagiri

Telugu

Andhra Pradesh



DD Uttar Pradesh

Hindi

Uttar Pradesh



DD Yadagiri

Telugu

Telangana


State Network


Channel

Language

Region



DD Chhattisgarh

Hindi

Chhattisgarh



DD Panaji 

Konkani

Goa



DD Haryana

Hindi

Haryana



DD Himachal Pradesh 

Hindi

Himachal Pradesh



DD Jharkhand

Hindi

Jharkhand



DD Manipur

Manipuri

Manipur



DD Meghalaya

Khasi and English

Meghalaya



DD Mizoram

Mizo

Mizoram



DD Nagaland

English

Nagaland



DD Tripura

Bengali and Kokborok

Tripura



DD Uttarakhand

Hindi

Uttarakhand


UT Network


Channel

Language

Region



DD Andaman and Nicobar

Hindi, Bengali, Tamil, Telugu and Malayalam

Andaman and Nicobar Islands



DD Chandigarh

Hindi and Punjabi

Chandigarh



DD Dadra and Nagar Haveli

Hindi, Gujarati and Marathi

Dadra and Nagar Haveli



DD Daman and Diu

Hindi, Gujarati and Marathi

Daman and Diu



DD Delhi

Hindi

Delhi



DD Lakshadweep

Malayalam

Lakshadweep



DD Pondicherry

Tamil, Telugu and Malayalam

Puducherry


Closed or renamed channels
 DD 2 (1984–1993): Renamed DD Metro.

DD Metro (1993–2003): Converted to DD News.

Metro Gold (October 2000 – September 2001): Aired on DD Metro.

DD International (March 1995 – September 2000): Renamed DD World.

DD World (September 2000 – January 2002): Renamed DD India.

DD CNNi (30 June 1995 – 31 May 1997)

DD 3 (1995–1996): Merged with DD Movie Club and renamed DD3-Movie Club.

DD Movie Club (1995–1996): Merged with DD 3 and renamed DD3-Movie Club.

DD3-Movie Club (1996–1998): Converted to DD Sports.

International broadcasting
The DD India satellite channel has been broadcast in 146 countries. In UK, it was available through the Eurobird satellite on the Sky system's channel 833; its logo was Rayat TV. Transmission via Sky Digital ended in June 2008, and via DirecTV in the United States the following month.

Criticism
Prasar Bharati is Doordarshan's parent body, and its board members are appointed by the Government of India through the Information and Broadcasting Ministry.[21]
Doordarshan has been used, especially during the Emergency, to disseminate government propaganda.[22] During Operation Blue Star in 1984, only government sources were used to report the story. Doordarshan was complicit in the production of a video claiming acts of violence which, when investigated by independent journalists, were found to be false.[citation needed]
In 2004 it censored a controversial documentary on Jayaprakash Narayan, an opposition leader during the Emergency.[23] When Doordarshan broadcast a 70-minute Vijayadashami speech by Rashtriya Swayamsevak Sangh (RSS) leader Mohan Bhagwat, the Narendra Modi administration and the BJP were criticised for "misusing" the public broadcaster. According to DD director-general Archana Datta, the "speech was like any other news event; therefore, we covered it."[24][25][26]
Since private television channels were authorised in 1991, Doordarshan has experienced a steep decline in viewership. Although it earns significant advertising revenue—due to its compulsory feed—from the highest bidder for national events (including cricket matches),[27] there has been a proposal to fund it by imposing a licence fee to own a television in India.[28]

See also
 All India Radio

Lok Sabha TV

Rajya Sabha TV

DD Free Dish

Ministry of Information and Broadcasting (India)

DD Retro

List of programs broadcast by DD National

References

External links



Wikimedia Commons has media related to Doordarshan.


 Doordarshan official site

Doordarshan news site

   v
t
e
  Ministry of Information and Broadcasting
 List of I&B Ministers • List of I&B Ministers of State • Government of India • Prime Minister (List • Office) • Cabinet
Broadcasting 
 Prasar Bharati

Doordarshan

Akashvani

Broadcast Engineering Consultants India

Direct to Home


Information 
 Directorate of Advertising and Visual Publicity

Registrar of Newspapers for India

Press Council of India

Press Information Bureau

Indian Institute of Mass Communication


Films 
 Directorate of Film Festivals

Films Division

Central Board of Film Certification

Children's Film Society, India

Film and Television Institute of India

Film Certification Appellate Tribunal

National Film Archive of India

Satyajit Ray Film and Television Institute

National Film Development Corporation of India



   v
t
e
 Prasar Bharati Network
Television channels 
 Doordarshan


Radio stations 
 Akashavani


Telecommunications 
 DD Free Dish



   v
t
e
  Terrestrial television channels of India
Doordarshan 
 DD Ahmedabad Metro

DD Andaman and Nicobar

DD Arunachal Pradesh

DD Assam

DD Bangalore Metro

DD Bangla

DD Bharati

DD Bihar

DD Chandana

DD Chandigarh

DD Chennai

DD Chhattisgarh

DD Coimbatore

DD Cricket

DD Dadra and Nagar Haveli

DD Daman and Diu

DD Delhi Metro

DD English

DD Faizabad

DD Free Dish

DD Girnar

DD Goa

DD Gorakhpur

DD Gyan Darshan 1

DD Gyan Darshan 2

DD HD

DD Haryana

DD Himachal Pradesh

DD Hyderabad Metro

DD India

DD Jaipur Metro

DD Jharkhand

DD Kashir

DD Kisan

DD Kolkata Metro

DD Kutchi

DD Lakshadweep

Lok Sabha TV

DD Lucknow Metro

DD Malayalam

DD Madhya Pradesh

DD Madurai

DD Manipur

DD Meghalaya

DD Mizoram

DD Mumbai Metro

DD Nagaland

DD National

DD News

DD North-East

DD Odia

DD Podhigai

DD Port Blair

DD Pondicherry

DD Punjabi

DD Rajasthan

Rajya Sabha TV

DD Sahyadri

DD Saptagiri

DD Sikkim

DD Sindhi

DD Sports

DD Tripura

DD Urdu

DD Uttar Pradesh

DD Uttarakhand

DD Yadagiri



   v
t
e
 Cable television providers
   v
t
e
 Cable, satellite, and other specialty television providers in Africa, Asia and Oceania
Cable 
 Cable TV (Hong Kong)

Cablevision (Lebanon)

Clear TV (Nepal)

Cablelink

Destiny Cable

Docomo Pacific

eonet (Japan)

Ethio telecom

First Media

Foxtel

GMM Z

Hathway

Hot

InCablenet

Inwi

Itscom (Japan)

JCTV (Japan)

Kbro

Kuwait Cablevision

Lanka Broadband Networks (Sri Lanka)

Malitel

Mascom

Montage Cable TV (Nigeria)

Mozaic TV (Qatar)

OkeVision

Optus Television

PPCTV

Rostelecom

Safaricom

Siti Cable

Sky Cable

Sonatel

TBC (Taiwan)

Tokai Cable

TransACT

Tata Sky

tn TV (Namibia)

TrueVisions

TVCabo (Mozambique)

United Communication Service

Unitel (Angola)

Vodacom (South Africa)

Vodafone New Zealand

VTVCab

WorldCall


Satellite 
 Airtel digital TV

Astro (Malaysia)

Azam TV

beIN (Middle East and North Africa)

CanalSat Afrique

CanalSat Calédonie

Canal+ (Myanmar)

Cignal

Consat (Nigeria)

DD Free Dish

DDishTV

Dialog TV (Sri Lanka)

Dish TV (India)

Dish TV (Sri Lanka)

Dish Home

DStv (Sub-Saharan Africa)

Foxtel (Australia)

GMM Z

G Sat

Indosat

K+ (Vietnam)

K-Vision

Kristal-Astro

KT SkyLife

ME Digital TV

MediaNet

Metro Digital

MBC

MNC Vision

myHD

NJOI

NTV Plus

OrangeTV

OSN (Middle East and North Africa)

Pacific Broadcasting Services Fiji

Pearl Digital TV

Pra International Green Group India

Real VU

Reliance Digital TV

Sky Direct

Sky Pacific

Sky PerfecTV!

Sky Television (New Zealand)

Sky Net

Sky Vision (Ivory Coast)

StarSat

StarTimes

Sun Direct

Tata Sky

Telone (Zimbabwe)

Transvision (Indonesia)

TrendTV

TrueVisions

Videocon d2h

WOWOW

Yes (Israel)

ZAP

Zuku


IPTV 
 Sky On Demand

BTV

BesTV

Cellcom TV (Israel)

DU

Fetch TV

Fine TV

GTA Teleguam

Hikari TV

Hot NEXT (Israel)

LG Uplus

Maroc Telecom

My.T

NET TV (Nepal)

Now TV (Hong Kong)

Olleh TV

Omantel

Partner TV (Israel)

PEO TV (Sri Lanka)

Rostelecom

Saudi Telecom Company

Singtel TV (Singapore)

SKY Media

StarHub TV (Singapore)

Uganda Telecom

Unifi TV (Malaysia)

USeeTV (Indonesia)

Yes STING (Israel)


Terrestrial 
 ABS-CBN TV Plus

Canal+ (Myanmar)

Freeview (Australia)

Freeview (New Zealand)

GMA Affordabox

MYTV (Malaysia)

StarTimes


Defunct 
 Aora

Austar

CTH (Thailand)

DishHD

Dream Satellite TV

Easy TV (Philippines)

Galaxy (Australia)

Igloo (New Zealand)

Mega TV (Malaysia)

Neighbourhood Cable (Australia)

SelecTV (Australia)

U Television (Malaysia)


 
 Middle East, Africa, Asia and Oceania

Europe

Americas

Canada

United States

Latin America and the Caribbean


 
   v
t
e
 Cable, satellite, and other specialty television providers in Canada
 Terrestrial and satellite
Satellite 
 Bell TV

Shaw Direct

Telus Satellite TV


Cable Major1 
 Bell (Cablevision for Val-d'Or, QC, MTS for Manitoba)

Cogeco (Ontario, Quebec)

Eastlink (Atlantic, Northern Ontario, Western Canada)

Rogers Cable (Ontario, New Brunswick, Newfoundland and Labrador)

Source (Hamilton, ON)

Shaw (Western Canada, Northwestern Ontario)

Vidéotron (Quebec)


Minor 
 Access (Saskatchewan)

Cable Axion (Magog, QC)

CityWest (Prince Rupert, BC)

DERYtelecom (Saguenay, QC)

Novus Cable (select areas of BC Lower Mainland)

Omineca Cablevision (Prince George, BC)

Westman (Brandon, MB)


See also 
 Defunct cable and DBS companies of Canada


 
IPTV 
 Bell
 Fibe (Aliant)

Fibe TV (ON/QC)

MTS TV (MB)


Comwave

SaskTel MaxTV

TekSavvy TV

Telus Optik TV

Tbaytel TV

VMedia

Zazeen


MMDS 
 Craig Wireless


 
 Middle East, Africa, Asia and Oceania

Americas

Europe

1More than 400,000 television service subscribers.


 
   v
t
e
 Cable, satellite, and other specialty television providers in Europe
Cable 
 A1 Bulgaria

A1 Croatia

Canal Digital

Caiway

Com Hem

Delta

Digi

DNA

Elisa

Eir

EVision

Elta-Kabel

ER-Telecom

Euskaltel

Get

Go

IPKO

Kabelkiosk

Kabel Noord

Kazakhtelecom

Kujtesa

Magenta Telekom

Magnet Networks

Magyar Telekom

Monaco Cable

Melita

MEO

MISS.NET

Mtel

M&H Company

Naxoo

NetCologne

Nowo

Orange TV

Post Telecom

Primacom

Rostelecom

R

SBB

SFR

STV AS

Swisscom

Tango TV

Telecable

Telekabel

Tele Columbus

Tele2Vision

Telemach

Telenet

Teledünya

Tet

Toya

Turksat Kablo TV

UPC Poland

UPC Switzerland

Virgin TV

Virgin Media Ireland

VodafoneZiggo

Vodafone Czech Republic

Vodafone Germany

Vodafone Hungary

Vodafone Romania

Vodafone Spain

Voo

WightFibre

YouSee


Satellite 
 A1 Bulgaria

A1 Croatia

Bulsatcom

Canal Digital

CanalDigitaal

Canal+

Cosmote TV

Cyfrowy Polsat

Digi

DigitAlb

Digiturk

D-Smart

Focus Sat

Freesat

Joyne

MagtiCom

Magio TV

Movistar+

Meo

Mts

Nos

Nova

NTV Plus

Orange S.A.

Orange Poland

Orange Romania

Orange Slovakia

Platforma Canal+

Saorsat

Sky UK

Sky Germany

Sky Ireland

Sky Italy

Skylink

Telekom Hungary

Telekom Romania

Télésat

Tivù Sat

Total TV

Tricolor TV

TV Vlaanderen

Viasat

Viasat Ukraine

Vip TV

Vivacom


IPTV 
 Andorra Telecom

A1 Bulgaria

A1 Croatia

A1 Telekom Austria

Altibox

Amis

Beeline

Bouygues Telecom

BT TV

Bulsatcom

Canal Digital

Cosmote TV

CytaVision

eir Vision

Fastweb

Free

HOME.TV

iNES

Infostrada TV

KPN

Magenta TV

Magnet Networks

MEO

Moja TV

Moldtelecom

Movistar+

Mtel

Mts

Neuf Cegetel TV

NOS

Nova

Open IPTV 

Optimus Clix

Orange Spain

Orange TV

Plusnet

PrimeTel 

Proximus TV

Scarlet 

SFR

ShqipTV

Síminn

Smart Telecom

Sunrise TV

Super TV

T-2

TalkTalk TV

Tele2 Netherlands

Telekom Romania

Telenor

Telia Digital-tv

TeliaSonera

Teo LT

TIMvision

Tivibu

T-Mobile Netherlands

YouView

Vivacom

Vodafone Greece

Vodafone Ireland

Vodafone Italy

Vodafone Portugal

XS4ALL


Terrestrial 
 A1 Macedonia

Boxer TV Access

Boxer TV A/S

Digea

Easy TV

EVOtv

Freeview

Digitenne

Mediaset Premium

PlusTV

PTDT

RiksTV

Saorview


Defunct 
 @Home

Alice Home TV

Alpha Digital

blizoo

BoomTV

Canal+ Spain

Casema

CenterTelecom

Kabel BW

Kabel Deutschland

Madritel

North-West Telecom

Numericable

On Telecoms

ONO

Radijus Vektor

Southern Telecom

Starman

TDC

Telfort

Unitymedia

UPC Hungary

UPC Netherlands

UPC Romania

Uralsvyazinform

VolgaTelecom


 
 Middle East, Africa, Asia and Oceania

Americas

Canada

Latin America and the Caribbean

United States


 
   v
t
e
 Cable, satellite, and other specialty television providers in Latin America and the Caribbean
Cable 
 Axtel TV (Mexico)

Airlink Communications (Trinidad and Tobago)

Altice Dominicana S.A.

Antigua Cable

Cablecom (Mexico)

Cablemás (Mexico)

Cable Onda

Cablevisión (Argentina)

CaboTelecom

Claro Colombia

Claro NET TV (Brazil)

Claro TV

Digicel Play (Caribbean)

Columbus/FLOW (Caribbean)

Independent Cable Network of Trinidad and Tobago (ICNTT)

Izzi Telecom (Mexico)

Massy Communications (Trinidad and Tobago)

Mayaro Cable TV (Trinidad and Tobago)

Megacable (Mexico)

Movistar TV (Peru)

Mundo Pacifico (Chile)

Red Intercable (Argentina)

RVR International (Trinidad and Tobago)

SuperCable

Telefónica del Sur

TV-Cable (Ecuador)

Une (Colombia)

VTR (Chile)


Satellite 
 CanalSat Caraïbes (Caribbean)

CANTV (Venezuela)

Claro TV

DirecTV (South America & Caribbean)

Dish México

Entel (Chile)

Green Dot (Caribbean)

Inter Satélital

Movistar TV

Oi TV (Brazil)

SKY Brasil

Sky México, Dominican Republic & Central America

Vivo TV (Brazil)


Fiber/IPTV 
 bmobile (Trinidad and Tobago)

Claro República Dominicana

Movistar TV (Argentina and Chile)

Oi TV (Brazil)

Vivo TV (Brazil)


Terrestrial microwave 
 Multi-Choice TV


Defunct satellite 
 DirecTV Brazil

DirecTV Mexico

GVT TV (Brazil)

Sky Argentina

Sky Chile

Sky Colombia

Sky Ecuador

Sky Peru

Sky Venezuela


Defunct cable 
 Vivo TV Plus (Brazil)


 
 Middle East, Africa, Asia and Oceania

Americas

Canada

Europe

United States


 
   v
t
e
 Cable, satellite, and other specialty television providers in the United States
Cable MVPD 
 Adams Cable

Altice USA
 Optimum (TV/Internet)

Suddenlink Communications


Armstrong

Atlantic Broadband

AT&T Alascom

Blue Ridge Communications

Blue Stream

Broadstripe

Buckeye Broadband

Cable One

Cable Services, Inc

Charter Spectrum

Comcast Xfinity

Comtech21

Consolidated Communications
 FairPoint Communications


Cox Communications

DoCoMo Pacific

Emery Telcom

Full Channel

GCI

Hargray

Hood Canal Communications

Mediacom

Midco

Northlake Telecom

Northland Communications

Liberty Puerto Rico

Ritter Communications

Santel Communications

Satview Broadband

Service Electric

Shentel

SRT Communications

TDS Telecom

TPG
 Grande Communications

RCN Corporation

Wave Broadband


Troy Cablevision

TruVista Communications

WOW!


Satellite MVPD 
 Claro

Dish Network

DirecTV

Glorystar

Headend in the Sky

Orby TV


Fiber MVPD / IPTV 
 AT&T U-verse

CenturyLink
 Prism TV


Cincinnati Bell Fioptics

Claro

Consolidated Communications
 FairPoint Communications


EPB

Frontier FiOS

Google Fiber

GTA Teleguam

Hawaiian Telcom

Midco

NEP Datastream TV

North State Communications

Smithville Fiber

Sonic.net

TDS Telecom

Verizon FiOS

Whidbey Telecom

Windstream Kinetic


Virtual MVPD 
 AT&T
 AT&T TV

WatchTV


FuboTV

Hulu with Live TV

Philo

Sling TV

Spectrum TV Stream

TVision Home

Xfinity Instant TV

YouTube TV


Over- the-top video 
 A&E Networks
 Crime Central

History Vault

Lifetime Movie Club


Amazon
 Prime Video

IMDb TV


AMC Networks
 Acorn TV

AMC Premiere

IFC Films Unlimited

Shudder

Sundance Now

UMC


Anime Network

Apple
 Apple TV+


BroadwayHD

Crackle Plus

CuriosityStream

Disney
 Disney+

Hulu

ESPN+


Facebook Watch

Fandor

Fox Corporation
 Fox Nation

Tubi


FunimationNow

Hallmark Movies Now

Lionsgate
 Starz

Tribeca Shortlist


LocalBTV

Locast

MGM
 Epix Now


Netflix

NBCUniversal
 Peacock

Xumo


Quibi

Reelz

Roku Channel

Stirr

UFC Fight Pass

Univision NOW

ViacomCBS
 BET+

CBS All Access

Comedy Central Now

CW Seed

MTV Hits

NickHits

Noggin

Pluto TV

Showtime

Smithsonian Channel Plus


WarnerMedia
 Boomerang

Cinemax

Crunchyroll

DC Universe

HBO Now

HBO Max


WWE Network

YuppTV


Defunct Cable 
 Adelphia Communications Corporation

Alameda Power and Telecom

Astound Broadband

AT&T Broadband
 MediaOne/Continental Cablevision

Tele-Communications Inc.


Baja Broadband
 US Cable


Bresnan Communications

Bright House Networks

Cablevision

Champion Broadband

Cobridge Communications

Community Home Entertainment

Graceba Total Communications

Insight Communications

Jones Intercable

King Videocable

Knology

Marcus Cable

NPG Cable

Paragon Cable

Rapid Communications

TelePrompTer/Group W Cable

Time Warner Cable

UA-Columbia Cablevision

Windjammer Communications


Satellite 
 AlphaStar

GlobeCast World TV

PrimeStar

United States Satellite Broadcasting

Voom HD Networks


IPTV 
 Sky Angel

Virtual Digital Cable


Terrestrial 
 USDTV

MovieBeam


Virtual MVPD 
 CenturyLink Stream

HD HomeRun Premium TV

PlayStation Vue


Over-the-top 
 Aereo

DramaFever

FilmStruck

go90

Seeso


 
   v
t
e
 Additional resources on North American television
North America 
 List of local television stations in North America

DTV transition

North American TV mini-template


Canada 
 Canadian networks

List of Canadian television networks

List of Canadian television channels

List of Canadian specialty channels

Local Canadian TV stations

List of United States stations available in Canada

2001 Vancouver TV realignment

2007 Canada broadcast TV realignment


Mexico 
 Mexican networks

Local Mexican TV stations


United States 
 American networks

List of American cable and satellite networks

List of American over-the-air networks

Television markets

Major network affiliates

Local American TV stations (W)

Local American TV stations (K)

Spanish-language TV networks

1994 United States broadcast TV realignment

2006 United States broadcast TV realignment

List of Canadian television stations available in the United States


 
 
 Middle East, Africa, Asia and Oceania

Americas

Europe


 





