  2019 American film directed by M. Night Shyamalan



Glass
 U.K. theatrical release poster
Directed byM. Night Shyamalan
Produced by 
 M. Night Shyamalan

Jason Blum

Marc Bienstock

Ashwin Rajan


Written byM. Night Shyamalan
Starring 
 James McAvoy

Bruce Willis

Anya Taylor-Joy

Sarah Paulson

Samuel L. Jackson


Music byWest Dylan Thordson
CinematographyMike Gioulakis
Edited by  Luke Ciarrocchi
Blu Murray

 Production company   
 Blinding Edge Pictures[1]

Blumhouse Productions[1]

Perfect World Pictures


Distributed by 
 Universal Pictures (United States)

Buena Vista International (International)[2]


 Release date 
 January 12, 2019 (2019-01-12) (Alamo Drafthouse Cinema)[3]

January 18, 2019 (2019-01-18) (United States)


 Running time129 minutes[4]
CountryUnited States
LanguageEnglish
Budget$20 million[5]
Box office$247 million[5]

Glass is a 2019 American psychological superhero thriller film[6] written and directed by M. Night Shyamalan, who also produced with Jason Blum, Marc Bienstock, and Ashwin Rajan. The film is a crossover and sequel to Shyamalan's previous films Unbreakable (2000) and Split (2016) and the third and final installment in the Unbreakable trilogy a.k.a. Eastrail 177 Trilogy.[7] Bruce Willis, Samuel L. Jackson, Spencer Treat Clark, and Charlayne Woodard reprise their Unbreakable roles, while James McAvoy and Anya Taylor-Joy return as their Split characters,[8] with Sarah Paulson, Adam David Thompson, and Luke Kirby joining the cast.
Despite interest in a sequel, Touchstone Pictures opted not to finance one. Shyamalan set out to write Split using a character he had written for Unbreakable but pulled from its script due to balance issues. Shyamalan realized the opportunity he had to create a trilogy of works, and used the ending of Split to establish the film as within the Unbreakable narrative. This included securing the rights to use Willis's Unbreakable character from Walt Disney Studios, with the promise of including Disney within the production and distribution of this third film alongside Universal Pictures. Split was a financial and critical success, and by April 2017, Shyamalan announced that he started the production process for Glass.
The film had its world premiere at Alamo Drafthouse Cinema on January 12, 2019 and was released in the United States on January 18, 2019, by Universal Pictures. Glass received generally unfavorable reviews from critics, who found the film "disappointing" and "underwhelming".[9][10][11] Nevertheless, the film was a financial success as it grossed $247 million worldwide against a $20 million production budget.

  Contents
 
1 Plot

2 Cast

3 Production
 
3.1 Development

3.2 Casting

3.3 Filming

3.4 Post-production

3.5 Music




4 Marketing

5 Release
 
5.1 Home media




6 Reception
 
6.1 Box office

6.2 Critical response

6.3 Accolades




7 Future

8 Notes

9 References

10 External links




Plot
Three weeks after the "Horde" incident,[a] David Dunn, a superhuman who survived the Eastrail 177 train wreck nineteen years ago,[b] now operates as a vigilante dubbed the "Overseer" alongside his son Joseph. Together, they track down Kevin Wendell Crumb, the "Horde", at an abandoned factory where he holds four cheerleaders hostage. David engages Kevin in a brief battle until armed forces led by Dr. Ellie Staple intervene and imprison them at the Raven Hill Memorial Mental Institute. Also being kept there is David's "destined" foe and global terrorist Elijah Price, who is kept under sedation and is considered completely harmless.
David and Kevin are placed into separate rooms that contain unique security measures based on their specific weaknesses. Staple explains that she believes they suffer from delusions of grandeur and do not have superpowers. Elijah's mother Mrs. Price, Joseph Dunn, and Casey Cooke, a victim who survived Kevin/the Horde's captivity, try and fail to convince Staple superhumans are real. As part of her final evaluation, Staple brings the three men to a room where she challenges them with explanations of why their seemingly superhuman abilities are not extraordinary. Kevin and David become distraught while Elijah remains catatonic. That night, Elijah escapes his cell to conduct research on Kevin before visiting him and telling him he feigned his sedated state and plans to escape the institute but requires one of Kevin's personalities—the Beast—to help him. Staple discovers Elijah's escape plan and proceeds to do a prefrontal lobotomy to silence him.
Upon returning to his cell, Elijah reveals he sabotaged the surgical laser and kills his caretaker Daryl before freeing Kevin. Elijah then manipulates David into fighting the Beast by revealing his plan to destroy a chemical lab inside a new skyscraper in Philadelphia that would kill thousands. David breaks free from his cell and pursues The Beast. Mrs. Price, Joseph and Casey arrive just as the trio escape. While David and the Beast fight, Joseph reveals that Kevin's father died in the train wreck caused by Elijah Price nineteen years earlier. Despite thanking him for his creation, the Beast mortally wounds Elijah out of distrust. Casey manages to bring Kevin's dormant original personality out, inadvertently allowing the police to fatally shoot him. Staple's men drown David in a flooded pothole while Staple reveals that she is part of a clandestine organization that has been suppressing the existence of superhumans for millennia to keep people from knowing about them and killing those with superhuman abilities as they see them as a threat to the balance of the world.
In the aftermath, Staple deletes the footage from the institution, only to realize Elijah had secretly live-streamed the events to a private network. Mrs. Price, Joseph, and Casey all receive a copy of the footage and upload it to the public, exposing the existence of superhumans to the world.

Cast
 James McAvoy as Kevin Wendell Crumb / The Horde: A 27-year-old former Philadelphia Zoo employee with 23 different personalities whose body chemistry changes with each personality, culminating in a 24th personality known as "The Beast", a savage superhuman cannibal whose abilities include wall-crawling, enhanced strength, speed, durability, and agility.[12]
 Owen Vitullo portrays an 8-year-old Kevin.


Bruce Willis as David Dunn / The Overseer: A superhuman vigilante with enhanced strength and durability, as well as the ability to see the crimes people have committed by touching them. In the film, Dunn goes by a new alias, "The Overseer".[13]
 Colin Becker portrays a 10-year-old David.


Samuel L. Jackson as Elijah Price / Mr. Glass: An intelligent wheel-chair bound mass murderer and comic book theorist with Type I osteogenesis imperfecta, who was institutionalized after David Dunn discovered his crimes.
 William Turner portrays a young Elijah

Johnny Hiram Jamison plays a 13-year-old Elijah via photographs.


Sarah Paulson as Dr. Ellie Staple: A psychiatrist specializing in delusions of grandeur who treats patients convinced they are superhuman beings and attempts to prove Dunn, Price and Crumb are not superhumans by any means she finds.

Anya Taylor-Joy as Casey Cooke: A 17-year-old girl with a history of abuse who was kidnapped by one of Kevin's identities as a potential sacrifice to "The Beast" but managed to survive.

Spencer Treat Clark as Joseph Dunn: David's son who has believed in his father's abilities since he was a child and sees him as a real-life superhero.

Charlayne Woodard as Mrs. Price: Elijah's mother, who took great care of her son, and always told him he was special no matter what others said.

Luke Kirby as Pierce: One of Elijah's caretakers at the facility.

Adam David Thompson as Daryl: An employee at the psych ward.

M. Night Shyamalan reprises his cameo role of Jai, the security guard from Dr. Fletcher's apartment building in Split, who recognizes and questions David Dunn if he worked at the football stadium, indicating that Jai is also the same stadium drug dealer from Unbreakable that David confronted. Additionally, Shannon Destiny Ryan, Diana Silvers, Nina Wisner, and Kyli Zion portrayed the cheerleaders at the start of the film whom one of Kevin's personalities kidnapped. Rosemary Howard and Bryan McElroy portray Kevin's parents, Penelope and Clarence, Howard reprising her role from Split.

Production
Development
After Unbreakable's release in 2000, rumors of possible sequels began circulating in different interviews and in film fansites. In 2000, Bruce Willis was quoted as hoping for an Unbreakable trilogy.[14] In December 2000, M. Night Shyamalan denied rumors he had written Unbreakable as the first installment of a trilogy.[14] In August 2001, he stated that because of successful DVD sales, he had approached Touchstone Pictures about an Unbreakable sequel, an idea Shyamalan said the studio originally declined because of the film's disappointing box office performance.[15]
In September 2008, Shyamalan and Samuel L. Jackson's stated discussions about making a sequel had been largely abandoned in light of the disappointing box office returns. Jackson indicated he was still interested in a sequel, but Shyamalan remained noncommittal.[16] In February 2010, Willis said that Shyamalan was "still thinking about doing the fight movie between me and Sam" and stated that as long as Jackson was able to participate, he would be "up for it".[17]
Shyamalan continued to work on other films, releasing Split in 2017. Kevin Wendell Crumb, played by James McAvoy, is a man suffering from dissociative identity disorder, that affects his body chemistry to such an extent that he adopts the mannerisms of each separate persona. One of these personalities is "The Beast", which drives Crumb's body into a feral superhuman state, guided by the desire to consume those who have not had a traumatic situation in their lives – those it does not consider "broken". Crumb had initially been written into the script for Unbreakable, but Shyamalan felt there were balancing issues with his inclusion and removed him from the story. Split was effectively rewritten from some of the scenes he had planned for Crumb and expanded out into a standalone picture.[18]
The final scene for Split includes the appearance of David Dunn, played by Willis. Shyamalan included Dunn there to connect Split to Unbreakable, by showing Dunn learning about the escape of "The Beast" and thereby realizing that other superhumans exist, as predicted by Mr. Glass (Jackson).[19] In 2017, Shyamalan stated although he hoped a third Unbreakable film would be made and already had an outline prepared, "I don't know what's going to happen when I go off in my room, a week after this film opens, to write the script."[20]
Unbreakable had been produced for and owned by Touchstone Pictures, a label of Walt Disney Studios, while Split was produced through Universal Pictures. Shyamalan obtained permission from Disney to reuse the character of Dunn. He met with Walt Disney Studios president Sean Bailey and came to a gentlemen's agreement whereby Bailey agreed to allow the use of the character in the film without a fee, and Shyamalan promised that Disney would be involved in a sequel, if developed.[21]
Split was met with critical and financial success and, in February 2017, Shyamalan affirmed his next film would be the third work in the trilogy.[22][23] Shyamalan finished the script by April 2017, announcing that it would be called Glass and have a target release date of January 18, 2019.[24][25] Universal was selected to distribute the film in the United States, while Disney distributes the film internationally through its Buena Vista International label.[26][27]

Casting
The cast includes returning actors from both films: Bruce Willis, Samuel L. Jackson, Spencer Treat Clark, and Charlayne Woodard from Unbreakable and James McAvoy and Anya Taylor-Joy from Split, all reprise their respective roles,[28][29] while Sarah Paulson plays a new character.[30][31] In November 2017, Adam David Thompson joined the cast in a then undisclosed role.[32]

Filming
Principal photography began on October 2, 2017, in Philadelphia, following a week of rehearsals,[33] with plans for a thirty-nine–day shoot.[34] On October 31, 2017, it was reported that Shyamalan would be filming at Allentown State Hospital for a few weeks.[35] On December 12, 2017, Shyamalan revealed that four scenes would be shot in January 2018, stating he would have to travel for their filming.[36]

Post-production
Deleted footage from Unbreakable was used as flashbacks to Elijah and Joseph's childhood.[37]

Music
West Dylan Thordson returned to score the film after his collaboration with the director on Split. He used themes from the score of Unbreakable by James Newton Howard, alongside that of Split, in composing Glass's music. The score is distributed digitally by Back Lot Music.[38]

Marketing
On April 25, 2018, the film was featured at CinemaCon, with Shyamalan in attendance. He presented footage from the film, along with the first official image, featuring Willis, Jackson, and McAvoy in character. He also expressed his intention for the film, saying, "The worlds of Unbreakable and Split finally collide in Glass. What if these real life superheroes and super-villains are somehow locked up together? What could go wrong?" Despite being preceded by hyper-realistic films about superheroes and villains, like The Dark Knight,[39] Shyamalan personally considered Glass to be the "first truly grounded comic book movie".[40]
On July 12, 2018, the first official photographs from production were released publicly, including shots of Samuel L. Jackson, Sarah Paulson, and James McAvoy.[41] A week later, the film was promoted at San Diego Comic-Con, with Shyamalan, Willis, Jackson, Taylor-Joy, and Paulson attending a panel, where the film's first trailer premiered.[42] The studio spent a total of $80 million promoting the film.[43]

Release
Glass was theatrically released on January 18, 2019, in the United States and Canada by Universal Pictures and in international territories by Buena Vista International, a sub-division of Walt Disney Studios Motion Pictures. The first screening for Glass occurred on January 12, 2019, at 25 Alamo Drafthouse Cinema locations.[3]

Home media
Glass was released by Universal Pictures Home Entertainment in the U.S., and Walt Disney Studios Home Entertainment internationally, under the Buena Vista Home Entertainment label, on digital on April 2, 2019,[44] and on Blu-Ray and DVD on April 16, 2019.[45]

Reception
Box office
Glass grossed $111 million in the United States and Canada, and $135.9 million in other territories, for a total worldwide gross of $247 million, against a production budget of $20 million.[5] Deadline Hollywood calculated the film made a net profit of $68 million, when factoring together all expenses and revenues.[43]
In the United States and Canada, Glass was projected to make $50–75 million from 3,841 theaters over its four-day MLK Day opening weekend.[46] It made $16 million on its first day, including $3.7 million from Thursday night previews, and went on to gross $40.5 million in its opening weekend, and $46.5 million over the four days, marking the third-best total for Martin Luther King Jr. weekend and of Shyamalan's career.[47] In its second weekend the film fell 53% to $19 million (a steeper drop than Split’s 35%), albeit retaining the top spot at the box office.[48] The film then finished first for a third straight weekend, grossing $9.5 million,[49] before finally being dethroned in its fourth weekend, grossing $6.3 million and finishing fifth.[50]
Internationally, the film was expected to gross $45–50 million in its first weekend, for a total of global opening of $105–120 million.[51] It ended up making $48.5 million from international markets, with a global opening of $89.1 million. It finished first in most markets; its highest-grossing countries were Russia ($5.2 million), Mexico ($4.5 million, the best-ever for a Shyamalan film), the United Kingdom ($4.3 million), France ($3.4 million), and South Korea ($2.8 million).[52]

Critical response
On review aggregator Rotten Tomatoes, the film holds an approval rating of 37% based on 377 reviews, with an average rating of 5.12/10. The website’s critical consensus reads: "Glass displays a few glimmers of M. Night Shyamalan at his twisty world-building best, but ultimately disappoints as the conclusion to the writer-director's long-gestating trilogy."[53] On Metacritic, which assigns weighted average ratings to reviews, the film has a score of 43 out of 100, based on 53 critics, indicating "mixed or average reviews".[54] Audiences polled by CinemaScore gave the film an average grade of "B" on an A+ to F scale, down from Split's "B+" but up from Unbreakable's "C", while those at PostTrak gave it an overall positive score of 70% and a "definite recommend" of 49%.[47]
David Ehrlich of IndieWire gave the film a "C−" and called it the biggest disappointment of Shyamalan's career: "The trouble with Glass isn't that its creator sees his own reflection at every turn, or that he goes so far out of his way to contort the film into a clear parable for the many stages of his turbulent career; the trouble with Glass is that its mildly intriguing meta-textual narrative is so much richer and more compelling than the asinine story that Shyamalan tells on its surface."[55] Writing for Rolling Stone, David Fear gave the film three out of five stars "Glass is not the flaming flop some folks have already suggested it is, nor is it the movie you want in terms of tying ambitious, highfalutin’ notions together about how we process our pulp mythos. In a world in which all movies are now either genocide or ice cream, it's a grand gesture characterized by a sense of ambivalence about what you've just seen – which may in and of itself be a sign of failure".[56]
Owen Gleiberman of Variety wrote: "It's good to see Shyamalan back (to a degree) in form, to the extent that he's recovered his basic mojo as a yarn spinner. But Glass occupies us without haunting us; it's more busy than it is stirring or exciting. Maybe that's because revisiting this material feels a touch opportunistic, and maybe it's because the deluge of comic-book movies that now threatens to engulf us on a daily basis has leeched what's left of the mystery out of comics."[57] Richard Roeper of the Chicago Sun-Times called the film "an underwhelming, half-baked, slightly sour, and even off-putting finale."[58] Joshua Rivera of GQ magazine stated "The timeline is barely comprehensible, with twists so openly telegraphed they’d have saved the Titanic."[59]
David Sims of The Atlantic compared the film to Batman Returns and Incredibles 2: "I appreciate the sheer brashness of Shyamalan's storytelling, which swirls the mythmaking inherent in characters such as David with the emotional scars borne by orphaned characters such as Superman."[60] Praise was also reserved for McAvoy who "once again [was] top notch" and "lit up the screen with his eerie physicality every time he appears."[61]

Accolades


Awards and nominations for Glass


List of awards and nominations



Award

Date of ceremony

Category

Recipient(s) and nominee(s)

Result



Fright Meter Awards

2019[62]

Best Supporting Actor

Samuel L. Jackson

Nominated



Best Supporting Actor

James McAvoy

Nominated



Best Supporting Actress

Sarah Paulson

Nominated



Best Director

M. Night Shyamalan

Nominated



Best Ensemble Cast

Glass

Nominated



Golden Raspberry Awards

March 16, 2020[63]

Worst Supporting Actor

Bruce Willis

Nominated



Golden Schmoes Awards

February 7, 2020[62]

Biggest Disappointment of the Year

Glass

Nominated



Golden Trailer Awards

May 29, 2019[62]

Best Thriller Poster

Glass

Won



Best Motion Poster

Glass

Nominated



Most Original Poster

Glass

Nominated



Hawaii Film Critics Society

January 13, 2020[62]

Worst Film of the Year

Glass

Nominated



London Critics Circle Film Awards

January 30, 2020[62]

Young British/Irish Performer of the Year

Anya Taylor-Joy

Nominated



People’s Choice Awards

November 10, 2019[62]

Favorite Drama Movie

Glass

Nominated



Favorite Drama Movie Star

Samuel L. Jackson

Nominated



Favorite Drama Movie Star

Sarah Paulson

Nominated



Saturn Awards

September 13, 2019[62]

Best Action or Adventure Film

Glass

Nominated



Yoga Awards

2020[62]

Worst Foreign Director

M. Night Shyamalan

Won


Future
M. Night Shyamalan has been asked numerous times if there would be any sequel for Glass. On January 8, 2019, he officially confirmed that no sequels are currently planned, adding that he has no interest building a cinematic universe.[64]

Notes

References

External links



Wikiquote has quotations related to: Glass (2019 film)


 Official website

Glass on IMDb

   v
t
e
 Films directed by M. Night Shyamalan
Films 
 Praying with Anger (1992)

Wide Awake (1998)

The Sixth Sense (1999)

Unbreakable (2000)

Signs (2002)

The Village (2004)

Lady in the Water (2006)

The Happening (2008)

The Last Airbender (2010)

After Earth (2013)

The Visit (2015)

Split (2016)

Glass (2019)


Related Unbreakable (film series)

   v
t
e
 Blinding Edge Pictures
Company founders 
 M. Night Shyamalan


Films 
 Unbreakable (2000)

Signs (2002)

The Village (2004)

Lady in the Water (2006)

The Happening (2008)

The Last Airbender (2010)

Devil (2010)

After Earth (2013)

The Visit (2015)

Split (2016)

Glass (2019)


Television series 
 Wayward Pines (2015–16)

Servant (since 2019)



  Film portal
Philadelphia portal
Speculative fiction portal
Speculative fiction/Horror portal





