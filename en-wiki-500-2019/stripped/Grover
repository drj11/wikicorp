  Character on Sesame Street
 This article is about the Sesame Street character. For other uses, see Grover (disambiguation).
  This article needs additional citations for verification. Please help improve this article by adding citations to reliable sources. Unsourced material may be challenged and removed. Find sources: "Grover" – news · newspapers · books · scholar · JSTOR  (June 2018) (Learn how and when to remove this template message)

Grover
Sesame Street character

First appearanceThe Ed Sullivan Show on December 24, 1967 (as Gleep) Sesame Street on May 1, 1970 (as Grover)
Created byFrank Oz[1]
Portrayed by  Frank Oz (1970–2012)
Eric Jacobson (1998–present)

In-universe information
AliasSuper Grover
SpeciesMuppet Monster
GenderMale

Grover, also known as Super Grover and Grover Monster, is a Muppet character on the popular PBS/HBO children’s television show Sesame Street. Self-described as lovable, cute and furry, he is a blue monster who rarely uses contractions when he speaks or sings. Grover was originally performed by Frank Oz from his earliest appearances. Eric Jacobson began performing Grover in 1998 and has performed the character regularly since 2000.

  Contents
 
1 Origins

2 Appearances
 
2.1 Charlie's Restaurant

2.2 Other segments

2.3 Super Grover

2.4 Grover's Mother ("Mommy")




3 Books

4 International

5 References




Origins
A prototype version of Grover appeared on The Ed Sullivan Show on Christmas Eve in 1967. This puppet had greenish-brown fur and a red nose. He also had a raspier voice and was played a bit more unkempt than Grover would later behave. The monster was referred to as "Gleep", a monster in Santa's workshop. He later made a cameo appearance in The Muppets on Puppets in 1968 with the Rock and Roll Monster. In 1969, clad in a necktie, he appeared in the Sesame Street Pitch Reel in the board-room sequences. During the first season of Sesame Street, the character was nicknamed "Fuzzyface" or "The Hairy One", though neither would be used for his actual name. In his book The Tipping Point, author Malcolm Gladwell notes that the character "was used in promotional films for IBM". The puppet first received the name "Grover" on May 1, 1970.
In an appearance on The Ed Sullivan Show on May 31, 1970, the character acquired his present   appearance with blue fur and a pink nose. In this appearance, Kermit the Frog tried to sing "What Kind of Fool Am I?" (accompanying himself on piano), but Grover repeatedly interrupted him. The true Grover "officially" debuted in the second season of Sesame Street.

Appearances
      The Deputy Secretary of State Antony Blinken meets with Grover to talk about refugees at the United Nations in New York City, 2016.
Charlie's Restaurant
One of the more frequent sketch segments featuring Grover involves him taking a series of customer service jobs. One of his customers is always Mr. Johnson, a balding, mustachioed customer who invariably becomes frustrated at Grover's bumbling service and/or his (Grover's) insistence that he is serving him properly.
The first Grover-Mr. Johnson series of sketches, set at "Charlie's Restaurant", aired in the early 1970s; here, Grover is employed as a waiter and Mr. Johnson is his customer.
The sketches followed the same basic premise: Mr. Johnson would order a menu item, Grover would serve the customer, a disagreement results (usually) as a result of Grover's mistakes, and Grover attempting (often, more than once) to correct the mistake with varied degrees of success. Under this backdrop, the sketches served to teach the childhood audience basic concepts such as same and different, big and little, hot and cold, the alphabet, following directions and patience, among other things. This was even parodied in an episode of Monsterpiece Theater, where Grover had to keep rushing out of the kitchen to tell Johnson that they had run out of parts of his order. Naturally, Alistair Cookie introduced this performance as "Much Ado About Nothing".
Repeats of the "Charlie's Restaurant" series of sketches aired for many years on Sesame Street. In the years since, new Grover-Mr. Johnson sketches have been produced, with Grover taking other customer service jobs and Mr. Johnson as his hapless customer. Every time, Mr. Johnson recognizes Grover as "that waiter from Charlie's".
Grover's jobs have ranged from a taxi driver and a photographer to a flight attendant and singing telegram artist. One sketch parodied the ABC television series Extreme Makeover: Home Edition in a segment where Grover began remodeling Mr. Johnson's home despite his express wishes. In another one, Mr. Johnson is the only patron, and Grover is the only actor, for a production of Spider-Monster: The Musical, a parody of the musical Spider-Man: Turn Off the Dark. The play is, of course, a complete calamity and finally comes crashing down on both of them.

Other segments
Grover also has an instructional persona who wears a cap and gown to provide educational context for simple, everyday things. His lessons are often wrong, leaving himself open to correction by a group of children or Muppets. This, combined with the failings of the Super Grover character, means that Grover can be very self-conscious and timid. He is often a source of slapstick humor and often accidentally injures himself.
Early in the series, Grover would often greet Kermit the Frog by running up to him and yelling, "Hey, froggy babeee!" and then giving him a hard slap on the back, which knocked him over.
Global Grover is a more recent series of segments, in which Grover hosts a trip to a foreign country to learn about their culture and customs.
In 2010, Grover starred in a parody of an Old Spice Commercial called "Smell Like a Monster" based on "The Man Your Man Could Smell Like", albeit a clam with "two tickets to the thing you love" bit his nose and he rode a cow rather than a horse.

Super Grover
Grover has a semi-secret superhero identity as the well-meaning but inept Super Grover, sometimes presented as the alter ego of Grover Kent, "ace doorknob salesman for Acme, Inc". Originally his superhero costume consisted of a pink cape and medieval knight's helmet, with a Superman-style crest on both the cape and his T-shirt, bearing a letter "G" on his chest instead of "S". During the 1970s and 1980s, Sesame Street ran a series of Super Grover sketches spoofing the classic Adventures of Superman series (in the opening of these, his name was hyphenated "Super-Grover"). An announcer (Jerry Nelson) introduced each episode with the lines:


Presenting the further adventures of everybody's favorite hero. The man who's faster than lightning, stronger than steel, smarter than a speeding bullet. It's... Super Grover!


With that, a fanfare sounds, Super Grover bursts through a paper wall bearing his crest, fruitlessly tries to move his helmet up off his eyes, and adds, "And I am cute, too!"


Announcer: And now, on to our story.
Super Grover: Yes! On to our story!


From there, episodes followed a simple formula: Super Grover is flying somewhere over Metro City when he hears the cries of a Muppet child in some small trouble and immediately sails in to assist. The excited child is quickly disillusioned as Super Grover crash-lands nearby. From there, Grover continues to be enthusiastic but no help whatsoever, his "dramatic" feats of strength or speed serving only to kill time while the child solves the problem on their own and wanders off. By then, Grover's efforts have usually landed him in a comical predicament of his own.
Super Grover has appeared in the Sesame Street theatrical films Follow that Bird (1985) and The Adventures of Elmo in Grouchland (1999, where it is revealed he stretches his arms out and spins into his costume in homage to Wonder Woman), as well as the 1983 PBS special Don't Eat the Pictures.
For Sesame Street's 41st season in 2010, the character was revamped as Super Grover 2.0, who debuted on Late Night with Jimmy Fallon, flying in and crash-landing behind the chair where he was meant to sit. His new costume consists of a helmet reminiscent of a Spartan or Centurion, a red cape, and a black rubber vest resembling bike racing gear. Both the cape and the vest are adorned with his crest, now with a lightning bolt added behind the "G". Of course, the helmet has a hinged visor which still tends to fall over Grover's eyes.  But now the tagline is Super Grover 2.0 – He Shows Up!

Grover's Mother ("Mommy")
"Grover's Mommy" plays an integral but often unrecognized role on Sesame Street. She has been seen almost exclusively in print, including the many illustrated books starring Grover. She was also occasionally seen in photographs, as a photo puppet, such as on the cover of Volume 4 of The Sesame Street Treasury. Over the course of time, her appearance has fluctuated greatly.
Her earliest known appearance as a Muppet is a 1970s sketch in which Grover speaks to the audience about being afraid of the dark. At the end of the sketch, his mom (Frank Oz) enters his room to kiss him goodnight. In this appearance, the puppet used for her is recycled from the early Grover puppet from the first season. Another early appearance (circa 1981) involves his mother (Kathryn Mullen) coming into the bathroom while Grover is telling the audience about how to take a bath.
She has more recently appeared (performed by Stephanie D'Abruzzo) in a brief Elmo's World sequence (from the "Families" episode), with her son as his alter-ego Super Grover, as her own alter-ego, "Super-Mommy". Grover crashlands, screaming "Moooommy!" and his mom follows yelling "Soooonny!" crashing on top of him. They recover, acknowledge each other, and both faint.

Books
In the 1971 children's book The Monster at the End of This Book, Grover goes to great effort to keep the reader from turning the pages of the book, because there is a monster on the final page. Grover nails pages together and builds a brick wall to block access; at the end it is discovered that the monster at the end of the book is Grover himself, who is mortified ("Oh, I am so embarrassed..."). The late 1990s saw a sequel to the book where Grover desperately tries to stop Elmo from reaching the end of the book, eventually directing him to leave the book and enter from the back. Therefore, when both of them reach the end, they wind up scaring each other.
Another popular children's book, Would You like to Play Hide & Seek in This Book with Lovable, Furry Old Grover?, had Grover trying different ways to hide from the reader, eventually getting upset and begging the reader to just say "no" they do not see him, even though he was just crouching down in a corner.
In 1974, Grover went on a learning expedition in Grover and The Everything In The Whole Wide World Museum.  He tours rooms such as "The Long Thin Things You Can Write With Room", as well as "The Things That Make So Much Noise You Can't Think Room". Grover wanders through "The Things That are Light Room", returns a rock to "The Things That are Heavy Room", and just when he wonders whether it is possible to have a museum that holds everything in the whole wide world, he comes upon a door labeled "Everything Else", which opens to take him out into the world. As of 1996, Publishers Weekly ranked the book at seventy-nine on their list of best-selling children's paperbacks,[2] and Lou Harry of Indianapolis Business Journal included the book on his list of twelve examples of how muppets have qualified as quality entertainment.[3] It was written by Norman Stiles and Daniel Wilcox, and illustrated by Joe Mathieu.
The Adventures of Grover in Outer Space is a Sesame Street storybook featuring Grover that was published in 1984.[4] When Grover Moved to Sesame Street was published in 1985.[5] He was also featured in I Want a Hat Like That (1987, reprinted 1999).[6]

International
Sesame Street is modified for different national markets, and Grover is often renamed..

 In Afghanistan, his name is Kajkoal,[7] meaning a bowl and refers to his mouth.

In Brazil, he is known as Arquibaldo, although in the 2007 versions maintains the name Grover.

In Czech Republic, he is called Bohouš.

In Egypt, he is called Gaafar.

In Germany, he is Grobi, a diminutive of the German grob, meaning "rough" or "rude".

In Gulf Cooperation Council, he is called Qarqor

In Indonesia, he is called Gatot.

In Israel, he is called Kruvi, which is a play on the word kruv ("cabbage").

In Latin America and Puerto Rico, he is known as Archibaldo.

In Norway, he is known as Gunnar and voiced by Harald Mæle.

In Pakistan, he is Banka, meaning immature or youthful.

In Poland, he is called Florek ("Sesame Street" only).

In Portugal, he is Gualter (Walter).

In South Korea, he is Geurobeo (그로버).

In Spain, he is called Coco, which is Spanish for coconut, referring to the shape of his head and mouth.

In Turkey, he is known as Açıkgöz, meaning "leery".

References

   v
t
e
 Sesame Street
  General
 
 Fictional location

Sesame Workshop
 productions


Characters
 Muppets

human

animated


Educational goals

Format

Influence

Licensing

Recurring segments

Accolades


 
  People
 
 Joan Ganz Cooney

Lloyd Morrisett

Gerald S. Lesser

Jon Stone

Jim Henson

Frank Oz

Caroll Spinney

Richard Hunt

Steve Whitmire

Ryan Dillon

Martin P. Robinson

Jerry Nelson

Joe Raposo

Kevin Clash

Kermit Love

Joey Mazzarino

Carol-Lynn Parente

List of guest stars

List of puppeteers


 
  Production
 
 History

Research

International co-productions
 characters


Elmo's World

"Snuffy's Parents Get a Divorce"

Music
 discography

songs

theme song



 
  Films
 
 Sesame Street Presents: Follow That Bird (1985)

The Adventures of Elmo in Grouchland (1999)

Untitled Sesame Street film (2022)


 
  Television specials
 
 This Way to Sesame Street (1969)

Julie on Sesame Street (1973)

Out to Lunch (1974)

Christmas Eve on Sesame Street (1978)

A Special Sesame Street Christmas (1978)

Big Bird in China (1982)

Don't Eat the Pictures: Sesame Street at the Metropolitan Museum of Art (1983)

Bedtime Stories and Songs (1986)

I'm Glad I'm Me (1986)

Learning About Letters (1986)

Learning About Numbers (1986)

Play-Along Games and Songs (1986)

Big Bird's Story Time (1987)

Getting Ready for School (1987)

Learning to Add and Subtract (1987)

Sing-Along (1987)

A Muppet Family Christmas (1987)

The Alphabet Game (1988)

The Best of Ernie and Bert (1988)

Big Bird's Favorite Party Games (1988)

Count it Higher (1988)

Put Down the Duckie: A Sesame Street Special (1988)

Big Bird in Japan (1989)

Sesame Street… 20 Years & Still Counting (1989)

Dance Along! (1990)

Monster Hits! (1990)

Rock & Roll! (1990)

Sing Yourself Silly! (1990)

Big Bird's Birthday or Let Me Eat Cake (1991)

Elmo's Sing-Along Guessing Game (1991)

Sing, Hoot and Howl (1991)

Sesame Street: 25 Wonderful Years (1993)

Sesame Street Stays Up Late! (1993)

Sing-Along Earth Songs (1993)

We All Sing Together (1993)

A New Baby in My House (1994)

Sesame Street Jam: A Musical Celebration (1994)

Sesame Street: All Star 25th Birthday: Stars and Street Forever! (1994)

The Best of Elmo (1994)

Big Bird Sings! (1995)

Cookie Monster's Best Bites (1995)

Learning to Share (1996)

Do the Alphabet (1996)

Elmocize (1996)

Slimey's World Games (1996)

Elmo Saves Christmas (1996)

Get Up and Dance (1997)

Telling the Truth (1997)

Quiet Time (1997)

Sing Yourself Sillier at the Movies (1997)

123 Count with Me (1997)

Elmo Says Boo! (1997)

William Wegman's Mother Goose (1997)

Big Bird Gets Lost (1998)

Fiesta! (1998)

Elmopalooza (1998)

The Alphabet Jungle Game (1998)

The Great Numbers Game (1998)

The Best of Kermit on Sesame Street (1998)

Let's Eat! Funny Food Songs (1999)

Kids' Favorite Songs (1999)

The Adventures of Elmo in Grouchland: Sing and Play (1999)

CinderElmo (1999)

Let's Make Music (2000)

Elmo's Musical Adventure: Peter and the Wolf (2001)

Elmo's Magic Cookbook (2001)

Kids' Favorite Songs 2 (2001)

Bert and Ernie's Word Play (2002)

Elmo Visits the Firehouse (2002)

Sesame Sings Karaoke (2003)

Three Bears and a New Baby (2003)

Zoe's Dance Moves (2003)

The Street We Live On! (2004)

What's the Name of That Song? (2004)

A Magical Halloween Adventure (2004)

All Star Alphabet (2005)

Elmo Visits the Doctor (2005)

Friends to the Rescue (2005)

Happy Healthy Monsters (2005)

Guess That Shape and Color (2006)

Elmo's Potty Time (2006)

A Sesame Street Christmas Carol (2006)

Elmo's Christmas Countdown (2007)

Kids' Favorite Country Songs (2007)

Love the Earth! (2008)

Abby in Wonderland (2008)

Bedtime with Elmo (2009)

The Best of Elmo 2 (2010)

The Cookie Thief (2015)

The Best of Elmo 3 (2015)

Once Upon a Sesame Street Christmas (2016)

The Magical Wand Chase (2017)

The Best of Elmo 4 (2018)

When You Wish Upon a Pickle (2018)

Sesame Street's 50th Anniversary Celebration (2019)

Sesame Street: Elmo's Playdate (2020)

The Monster at the End of This Story (2020)


 
  U.S. spin-offs
 
 Play with Me Sesame

Sesame Beginnings

Bert and Ernie's Great Adventures


 
  International co-productions
 Original show adaptations 
 1, rue Sésame (France)

5, Rue Sésame (France)

Alam Simsim (Egypt)

Baghch-e-Simsim (Afghanistan)

Barrio Sésamo (Spain)

Batibot (Philippines)

Galli Galli Sim Sim (India)

Hikayat Simsim (Jordan)

Iftah Ya Simsim (Kuwait/Arab world)[a]

Jalan Sesama (Indonesia)

Kilimani Sesame (Tanzania)

Plaza Sésamo (Mexico/Latin America)

Rechov Sumsum (Israel)

Sabai Sabai Sesame (Cambodia)

Sesam Stasjon (Norway)

Sesame Park (Canada)

Sesame Square (Nigeria)

Sesame Tree (UK)

Sesamisutorīto (Japan)

Sesamstraat (Netherlands)

Sesamstraße (Germany)

Shalom Sesame (Israel)

Shara'a Simsim (Palestine)

Sim Sim Hamara (Pakistan)

Sisimpur (Bangladesh)

Susam Sokağı (Turkey)

Svenska Sesam (Sweden)

Szezám utca (Hungary)

Takalani Sesame (South Africa)

Ulica Sezamkowa (Poland)

Ulitsa Sezam (Russia)

Vila Sésamo (Brazil)

Zhima Jie (China)


Spin-offs 
 Open Sesame (worldwide)

The Furchester Hotel (UK)


 a  Relocated production processes from Kuwait to United Arab Emirates in 2015 after a 25-year hiatus.
 
 
  Songs
 
 Bein' Green

C Is For Cookie

I Love Trash

Mah Nà Mah Nà

Monster in the Mirror

Rubber Duckie

Sesame's Treet

Sing


 
  Books
 
 The Monster at the End of This Book (1971)

Sesame Street Together Book (1971)

Ernie's Work of Art (1979)

The House of Seven Colors (1985)

Happy Birthday, Cookie Monster (1986)


 
  Literature
 
 Children and Television: Lessons from Sesame Street

Street Gang: The Complete History of Sesame Street

The Sesame Street Dictionary

Sesame Street Magazine


 
  Video games
 
 Alpha Beam with Ernie

The Adventures of Elmo in Grouchland

Elmo's A-to-Zoo Adventure

Cookie's Counting Carnival

Ready, Set, Grover!

Once Upon a Monster

Elmo's Musical Monsterpiece

Kinect Sesame Street TV

Sunny Days: The Children's Television Revolution That Changed America


 
  Attractions
 
 Air Grover

Grover's Alpine Express

Sesame Place

Sesame Street 4-D Movie Magic

Spaghetti Space Chase


 
  Related
 
 Sesame Street in the UK

Sesame Street Live

Comic strip

Syndication packages

The Muppets

Being Elmo: A Puppeteer's Journey

I Am Big Bird: The Caroll Spinney Story

The World According to Sesame Street

Big Bag

Oobi
 episodes


Panwapa

Teletape Studios

Kaufman Astoria Studios

The Joan Ganz Cooney Center


 

   v
t
e
 Sesame Street characters
Muppets of Sesame Street 
 Abby Cadabby

Bert and Ernie
 Bert

Ernie


Big Bird

Cookie Monster

Count von Count

Elmo

Grover

Guy Smiley

Julia

Kermit the Frog

Murray Monster

Oscar the Grouch

Roosevelt Franklin

Rosita

Sherlock Hemlock

Mr. Snuffleupagus

Two-Headed Monster

Yip Yips


Human characters 
 Linda

Mr. Hooper

Mr. Noodle

The Number Painter

The Robinson Family


Animated characters 
 Teeny Little Super Guy


International co-production  characters 
 Don Pimpón

Kami

Pong Pagong







