  2019 American action film



John Wick: Chapter 3 – Parabellum
 Theatrical release poster
Directed byChad Stahelski
Produced by 
 Basil Iwanyk

Erica Lee


Screenplay by 
 Derek Kolstad

Shay Hatten

Chris Collins

Marc Abrams


Story byDerek Kolstad
Based onCharacters by Derek Kolstad
Starring 
 Keanu Reeves

Halle Berry

Laurence Fishburne

Mark Dacascos

Asia Kate Dillon

Lance Reddick

Anjelica Huston

Ian McShane


Music by 
 Tyler Bates

Joel J. Richard


CinematographyDan Laustsen
Edited byEvan Schiff
 Production company   
 Summit Entertainment

Thunder Road Pictures

87Eleven Productions


Distributed byLionsgate
 Release date 
 May 9, 2019 (2019-05-09) (Brooklyn)

May 17, 2019 (2019-05-17) (United States)


 Running time131 minutes[1]
CountryUnited States
LanguageEnglish
Budget$75 million[2][3]
Box office$326.7 million[4]

John Wick: Chapter 3 – Parabellum is a 2019 American neo-noir action thriller film starring Keanu Reeves as the eponymous character. It is the third installment in the John Wick film series, following John Wick (2014) and John Wick: Chapter 2 (2017). The film is directed by Chad Stahelski[5] and written by Derek Kolstad, Shay Hatten, Chris Collins, and Marc Abrams, based on a story by Kolstad. It also stars Halle Berry, Laurence Fishburne, Mark Dacascos, Asia Kate Dillon, Lance Reddick, Anjelica Huston, and Ian McShane. In the film, which picks up minutes after the end of the previous film, ex-hitman John Wick finds himself on the run from legions of assassins after a $14 million contract is put on his head due to his recent actions.
The third installment was announced in June 2017. Much of the returning cast and crew was confirmed in February 2018, with new members joining that May. Filming began that month and lasted through November, taking place in New York City, Montreal, and Morocco.
Parabellum was theatrically released in the United States on May 17, 2019, by Lionsgate. It grossed $326 million worldwide, becoming the highest-grossing film of the franchise in just 10 days, and received positive reviews from critics, with praise for the action sequences, visual style, and Reeves's performance.[6] A sequel, John Wick: Chapter 4, is set to be released on May 27, 2022.[7][8]

  Contents
 
1 Plot

2 Cast

3 Production
 
3.1 Casting

3.2 Filming

3.3 Special effects




4 Music

5 Release

6 Reception
 
6.1 Box office

6.2 Critical response

6.3 Accolades




7 Sequel

8 References

9 External links




Plot
Less than an hour after the events of the second film, John Wick is now a marked man on the run in Manhattan. Before he is labeled "excommunicado" as a result of his unauthorized killing of High Table member Santino D'Antonio, Wick travels to the New York Public Library and retrieves two concealed items – a "marker" medallion and a crucifix. Then he is injured following a fight with a giant assassin named Ernest. Wick leaves to get medical treatment, but his excommunicado begins before the doctor can finish. Wick patches the injury himself and leaves but is found by a gang and after a fight in an antique dealership escapes onto the streets.
John meets with Director, the head of the Ruska Roma, with the crucifix and requests her help in getting safe passage to Casablanca. The Director reluctantly helps, as John is a former protégé and is from Belarus, knowing that this act will violate her peace treaty with the High Table. An Adjudicator of the High Table meets with Winston, the manager of the New York Continental, and the Bowery King, the leader of a network of vagrants. The Adjudicator punishes both for helping John, giving them seven days to resign their positions, and hires Zero, a Japanese ninja. Zero and his students find the Director, slaughtering her guards and stabbing her hands as punishment for helping John.
In Casablanca, John meets Sofia, a former friend and the manager of the Moroccan Continental. He asks her to honor a debt she owes him for aiding her daughter by directing him to the Elder, the only person above the High Table. Sofia takes John to Berrada, her former boss, who tells John he may find the Elder by wandering through the desert until he cannot walk any longer. In exchange, Berrada asks for one of Sofia's dogs. When Sofia refuses, he shoots the dog with the bullet lodging in its bullet proof vest. In retaliation, Sofia tries to kill Berrada but she only wounds him at John's warning. The two fight their way out of the kasbah and drive into the desert, where she leaves him.
The Adjudicator and Zero confront the Bowery King, who refuses to abdicate his position. Zero's students slaughter almost all of his henchmen. As penance for helping John, Zero cuts the Bowery King seven times with a wakizashi.
Collapsing in the desert, John is brought to the Elder and states that he is desperate to live to "earn" the memory of the love he once had with his wife. The Elder agrees to forgive Wick on condition that he assassinates Winston and keeps working for the High Table until his death. To show his commitment, John severs his ring finger and offers his wedding ring to the Elder. John returns to New York City and is attacked by Zero and his students, before reaching the protection of the Continental. John meets with Winston, who encourages John not to die as a killer but as a man who loved and was loved by his wife.
The Adjudicator arrives, but Winston has no intention of giving up his office and John refuses to kill him, leading the Adjudicator to "deconsecrate" the NY Continental, revoking its neutral ground status. The Adjudicator notifies Zero and sends heavily armed and armored enforcers to kill John and Winston. The latter decides to defend the hotel by furnishing John with weapons, alongside Charon and other hotel staff. After killing the enforcers, John is ambushed by Zero and his students, so he proceeds to kill all but two of them, who fight him respectfully and enthusiastically. Eventually John confronts and fatally wounds Zero.
The Adjudicator agrees to a parley with Winston, who explains his rebellion as a "show of strength" and offers fealty to the High Table. After John arrives, the Adjudicator questions what should be done with him. Winston shoots John repeatedly, leading him to fall from the roof of the hotel. The Continental is "reconsecrated," but the Adjudicator informs Winston that John's body has disappeared, and that he remains a threat to them both. A seriously injured John and his dog are delivered to a now heavily scarred Bowery King, who states that he is really pissed off at the High Table and asks John if he is too. The film ends with John replying "Yeah."

Cast
 Keanu Reeves as John Wick

Halle Berry as Sofia, an ex-assassin, former friend of John and the manager of the Continental Hotel in Casablanca.

Ian McShane as Winston

Laurence Fishburne as the Bowery King

Mark Dacascos as Zero

Asia Kate Dillon as the Adjudicator

Lance Reddick as Charon, the concierge at the Continental Hotel

Tobias Segal as Earl

Anjelica Huston as the Director

Saïd Taghmaoui as the Elder

Jerome Flynn as Berrada

Randall Duk Kim as the Doctor. Duk Kim reprised his role from the first film.

Margaret Daly as Operator

Robin Lord Taylor as Administrator

Susan Blommaert as Librarian

Jason Mantzoukas as Tick Tock Man, one of the Bowery King's men

Cecep Arif Rahman as Shinobi #1, one of Zero's pupils

Yayan Ruhian as Shinobi #2, another of Zero's pupils

Tiger Chen as Triad

Boban Marjanović as Ernest

Production
In October 2016, Chad Stahelski, who made his directorial debut with John Wick and served as Reeves' Matrix stunt double,[9] stated that a third film in the John Wick series was in the works,[10] and in June 2017 it was reported that Derek Kolstad, who wrote the two prior films, would return to write the screenplay.[11] In January 2018, it was reported that Stahelski would return to direct.[12]
According to Reeves, the film's title was taken from the famous 4th-century Roman military quote "Si vis pacem, para bellum," which means, "If you want peace, prepare for war."[13] In an interview with The New York Times, McShane said that the film would be big, good, and that nothing is the same while also hinting that part of the action could be the High Table's payback not only on Wick but also on his close friend Winston.[14]

Casting
In January 2018, it was reported that Hiroyuki Sanada was in talks to join the cast.[12] Later, it was revealed that Ian McShane, Laurence Fishburne, and Lance Reddick would reprise their roles from previous John Wick films.[15] In May 2018, Halle Berry, Anjelica Huston, Asia Kate Dillon, Mark Dacascos, Jason Mantzoukas, Yayan Ruhian, Cecep Arif Rahman, and Tiger Chen joined the cast.[16][17][18] In November 2018, Said Taghmaoui confirmed his involvement in the film.[19]

Filming
Principal photography began May 5, 2018, in New York City and Montreal, along with the additional filming locations of Morocco. Principal photography lasted until November 17, 2018.[15][19][20] Cinematographer Dan Laustsen was asked about how challenging it was to use as many extended fight scene takes as possible while filming the high action screenplay. He stated, "Of course it is [a challenge], because all the fights—Chad [Stahelski] is doing most of the fights himself. We play that as wide as we can. Because that way we see it’s him. We do that a lot, we try to play it as wide as we can and do long shots. Of course, because Chad has a background from the stunt world he knows exactly how to block this kind of stuff. I’m not the best stunt person in the world, but I’m learning."[21] In an interview with Jimmy Fallon, Halle Berry said, "I broke three ribs in rehearsal."[22]

Special effects
The visual effects are provided by Method Studios, Image Engine, and Soho VFX.[23]

Music
Tyler Bates and Joel J. Richard return to score the film. The soundtrack was released by Varese Sarabande Records. Also featured is the song "Ninja Re Bang Bang" by Japanese artist Kyary Pamyu Pamyu.[24]

Release
Parabellum premiered in Brooklyn, New York on May 9, 2019.[25] The film was theatrically released in the United States on May 17, 2019, by Lionsgate. The film was released on Digital HD on August 23, 2019 and was released on DVD, Blu-ray, and 4K Ultra HD on September 10, 2019.[26]
The studio spent an estimated $48 million on prints and advertisements promoting the film.[27]

Reception
Box office
John Wick: Chapter 3 – Parabellum grossed $171 million in the United States and Canada, and $155.7 million in other territories, for a worldwide total of $326.7 million.[4] Deadline Hollywood calculated the net profit of the film to be $89 million, when factoring together all expenses and revenues.[27]
In the United States and Canada, Parabellum was released alongside A Dog's Journey and The Sun Is Also a Star and was initially projected to gross $30–40 million from 3,850 theaters in its opening weekend.[28][29] The film made $5.9 million from Thursday night previews, more than the total of the Thursday night previews for the previous two films ($950,000 and $2.2 million). It then made $22.7 million on its first day (including previews), increasing its projected gross to $56 million. It went on to debut at $57 million, becoming the first film to dethrone Avengers: Endgame atop the box office.[30] It was the best opening of the series, and more than the first film made during its entire theatrical run ($43 million).[31] In its second weekend the film made $24.4 million, finishing second behind newcomer Aladdin.[32] It then earned $11.1 million in its third weekend and $6.4 million in its fourth.[33][34]

Critical response
On review aggregator Rotten Tomatoes, the film holds an approval rating of 90%, with an average rating of 7.47/10, based on 329 reviews. The website's critical consensus reads, "John Wick: Chapter 3 – Parabellum reloads for another hard-hitting round of the brilliantly choreographed, over-the-top action that fans of the franchise demand."[35] On Metacritic, the film has a weighted average score of 73 out of 100, based on 50 critics, indicating "generally favorable reviews".[36] Audiences polled by CinemaScore gave the film an average grade of "A–" on an A+ to F scale, the same as its predecessor, while those at PostTrak gave it 4.5 out of 5 stars and a "definite recommend" of 75%.[31] Rotten Tomatoes also ranked it at No. 2 on its list of "The Best Action Movies of 2019".[37]
Peter Sobczynski of RogerEbert.com gave the film 4 out of 4 stars, calling it "a work of pop cinema so blissfully, albeit brutally, entertaining that you come out of it feeling even more resentful of its multiplex neighbors for not making a similar effort."[38] Chris Nashawaty of Entertainment Weekly gave the film a grade of "A-", writing that "as gorgeously choreographed, gratuitously violent action movies go, it's high art".[39] Wendy Ide of The Observer gave the film 4 out of 5 stars, calling it "a flying kick to the senses" and writing that "The spectacular third installment in Keanu Reeves's fighting franchise overwhelms with opulent martial arts set pieces".[40]

Accolades



Award

Category

Recipient

Result



Art Directors Guild Awards

Best Contemporary Film

Kevin Kavanaugh

Nominated



Critics Choice Movie Awards

Best Action Movie

John Wick: Chapter 3 – Parabellum

Nominated



Golden Raspberry Awards[41]

The Razzie Redeemer Award

Keanu Reeves

Nominated



People's Choice Awards[42]

Favorite Action Movie of 2019

John Wick: Chapter 3 – Parabellum

Nominated



Favorite Male Movie Star of 2019

Keanu Reeves

Nominated



Saturn Awards

Best Action or Adventure Film

John Wick: Chapter 3 – Parabellum

Nominated



Best Actor

Keanu Reeves

Nominated



Best Editing

Evan Schiff

Nominated


Sequel
On May 20, 2019, following the third film's successful debut, John Wick: Chapter 4 was announced and given a May 21, 2021 release date.[43] On May 1, 2020, the film was delayed to May 27, 2022 due to the COVID-19 pandemic.

References

External links
 Official website

John Wick: Chapter 3 – Parabellum on IMDb



John Wick: Chapter 3 – Parabellum at the Internet Movie Firearms Database

John Wick: Chapter 3 – Parabellum at Metacritic

John Wick: Chapter 3 – Parabellum at Rotten Tomatoes

John Wick: Chapter 3 – Parabellum at Box Office Mojo

   v
t
e
 John Wick
Films 
 John Wick

John Wick: Chapter 2

John Wick: Chapter 3 – Parabellum


Music 
 John Wick: Original Motion Picture Soundtrack

"A Job to Do"


Games 
 John Wick Hex


Characters 
 John Wick


 
  Category







