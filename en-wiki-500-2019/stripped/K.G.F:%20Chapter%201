  2018 action film by Prashanth Neel



K.G.F: Chapter 1
 Theatrical release poster
Directed byPrashanth Neel
Produced byVijay Kiragandur
Written byDialogues:  
 Prashanth Neel

Chandramouli M

Vinay Shivangi


Screenplay byPrashanth Neel
Story byPrashanth Neel
Starring  Yash
Srinidhi Shetty
Ananth Nag
Malavika Avinash
Vasishta N. Simha

Narrated byAnant Nag
Music byRavi Basrur
CinematographyBhuvan Gowda
Edited byShrikanth
 Production company  Hombale Films
Distributed by  KRG Studios (Kannada)
Excel Entertainment & AA Films (Hindi)
Vishal Film Factory (Tamil)
Vaaraahi Chalana Chitram (Telugu)
Global United Media (Malayalam)

 Release date 
 20 December 2018 (2018-12-20) (United States & Canada)

21 December 2018 (2018-12-21) (India)


 Running time155 minutes[1]
CountryIndia
LanguageKannada
Budget₹50–80 crore[a]
Box officeest. ₹243–250 crore[4][5]

K.G.F: Chapter 1 is a 2018 Indian Kannada-language period action film[6][7] written and directed by Prashanth Neel, and produced by Vijay Kiragandur under the banner Hombale films. It is the first instalment in the two-part series, followed by K.G.F: Chapter 2. The film centres around Raja Krishnappa Bairya "Rocky", born into poverty, who arrives in Bombay (now Mumbai) in the 1960s, on a quest for power and wealth as desired by his dying mother. Involved with the gold mafia there, he is recruited to kill Garuda, the oppressive heir-in-waiting, in Kolar Gold Fields. Yash stars as Rocky, while debutante Ramachandra Raju features as Garuda. Anant Nag narrates the film while also playing a supporting role alongside Srinidhi Shetty, Vasishta N. Simha, Ayyappa P. Sharma and Harish Rai. At the 66th National Film Awards, the film won 2 awards for Best Action and Best Special Effects.[8]
The development of the film began in early 2015 when Neel completed writing the screenplay.[9] However, filming began only two years later, in March 2017.[10] Majority of the film is set in Kolar Gold Fields and was filmed on location.[11] Bhuvan Gowda served as the cinematographer and Shrikanth as the editor. Ravi Basrur scored music for the soundtrack and the film's background.[12]
After a North American premiere on 20 December 2018, K.G.F: Chapter 1 was released in Kannada and in dubbed versions of Hindi, Telugu, Tamil and Malayalam languages the following day, and received positive response from audiences.[13] However, critics gave the film mixed reviews; while they praised the cinematography, art direction and the film's score, the screenplay and editing were criticised. The film performed well commercially and collected ₹76 crore four days into release, in the process breaking the record for the highest grossing Kannada film of all time.[14] The movie was released online by Amazon Prime Video on 5 February 2019[15] which earlier bought the digital rights for ₹18 crores and Colors Kannada bought the satellite rights for an undisclosed record price.[16] The shooting for K.G.F: Chapter 2 began on 13 March 2019.[17]

  Contents
 
1 Plot

2 Cast

3 Production

4 Soundtrack

5 Release

6 Reception
 
6.1 Critical reception




7 Box office
 
7.1 Trivia




8 Accolades

9 See also

10 Notes

11 References

12 External links




Plot
Journalist Anand Ingalagi's book titled El Dorado , that detailed events at the Kolar Gold Fields between 1951 and present day, has been banned by the Government of India and all published copies burnt. However, a television news channel procures a copy and interviews him circling the events he had cited.
Ingalagi narrates that ore of gold was discovered by commissioned government officials in the southern part of India's Mysore State (in present-day Kolar district, Karnataka) in 1951. Raja Krishnappa "Rocky" Bairya is also born on the day of the discovery in the Mysore region, to a poor and widowed woman. Suryavardhan, a local don who accompanied the officials, kills them. Discerning the increasing demand for gold by the Cold War belligerents, Suryavardhan deceitfully sets up a company, Narachi, in that land, bought for a lease of 99 years under the pretext of mining limestone. He has five associates, all of who operate away from KGF: Kamal, the son of a former associate Bharghav; Rajendra Desai, the father of Reena, Andrews, who oversees operations in the Konkan and Malabar Coasts; Guru Pandian, a powerful politician and president of the DYSS party; and brother Adheera. However, each of them has their eyes on the mines. Andrews' underboss Shetty is a gold smuggler in Bombay, the city that also has another smuggler, Dubai-based Inayat Khalil's eyes set on.
Rocky arrives in Bombay as a ten-year-old on a quest for power and wealth as desired by his dying mother and begins to work for Shetty. He becomes Shetty's right-hand man later and oversees the arrival of gold bars from Africa to the Bombay coast with an iron fist. Rocky soon rises in strength and power rivalling that of Shetty's. This attracts Andrews who offers him Bombay in return for assassinating Garuda, the younger son of Suryavardhan, who is to inherit KGF after his paralysed father's passing. Rocky accepts the offer and heads to Bangalore where an event is staged for the purpose. Once there, he witnesses the sheer presence, authority and power that Garuda commands and has the attempt averted.
In 1978, at the KGF, a bedridden Suryavardhan announces Garuda as his successor overlooking Adheera, who he wants to serve as the latter's aide. Garuda's ruthless way of functioning aided by his commander Vanaram comes to fore. Rocky soon makes his way into KGF evading a unit of henchmen. Once there, he witnesses the brutality that the slaves are subjected to. Although apathetic at first, he is moved by a couple of incidents involving a cold-blooded killing of a mother and son at the hands of an overseer and the slaves' children hoping that he rises to liberate them. Beginning his attempt at it and to indicate to Desai and his men through informants, Kulkarni and Garuda's virtuous brother Virat, that he was alive, as planned, Rocky engages in a fight with a unit of twenty-three overseers killing each one of them in order to save a blind slave they were to kill. With the act, Rocky emerges as a hero in the slaves' eyes.
An alerted Vanaram now informs Garuda who heads toward the mines from his palatial residence. To avert thousands of slaves being killed due to his wrath, Virat smothers father Suryavardhan to death and sidetracks Garuda, who rushes back home. At the mines, Rocky seizes the opportunity, conspires and unsuspectingly heads through a tunnel to the site where Garuda has decided to behead three slaves as offerings to a deity. Upon Garuda's return to the site and sacrifice of two slaves, a concealed Rocky emerges and beheads him. The ecstatic slaves accept him as their leader as they cheer him on. The narrator concludes that Rocky intentionally chose KGF as the site to assassinate Garuda, in order to earn the will of an "army of people", the slaves, before he seized KGF to his control but still there are people who are waiting to acquire the throne and how Rocky will face them as one man army and why he was given a death warrant by Prime Minister Ramika Sen will be shown in Chapter 2.

Cast
 
 Yash as Raja "Rocky Bhai" Krishnappa Bhairya[18]
 Anmol Vijay as young Rocky[19]


Srinidhi Shetty as Reena Desai

Archana Jois as Shanthamma, Rocky's mother[20]

Anant Nag as Anand Ingalagi
 Ashok Sharma as young Anand Ingalagi


Ramesh Indira as Suryavardhan

Ramachandra Raju as Garuda

Vinay Bidappa as Virat

Laxman as Rajendra Desai

Vasishta N. Simha as Kamal

Achyuth Kumar as Guru Pandian

B. S. Avinash as Andrews

Tharak Ponnappa as Daya, Andrews' aide

Dinesh Mangaluru as Shetty

Harish Rai as Khasim, Shetty's aide and Rocky's confidante

Balakrishna as Inayat Khalil

T. S. Nagabharana as the owner of television news channel 24/News

Malavika Avinash as Deepa Hegde, the chief editor of 24/News

Govinda Gowda as a peon at 24/News

Mohan Juneja as Nagaraju, an informer to Ingalagi

Ayyappa P. Sharma as Vanaram, commander for KGF

John Kokken as John, an overseer at KGF

Puneeth Rudranag as Rugga, an overseer at mines

Neenasam Ashwath as Kulkarni

B. Suresha as Vittal, a slave at KGF

T. N. Srinivas Murthy as Narayan, a slave at KGF

Lakshmipathi as Kencha, a slave at KGF, the storyteller

Roopa Rayappa as Shanthi, a slave at KGF

Sampath Maitreya as Shanti's Husband

Yashwant Shetty

Shambavi Venkatesh

Tamannaah as Milky in a special appearance for an item number "Jokae"


 Hindi dubbed version

 Mouni Roy as Lucy in special appearance for an item number "Gali Gali"[21]

Production
The art director Shivakumar recreated the Kolar Gold Fields film set of the 80s and VFX was used as an extension.[22]
As of 25 June 2017, the project was 50 per cent complete and production had restarted after sets were rebuilt following their destruction by heavy rains.[23]
KGF was released in five languages. It is a complete period drama and is set in the 70s and early 80s. Yash grew his beard and long hair for his role as Rocky, a slick and a suave person of the 70s era. Apart from Yash and Srinidhi, some of the Kannada actors include Ananthnag, Malavika Avinash, Achyuth Kumar, Vasishta N. Simha[24] and B Suresha. Ramya Krishna and Nassar were falsely reported to be a part of the film.[25]
The teaser of KGF was released on Yash's birthday, 8 January 2018. The trailer was released on 9 November 2018.[26][27][28] It also had inspiration from action movies and western movies[29] like The Good, the Bad and the Ugly and For a Few Dollars More.[30]

Soundtrack
 Main article: K.G.F: Chapter 1 (soundtrack)
Ravi Basrur composed the soundtrack album and the film's score, while Tanishk Bagchi remastered the track "Gali Gali" from Tridev (1989) for the film's Hindi version, and D. Imman composed the karaoke versions of the songs.[31] The audio rights of the film were purchased by Lahari Music in Kannada, Tamil, Telugu and Malayalam for ₹3.6 crore, a record sum for any South Indian film, except the Hindi version which was bought by T-Series.[32] All the songs were released as singles, and all versions of the first track of the album "Salaam Rocky Bhai", was released on 4 December 2018, except the Hindi version which was released on 7 December 2018.[33]
The complete soundtrack album of the film was released on 19 December 2018, in all South Indian languages and the soundtrack for the Hindi version was released on 26 December 2018. The karaoke versions of the songs were released on 9 January 2019. The film's score was released in two volumes. Volume 1 was released on 10 September 2019, and second volume was released on 12 October 2019.

Release
K.G.F: Chapter 1 received a U/A certificate from the Central Board of Film Certification in early December 2018.[34][35] The distribution rights for the Kannada version were bought by KRG Studios, a sister company of the film's studio Hombale Films. While Excel Entertainment and AA Films bought rights for the Hindi version, the Tamil version's rights were sold to Vishal Film Factory, Telugu to Vaaraahi Chalana Chitram and Malayalam to Global United Media.[36][37] The film was released in the U.S. and Canada on 20 December 2018 and in India the following day.[38] The day also saw releases in parts of Africa, Hong Kong and parts of Eastern Europe including Cyprus, the first in these regions for a Kannada film.[39] It was reported that the film would be released in 1,800–2,200 screens worldwide, the widest ever release for a Kannada film.[40][41][42] However, it was later reported that the film was released in 2,460 screens, that included 1,500 for the Hindi version, 400 each for Kannada and Telugu, 100 for Tamil and 60 for Malayalam.[43] The Hindi-dubbed version of the film was released in 71 screens in Pakistan on 11 January and became the first Kannada film to be released there, although Lucia (2013) was screened at film festivals and did not see a theatrical release in the country.[44][45]

Reception
Critical reception
K.G.F: Chapter 1 received positive response from audiences while critics gave the film mixed reviews.[46] While its cinematography, art direction, narration, stunt sequences, background score and acting performances received acclamation, critics had mixed comments about the screenplay, dialogues and editing.[47]
Reviewing for Deccan Herald, Vivek M. V. felt that the "grandeur" lay in the film's "fantastically gripping story". He wrote praises of the film's narration, its "brilliant" editing and "riveting sequences".[48] Sunayana Suresh of The Times of India gave the film rating of three-and-a-half out of five and wrote that it had a "fast-paced first half ... but the second half and the climax sets up the right premise for the second part of the film." She called the screenplay narrated in a "non-linear" fashion the "most interesting part of the film". She commended the performance as Yash in that he "lives his character to the fullest".[49] Troy Ribeiro of News18 echoed her sentiment and wrote, "Yash's endurance, strife and sincerity ... get projected as perfunctory" in the context of "tight close-ups and mid-shots the camera stops us from getting emotionally connected to" every actor in the film. He further wrote, "With intense atmospheric lighting, every frame in the film looks aesthetic and natural. Brilliant cinematography and equally challenging action sequences are put together with razor-sharp edits. They give the film a racy pace."[50] The reviewer for Hindustan Times, Priyanka Sundar, called the film a "story of greed and redemption" and remarked that it "burns bright". While praising the "promising" background score, "sharp" editing and "stunning" visuals, she felt that the screenplay could have been "tighter".[51]
Janani K. of India Today felt the film was "dragged and over-stretched" and gave it a three out of five-star rating. While she commended Yash's "extraordinary performance" and the "brilliantly choreographed stunt sequences", she wrote that despite having "universal theme, [the film] gets lost in translation, thanks to sloppy editing and atrocious dialogues."[52] Subha J. Rao of Firstpost gave the film a similar rating and praised the film's music, cinematography and art direction; particularly the latter in "bringing alive the grime and heat of the gold mines". However, she felt "[t]ighter editing ... would have smoothened out the kinks" in the film.[53] Shashiprasad S. M. of Deccan Chronicle scored the film a two-and-a-half out of five star rating and described the film as "a visual spectacle". Barring that, he felt it fell short of "instilling the much-needed life into it."[54] Karthik Keramalu of Film Companion felt that the film fell short "becoming a great movie by a long mile". He dismissed the dialogues delivered by Yash's character as "lengthy sermon about his own valour", while also criticizing the film's editing.[55]
Excepting what he described the film's climax as "spectacular with the support of a brilliant cast" and "spot on" sets and location, Muralidhara Khajane of The Hindu felt there was "nothing in the film that we have not seen before". While writing that "[t]here is a certain finesse to the edgy, moody cinematography", he concluded that the film lacked a "soul, a believable story and a rounded protagonist."[56] Manoj Kumar R. of The Indian Express scored the film two-and-a-half out of five stars and deemed it "[a]n overstretched exercise in hero worship". While drawing comparisons of certain scenes to those from Baahubali, he felt the film had a "flimsy storyline", which he added is made up for by "terrific background orchestra".[57] Also writing for the same news publication, Shubhra Gupta drew comparisons of the film in plot to those of Nayakan, Deewaar, Parinda in its first half. She felt that the film had "nothing more" than "striking cinematography, and the brown and sepia colours which suffuse the screen".[58]
The character of Rocky was well received by critics. Janani K of India Today said that "You take a look at [Rocky] and instantly you know that this guy will do the impossible and pull people out of their misery."[59] Sunaina Suresh of Times of India said that "The growth of Rocky is shown steadily and the makers kept a clever story telling pattern right through, that keeps pace with the narrative." And further added that "The first chapter shows Rocky as the maverick mastermind who will stop at nothing in order to achieve his mission."[60] Troy Ribeiro of News 18 said that "Rocky is the new Superhero in the town."[61]

Box office
 See also: List of highest-grossing Indian films
KGF: Chapter 1 on the first day of its release collected ₹25 crore worldwide, which was the highest opening in the Kannada film industry, In Karnataka's capital, Bengaluru, the film earned about ₹5 crore on day 1.[62] First weekend collections stood around ₹60 crore worldwide from all its versions.[63] The film grossed around ₹113 crore worldwide in the first week of its release, becoming the first Kannada film to cross ₹100 crore,[64] then it grossed over ₹250 crores in 50 days, becoming the first Kannada movie to gross over 200 crores.[5]
With a collection of 134 crores in its home state, it became the highest grossing movie in Karnataka at the time.[65][66] K.G.F: Chapter 1, completed its theatrical run over 100-days in couple of centres of Karnataka.[67]

Trivia
 It became the first Kannada film to cross ₹100, ₹200 and ₹250 crore at the Indian box office.[5][68]

K.G.F: Chapter 1 became the fourth highest grossing Hindi dubbed film after the Baahubali franchise and 2.0.[69]

K.G.F was the first Sandalwood film to cross half million ($500k) dollar in United States and cumulative $1.5 million dollar in overseas.[70]

Accolades



Year

Ceremony

Category

Recipient(s) and nominee(s)

Result

Ref.



2018

Karnataka State Film Awards

Best Music Director

Ravi Basrur

Won

[71]



Best Art Director

Shiva Kumar K

Won



2019

Zee Kannada Hemmeya Kanndiga Awards

Best Film

K.G.F

Won

[72]



Best Director

Prashanth Neel

Won



Best Actor

Yash

Won



Best Cinematographer

Bhuvan Gowda

Won



Best Music Director

Ravi Basrur

Won



Best Villain

Ramachandra Raju

Won



Best Lyrisist

Nagendra Prasad

Won



Voice Of The Year

Vijay Prakash

Won



2019

66th National Film Awards

Best Special Effects

Unifi Media

Won

[73]



Best Stunt Choreographer

Vikram More, Anbariv

Won



2019

SIIMA Awards

Best Film

K.G.F

Won

[74][75]



Best Actor

Yash

Won



Pantaloons style icon male of the year

Yash

Won



Best Director

Prashanth Neel

Won



Best Actor in a Negative Role

Ramachandra Raju

Nominated



Best Debut Actress

Srinidhi Shetty

Nominated



Best Supporting Actress

Archana Jois

Won



Best Cinematographer

Bhuvan Gowda

Won



Best Music

Ravi Basrur

Won



Best Supporting actor - Male

Achyuth Kumar

Won



Best Playback Singer - Male

Vijay Prakash

Won



2019

Filmfare Awards South

Best Film – Kannada

K.G.F: Chapter 1

Won

[76]



Best Director – Kannada

Prashanth Neel

Nominated



Best Actor – Kannada

Yash

Won



Best Music Director – Kannada

Ravi Basrur

Nominated



Best Female Debut

Srinidhi Shetty

Nominated


See also
 List of films featuring slavery

Mining in India

Notes

References

External links
 K.G.F: Chapter 1 on IMDb

K.G.F: Chapter 1 at Rotten Tomatoes

   v
t
e
 National Film Award for Best Special Effects
1991–2000 
 No Award (1991)

Angaar (1992)

Thiruda Thiruda (1993)

Kadhalan (1994)

Kaalapani (1995)

Indian (1996)

No Award (1997)

Jeans (1998)

Hey Ram (1999)

No Award (2000)


2001–present 
 Aalavandhan (2001)

Magic Magic (2002)

Koi... Mil Gaya (2003)

Anji (2004)

Anniyan (2005)

Krrish (2006)

Sivaji: The Boss (2007)

Mumbai Meri Jaan (2008)

Magadheera (2009)

Enthiran (2010)

Ra.One (2011)

Eega (2012)

Jal (2013)

No Award (2014)

Baahubali: The Beginning (2015)

Shivaay (2016)

Baahubali 2: The Conclusion (2017)

Awe (2018)

K.G.F: Chapter 1 (2018)



   v
t
e
 Films of Prashanth Neel
Director 
 Ugramm (2014)

K.G.F: Chapter 1 (2018)



   v
t
e
 AA Films
Films distributed North Indian 
 Fukrey (2013)

Hasee Toh Phasee (2014)

Humpty Sharma Ki Dulhania (2014)

Ungli (2014)

Bangistan (2015)

Welcome 2 Karachi (2015)

Mirzya (2016)

Veerappan (2016)

Firangi (2017)

FU: Friendship Unlimited (2017)

Haseena Parkar (2017)

Bareilly Ki Barfi (2017)

Behen Hogi Teri (2017)

Fukrey Returns (2017)

Sachin: A Billion Dreams (2017)

Kaalakaandi (2018)

Nawabzaade (2018)

Phamous (2018)

Bucket List (2018)

Raazi (2018)

Hate Story 4 (2018)

Sonu Ke Titu Ki Sweety (2018)

Fanney Khan (2018)

Gold (2018)

Badhaai Ho (2018)

Stree (2018)


South Indian 
 Baahubali 2: The Conclusion (2017)

The Ghazi Attack (2017)

2.0 (2018)

K.G.F: Chapter 1 (2018)


 

   v
t
e
 Vishal Film Factory
Key People 
 Vishal


Films produced 
 Pandiya Naadu (2013)

Naan Sigappu Manithan (2014)

Poojai (2014)

Aambala (2015)

Kathakali (2016)

Irumbu Thirai (2018)

Thupparivaalan (2017)

Sandakozhi 2 (2018)

Thupparivaalan 2 (TBA)


Films distributed 
 Pattathu Yaanai (2013)

Jeeva (2014)

K.G.F: Chapter 1 (2018; Tamil version)







