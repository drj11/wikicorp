  American actress



 Kristen Bell
 Bell at Paris Fashion Week in 2019
Born Kristen Anne Bell  (1980-07-18) July 18, 1980 (age 39)  Huntington Woods, Michigan, U.S.
Alma materNew York University
Occupation 
 Actress

singer

director

producer

author


Years active1992–present
Spouse(s) Dax Shepard (m. 2013)
Children2

Kristen Anne Bell (born July 18, 1980)[1] is an American actress, singer, director, producer, and author. She began her professional acting career by starring in stage productions while attending the Tisch School of the Arts at New York University. In 2001, she made her Broadway stage debut as Becky Thatcher in the comedy musical The Adventures of Tom Sawyer and appeared in a Broadway revival of The Crucible the following year. In 2004, she appeared in the action thriller film Spartan and received critical praise for her performance in the television drama film Gracie's Choice.
Bell garnered critical acclaim for her first major role as the title character in the teen noir drama television series Veronica Mars (2004–2007). For her performance, she was awarded a Saturn Award for Best Actress on Television. She reprised the eponymous role in the 2014 film continuation and the 2019 revival series. During her time on Veronica Mars, Bell starred as Mary Lane in the musical film Reefer Madness: The Movie Musical (2005), a reprise of the role she had played in the New York musical upon which the film was based. From 2007 to 2008, Bell starred as Elle Bishop in the science fiction drama series Heroes. From 2007 to 2012, she voiced the titular narrator in the teen drama series Gossip Girl.
In 2008, she had her breakout film role as the title character in Forgetting Sarah Marshall. She has since appeared in a number of comedy films, including Couples Retreat (2009), When in Rome (2010), You Again (2010), The Boss (2016), Bad Moms (2016), and A Bad Moms Christmas (2017). Bell garnered further recognition for voicing Princess Anna in the Disney animated fantasy films Frozen (2013), Ralph Breaks the Internet (2018), and Frozen II (2019), and the short films Frozen Fever (2015) and Olaf's Frozen Adventure (2017).
From 2012 to 2016, Bell starred as Jeannie van der Hooven, the female lead on the Showtime comedy series House of Lies. From 2016 to 2020, she starred in the lead role of Eleanor Shellstrop on the acclaimed NBC comedy series The Good Place, for which she received a Golden Globe Award nomination for Best Actress – Television Series Musical or Comedy.

  Contents
 
1 Early life and family

2 Career
 
2.1 1992–2003: Early work

2.2 2004–2006: Veronica Mars and other roles

2.3 2007–2011: Film breakthrough and Gossip Girl

2.4 2012–present: Frozen franchise and The Good Place
 
2.4.1 Upcoming projects







3 In the media

4 Personal life
 
4.1 Relationships and family

4.2 Beliefs, interests and charity work

4.3 Mental health

4.4 Entrepreneurship




5 Filmography
 
5.1 Film

5.2 Television

5.3 Video games

5.4 Web series

5.5 Music videos

5.6 Theatre




6 Soundtrack appearances

7 Awards and nominations

8 References

9 External links




Early life and family
Bell was born on July 18, 1980, in Huntington Woods, Michigan, a suburb of Detroit, where she was raised. Her mother, Lorelei (née Frygier), is a registered nurse, and her father, Tom Bell, works as a television news director in Las Vegas, Nevada.[2][3] Her parents divorced when she was two years old, and she has two stepsisters from her father's second marriage. She also has two half-sisters and two half-brothers from her mother's second marriage.[citation needed] Her mother is of Polish descent, and her father has German, Scottish and Irish ancestry.[4]
Bell stated that she did not like her first name at the age of four. Bell's mother convinced her to go by her middle name, Anne, instead; she used the name Annie until high school.[5] Bell once broke both her wrists at the same time playing street hockey with friends.[6]
Just before her freshman year of high school, Bell's parents decided to remove her from the public school system.[7] She attended Shrine Catholic High School in nearby Royal Oak, where she took part in the drama and music club.[8] During her time at the school, she won the starring role in the school's 1997 production of The Wizard of Oz, as Dorothy Gale, and also appeared in productions of Fiddler on the Roof (1995), Lady, Be Good (1996), and Li'l Abner (1998). In 1998, the year she graduated, Bell was named the yearbook's "Best Looking Girl" by senior class vote.[9]
Shortly after her high school graduation, Bell moved to New York City to attend New York University's Tisch School of the Arts,[5] majoring in musical theater.[10] In 2001, during her senior year, Bell left a few credits shy of graduating[11] to take a role in the Broadway musical version of The Adventures of Tom Sawyer.

Career
1992–2003: Early work
In 1992, Bell went to her first audition and won a dual role as a banana and a tree[12] in a suburban Detroit theater's production of Raggedy Ann and Andy.[5] Her mother had established her with an agent before Bell was 13, which allowed her to appear in newspaper advertisements for several Detroit retailers and television commercials. She also began private acting lessons.[5] In 1998, she appeared with an uncredited role in the locally filmed movie Polish Wedding.
In 2001, Bell left New York University to play a role as Becky Thatcher in the short-lived Broadway musical of The Adventures of Tom Sawyer. That same year, she made her credited film debut in Pootie Tang. Her one line in the film was cut, and her appearance exists only as a scene shown during the credit sequence.[13] Additionally, she auditioned for the television series Smallville for the role of Chloe Sullivan, which was eventually won by Allison Mack.[14]
In 2002, she appeared in the Broadway revival of The Crucible with Liam Neeson, Angela Bettis and Laura Linney. Bell then moved to Los Angeles in 2002 because of her friendship with writers Kevin Murphy and Dan Studney,[13] and appeared in a handful of television shows as a special guest, finding trouble gaining a recurring role in a television series. Bell said she "tested like eight times and booked nothing and every show [she] tested for got picked up", including auditions for Skin and a Norm Macdonald series.[12]

2004–2006: Veronica Mars and other roles
In 2004, Bell appeared in the Lifetime television film Gracie's Choice, which received one of the network's highest ratings.[11] She made her debut in a theatrically released film with David Mamet's Spartan, as Laura Newton, the kidnapped daughter of the U.S. president, acting alongside Val Kilmer. Bell also guest-starred on the HBO drama Deadwood in a two-episode story arc ("Bullock Returns to the Camp" and "Suffer the Little Children").

      Bell on the set of Veronica Mars in 2004
At 24, she won the role of the title character in UPN's drama Veronica Mars, which was launched in the fall of 2004. Created by Rob Thomas, the series starred Bell as the seventeen-year-old anti-establishment high school student/detective. Bell drew on the parallels between the character of Veronica and her own life, since Bell's parents had divorced and her best friend had also died.[11] The series earned acclaim from television critics, as did Bell's performance. Some critics asserted that her performance was overlooked, and deserved consideration for an Emmy Award.[15][16][17]
Aside from working on Veronica Mars, Bell starred in Reefer Madness: The Movie Musical, reprising the role she played in the short-lived 2001 Off-Broadway musical. The musical was a spoof of the 1936 exploitation film of the same name. Reefer Madness: The Movie Musical debuted on the Showtime network on April 16, 2005. Also in April, Bell starred as Gracie in Fifty Pills, an entry for the Tribeca Film Festival. She appeared in a short independent film called The Receipt and the horror film Roman, which was directed by her Crucible co-star Angela Bettis. Released on August 11, 2006, Pulse starred Bell as the lead Mattie. A remake of the Japanese horror film Kairo, the film grossed US$27.9 million worldwide[18] but garnered negative response from critics. Frank Scheck of The Hollywood Reporter commented, "despite the starring presence of Kristen Bell, [the] young actress has far less interesting material to work with here than she does as [the character] 'Veronica Mars.'"[19]

2007–2011: Film breakthrough and Gossip Girl
Veronica Mars continued on UPN for a second season; for the third season, the show was renewed and appeared on the newly created The CW. On January 19, 2007, CW Entertainment President Dawn Ostroff announced that while she was pleased with the gradual improvement of Veronica Mars's ratings, the series would be put on hiatus after February sweeps to air a new reality series, Pussycat Dolls Present. On May 17, 2007, Ostroff announced the cancellation of the series.[20] A two-hour series finale aired in the United States on May 22, 2007, and on June 11, 2007, Thomas officially announced in an email to TV Guide's Michael Ausiello that Veronica Mars had been canceled by the CW.[21] A Veronica Mars feature film and comic book series continuation had been discussed,[22] and for a short time there was talk of another collaboration between Bell and creator Thomas that would be unrelated to the Veronica Mars series.[20]
Following the cancellation of Veronica Mars, Bell voiced interest in appearing on Heroes because she was a fan.[23] On July 29, 2007, during a train ride back to Los Angeles from the San Diego Comic-Con with Heroes actors Zachary Quinto and Masi Oka, and writers from the series, the writers had mentioned that if she "ever want[ed] to come on Heroes, give us [writers] a call," to which Bell said she would "love to."[24] Meanwhile, there were discussions about a role on Lost, but Bell turned down the role[25][26] of Charlotte Staples Lewis.[27] Bell portrayed Elle Bishop on Heroes, a "mysterious young lady" with an "awesome power".[24] She did not have to audition for the role of Elle,[13] who made her first appearance in an October 2007 episode, and appeared in twelve episodes during the run of the series.[28] The casting of Bell, Heroes creator Tim Kring explained, "was not easy to pull off", but because of the large ensemble cast of the series and multiple story arcs, "we found a way to jump into a small window in [Bell's] schedule."[28] Bell lent her voice to the CW series Gossip Girl: she voiced the title character in every episode of the series, appearing in person only for a surprise cameo in the final episode, portraying herself.

      Bell at the 2008 Tribeca Film Festival
Shortly after the cancellation of Veronica Mars in early 2007, Bell filmed on location in Hawaii for a starring role as the title character in the Judd Apatow comedy Forgetting Sarah Marshall. She regarded the improvisational comedy in the film as "a lovely experience".[13] The film, written by and also starring Jason Segel, was released theatrically on April 18, 2008, and greatly increased Bell's profile. Bell lent her voice and likeness to the video game Assassin's Creed, which was released on November 13, 2007, for the PlayStation 3 and Xbox 360 and April 8, 2008, for the PC.[29] Bell reprised her role of Lucy in Assassin's Creed II released on November 17, 2009, and again in Assassin's Creed: Brotherhood, released on November 16, 2010.[30] In the spring of 2006, she finished filming the Star Wars-themed comedy Fanboys. Director Kyle Newman received additional funding to shoot new scenes, but the busy schedules of the actors only allowed for filming in September 2007. As a result, the release was delayed until January 14, 2008.[31] Bell also starred in the 2009 comedies Serious Moonlight, alongside Meg Ryan, and Couples Retreat, which chronicles four couples who partake in therapy sessions at a tropical island resort. Jason Bateman played her husband.[32] She also provided the voice for Cora in Astro Boy. On March 31, 2008, Bell began shooting the Mark Steven Johnson-written Disney film When in Rome on location in Rome and New York; the film was released in 2010.[33] Bell reprised her role as Sarah Marshall for a cameo appearance in the film Get Him to the Greek, a spin-off sequel from Forgetting Sarah Marshall, released June 4, 2010.
She co-starred with singers Christina Aguilera and Cher in the musical film Burlesque, which was released on Thanksgiving in 2010. Bell had a cameo in the slasher horror film Scream 4, which was released on April 15, 2011.[34]

2012–present: Frozen franchise and The Good Place
In 2012, Bell starred in the family drama film Big Miracle. She has also appeared in the music video for "Madder Red" by Brooklyn experimental rock band Yeasayer. Bell portrayed Mary Magdalene in The Truth & Life Dramatized Audio New Testament Bible, a 22-hour, celebrity-voiced, dramatized audio adaptation of the New Testament that uses the RSV-CE translation.

      Bell at the premiere of Frozen in 2013 at the El Capitan Theatre
Bell starred as Jeannie van der Hooven, the female lead on the Showtime comedy series House of Lies, which premiered on January 8, 2012. The series ended on June 12, 2016. Bell appeared in a supporting role in the science-fiction comedy Safety Not Guaranteed (2012). She starred in the drama film The Lifeguard, written and directed by Liz W. Garcia, which began filming in July 2012 and was released in August 2013.[35] She also voiced Anna in Frozen, which was released on November 22, 2013. In 2013, for multiple episodes, Bell played Ingrid de Forest, an Eagleton City Councilwoman, on Parks and Recreation.
On March 13, 2013, it was confirmed that a Veronica Mars film would finally be coming to fruition. Bell and series creator Rob Thomas launched a fundraising campaign to produce the film through Kickstarter and attained the $2 million goal in less than ten hours.[36] The main cast members of the series all reprised their roles in the feature film. Production of the film took place during summer 2013, and it was released theatrically and on video-on-demand on March 14, 2014.[37][38]
In September 2014, Bell starred with her husband, Dax Shepard, in a commercial for the Samsung Galaxy Tab S.[39] It was so popular (with over 20 million YouTube views) that they did another for the holiday season.[40] The ad agency McKinney was behind both.[41] In 2016, Bell voiced the sloth Priscilla in the animated comedy film Zootopia, and starred as Claire in the comedy film The Boss. Bell starred as Kiki in the 2016 comedy film Bad Moms, a role she reprised in the 2017 sequel, A Bad Moms Christmas.[42] In 2016, Bell began starring as Eleanor Shellstrop in the NBC comedy series The Good Place.[43]
Also in 2017, she appeared in the biographical comedy-drama The Disaster Artist, the action comedy CHiPs and the comedy How to Be a Latin Lover. In November 2017, she played a housewife (with Dax Shepard as her husband) preparing for a Christmas party in Sia's music video for "Santa's Coming For Us".[44]
In 2018, she began hosting the web series Momsplaining with Kristen Bell, with episodes airing on the Ellen DeGeneres video platform Ellentube. In the series, Bell gives new and expecting mothers tips about motherhood. The title of the series is a pun on the commonly used term "mansplaining".[45][46][47] Bell later had the leading role in the Netflix comedy-drama film Like Father. She also voiced the character of Jade Wilson in the animated comedy film Teen Titans Go! To the Movies and reprised her voice role as Princess Anna in the animated comedy sequel film Ralph Breaks the Internet.[48]
On September 20, 2018, Hulu confirmed that Bell would reprise her role of Veronica Mars in an 8-episode fourth season of the drama series Veronica Mars, which would premiere in July 2019.[49] In 2019, she again reprised her role of Princess Anna in the video game, Kingdom Hearts III[50] and the sequel, Frozen II[51] which was released on November 22, 2019.[52] Bell served as host and executive producer on the Disney+ docuseries Encore!, which premiered in November 2019.[53] She made her directorial debut with the eighth episode of the fourth season of The Good Place.[54] The series concluded after its fourth season, airing its final episode in January 2020.[55]
In 2020, Bell voiced Molly Tillerman in the Apple TV+ animated musical comedy series Central Park, which reunited her with Frozen co-star Josh Gad and Bad Moms co-star Kathryn Hahn.[56] Central Park received a two-season order from Apple and the series premiered on May 29, 2020. However, in June 2020, it was announced that Bell, who is Caucasian, would no longer voice the biracial character of Molly. The role would be re-cast with a person of color amid the George Floyd protests, with Bell voicing a new role.[57]

Upcoming projects
Bell is set to produce and voice a character in the Prime Video animated musical pre-school series Do, Re, & Mi.[58] Moreover, she will star alongside Leslie Jones as pair of housewives who create a $40 million coupon scam in the comedy film Queenpins.[59] Bell will also narrate the HBO Max drama series Gossip Girl. The series serves as a soft reboot and sequel to the 2007 series of the same, which Bell also narrated.

In the media
In 2006 and again in 2013,[60] Bell was selected "World's Sexiest Vegetarian" on PETA's yearly poll.[61] She was placed No. 68 on Maxim's 2005 "Hot 100" list,[62] No. 11 in Maxim's 2006 "Hot 100" list,[63] and No. 46 in Maxim's 2007 "Hot 100" list in which she was stated to have "single-handedly saved The CW from becoming the worst network ever".[64] In 2006, Maxim also placed Bell at the top of the "Fall TV's Criminally Sexy Investigators" List.[61] In 2008, she was featured at No. 59 on AskMen's Top 99 Women of 2008 List.[65] Reflecting on her admitted popularity with "geeks", Bell was voted the fourth-sexiest woman on TV by the staff at Wizard magazine.[66]
Bell stated she never thought of herself as womanly because "I always play and look and act 10 years younger than I am." However, she said, "Something magical happened when I turned 25—I looked in the mirror and was like, 'You might not get carded for an R-rated movie anymore.' Like I didn't have a little stick figure anymore."[67] Bell has said that many of her characters are tomboys because she was "not homely enough to play the nerdy girl and not nearly pretty enough to play the pretty girl".[67]

      Bell signing autographs at San Francisco's Metreon (June 2006)
Bell has been associated with the idea that "nerdy is the new cool", and she explained, "what was previously perceived as nerdy is now viewed as original. What I like about nerdiness, geekiness, is it doesn't really matter what you're into—it just means you're not a follower."[67] She has also said, "I love nerds. Comic-Con junkies are the tastemakers of tomorrow. Isn't that funny? The tables have turned."[13] Vanessa Juarez of Entertainment Weekly commented that Bell's roles on Veronica Mars, Heroes and as a Star Wars fanatic in Fanboys have "solidif[ied] her placement at the center of the geek universe," while Rodney Rothman stated, "I guess she's cornered the market on losers."[68] Bell's work is often compared to Sarah Michelle Gellar's portrayal of the title character on the cult television series Buffy the Vampire Slayer.[69] Frank Scheck of The Hollywood Reporter stated that Bell was "arguably the television successor [to Gellar's portrayal of Buffy] when it comes to fighting bad guys." Bell is sometimes confused with Lauren Conrad from The Hills. "Yeah, sometimes fans yell, 'Hey, Lauren' to me, but usually from a distance," said Bell.[19]
Despite "new celebrity" status, Bell claimed that she was not concerned because "no one ever recognizes me anyway". She has said that her friend Hayden Panettiere is more famous than she is and attracts more attention; as Bell explained, "I hang out with Hayden quite a bit—they never take pictures of me. I just step to the side, and I push myself in front of her when she wants to get out of it, or put her in the car."[67]
Bell was a recurring guest on The Late Late Show with Craig Ferguson, appearing in interviews as well as sketches. On The Late Late Show, she shows a humorous hostility towards Craig Ferguson's robot skeleton sidekick Geoff Peterson, claiming that she had wanted to be Ferguson's sidekick on his show and taking it upon herself to cut Peterson down every chance she gets. Both Bell and Peterson appeared with Ferguson during the five Late Late Show episodes filmed in France.[70]
In January 2011, it was announced that Bell would be the new face of Neutrogena.[71]
In 2019, Bell was featured on the cover of Entrepreneur magazine's April–May issue. In the article, she discussed her snack bar company, This Saves Lives, which donates its sales to help feed malnourished children around the globe.[72]
In November 2019, Bell and Idina Menzel, who play sisters in Disney's Frozen franchise, received neighboring stars—Bell's was the 2681st and Menzel's was the 2682nd—on the Hollywood Walk of Fame.[73][74]
In  2020, Bell published a children's book The World Needs More Purple People[75] with Benjamin Hart.

Personal life
Relationships and family
In 2007, Bell ended a five-year relationship with former fiancé Kevin Mann.[76] She later told Complex magazine that dating "makes me want to vomit. And not out of grossness—OK, a little bit out of grossness, but just nerves... I've always been a serial monogamist."[67]
In late 2007, Bell began dating actor Dax Shepard, who is also from Metro Detroit. The couple announced their engagement in January 2010.[77] They decided to delay marriage until the state of California passed legislation legalizing same-sex marriage.[78] They co-starred in the 2010 film When in Rome, the 2012 film Hit and Run,[79] and the 2017 film CHiPs. After section 3 of the Defense of Marriage Act was ruled unconstitutional by the Supreme Court on June 26, 2013, Bell asked Shepard to marry her through Twitter,[80] which he accepted.[81] They were married at the Beverly Hills County Clerk's Office on October 16, 2013.[82] They have two daughters, the first, named Lincoln, born in 2013[83] and the second, Delta, born in 2014.[84]

Beliefs, interests and charity work
      Bell with the Dancing Merengue Dog at the 2012 Bonnaroo Music Festival
At age 11, Bell became a vegetarian.[61] In an interview with PETA, Bell stated, "I have always been an animal lover. I had a hard time disassociating the animals I cuddled with—dogs and cats, for example—from the animals on my plate, and I never really cared for the taste of meat. I always loved my Brussels sprouts!"[85] By 2012 Bell had become vegan with her husband after watching the documentary Forks Over Knives.[86][87] During her first pregnancy, she returned to eating dairy and eggs, however.[88] During her time in Michigan, Bell fostered animals from the Michigan Humane Society, and she now supports the San Diego-based Helen Woodward Animal Center. Bell often attends fundraisers for the American Society for the Prevention of Cruelty to Animals and other non-profit organizations dedicated to protecting animals. She has had several dogs, including a Welsh Corgi-Chow Chow mix named Lola, a Welsh Corgi-Chihuahua mix named Shakey, and a black Labrador Retriever named Sadie, who was 11 years old when she was rescued from Hurricane Katrina and adopted by Bell in 2005.[7][89]
Bell has stated she is non-religious and identifies as a humanist.[90] Bell and other Veronica Mars cast members, including Ryan Hansen, are involved with the charity Invisible Children, Inc. The goal of the organization is to create awareness of the plight of Northern Ugandans who are caught in the midst of a civil war between the government and Joseph Kony's Lord's Resistance Army.[91] Bell has also done a public service announcement for Do Something's Healthy Living Campaign.[92] In 2014, Bell launched a Prizeo campaign offering fans a chance to win a date with her in return for donating to Invisible Children, Inc.[93] Kristen Bell won a 2020 Webby Special Achievement Award.[94]
Bell supported and campaigned for Barack Obama during the 2008 United States presidential election. Along with Rashida Jones, she visited college campuses in Missouri to discuss the candidates and encourage voter registration.[95][96][97] Bell showed support for the Writers Guild of America in the writers' strike, appearing in the picket lines in December 2007, stating, "the writers are just looking for some fairness."[98]
Bell and her husband Dax Shepard are staunchly pro-vaccination.[99][100]
Bell practices the Transcendental Meditation program, stating: "I really enjoy powering down for 20 minutes because it’s like shutting off light switches in my brain."[101]

Mental health
In May 2016, Bell stated that she has struggled with and received treatment for depression and anxiety.[102] She said, "It's important for me to be candid about this so people in a similar situation can realize that they are not worthless and that they do have something to offer."[103]

Entrepreneurship
In 2019, Bell and her husband founded the company Hello Bello, a plant-based baby care product line. The product line focuses on creating environmentally friendly and affordable products. The company partnered exclusively with Walmart and the line is sold in all locations, as well as online.[104]

Filmography
Film


Year

Title

Role

Notes



1998

Polish Wedding

Teenage Girl

Uncredited[105]



2001

Pootie Tang

Record Executive's Daughter





2002

People Are Dead

Angela's Friend #1





2003

The Cat Returns

Hiromi (voice)

English dub



2004

Spartan

Laura Newton





2005

Reefer Madness

Mary Lane





2005

Deepwater

Nurse Laurie





2005

Last Days of America

Friend in New York #1





2005

The Receipt

Pretty Girl

Short film



2006

Fifty Pills

Gracie





2006

Pulse

Mattie





2006

Roman

The Girl / Isis





2007

Flatland: The Movie

Hex (voice)

Short film



2008

Fanboys

Zoe





2008

Forgetting Sarah Marshall

Sarah Marshall





2009

Serious Moonlight

Sara





2009

Astro Boy

Cora (voice)





2009

Couples Retreat

Cynthia





2010

Astro Boy vs. The Junkyard Pirates

Cora (voice)

Short film



2010

Lost Masterpieces of Pornography

June Crenshaw

Short film



2010

When in Rome

Beth





2010

Get Him to the Greek

Sarah Marshall

Cameo



2010

You Again

Marni Olsen





2010

Burlesque

Nikki





2011

Scream 4

Chloe





2012

Safety Not Guaranteed

Belinda St. Sing





2012

Big Miracle

Jill Jerard





2012

Flatland 2: Sphereland

Hex (voice)





2012

Hit and Run

Annie

Also co-producer



2012

Stuck in Love

Tricia





2013

Movie 43

Supergirl

Segment: "Super Hero Speed Dating"



2013

Some Girl(s)

Bobbi





2013

The Lifeguard

Leigh





2013

Frozen

Anna (voice)





2014

Veronica Mars

Veronica Mars

Also executive producer



2015

Frozen Fever

Anna (voice)

Short film



2015

Unity

Narrator

Documentary



2016

Zootopia

Priscilla (voice)





2016

The Boss

Claire Rawlings





2016

Bad Moms

Kiki





2017

The Disaster Artist

Herself





2017

CHiPs

Karen Baker





2017

How to Be a Latin Lover

Cindy





2017

A Bad Moms Christmas

Kiki





2017

Olaf's Frozen Adventure

Anna (voice)

Short film



2018

Pandas

Narrator

Documentary



2018

Teen Titans Go! To the Movies

Jade Wilson (voice)





2018

Like Father

Rachel Hamilton





2018

Ralph Breaks the Internet

Anna (voice)





2019

Frozen II

Anna (voice)




Television


Year

Title

Role

Notes



2003

The Shield

Jessica Hintel

Episode: "The Quick Fix"



2003

American Dreams

Amy Fielding

Episode: "Act of Contrition"



2003

The O'Keefes

Virginia's Owner

2 episodes



2003

The King and Queen of Moonlight Bay

Alison Dodge

Television film



2003

Everwood

Stacey Wilson

Episode: "Extra Ordinary"



2004

Gracie's Choice

Gracie Thompson

Television film



2004

Deadwood

Flora Anderson

2 episodes



2004–2007, 2019

Veronica Mars

Veronica Mars

Lead role: 72 episodes



2007–2008

Heroes

Elle Bishop

Main cast (season 2) Recurring (season 3): 12 episodes



2007–2012

Gossip Girl

Gossip Girl (voice) / Herself

Narrator: 120 episodes Herself (Episode: "New York, I Love You XOXO")



2009

The Cleveland Show

Mandy (voice)

Episode: "Da Doggone Daddy-Daughter Dinner Dance"



2009–2010

Party Down

Uda Bengt

2 episodes



2011

Glenn Martin, DDS

Hayley (voice)

Episode: "Videogame Wizard"



2011

Robot Chicken

Hermione Granger / Sara Lee (voice)

Episode: "Some Like It Hitman"



2012–2016

House of Lies

Jeannie van der Hooven

Lead role: 58 episodes



2012

Unsupervised

Megan (voice)

13 episodes



2012

Lovin' Lakin

Herself

Episode: "Lakin Runs Into Kristen Bell"



2012–2014

CMT Music Awards

Herself / Co-Host

Alongside Toby Keith and Jason Aldean



2013–2014

Parks and Recreation

Ingrid de Forest

3 episodes



2013

Hollywood Game Night

Herself

Episode: "The One with the Friends"



2013

Lady Gaga and the Muppets Holiday Spectacular

Special



2015

30th Independent Spirit Awards

Herself / Host

Special



2015

Repeat After Me

Herself

Episode: "1.2"



2015

The Simpsons

Harper Jambowski (voice)

Episode: "Friend with Benefit"



2015

Liv and Maddie

Herself

Episode: "Ask Her More-a-Rooney"



2015

It's Your 50th Christmas, Charlie Brown

Herself / Host

Special



2016

iZombie

Herself (voice)

Episode: "Fifty Shades of Grey Matter"



2016

LEGO Frozen Northern Lights

Anna (voice)

Special



2016–2020

The Good Place

Eleanor Shellstrop

Lead role: 53 episodes   Directed episode: "The Funeral to End All Funerals"



2017

Nobodies

Herself

Episode: "Too Much of a Good Thing"



2017

Jimmy Kimmel Live!

Herself / Guest Host

May 4, 2017; standing in for Jimmy Kimmel on paternity leave



2017

BoJack Horseman

Ruthie (voice)

Episode: "Ruthie"



2017

Family Guy

Martha (voice)

Episode: "Petey IV"



2017

Encore!

Herself / Host

Special



2017–2018

Big Mouth

Various voices

4 episodes



2018

24th Screen Actors Guild Awards

Herself / Host

Special



2018

The Ellen DeGeneres Show

Herself / Guest Host

May 31, 2018; standing in for Ellen DeGeneres



2018

The Joel McHale Show with Joel McHale

Herself

Episode: "Pizza Ghost"



2019

Last Week Tonight with John Oliver

Episode: "Compounding Pharmacies"



2019–present

Encore!

Herself / Host

12 episodes



2020

#KidsTogether: The Nickelodeon Town Hall

Moderator of COVID-19 special[106]



2020

Central Park

Molly Tillerman / TBA (voice)[107]

Main role: 13 episodes



2020

Into the Unknown: Making Frozen II

Herself

6 episodes[108]



2021

Gossip Girl

Gossip Girl (voice)

Narrator


Video games


Year

Title

Role

Notes



2007

Assassin's Creed

Lucy Stillman

Also likeness



2009

Astro Boy: The Video Game

Cora





2009

Assassin's Creed II

Lucy Stillman

Also likeness



2010

Assassin's Creed: Brotherhood

Also likeness



2013

Disney Infinity[109]

Anna





2014

Disney Infinity: Marvel Super Heroes[110]





2015

Disney Infinity 3.0[111]





2019

Kingdom Hearts III[112]




Web series


Year

Title

Role

Notes



2012

Burning Love

Mandy

4 episodes



2014

Play It Again, Dick

Herself

7 episodes[113]



2017

Ryan Hansen Solves Crimes on Television

Episode: "Freezed"[114]



2018–2020

Momsplaining with Kristen Bell

25 episodes[45][46][47]


Music videos



Year

Title

Artist



2010

"Madder Red"

Yeasayer



2017

"Santa's Coming for Us"

Sia



2019

"Lost In The Woods"

Weezer


Theatre



Year

Title

Role

Notes



2001

The Adventures of Tom Sawyer

Becky Thatcher

Broadway



2001

Reefer Madness

Mary Lane

Off-Broadway



2002

The Crucible

Susanna Walcott

Broadway



2002

A Little Night Music

Fredrika Armfeldt

Kennedy Center, Washington



2003

Sneaux

Sneaux Devareaux

Los Angeles



2004

A Little Night Music

Fredrika Armfeldt

Los Angeles Opera



2014

Hair

Sheila

Hollywood Bowl, Los Angeles


Soundtrack appearances



Track title

Performer(s)

Peak position

Certifications

Album



US [115]

CAN [115]

AUS [116]

IRE [117]

KOR [118]

UK [119]



"Do You Want to Build a Snowman?"

Bell, Agatha Lee Monn, Katie Lopez

51

61

45

35

5

26


 RIAA: Platinum[120]

ARIA: Gold

BPI: Gold


Frozen (Original Motion Picture Soundtrack)



"For the First Time in Forever"

Bell and Idina Menzel

57

70

62[121]

54

4

38


 RIAA: Gold[122]

ARIA: Gold

BPI: Silver




"Love Is an Open Door"

Bell and Santino Fontana

49

—

91[123]

—

21

56


 RIAA: Gold[124]

BPI: Silver




"Text Me Merry Christmas"

Bell and Straight No Chaser

—

—

—

—

—

—







"Tell Me How Long"

Bell

—

—

—

—

—

—



Chasing Coral (Original Motion Picture Soundtrack)



"Some Things Never Change"

Bell, Idina Menzel, Josh Gad, Jonathan Groff

—

—

—

—

—

—



Frozen II (Original Motion Picture Soundtrack)



"The Next Right Thing"

Bell

—

—

—

—

—

—




Awards and nominations



Year
Award
Category
Work
Result
Ref.



2005
Saturn Awards
Best Actress on Television
Veronica Mars
Nominated
[125]



TCA Awards
Individual Achievement in Drama
[126]



Satellite Awards
Outstanding Actress in a Series, Drama




Outstanding Actress in a Miniseries or a Motion Picture Made for Television
Reefer Madness: The Movie Musical
Won




Teen Choice Awards
Choice TV Breakout Performance – Female
Veronica Mars
Nominated
[127]



Gold Derby Awards
Drama Lead Actress
Won
[128]



Breakthrough Performer of the Year
Nominated
[128]



2006
Drama Lead Actress
Won
[129]



Saturn Awards
Best Actress on Television
[130]



Satellite Awards
Best Actress in a Series, Drama
Nominated




Teen Choice Awards
TV - Choice Actress: Drama/Action Adventure
[131]



2007
Saturn Awards
Best Actress in a Television Program
[132]



2008
Teen Choice Awards
Choice Movie Actress: Comedy
Forgetting Sarah Marshall
[133]



Choice Movie Breakout Female
[133]



2009
Saturn Awards
Best Guest Starring Role in a Television Series
Heroes
[134]



MTV Movie Awards
Best WTF Moment (shared with Jason Segel)
Forgetting Sarah Marshall
[135]



Teen Choice Awards
Choice TV Actress: Action/Adventure
Heroes
[136]



Choice Movie Actress: Comedy
Couples Retreat
[137]



2010
Choice Movie Actress: Romantic Comedy
When in Rome
[137]



2011
Scream Awards
Best Cameo (shared with Anna Paquin)
Scream 4




2012
Gotham Awards
Best Ensemble Performance
Safety Not Guaranteed
[138]



2013
Alliance of Women Film Journalists
Best Animated Female
Frozen
Won
[139]



Streamy Awards
Best Female Performance: Comedy
Burning Love
Nominated
[140]



2014
Teen Choice Awards
Choice Movie Actress: Drama
Veronica Mars
[141]



Choice Animated Movie: Voice

Frozen

[141]



2015
People's Choice Awards
Favorite Cable TV Actress
House of Lies
[142]



2016
Favorite Premium Cable TV Actress
Won
[143]



2017
Favorite Comedic Movie Actress
Bad Moms & The Boss
Nominated
[144]



Favorite Actress in a New TV Series
The Good Place
Won
[144]



TCA Awards
Individual Achievement in Comedy
Nominated
[145]



2018
Critics' Choice Television Awards
Best Actress in a Comedy Series
[146]



People's Choice Awards

Comedy TV Star of 2018

[147]



Gold Derby Awards

Comedy Actress

[148]



Ensemble of the Year

[148]



2019

Comedy Actress of the Decade

[149]



Golden Globe Awards
Best Actress – Television Series Musical or Comedy
[150]



People's Choice Awards
Comedy TV Star of 2019
Won
[151]



Nickelodeon Kids' Choice Awards

Favorite Female Voice from an Animated Movie

Teen Titans Go! To the Movies

Nominated

[152]



Daytime Emmy Awards

Outstanding Special Class – Short Format Daytime Program

Momsplaining with Kristen Bell

[153]



Hollywood Walk of Fame
Motion picture star
All film work
Inducted
[154]



2020

Critics' Choice Movie Awards

#SeeHer Award

Won

[155]



Alliance of Women Film Journalists

Best Animated Female

Frozen II

Nominated

[156]



Nickelodeon Kids' Choice Awards

Favorite Female Voice from an Animated Movie

[157]


References

External links



Wikimedia Commons has media related to Kristen Bell.





Wikiquote has quotations related to: Kristen Bell


 Kristen Bell on IMDb

Kristen Bell at the Internet Broadway Database 

Kristen Bell at the TCM Movie Database

Kristen Bell at AllMovie

  Awards for Kristen Bell
 
   v
t
e
 People's Choice Award for Favorite Actress in a New TV Series
 
 Sarah Michelle Gellar (2014)

Viola Davis (2015)

Priyanka Chopra (2016)

Kristen Bell (2017)



   v
t
e
 Satellite Award for Best Actress – Miniseries or Television Film
 
 Helen Mirren (1996)

Jennifer Beals (1997)

Angelina Jolie (1998)

Linda Hamilton (1999)

Jill Hennessy (2000)

Judy Davis (2001)

Vanessa Williams (2002)

Meryl Streep (2003)

Dianne Wiest (2004)

Kristen Bell (2005)

Judy Davis (2006)

Samantha Morton (2007)

Judi Dench (2008)

Drew Barrymore (2009)

Claire Danes (2010)

Kate Winslet (2011)

Julianne Moore (2012)

Elisabeth Moss (2013)

Frances McDormand (2014)

Sarah Hay (2015)

Sarah Paulson (2016)

Nicole Kidman (2017)

Amy Adams (2018)

Michelle Williams (2019)



   v
t
e
 Saturn Award for Best Actress on Television
 
 Gillian Anderson (1996)

Kate Mulgrew (1997)

Sarah Michelle Gellar (1998)

Margaret Colin (1999)

Jessica Alba (2000)

Yancy Butler (2001)

Jennifer Garner (2002)

Amber Tamblyn (2003)

Claudia Black (2004)

Kristen Bell (2005)

Jennifer Love Hewitt (2006)

Jennifer Love Hewitt (2007)

Mary McDonnell (2008)

Anna Torv (2009)

Anna Torv (2010)

Anna Torv (2011)

Anna Torv (2012)

Vera Farmiga (2013)

Caitriona Balfe (2014)

Caitriona Balfe (2015)

Melissa Benoist (2016)

Sonequa Martin-Green (2017)

Emilia Clarke (2018/2019)




 Authority control  
 BNE: XX4617860

BNF: cb155485434 (data)

CANTIC: a11788811

GND: 137143095

ISNI: 0000 0001 1679 603X

LCCN: no2006010060

MusicBrainz: ac579536-1398-44db-a37b-9d5963a46929

NKC: xx0085807

NTA: 311440606

SUDOC: 160591325

VIAF: 81375405

 WorldCat Identities: lccn-no2006010060









