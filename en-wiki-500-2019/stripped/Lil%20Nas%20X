  Not to be confused with Nas.



 American rapper, singer, and songwriter
 Lil Nas X
 Nas X at the American Music Awards in November 2019
Born Montero Lamar Hill  (1999-04-09) April 9, 1999 (age 21)  Lithia Springs, Georgia, U.S.
NationalityAmerican
Occupation 
 Rapper

singer

songwriter


Years active2018–present
AwardsFull list
Musical career
Genres 
 Hip hop[1]

country rap[1][2][3]

trap[1]

pop[1]

rock[1]

R&B[1]


InstrumentsVocals
LabelsColumbia
Websitelilnasx.com


Montero Lamar Hill (born April 9, 1999), known as Lil Nas X (/nɑːz/ NAHZ), is an American rapper, singer, and songwriter. He rose to prominence with the release of his country rap single "Old Town Road", which first achieved viral popularity on the micro-platform app TikTok in early 2019 before climbing music charts internationally and becoming diamond certified by November of that same year.[a] 
"Old Town Road" topped the Billboard Hot 100 chart and remained there for nineteen weeks, becoming the longest-running number-one song since the chart debuted in 1958.[4] Several remixes of the song were released, the most popular of which featured country singer Billy Ray Cyrus. Nas X came out as gay while "Old Town Road" was atop the Hot 100, becoming the only artist to do so while having a number-one record.[5] Following the success of "Old Town Road", Nas X released his debut EP, titled 7, which spawned two further singles⁠, with "Panini" peaking at number five on the Hot 100.
Nas X was the most-nominated male artist at the 62nd Annual Grammy Awards,[6] where he ultimately won awards for Best Music Video and Best Pop Duo/Group Performance. "Old Town Road" earned him two MTV Video Music Awards including Song of the Year; the American Music Award for Favorite Rap/Hip Hop Song; and Nas X is the first and only openly LGBTQ artist to win a Country Music Association award.[7] Time named him as one of the 25 most influential people on the Internet in 2019.[8] In addition, he was featured on Forbes 30 under 30 list in 2020.[9]

  Contents
 
1 Early life and education

2 Career
 
2.1 2015–2017: Internet personality

2.2 2018–2019: "Old Town Road"'s success and 7 EP

2.3 Public recognition




3 Public image

4 Personal life

5 Discography
 
5.1 Extended plays

5.2 Mixtapes

5.3 Singles

5.4 Other charted songs




6 Awards and nominations

7 Notes

8 References

9 External links




Early life and education
Montero Lamar Hill was born in Lithia Springs, Georgia,[10] a small city outside of Atlanta, on April 9, 1999.[11] His parents divorced when he was six,[12] and he settled in the Bankhead Courts housing project with his mother and grandmother. Three years later, he moved in with his father, a gospel singer,[12] north of the city in Austell, Georgia. Although initially reluctant to leave, he later regarded it as an important decision: "There's so much shit going on in Atlanta—if I would have stayed there, I would have fallen in with the wrong crowd."[13] He started "using the Internet heavily right around the time when memes started to become their own form of entertainment"; about when he was thirteen.[14]
He spent much of his teenage years alone, and turned to the Internet, "particularly Twitter, creating memes that showed his disarming wit and pop-culture savvy."[12] His teenage years also saw him struggling with his coming out to himself as being gay; he prayed that it was just a phase,[15][16] but around sixteen or seventeen he came to accept it.[17] He began playing trumpet in the fourth grade and was first chair by his junior high years, but quit out of fear of looking uncool.[18]
He attended Lithia Springs High School and graduated in 2017.[19] He attended the University of West Georgia for one year before dropping out to pursue a musical career. During this time, he stayed with his sister and supported himself with jobs at Zaxby's restaurants and the Six Flags Over Georgia theme park.[13] In September 2019 he revisited his high school to perform a surprise concert.[20]

Career
2015–2017: Internet personality
Hill said he began to isolate himself from "outside-of-class activities" during his teenage years. He spent large amounts of time online in hopes of building a following as an internet personality to promote his work, but was unsure what to focus on creatively. In a Rolling Stone interview he stated, "I was doing Facebook comedy videos, then I moved over to Instagram, and then I hopped on Twitter ... where I really was a master. That was the first place where I could go viral."[21] As of August 2019[update] Nas X has: 2.3 million followers on Twitter; four million followers on Instagram; "5.2 million subscribers on YouTube, and over 44 million monthly listeners on Spotify."[22] He posted short-format comedy videos on Facebook and Vine.[13]
During this period, he reportedly created and ran Nicki Minaj fan accounts on Twitter, including one called "@NasMaraj", according to a New York Magazine investigation.[23][24] In 2017, this account gained attention for its flash fiction-style interactive "scenario threads" popularized on Twitter using dashboard app TweetDeck.[25][26][23] The investigation linked @NasMaraj to the practice of "Tweetdecking", or using multiple accounts in collaboration to artificially make certain tweets go viral. The @NasMaraj account was suspended by Twitter due to "violating spam policies".[23] After the suspension of @NasMaraj, New York Magazine's investigation concluded that he subsequently opened a new account with handle "@NasMarai", and that his current Twitter account at the time was a repurposed version of that "@NasMarai" account with a changed handle.[23] After media reports linked Nas X to the Minaj fan accounts, he called the reports a "misunderstanding", effectively denying having run the accounts.[27][28]
He then landed on music as a path to success, and started writing and recording songs in his closet.[12] He adopted the name Lil Nas X, which is a tribute to the rapper Nas.[29] In late October 2018, he ran across the music that would be "Old Town Road".[12]

2018–2019: "Old Town Road"'s success and 7 EP
 Main article: Old Town Road
On December 3, 2018, Nas X released the country rap song "Old Town Road".[b] He bought the beat for the song anonymously on an online store from Dutch producer YoungKio for $30;[42] it samples Nine Inch Nails' track "34 Ghosts IV" from their sixth studio album Ghosts I–IV (2008).[43] He recorded at a "humble" Atlanta studio,  CinCoYo, on their "$20 Tuesdays" taking less than an hour.[44] Nas X began creating memes to promote "Old Town Road" before it was picked up by short-form video social media TikTok users.[11] TikTok encourages its 500 million global users to "endless imitation," with videos generating copies usually using the same music; the "app’s frantic churn of content ...acts as a potent incubator for viral music hits."[45] Nas X estimated he made about a hundred memes to promote it;[12] the song went viral in early 2019 due to the #Yeehaw Challenge meme on TikTok. Millions of users posted videos of themselves dressed as a wrangler or cowgirl, with most #yeehaw videos using the song for their soundtrack; as of July 2019, they have been seen more than 67 million times.[11] Another core audience tied to social media is children who are hidden in the statistics of adult listeners.[46] Quartz.com says the song certainly owes part of its success to the demographic, and notes they are attracted to the song being repetitive, easy to sing along to, and using lyrics about riding horses and tractors, which kids can relate to.[46] It debuted at number 83 on the Billboard Hot 100 chart, later climbing to number one.[47] The track also debuted on the Hot Country Songs chart at number 19 and Hot R&B/Hip-Hop Songs at number 36.[48] After an "intense bidding war", Nas X  signed with Columbia Records in March 2019.[11] Billboard controversially removed the song from the Hot Country songs chart in March 2019 telling Rolling Stone:

"When determining genres, a few factors are examined, but first and foremost is a musical composition. While "Old Town Road" incorporates references to country and cowboy imagery, it does not embrace enough elements of today's country music to chart in its current version.[49]
In Robert Christgau's opinion, "Taking 'Old Town Road' off the country chart strikes me as racist pure and simple, because country radio remains racist regardless of the Darius Ruckers and Kane Browns it makes room for."[50] Another Billboard spokesperson told Genius, "Billboard's decision to take the song off of the country chart had absolutely nothing to do with the race of the artist."[49] Despite being removed from the main Country Songs chart, the song charted on Billboard's Country Airplay chart, debuting at 53,[51] and peaking at 50.[52] In response, Sony Music Nashville CEO Randy Goodman told Billboard that his team started testing the song in some country radio markets, adding "it would be negligent not to look at it".[51] In May 2019, the issues of racism in country music culture came up again when Wrangler announced its Lil Nas X collection, and some consumers threatened a boycott.[53] Media outlets also noted that the song brings attention to the historic cultural erasure of African-Americans from both country music and the American frontier era.[54][55][56][57]
Country music star Billy Ray Cyrus supported "Old Town Road",[58] and became the featured vocalist in an April 2019 remix, the first of several.[59][c] That same month, Nas X broke Drake's record for the most U.S. streams in one week with 143 million streams for the week ending April 11, surpassing Drake's "In My Feelings", which had 116.2 million streams in a week in July 2018;[64] as of August 2019 it has streamed over a billion plays on Spotify alone.[12] NBC News's Michael Arceneaux noted,

In the social media age, Lil Nas X is arguably the first micro-platform crossover star.[65]
In May 2019, the video was released and as of August 2019,  has over 370 million views.[61][22] Nas X released his debut EP, 7, on June 21, 2019.[66][67] The EP debuted at number two on the Billboard 200 chart.[68] On June 23, 2019, Nas X performed with Cyrus at the 2019 BET Awards.[69] On June 30, Nas X made his international debut at the largest greenfield festival in the world, the U.K.’s annual Glastonbury Festival, when he and Billy Ray Cyrus made a surprise appearance and joined Miley Cyrus for the song, before performing his new single "Panini" solo in a set seen nationally on BBC.[70] On the same day, Nas X became one of the most visible Black queer male singers when he came out as gay.[71] This was especially significant for an artist in the country and hip hop genres, both of which emphasize machismo and "historically snubbed queer artists".[71][d] Black queer male artists in hip hop having mainstream acceptance arguably started in 2012 with Frank Ocean’s coming out just before Channel Orange was released.[71][72][73][e] For end of June 2019 sales, Rolling Stone premiered the Rolling Stone Top 100 in early July with three Nas X songs: "Rodeo" with Cardi B at number nine; "Panini" at four; and "Old Town Road" as the first-ever number-one song on the chart.[75][f]

Public recognition
In July 2019, Time named him one of the 25 most influential people on the Internet for his "global impact on social media", and "overall ability to drive news".[76] In late July 2019, the MTV Video Music Award (VMA) nominations were announced, with Nas X receiving eight.[77][g] He performed "Panini," an "ode to the cartoon rabbit of the same name from Cartoon Network's Chowder," with a troupe of "Tron-inflected dancers" at the 2019 MTV VMAs,[78] where he also won two awards: Song of the Year, for which he is the first LGBTQ person to do so;[79] and the video's Calmatic for Best Direction.[80] He was nominated for five fan-chosen 2019 Teen Choice Awards, winning Choice R&B/Hip-Hop Song for the Cyrus remix of "Old Town Road".[81][h] "Old Town Road" is also the YouTube top song of the summer in the U.S. and over fifty other countries and territories; it is also their second top global song of the summer.[83] Nas X and Billy Ray Cyrus’ remix also won the Country Music Association (CMA) Awards collaboration category, CMA Music Event of the Year; Nas X is the first out gay man to ever be nominated for a CMA award, and the only openly LGBTQ person to win.[84][85][7] Vox noted the Event Award is not a part of the CMA televised celebration, and they snubbed Nas X from bigger appropriate categories.[86] The "Old Town Road" remix with Cyrus has been nominated for a People's Choice Award for Song of 2019, Nas X was also nominated for "Male Artist of 2019" at the 45th People's Choice Awards.[87] In October, at the 2019 BET Hip Hop Awards Nas X, with Cyrus, won for Best Collab/Duo or Group, and Single of the Year.[88][89][i] Also in October 2019, Nas X's label Columbia Records/Sony Music Entertainment, won the Music & Sound Recordings Award from SAG-AFTRA for "Old Town Road (Remix)" featuring Cyrus; the awards are presented "for work that exemplifies equal access" of LGBTQ and "other misrepresented or underrepresented groups."[90] In November 2019, Nas X won the American Music Award for Favorite Rap/Hip Hop Song, he was nominated for four others including three for "Old Town Road" featuring Billy Ray Cyrus.[j][91] In November 2019, Nas X was nominated for six Grammy Awards, including Record of the Year, Album of the Year and Best New Artist, and eventually won Best Music Video and Best Pop Duo/Group Performance.[92] Lil Nas X's success caused him to become the first person of color and the first openly gay performer to be listed by Forbes in its annual Highest-Paid Country Acts List.[93][94]

Ken Burns, who produced the PBS documentary  Country Music, noted,"Well, to me, Lil Nas X is my mic drop moment. We spend eight episodes and sixteen and a half hours talking about the fact that country music has never been one thing. ... And there's a huge African American influence, and it permeates throughout the whole story. ... And here we are in a new modern age that we’re not touching, with all these classic, binary arguments about Billboard not listing ['Old Town Road'] on the country chart, and it turns out to be not just the No. 1 country hit but the No. 1 single, period, and it’s by a black gay rapper! ... It just is proving that all of those cycles that we have been reporting on across the decades—all of the tensions in country music of race, class, poverty, gender, creativity versus commerce, geography—are still going on."[95]
"Panini" was released as Nas X's second single through Columbia Records in June 2019.[96][97] It is named after the fictional cabbit of the same name in the animated television series Chowder,[98] and does not refer to the sandwich of the same name. In mid-September 2019 "Panini" had its first remix released with rapper DaBaby.[99]
In early July 2019, "Old Town Road" achieved its 13th week at the top spot on the Billboard 100, becoming the first hip hop song to do so.[100][k] It is also the first song to sell ten million copies while in the top spot.[61][100] On its fifteenth week at the top, Nas X became the first openly gay artist to have a song last as long, beating out Elton John’s 1997 double A-side—where both sides of the record are promoted as hits, "Candle in the Wind 1997"/"Something About the Way You Look Tonight".[5] At nineteen weeks at number one, Nas X holds the record for the most weeks since the chart was first introduced in 1958.[4][l] As of August 2019[update], the song has also charted nineteen weeks atop the Hot R&B/Hip-Hop Songs chart;[4] beating a three-way tie record.[102][m] At nineteen weeks at the top of the Hot Rap Songs chart the song has also beaten a three-way tie.[4][n] By November 2019, the song was Diamond Certified, moving a combined sales and streaming ten million units.[106]

Public image
      Nas X wore a custom sequined "diamond fantasy moment" Christian Cowan suit in homage to Prince  for the 2019 MTV Video Music Awards' red carpet.[107]
Nas X has been noted for his public fashions; in July 2019, Vogue noted Nas X as a "master" at giving the cowboy aesthetic a glam look in his appearances and on Instagram.[108] His stylist, Hodo Musa, says he aims for items that are "electric, playful, colorful, and futuristic."[107] For his on-stage look at the 2019 MTV Video Music Awards he wore a cowboy motif cherry-red Nudie suit.[109] Wrangler, which is mentioned in the "Old Town Road" lyrics, has consistently sold out of Nas X co-branded fashions.[110]
For the 62nd Annual Grammy Awards Nas X wore several outfits including a head-to-toe couture fuschia Versace suit with a pink harness which took 700 hours to construct.[111]

Personal life
In early June 2019, Nas X came out to his sister and father, and he felt the universe was signalling him to do so despite his uncertainty whether his fans would stick by him or not.[12] On June 30, 2019, the last day of Pride Month, Nas X came out publicly as gay,[112][113] tweeting: "some of y'all already know, some of y'all don't care, some of y'all not gone fwm no more. but before this month ends i want y'all to listen closely to c7osure. 🌈🤩✨"[114] The tweet confirmed earlier suspicions when he first indicated this in his track "c7osure". Rolling Stone noted the song "touches on themes such as coming clean, growing up and embracing one's self".[115] The next day he tweeted again, this time highlighting the rainbow-colored building on the cover art of his EP 7, with the caption reading "deadass thought i made it obvious".[116][117] He was unambiguous in an interview several days later on BBC Breakfast, where he stated that he was gay and understands that his sexuality is not readily accepted in the country or rap music communities.[118] The response to the news was mostly positive, but also garnered a large amount of homophobic backlash on social media, to which Nas X also reacted.[117][119] The backlash also came from the hip hop community, drawing attention to homophobia in hip hop culture.[119][120] In January 2020, rapper Pastor Troy made some homophobic comments on the outfit Nas X wore during the Grammy Awards, to which Nas X nonchalantly responded: "Damn I look good in that pic on god.".[121][122][123][124][125]

Discography
Extended plays

List of extended plays, with selected details and chart positions


Title

Details

Peak chart positions

Sales

Certification



US [126]

AUS [127]

AUT [128]

CAN [129]

DEN [130]

FRA [131]

IRE [132]

NZ [133]

SWE [134]

UK [135]



October 31st


 Released: October 16, 2018

Label: Self-released

Formats: Digital download, streaming


—
—
—
—
—
—
—
—
—
—







7


 Released: June 21, 2019

Label: Columbia

Formats: CD, Digital download, streaming


2
5
53
1
9
15
11
5
10
23


 US: 4,000[126]

CAN: 1,000[136]



 RIAA: Platinum[137]

IFPI DEN: Gold[138]

IFPI NOR: Platinum[139]

MC: Platinum[140]



Mixtapes

List of mixtapes, with selected details


Title

Details



Nasarati


 Released: July 24, 2018[141]

Label: Self-released

Formats: Digital download, streaming



Singles


Title

Year

Peak chart positions

Sales

Certifications

Album



US [142] [143]

US R&B /HH [144]

AUS [145]

CAN [146]

DEN [147]

IRE [148]

NOR [149]

NZ [133]

SWE [150]

UK [135]



"Donald Trump"[151]

2018

—

—

—

—

—

—

—

—

—

—





Nasarati



"Thanos"[152]

—

—

—

—

—

—

—

—

—

—







"Rookie"[153]

—

—

—

—

—

—

—

—

—

—





October 31st



"Same Shit"[154]

—

—

—

—

—

—

—

—

—

—







"Rio De Janeiro"[155]

—

—

—

—

—

—

—

—

—

—







"Grab That!"[156]

—

—

—

—

—

—

—

—

—

—







"Old Town Road" (solo or remix featuring Billy Ray Cyrus)

1
1
1
1
1
1
1
1
2
1


 US: 1,613,000[157]

CAN: 210,000[136]



 RIAA: 12× Platinum[137]

ARIA: 11× Platinum[145]

BPI: 3× Platinum[158]

GLF: 2× Platinum[159]

IFPI DEN: Platinum[160]

MC: Diamond[140]

RMNZ: 4× Platinum[161]


7



"Panini"

2019

5
2
15
8
28
18
26
14
42
21




 RIAA: 4× Platinum[137]

ARIA: 2× Platinum[162]

BPI: Gold[158]

IFPI DEN: Gold[163]

MC: 4× Platinum[140]

RMNZ: Gold[164]




"Rodeo" (with Cardi B or Nas)

2020

22
12
72
44
—
35
—
—[A]
—
55




 RIAA: Platinum[137]

MC: Gold[140]




"—" denotes a recording that did not chart or was not released in that territory.


Other charted songs


Title

Year

Peak chart positions

Album



US Bub. [166]

US RS [167]

US Rock [168]

NZ Hot [169]



"F9mily (You & Me)" (with Travis Barker)

2019

—
97
6
34

7



"Kick It"

—
—
—
33



"Bring U Down" (featuring Ryan Tedder)

—
—
7
—



"C7osure (You Like)"

14
—
—
27



"—" denotes a recording that did not chart


Awards and nominations
 Main article: List of awards and nominations received by Lil Nas X
Notes


References

External links
 Official website 

Lil Nas X on IMDb 




Wikimedia Commons has media related to Lil Nas X.


   v
t
e
 Lil Nas X
 
 Discography

Awards and nominations


Extended plays 
 7


Singles 
 "Old Town Road"

"Panini"

"Rodeo"


Other songs 
 "F9mily (You & Me)"

"C7osure (You Like)"



   v
t
e
 Grammy Award for Best Music Video
1984−2000 
 "Girls on Film"/"Hungry Like the Wolf" – Duran Duran (1984)

"Jazzin' for Blue Jean" – David Bowie (1985)

"We Are the World – The Video Event" – USA for Africa (1986)

"Brothers in Arms" – Dire Straits (1987)

No Award (1988)

No Award (1989)

"Leave Me Alone" – Michael Jackson (1990)

"Opposites Attract" – Paula Abdul (1991)

"Losing My Religion" – R.E.M. (1992)

"Digging in the Dirt" – Peter Gabriel (1993)

"Steam" – Peter Gabriel (1994)

"Love Is Strong" – The Rolling Stones (1995)

"Scream" – Michael Jackson & Janet Jackson (1996)

"Free as a Bird" – The Beatles (1997)

"Got 'til It's Gone" – Janet Jackson (1998)

"Ray of Light" – Madonna (1999)

"Freak on a Leash" – Korn (2000)


2001−present 
 "Learn to Fly" – Foo Fighters (2001)

"Weapon of Choice" – Fatboy Slim featuring Bootsy Collins (2002)

"Without Me" – Eminem (2003)

"Hurt" – Johnny Cash (2004)

"Vertigo" – U2 (2005)

"Lose Control" – Missy Elliott featuring Ciara & Fatman Scoop (2006)

"Here It Goes Again" – OK Go (2007)

"God's Gonna Cut You Down" – Johnny Cash (2008)

"Pork and Beans" – Weezer (2009)

"Boom Boom Pow" – Black Eyed Peas (2010)

"Bad Romance" – Lady Gaga (2011)

"Rolling in the Deep" – Adele (2012)

"We Found Love" – Rihanna featuring Calvin Harris (2013)

"Suit & Tie" – Justin Timberlake featuring Jay Z (2014)

"Happy" – Pharrell Williams (2015)

"Bad Blood" – Taylor Swift featuring Kendrick Lamar (2016)

"Formation" – Beyoncé (2017)

"Humble" – Kendrick Lamar (2018)

"This Is America" – Childish Gambino (2019)

"Old Town Road" – Lil Nas X featuring Billy Ray Cyrus (2020)



 Authority control  
 GND: 1184637822

LCCN: no2019122757

MusicBrainz: 0b30341b-b59d-4979-8130-b66c0e475321

VIAF: 11155646204818212792

 WorldCat Identities: lccn-no2019122757







