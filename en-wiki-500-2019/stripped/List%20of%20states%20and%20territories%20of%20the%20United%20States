  The US has 50 states and various other territories


 "States of America" redirects here. For the sovereign states of the Americas, see List of sovereign states and dependent territories in the Americas. For the nation sometimes referred to as the United States of America, see United States.
 For a broader coverage related to this topic, see U.S. state and Territories of the United States.



      A map of the United States showing its 50 states, District of Columbia, and 5 major U.S. territories
The United States of America is a federal republic[1] consisting of 50 states, a federal district (Washington, D.C., the capital city of the United States), five major territories, and various minor islands.[2][3] The 48 contiguous states and Washington, D.C., are in North America between Canada and Mexico, while Alaska is in the far northwestern part of North America and Hawaii is an archipelago in the mid-Pacific. Territories of the United States are scattered throughout the Pacific Ocean and the Caribbean Sea.
States possess a number of powers and rights under the United States Constitution, such as regulating intrastate commerce, running elections, creating local governments, and ratifying constitutional amendments. Each state has its own constitution, grounded in republican principles, and government, consisting of three branches: executive, legislative, and judicial.[4] All states and their residents are represented in the federal Congress, a bicameral legislature consisting of the Senate and the House of Representatives. Each state is represented by two senators, while representatives are distributed among the states in proportion to the most recent constitutionally mandated decennial census.[5] Additionally, each state is entitled to select a number of electors to vote in the Electoral College, the body that elects the president of the United States, equal to the total of representatives and senators in Congress from that state.[6] Article IV, Section 3, Clause 1 of the Constitution grants to Congress the authority to admit new states into the Union. Since the establishment of the United States in 1776, the number of states has expanded from the original 13 to the current total of 50, and each new state is admitted on an equal footing with the existing states.[7]
As provided by Article I, Section 8 of the Constitution, Congress exercises "exclusive jurisdiction" over the federal district, which is not part of any state. Prior to passage of the 1973 District of Columbia Home Rule Act, which devolved certain Congressional powers to an elected mayor and council, the district did not have an elected local government. Even so, Congress retains the right to review and overturn laws created by the council and intervene in local affairs.[8] As it is not a state, the district does not have representation in the Senate. However, since 1971, its residents have been represented in the House of Representatives by a non-voting delegate.[9] Additionally, since 1961, following ratification of the 23rd Amendment, the district has been entitled to select three electors to vote in the Electoral College.
In addition to the 50 states and federal district, the United States has sovereignty over 14 territories. Five of them (American Samoa, Guam, the Northern Mariana Islands, Puerto Rico, and the U.S. Virgin Islands) have a permanent, nonmilitary population, while nine of them do not. With the exception of Navassa Island, Puerto Rico, and the U.S. Virgin Islands, which are located in the Caribbean, all territories are located in the Pacific Ocean. One territory, Palmyra Atoll, is considered to be incorporated, meaning the full body of the Constitution has been applied to it; the other territories are unincorporated, meaning the Constitution does not fully apply to them. Ten territories (the Minor Outlying Islands and American Samoa) are considered to be unorganized, meaning they have not had an Organic Act enacted by Congress; the four other territories are organized, meaning they have had an Organic Act that has been enacted by Congress. The five inhabited territories each have limited autonomy and a non-voting delegate in Congress, in addition to having territorial legislatures and governors, but residents cannot vote in federal elections.
California is the most populous state, with 38,332,521 residents (2013 estimate); Wyoming is the least populous, with an estimated 582,658 residents. The District of Columbia, with an estimated 646,449 residents as of 2012, has a higher population than the two least populous states (Wyoming and Vermont). The largest state by area is Alaska, encompassing 665,384 square miles (1,723,340 km2), while the smallest is Rhode Island, encompassing 1,545 square miles (4,000 km2). The first state to ratify the current Constitution was Delaware, which it did on December 7, 1787, while the newest state is Hawaii, which was admitted to the Union on August 21, 1959. The largest territory in terms of both population and size is Puerto Rico, with 3,725,789 residents as of the 2010 Census and a total area of 5,325 square miles (13,790 km2).

  Contents
 
1 States

2 Federal district

3 Territories
 
3.1 Inhabited territories

3.2 Uninhabited territories

3.3 Disputed territories




4 See also

5 Notes

6 References

7 External links




States
The table below lists the 50 states, with their current capital, largest city,[A] the date they ratified the U.S. Constitution or were admitted to the Union, population and area data, and number of representative(s) in the U.S. House of Representatives.[B]


States of the United States of America


Flag, name & postal abbreviation[12]

Cities

Ratification or admission[C]

Population [D][14]

Total area[15]

Land area[15]

Water area[15]

Number of Reps.



Capital

Largest[16]

mi2

km2

mi2

km2

mi2

km2



 Alabama

AL

Montgomery

Birmingham

Dec 14, 1819

4,903,185

52,420

135,767

50,645

131,171

1,775

4,597

7



 Alaska

AK

Juneau

Anchorage

Jan 3, 1959

731,545

665,384

1,723,337

570,641

1,477,953

94,743

245,384

1



 Arizona

AZ

Phoenix

Feb 14, 1912

7,278,717

113,990

295,234

113,594

294,207

396

1,026

9



 Arkansas

AR

Little Rock

Jun 15, 1836

3,017,804

53,179

137,732

52,035

134,771

1,143

2,961

4



 California

CA

Sacramento

Los Angeles

Sep 9, 1850

39,512,223

163,695

423,967

155,779

403,466

7,916

20,501

53



 Colorado

CO

Denver

Aug 1, 1876

5,758,736

104,094

269,601

103,642

268,431

452

1,170

7



 Connecticut

CT

Hartford

Bridgeport

Jan 9, 1788

3,565,278

5,543

14,357

4,842

12,542

701

1,816

5



 Delaware

DE

Dover

Wilmington

Dec 7, 1787

973,764

2,489

6,446

1,949

5,047

540

1,399

1



 Florida

FL

Tallahassee

Jacksonville

Mar 3, 1845

21,477,737

65,758

170,312

53,625

138,887

12,133

31,424

27



 Georgia

GA

Atlanta

Jan 2, 1788

10,617,423

59,425

153,910

57,513

148,959

1,912

4,951

14



 Hawaii

HI

Honolulu

Aug 21, 1959

1,415,872

10,932

28,313

6,423

16,635

4,509

11,678

2



 Idaho

ID

Boise

Jul 3, 1890

1,787,065

83,569

216,443

82,643

214,045

926

2,398

2



 Illinois

IL

Springfield

Chicago

Dec 3, 1818

12,671,821

57,914

149,995

55,519

143,793

2,395

6,202

18



 Indiana

IN

Indianapolis

Dec 11, 1816

6,732,219

36,420

94,326

35,826

92,789

593

1,537

9



 Iowa

IA

Des Moines

Dec 28, 1846

3,155,070

56,273

145,746

55,857

144,669

416

1,077

4



 Kansas

KS

Topeka

Wichita

Jan 29, 1861

2,913,314

82,278

213,100

81,759

211,754

520

1,346

4



 Kentucky[E]

KY

Frankfort

Louisville

Jun 1, 1792

4,467,673

40,408

104,656

39,486

102,269

921

2,387

6



 Louisiana

LA

Baton Rouge

New Orleans

Apr 30, 1812

4,648,794

52,378

135,659

43,204

111,898

9,174

23,761

6



 Maine

ME

Augusta

Portland

Mar 15, 1820

1,344,212

35,380

91,633

30,843

79,883

4,537

11,750

2



 Maryland

MD

Annapolis

Baltimore

Apr 28, 1788

6,045,680

12,406

32,131

9,707

25,142

2,699

6,990

8



 Massachusetts[E]

MA

Boston

Feb 6, 1788

6,892,503

10,554

27,336

7,800

20,202

2,754

7,134

9



 Michigan

MI

Lansing

Detroit

Jan 26, 1837

9,986,857

96,714

250,487

56,539

146,435

40,175

104,052

14



 Minnesota

MN

St. Paul

Minneapolis

May 11, 1858

5,639,632

86,936

225,163

79,627

206,232

7,309

18,930

8



 Mississippi

MS

Jackson

Dec 10, 1817

2,976,149

48,432

125,438

46,923

121,531

1,508

3,907

4



 Missouri

MO

Jefferson City

Kansas City

Aug 10, 1821

6,137,428

69,707

180,540

68,742

178,040

965

2,501

8



 Montana

MT

Helena

Billings

Nov 8, 1889

1,068,778

147,040

380,831

145,546

376,962

1,494

3,869

1



 Nebraska

NE

Lincoln

Omaha

Mar 1, 1867

1,934,408

77,348

200,330

76,824

198,974

524

1,356

3



 Nevada

NV

Carson City

Las Vegas

Oct 31, 1864

3,080,156

110,572

286,380

109,781

284,332

791

2,048

4



 New Hampshire

NH

Concord

Manchester

Jun 21, 1788

1,359,711

9,349

24,214

8,953

23,187

397

1,027

2



 New Jersey

NJ

Trenton

Newark

Dec 18, 1787

8,882,190

8,723

22,591

7,354

19,047

1,368

3,544

12



 New Mexico

NM

Santa Fe

Albuquerque

Jan 6, 1912

2,096,829

121,590

314,917

121,298

314,161

292

757

3



 New York

NY

Albany

New York

Jul 26, 1788

19,453,561

54,555

141,297

47,126

122,057

7,429

19,240

27



 North Carolina

NC

Raleigh

Charlotte

Nov 21, 1789

10,488,084

53,819

139,391

48,618

125,920

5,201

13,471

13



 North Dakota

ND

Bismarck

Fargo

Nov 2, 1889

762,062

70,698

183,108

69,001

178,711

1,698

4,397

1



 Ohio

OH

Columbus

Mar 1, 1803

11,689,100

44,826

116,098

40,861

105,829

3,965

10,269

16



 Oklahoma

OK

Oklahoma City

Nov 16, 1907

3,956,971

69,899

181,037

68,595

177,660

1,304

3,377

5



 Oregon

OR

Salem

Portland

Feb 14, 1859

4,217,737

98,379

254,799

95,988

248,608

2,391

6,191

5



 Pennsylvania[E]

PA

Harrisburg

Philadelphia

Dec 12, 1787

12,801,989

46,054

119,280

44,743

115,883

1,312

3,397

18



 Rhode Island[F]

RI

Providence

May 29, 1790

1,059,361

1,545

4,001

1,034

2,678

511

1,324

2



 South Carolina

SC

Columbia

Charleston

May 23, 1788

5,148,714

32,020

82,933

30,061

77,857

1,960

5,076

7



 South Dakota

SD

Pierre

Sioux Falls

Nov 2, 1889

884,659

77,116

199,729

75,811

196,350

1,305

3,379

1



 Tennessee

TN

Nashville

Jun 1, 1796

6,829,174

42,144

109,153

41,235

106,798

909

2,355

9



 Texas

TX

Austin

Houston

Dec 29, 1845

28,995,881

268,596

695,662

261,232

676,587

7,365

19,075

36



 Utah

UT

Salt Lake City

Jan 4, 1896

3,205,958

84,897

219,882

82,170

212,818

2,727

7,064

4



 Vermont

VT

Montpelier

Burlington

Mar 4, 1791

623,989

9,616

24,906

9,217

23,871

400

1,035

1



 Virginia[E]

VA

Richmond

Virginia Beach

Jun 25, 1788

8,535,519

42,775

110,787

39,490

102,279

3,285

8,508

11



 Washington

WA

Olympia

Seattle

Nov 11, 1889

7,614,893

71,298

184,661

66,456

172,119

4,842

12,542

10



 West Virginia

WV

Charleston

Jun 20, 1863

1,792,147

24,230

62,756

24,038

62,259

192

497

3



 Wisconsin

WI

Madison

Milwaukee

May 29, 1848

5,822,434

65,496

169,635

54,158

140,268

11,339

29,367

8



 Wyoming

WY

Cheyenne

Jul 10, 1890

578,759

97,813

253,335

97,093

251,470

720

1,864

1


Federal district

Federal district of the United States


Name & postal abbreviation[12]

Established

Population [G][14]

Total area[15]

Land area[15]

Water area[15]

Number of Reps.



mi2

km2

mi2

km2

mi2

km2



 District of Columbia

DC

Jul 16, 1790[17]

705,749

68

176

61

158

7

18

1[H]



Territories
 Further information: Insular area
       A map showing the location of each territory controlled by the United States. The United States is marked in blue, inhabited territories are marked in green, and uninhabited territories are marked in orange.
Inhabited territories

Inhabited territories of the United States


Name & postal abbreviation[12]

Capital

Acquired [19]

Territorial status[20]

Population [I]

Total area[15]

Land area[15]

Water area[15]

Number of Reps.



mi2

km2

mi2

km2

mi2

km2



 American Samoa

AS

Pago Pago[21]

1900

 Unincorporated, unorganized[J]

57,400[22]

581

1,505

76

198

505

1,307

1[H]



 Guam

GU

Hagåtña[23]

1899

 Unincorporated, organized

161,700[24]

571

1,478

210

543

361

935

1[H]



 Northern Mariana Islands

MP

Saipan[25]

1986

 Unincorporated, organized[K]

52,300[24]

1,976

5,117

182

472

1,793

4,644

1[H]



 Puerto Rico

PR

San Juan[26]

1899

 Unincorporated, organized[K]

3,193,694[14]

5,325

13,791

3,424

8,868

1,901

4,924

1[L]



 U.S. Virgin Islands

VI

Charlotte Amalie[27]

1917

 Unincorporated, organized

103,700[28]

733

1,898

134

348

599

1,550

1[H]


Uninhabited territories

Territories of the United States with no indigenous population


Name

Acquired[19]

Territorial status[20]

Land area[M]



mi2

km2



Baker Island[29]

1856

 Unincorporated; unorganized

0.9

2.2



Howland Island[29]

1858

 Unincorporated, unorganized

0.6

1.6



Jarvis Island[30]

1856

 Unincorporated, unorganized

2.2

5.7



Johnston Atoll[31]

1859

 Unincorporated, unorganized

1

2.6



Kingman Reef[32]

1860

 Unincorporated, unorganized

0.005

0.01



Midway Atoll[N][34]

1867

 Unincorporated, unorganized

3

7.8



Navassa Island[35]

1858[O]

 Unincorporated, unorganized

3

7.8



Palmyra Atoll[P][37]

1898

 Incorporated, unorganized

1.5

3.9



Wake Island[Q][38]

1899[R]

 Unincorporated, unorganized

2.5

6.5



Disputed territories
 Main article: List of territorial disputes § Central America and the Caribbean

Territories claimed but not administered by the United States


Name

Claimed [19]

Territorial status[40]

Area

Administered by[40]

Also claimed by[40]



mi2

km2



Bajo Nuevo Bank (Petrel Island)[19]

1869

 Unincorporated, unorganized (disputed sovereignty)

56

145[S][41]

 Colombia

 Jamaica  Nicaragua



Serranilla Bank[19]

1880

 Unincorporated, unorganized (disputed sovereignty)

463

1,200[T][42]

 Colombia

 Honduras  Nicaragua


See also
 
 
Geography portal

North America portal

United States portal

 List of regions of the United States

Lists of U.S. state topics

Political divisions of the United States

List of Indian reservations in the United States

 
Notes

References

External links



Wikimedia Commons has media related to Subdivisions of the United States.


 State Resource Guides, from the Library of Congress

State and Territorial Governments on USA.gov

   v
t
e
  Political divisions of the United States
States 
 Alabama

Alaska

Arizona

Arkansas

California

Colorado

Connecticut

Delaware

Florida

Georgia

Hawaii

Idaho

Illinois

Indiana

Iowa

Kansas

Kentucky

Louisiana

Maine

Maryland

Massachusetts

Michigan

Minnesota

Mississippi

Missouri

Montana

Nebraska

Nevada

New Hampshire

New Jersey

New Mexico

New York

North Carolina

North Dakota

Ohio

Oklahoma

Oregon

Pennsylvania

Rhode Island

South Carolina

South Dakota

Tennessee

Texas

Utah

Vermont

Virginia

Washington

West Virginia

Wisconsin

Wyoming


Federal district Washington, D.C.
Insular areas 
 American Samoa

Guam

Northern Mariana Islands

Puerto Rico 

U.S. Virgin Islands


Outlying islands 
 Baker Island

Howland Island

Jarvis Island

Johnston Atoll

Kingman Reef

Midway Atoll

Navassa Island

Palmyra Atoll

Wake Island


Indian reservations 
 List of Indian reservations


Other 
 Federal enclave



   v
t
e
 United States state-related lists
 List of states and territories of the United States
Demographics 
 Population
 African American

Amish

Asian

Birth and death rates

Density

Hispanic and Latino

Historical

Household income

LGBT

Non-Hispanic white

Pacific Islander

Spanish-speaking


Educational attainment

Largest cities by population

Median age

Most popular given names

Most populous counties

Net migration

Populated places

Population density

Religiosity
 Irreligion



Economy 
 Billionaires

Budgets

Companies

Credit ratings

Employment rates

Exports and imports

Federal tax revenue

Federal taxation and spending

Gross domestic product
 Growth rate

Per capita


Income
 Inequality


Median wages

Minimum wages

Poverty rates

R&D spending

Sales taxes

Sovereign wealth funds

State income taxes

Unemployment rates

Union affiliation

Vehicles per capita


Environment 
 Botanical gardens

Carbon dioxide emissions

Parks

National Natural Landmarks

National Wildlife Refuges

Nature centers

Renewable energy

Superfund sites

Wilderness areas


Geography 
 Area

Bays

Beaches

Coastline

Elevation

Extreme points

Forest

Geographic centers

Highest cities

Islands

Lakes

Landlocked states

Mountains

Regions

Volcanoes


Government 
 Agriculture commissioners

Attorneys general

Capitals

Capitol buildings

Comparison

Congressional districts
 Members


Counties
 Alphabetical

List


Courts

Governors
 Lieutenant governors


Legislatures

Libraries and archives

Official languages

Poets laureate

Political divisions

Politics by state or territory
 Political party strength


State auditors

State legislators
 Alabama–Missouri

Montana–Wyoming

State senators


State secretaries of state

State Speakers

State Chief Justices

State President Pro Tempore

State superintendents of education

State supreme courts

State treasurers

Statewide elected executive officials


Health 
 Fertility rates

Hospitals

Human Development Index
 American Human Development Index


Life expectancy

Infant mortality rates

Obesity rates


History 
 Date of statehood

Name etymologies

Historical societies

Museums

National Historic Landmarks

National Register of Historic Places

State partitions

Historic regions


Law 
 Abortion

Age of consent

Alcohol
 Dry communities


Alford plea

Cell phone use while driving

Constitutions

Firearms
 Homicide


Law enforcement agencies

Legality of cannabis

Peace Index

Prisons
 Incarceration rate


Same-sex unions
 Former constitutional bans

Marriage law


Seat belt laws

Self-representation

Smoking bans

Speed limits (by jurisdiction)

Statutory codes


Miscellaneous 
 Abbreviations

Airports

Bus transit systems

Casinos

Demonyms

Fictional states

Flags

Hotels

Insignia
 Coats of arms


License plates

Malls

Mottos

Newspapers

Nicknames

Numbered highways

Quarters
 50 states

District of Columbia and territories

America the Beautiful quarters


Snowiest places

Symbols

Tallest buildings

Temperature extremes

Time zones


 
  Category

 Commons

 Portals



   v
t
e
 United States articles
History By event 
 Pre-Columbian era

Colonial era
 Thirteen Colonies

military history

Founding Fathers

Continental Congress

Continental Association


Lee Resolution

Declaration of Independence

American Revolution
 War

Treaty of Paris


Articles of Confederation
 Confederation Period


American frontier

Drafting and ratification of Constitution
 Bill of Rights


Federalist Era

War of 1812

Territorial acquisitions

Territorial evolution

Mexican–American War

Civil War

Reconstruction era

Indian Wars

Gilded Age

Progressive Era

Women's suffrage

Civil rights movement 1865–1896 / 1896–1954 / 1954–1968

Spanish–American War

Imperialism

World War I

Roaring Twenties

Great Depression

World War II
 home front


American Century

Cold War

Korean War

Space Race

Feminist Movement

Vietnam War

Post-Cold War (1991–2008)

War on Terror
 War in Afghanistan

Iraq War


Recent events (2008–present)


By topic 
 Outline of U.S. history

Demographic

Discoveries

Economic
 debt ceiling


Inventions
 before 1890

1890–1945

1946–1991

after 1991


Military

Postal

Technological and industrial


 
Geography  
 Territory
 Contiguous United States

Continental America

counties

federal district

federal enclaves

Indian reservations

insular zones

minor outlying islands

populated places

states


Earthquakes

Extreme points

Islands

Mountains
 peaks

ranges

Appalachian

Rocky


National Park Service
 National Parks


Regions
 East Coast

West Coast

Great Plains

Gulf

Mid-Atlantic

Midwestern

New England

Pacific

Central

Eastern

Northern

Northeastern

Northwestern

Southern

Southeastern

Southwestern

Western


Longest rivers
 Arkansas

Colorado

Columbia

Mississippi

Missouri

Red (South)

Rio Grande

Yukon


Time

Water supply and sanitation

World Heritage Sites


 
Politics  Federal Executive 
 Cabinet

Civil service

Executive departments

Executive Office

Independent agencies

Law enforcement

President of the United States
 Powers


Public policy


Legislative 
 House of Representatives
 current members

Speaker


Senate
 current members

President pro tempore

Vice President



Judicial 
 Courts of appeals

District courts

Supreme Court


Law 
 Bill of Rights
 civil liberties


Code of Federal Regulations

Constitution
 federalism

preemption

separation of powers

civil rights


Federal Reporter

United States Code

United States Reports


Intelligence 
 Central Intelligence Agency

Defense Intelligence Agency

Federal Bureau of Investigation

National Geospatial-Intelligence Agency

National Reconnaissance Office

National Security Agency

Office of the Director of National Intelligence


Uniformed 
 Armed Forces
 Army

Marine Corps

Navy

Air Force

Space Force

Coast Guard

National Guard


NOAA Corps

Public Health Service Corps


 
 
 51st state
 political status of Puerto Rico

District of Columbia statehood movement


Elections
 Electoral College


Foreign relations
 Foreign policy


Hawaiian sovereignty movement

Ideologies
 anti-Americanism

exceptionalism

nationalism


Local government

Parties
 Democratic

Republican

Third parties


Red states and blue states
 Purple America


Scandals

State government
 governor

state legislature

state court


Imperial Presidency

Corruption


 
 
Economy  
 By sector
 Agriculture

Banking

Communications

Energy

Insurance

Manufacturing

Mining

Science and technology

Tourism

Trade

Transportation

by state


Currency

Exports

Federal budget

Federal Reserve System

Financial position

Labor unions

Public debt

Social welfare programs

Taxation

Unemployment

Wall Street


 
Society  Culture 
 Americana

Architecture

Cinema

Crime

Cuisine

Dance

Demography

Economic issues
 affluence

eviction

home-ownership

household income

income inequality

labor unions

middle class

personal income

poverty

standard of living

wealth


Education
 attainment


Family structure

Fashion

Flag

Folklore

Great American Novel

Health
 health care

health insurance


Holidays

Homelessness

Human rights

Languages
 American English

Indigenous languages

ASL
 Black American Sign Language


HSL

Plains Sign Talk

Arabic

Chinese

French

German

Italian

Russian

Spanish


Literature

Media
 Journalism

Internet

Newspapers

Radio

Television


Music

Names

National symbols
 Columbia

Statue of Liberty

Uncle Sam


People

Philosophy

Political ideologies

Public holidays

Race

Religion

Sexuality / Adolescent Sexuality

Social class

Society

Sports

Theater

Transportation

Video games

Visual art


Social class 
 Affluence

American Dream

Educational attainment

Homelessness

Home-ownership

Household income

Income inequality

Middle class

Personal income

Poverty

Professional and working class conflict

Standard of living


Issues 
 Ages of consent

Capital punishment

Crime
 incarceration


Criticism of government

Discrimination
 affirmative action

antisemitism

hair texture

intersex rights

Islamophobia

LGBT rights

racism

same-sex marriage


Drug policy

Energy policy

Environmental movement

Gun politics

Health care
 abortion

health insurance

hunger

obesity

smoking


Human rights

Immigration
 illegal


International rankings

National security
 Mass surveillance

Terrorism


Separation of church and state


 
 
   Outline
Index

 Category

Portal


   v
t
e
 Articles on first-level administrative divisions of North American countries
 
 Antigua and Barbuda

Bahamas

Barbados

Belize

Canada

Costa Rica

Cuba

Dominica

Dominican Republic

El Salvador

Grenada

Guatemala

Haiti

Honduras

Jamaica

Mexico

Nicaragua

Panama

St. Kitts and Nevis

St. Lucia

St. Vincent and the Grenadines

Trinidad and Tobago

United States


 Table of administrative subdivisions by country





