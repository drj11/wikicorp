 

 American singer, rapper, and songwriter from Minnesota



 Lizzo
 Lizzo performing in London; November 2019
Background information
Birth nameMelissa Viviane Jefferson
Born (1988-04-27) April 27, 1988 (age 32)[1] Detroit, Michigan, U.S.
OriginMinneapolis, Minnesota, U.S.
Genres 
 Hip hop

funk-pop

R&B

soul

doo-wop


Occupation(s) 
 Singer

rapper

songwriter

actress


Instruments 
 Vocals

flute


Years active2010–present
Labels 
 TGNP

Atlantic

Nice Life

BGSW


Associated acts 
 The Chalice

Ricky Reed


Websitelizzomusic.com





Melissa Viviane Jefferson (born April 27, 1988), known professionally as Lizzo, is an American singer, rapper, songwriter, and flutist.[2] Born in Detroit, Michigan, she moved to Houston, Texas, where she began performing, before moving to Minneapolis, where she began her recording career. Before signing with Nice Life and Atlantic Records, Lizzo released two studio albums—Lizzobangers (2013), and Big Grrrl Small World (2015). Lizzo's first major-label EP, Coconut Oil, was released in 2016.
She attained mainstream success with the release of her third studio album, Cuz I Love You (2019), which peaked inside the top five of the US Billboard 200. The album spawned two singles: "Juice" and "Tempo". The deluxe version of the album included Lizzo's 2017 single "Truth Hurts", which became a viral sleeper hit, topping the US Billboard Hot 100 two years after its initial release. Around this time, her 2016 single "Good as Hell" also climbed the charts, reaching the top ten of the UK Singles Chart and the Hot 100. Lizzo received eight nominations at the 62nd Annual Grammy Awards, the most for any artist that year, including Album of the Year for the deluxe version of Cuz I Love You, Song of the Year and Record of the Year for "Truth Hurts", as well as Best New Artist. She eventually won the awards for Best Urban Contemporary Album, Best Pop Solo Performance for "Truth Hurts", and Best Traditional R&B Performance for the song "Jerome".
Aside from singing and rapping, Lizzo is also an actress; she served as a voice performer in the animated film UglyDolls (2019) and appeared in the crime comedy-drama film Hustlers (2019). In 2019, Time named Lizzo as "Entertainer of the Year" for her meteoric rise and contribution to music.[3]

  Contents
 
1 Early life

2 Career
 
2.1 2011–2015: Early work

2.2 2016–present: Commercial breakthrough




3 Personal life

4 Artistry

5 Discography

6 Filmography

7 Concert tours

8 Awards and nominations

9 References

10 External links




Early life
Lizzo was born Melissa Viviane Jefferson in Detroit, Michigan.[4] At the age of ten, her family relocated to Houston, Texas.[5] Lizzo started rapping as a teenager in a southwest part of Houston known as Alief.[6]  At the age of 14, she formed a musical group called Cornrow Clique with her friends.[6] At this time she acquired the nickname "Lizzo", a variant of "Lissa" inspired by Jay-Z's "Izzo (H.O.V.A.)".[7] After graduating from Alief Elsik High School, she studied classical music focusing on flute at the University of Houston.[8] At 21, after the death of her father, she lived out of her car for a year as she tried to break into the music industry.[9] She moved to Minneapolis, Minnesota, in 2011.[10]

Career
2011–2015: Early work
While living in Minneapolis, Lizzo performed with indie groups including the electro soul-pop duo, Lizzo & the Larva Ink.[11] During this time she helped form a three-piece all-female rap/R&B group: the Chalice. In 2012, the Chalice released their first album, We Are the Chalice, which was locally successful.[12][13]
Lizzo's hip-hop-focused debut album, Lizzobangers, produced by Lazerbeak and Ryan Olson, was released on October 15, 2013.[14][15][16] Killian Fox of The Guardian gave the album 4 stars out of 5, saying: "At times joyfully nonsensical, Lizzo's stream-of-consciousness rhymes can also be lethally pointed."[17] The album topped Star Tribune's "Twin Cities Critics Tally 2013" list.[18] Music videos were created for the songs "Batches & Cookies",[19] "Faded",[20] "Bus Passes and Happy Meals",[21] and "Paris".[22]
Lizzo toured the US and UK in the fall of 2013 opening for Har Mar Superstar, and additionally sang with his band.[23][24] In October 2013, Lizzo won City Pages' "Picked to Click" award for best new Twin Cities artist.[25] The following month Time named her one of 14 music artists to watch in 2014.[26] The album was subsequently re-released through Virgin Records.[27] Later that year, Lizzo shared the stage with St. Paul and the Broken Bones, performing "A Change Is Gonna Come" together.[28]

      Lizzo performing in 2014
Following the release of her first album, Lizzo immediately began working on new music.[17] In 2014, she participated in StyleLikeU's What's Underneath project, where she removed her clothes as she talked about her relationships with her body.[29] Inspired by the experience, she wrote "My Skin",[29] which she described as "the thesis statement" of her forthcoming second album.[30] In an interview with Vice, regarding body image, she said: You can wake up and change many things about your appearance, but the inevitability of waking up in your skin is what unifies us.[29]In September 2014, Lizzo was featured alongside her Chalice bandmates Sophia Eris and Claire de Lune on the song "BoyTrouble" on Prince's and 3rdEyeGirl's album Plectrumelectrum.[31] On working with Prince, Lizzo says the experience was "surreal... almost like a fairytale" and that it was "something I will never actually get over."[32] On October 7, 2014, Lizzo appeared as the musical guest on the Late Show with David Letterman.[33]
Lizzo's second studio album, Big Grrrl Small World, was released on December 11, 2015.[34] Spin placed the album at number 17 on the "50 Best Hip-Hop Albums of 2015" list.[35] Hilary Saunders of Paste praised Lizzo's "ability to rap and sing with equal tenacity."[36] Her collaboration with Your Smith (then Caroline Smith), "Let 'Em Say", was featured in the season three premiere of Broad City.[37]

2016–present: Commercial breakthrough
      Lizzo performing at the Palace Theatre in 2018
Lizzo was one of the hosts of MTV's short-lived 2016 live music performance series Wonderland.[6] After signing with Atlantic Records that same year, Lizzo released her first major-label extended play, Coconut Oil, on October 7, 2016.[38] "Good As Hell" was released as the lead single from the Coconut Oil on March 8, 2016 as part of the soundtrack for the 2016 film Barbershop: The Next Cut.[39] Lizzo co-wrote each song on the album, while enlisting Ricky Reed, Christian Rich, Dubbel Dutch, and Jesse Shatkin for the album's production. The result was a departure from Lizzo's previous alternative hip hop releases. Lyrically, the extended play explores themes of body positivity, self-love, and the journey to those ideals.[40]
Coconut Oil received positive reviews from music critics. Syra Aburto, writing for Nylon, wrote that "like the product it's named after, [Lizzo's] latest project, Coconut Oil, is essential for healthy living."[41] Rolling Stone placed it at #14 on its list of the "20 Best Pop Albums of 2016".[42] Commercially, Coconut Oil peaked at number 44 on US Top R&B/Hip-Hop Albums, making it Lizzo's first release to chart. To promote the extended play, Lizzo embarked on the Good as Hell Tour in 2017.[43] In May she headlined The Infatuation's annual food festival, EEEEEATSCON[44] and also appeared as a guest judge on the tenth season of RuPaul's Drag Race.[45] In early 2018, Lizzo toured both with Haim and Florence and the Machine.[6]

After struggling with body issues at an early age, Lizzo became an advocate for body positivity and self-love as she attracted more mainstream attention,[6] while making diversity the focus of her music, in regards to one's body, sexuality, race, and more.[6] Her group of back-up dancers, the Big Grrrls, consists of all plus-size dancers.[46] Highlighting body inclusivity and celebrating individuality, Lizzo appeared in ModCloth's "Say It Louder" campaign, which launched on June 11, 2018.[47][48] In the same month, she sported the first plus-size outfit made for FIT's Future of Fashion runway show by Grace Insogna for her performance at NYC Pride's Pride Island event.[49] Lizzo was profiled in the June 2018 Teen Vogue Music Issue.[50]      Lizzo performing in 2018In 2019, in addition to her musical projects, Lizzo ventured into acting, with a voice performance in the animated film UglyDolls, and a supporting part in the crime comedy-drama film Hustlers.[51][52]
"Juice", the lead single from her third studio album, was released on January 4, 2019, by Atlantic Records.[53] The next month, she announced the title of the album, Cuz I Love You, which was eventually released on April 19, 2019.[54] After the release of her album, she performed at the Coachella Music Festival for the first time.[55]
The release of Cuz I Love You marked a turning point in Lizzo's career, as she began to attract more mainstream attention; the album debuted at number six on the Billboard 200 and eventually peaked at number four on the chart, three months after its initial release.[56]
After inspiring an internet meme on the TikTok video sharing app[57][58] and being featured in the 2019 Netflix film Someone Great,[59] Lizzo's 2017 single, "Truth Hurts", began to gain popularity and was added to the deluxe version of Cuz I Love You. The single became a viral sleeper hit,[60] and, in turn, increased interest for Cuz I Love You, which remained in the top 10 of the Billboard 200 for several months.[61]
"Truth Hurts" has since become Lizzo's first number-one hit on the Billboard Hot 100.[62] It topped the Hot 100 chart for seven weeks.[63] She made history as the first Black solo female R&B singer to claim the top spot on the Hot 100 since Rihanna's 2012 hit "Diamonds." [64] A week later, on September 9, 2019, Cuz I Love You became certified gold by the RIAA with over 500,000 equivalent units sold.[65]
The music video for the song, in which Lizzo "marries herself",[66] has amassed more than 110 million views on YouTube.[67] In an interview, she revealed that the initial lack of success for “Truth Hurts”—what she had thought to be her best song yet at the time—caused her to seriously consider quitting the music industry altogether.[68]
Lizzo is also well known for her ability to play the flute.[69] She began playing as a child, and has continued to improve her flute playing skills into adulthood.[70] She has performed with the flute, she has named Sasha Flute,[2] in several of her musical performances, including when she performed "Truth Hurts" at the 2019 BET Awards.[71] Her performance at the BET Awards earned her a standing ovation from the crowd, which included fellow singer Rihanna.[72]
Throughout the summer of 2019, Lizzo frequently performed, including on the West Holts stage at the Glastonbury Festival,[73][74] and as a headliner at the Indianapolis and Sacramento pride festivals.[75][76]
On July 23, 2019, Lizzo was nominated for Push Artist of the Year and Best New Artist at the 2019 MTV Video Music Awards.[77] She performed a medley of "Truth Hurts" and "Good as Hell" at the 2019 MTV Video Music Awards; her performance received critical praise.[78]
Around this time, her 2016 single "Good as Hell" also climbed the charts around the world, reaching the top three of the Billboard Hot 100 and the top ten of the UK Singles Chart.[79] The song also reached the top 10 in Australia and Belgium.

      Lizzo at the 62nd Grammy Awards
Lizzo made her Saturday Night Live debut as  musical guest on the December 21, 2019 episode, which Eddie Murphy hosted. The episode was the final episode of both the year and the decade.[80] In January 2020, Lizzo headlined FOMO Festival, performing in four Australian cities and Auckland, New Zealand.[81][82][83] She also performed a sold-out show at the Sydney Opera House, where she had previously performed as a young flute player.[84][85][86]

Personal life
When asked about her gender and sexuality, Lizzo said, "I personally don't ascribe to just one thing.... That's why the colors for LGBTQ+ are a rainbow! Because there's a spectrum and right now we try to keep it black and white. That's just not working for me."[87] She has a strong LGBTQ+ following and has dubbed her fans "Lizzbians".[88]
In June 2019, to mark the 50th anniversary of the Stonewall riots, an event widely considered a watershed moment in the modern LGBTQ rights movement, Queerty named her one of the Pride50 "trailblazing individuals who actively ensure society remains moving towards equality, acceptance and dignity for all queer people".[89]
Lizzo grew up attending the Church of God in Christ.[90]
Lizzo is praised for her plus-size body positivity and self confidence.[91] Lizzo gives partial credit to social media, and the internet in general, for changing the narrative around size and giving visibility to larger women like she is.[92] She is also very open about her mental health and its impact on her career.[93]
On January 5, 2020, Lizzo stopped using Twitter, citing "too many trolls" for her departure. She had been the target of body shaming, amongst other reasons. She said she'll "be back when I feel like it".[94] Her Twitter account has since been updated by her management, while she remains active on her Instagram.[95][96]
In June 2020, Lizzo revealed on TikTok that she is vegan.[97]

Artistry
Lizzo's music primarily incorporates hip hop.[98][99] Her music also incorporates genres such as soul,[99] R&B[99] and funk-pop.[99] Lizzo's influences include Missy Elliott,[100] Lauryn Hill,[101] Beyoncé,[102] and Diana Ross.[103] Lizzo stated in an interview in 2018, "I was always afraid of being a singer, but then when I heard Lauryn Hill, I was like, maybe I can do both," further adding that her debut album was inspired by The Miseducation of Lauryn Hill, "rapping, singing, being political."[101]

Discography
 Main article: Lizzo discography
 Lizzobangers (2013)

Big Grrrl Small World (2015)

Cuz I Love You (2019)

Filmography

Film


Year
Title
Role
Notes



2019
UglyDolls
Lydia
Voice role



2019
Hustlers
Liz




Television


Year
Title
Role
Notes



2016
Brad Neely's Harg Nallin' Sclopio Peepio
Herself
4 episodes



2016
Wonderland
Host
10 episodes



2018
Yeti! Yeti!
Magic Mushroom





Guest appearances as herself


Year
Title
Notes



2014
Made in Chelsea: NYC
Season 1, episode 4



2014
Late Show with David Letterman
Season 22, episode 29



2015
Access Hollywood




2015
The Late Show with Stephen Colbert
Season 1, episode 56



2016
Sooo Many White Guys
1 episode; podcast



2016
The Real
1 episode



2016
Party Legends
"Make Mistakes"



2016
Full Frontal with Samantha Bee
"Post-Election"



2017, 2018
Trivial Takedown
2 episodes



2018
Articulate with Jim Cotter
"Caroline Shaw, Lizzo, Robert Janz"



2018
Hannibal Buress: Handsome Rambler
"The Lizzo Episode"



2018
RuPaul's Drag Race
Season 10, episode 10



2019
The Ellen DeGeneres Show
1 episode



2019
The Tonight Show Starring Jimmy Fallon
1 episode



2019
The Daily Show with Trevor Noah
April 18, 2019



2019

2 Dope Queens

1 episode; podcast



2019
C à vous
1 episode



2019
Neo Magazin Royale
1 episode



2019
The Jonathan Ross Show
1 episode



2019
Saturday Night Live
Season 45, episode 10



Concert tours
Headlining

 Good as Hell Tour (2017)[104]

Cuz I Love You Tour (2019)[105]

Cuz I Love You Too Tour (2019)[106]

Supporting

 Haim – Sister Sister Sister Tour (2018)[107]

Florence and the Machine – High as Hope Tour (2018)[108]

Awards and nominations
 Main article: List of awards and nominations received by Lizzo
References

External links



Wikimedia Commons has media related to Lizzo.


 Official website 

   v
t
e
 Lizzo
 
 Discography

Awards and nominations


Studio albums 
 Lizzobangers

Big Grrrl Small World

Cuz I Love You


Extended plays 
 Coconut Oil


Singles 
 "Good as Hell"

"Water Me"

"Truth Hurts"

"Boys"

"Juice"

"Tempo"

"Cuz I Love You"


Featured singles 
 "Blame It on Your Love"


Tours 
 Cuz I Love You Too Tour



   v
t
e
 Grammy Award for Best Progressive R&B Album
Best Contemporary R&B Album   (2003–2011) 
 Ashanti –  Ashanti (2003)

Dangerously in Love – Beyoncé (2004)

Confessions – Usher (2005)

The Emancipation of Mimi – Mariah Carey (2006)

B'Day – Beyoncé (2007)

Because of You – Ne-Yo (2008)

 Growing Pains – Mary J. Blige (2009)

I Am... Sasha Fierce – Beyoncé (2010)

Raymond v. Raymond – Usher (2011)


Best Urban Contemporary Album   (2013–present) 
 Channel Orange – Frank Ocean (2013)

Unapologetic – Rihanna (2014)

Girl – Pharrell Williams (2015)

Beauty Behind the Madness – The Weeknd (2016)

Lemonade – Beyoncé (2017)

Starboy – The Weeknd (2018)

Everything Is Love – The Carters (2019)

Cuz I Love You (Deluxe) – Lizzo (2020)



 Authority control  
 GND: 1193108578

ISNI: 0000 0004 6307 6061

LCCN: no2019088003

MusicBrainz: 8fb5370b-9568-4b61-9da5-2aa12c9928db

VIAF: 51156130929358311064

 WorldCat Identities: lccn-no2019088003







