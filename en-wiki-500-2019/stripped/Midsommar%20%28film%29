  Not to be confused with the 2003 film Midsommer.
 2019 Swedish-American horror film
Midsommar
 Theatrical release poster
Directed byAri Aster
Produced by 
 Patrik Andersson

Lars Knudsen


Written byAri Aster
Starring 
 Florence Pugh

Jack Reynor

William Jackson Harper

Vilhelm Blomgren

Ellora Torchia

Archie Madekwe

Will Poulter


Music byBobby Krlic
CinematographyPawel Pogorzelski
Edited byLucian Johnston
 Production companies   
 Square Peg

B-Reel Films


Distributed by  A24 (United States)
Nordisk Film (Sweden)

 Release date 
 July 3, 2019 (2019-07-03) (United States)

July 10, 2019 (2019-07-10) (Sweden)


 Running time148 minutes[1]
Country  United States
Sweden

LanguageEnglish
Budget$9 million[2]
Box office$47.9 million[2]

Midsommar is a 2019 folk horror film written and directed by Ari Aster and starring Florence Pugh, Jack Reynor, William Jackson Harper, Vilhelm Blomgren, Ellora Torchia, Archie Madekwe, and Will Poulter. It follows a group of friends who travel to Sweden for a festival that occurs once every 90 years, only to find themselves in the clutches of a pagan cult.
A co-production between the United States and Sweden, the film was initially pitched to Aster as a straightforward slasher film set amongst Swedish cultists.[3] Aster devised a screenplay using elements of the concept, but made a deteriorating relationship the central conflict after he experienced a difficult breakup. The film was shot on location in Budapest in the summer and autumn of 2018.
Midsommar was theatrically released in the United States on July 3, 2019, by A24 and in Sweden on July 10, 2019, by Nordisk Film. The film grossed $46 million and received positive reviews from critics, with many praising Aster's direction and Pugh's performance.

  Contents
 
1 Plot

2 Cast

3 Production

4 Release
 
4.1 Director's cut

4.2 Home media




5 Reception
 
5.1 Box office

5.2 Critical response

5.3 Accolades




6 References

7 External links




Plot
Psychology student Dani Ardor is traumatized after her sister Terri kills herself and their parents by filling their home with carbon monoxide. The incident strains Dani's relationship with her emotionally distant boyfriend, anthropology student Christian Hughes. She later learns that Christian and his friends Mark and Josh have been invited by their mutual Swedish friend Pelle to attend a midsummer celebration at Pelle's ancestral commune, the Hårga, in Hälsingland. The celebration occurs only once every 90 years and Josh, also an anthropology student, wants to write his thesis on it. Christian had hidden the trip from Dani, wanting to break up with her before leaving, but invites her along after she finds out about it.
The group flies to Sweden and arrives at the commune, where they meet Simon and Connie, an English couple from London who were invited by Pelle's communal brother Ingemar. He offers the group psychedelic mushrooms, and Dani has hallucinations of Terri while under the drug's influence. Tensions rise after the group witnesses an ättestupa, a tradition which sees two commune elders killing themselves by leaping from a cliff onto a rock. When one of the elders survives the fall, the commune members mimic his wails of agony and crush his skull with a mallet, while commune elder Siv attempts to calm Dani and her friends by explaining that this is a normal thing every member does when they reach the age of 72. The scene disturbs the group but they decide to stay long enough for Josh to finish his thesis, while Simon and Connie decide to leave. An elder tells Connie that Simon has already left without her. Confused and suspicious, Connie decides to leave on her own; later, a woman's scream is heard in the distance.
Christian also decides to write his thesis on the Hårga, creating a rift between him and Josh, who accuses him of copying his idea. Meanwhile, Dani's already weak mental state deteriorates, exacerbated by her decision to leave her supply of the anti-anxiety medication Ativan at home. After Mark unwittingly urinates on an ancestral tree and incites the fury of the commune, he is lured away during dinner by a female member who feigns interest in him. That night, Josh sneaks into the temple to take a photograph of the commune's sacred runic text, which an elder had previously forbidden him from doing. He sees a nude man wearing Mark's skinned face and is hit over the head, after which his body is dragged away. The next day, Dani is coerced into taking more psychedelic drugs and ends up winning a maypole dancing competition, subsequently being crowned May Queen. At the same time, Christian is given more drugs before being coerced into a sex ritual designed to impregnate one of the female members, Maja, while older naked female members watch. After discovering Christian and Maja having sex, Dani has a panic attack, during which the commune's younger women surround her and mimic her cries. After the ritual, Christian comes to his senses and tries to run away, but he instead discovers Josh's leg planted in a flowerbed and Simon's body (which has been turned into a blood eagle) in a barn. Christian is then paralyzed by an elder.
The commune leaders explain to Dani that, to purge the commune of its evil, nine human sacrifices must be offered. The first four victims are outsiders (Mark, Josh, Simon, and Connie) lured to them by Pelle and Ingemar, while the next four victims are commune members (the two elders sacrificed during the ättestupa and two volunteers from the commune in the form of Ingemar and Ulf). As May Queen, Dani must choose either Christian or a local villager to be the ninth and final victim. She chooses to sacrifice Christian, who is stuffed into a disemboweled brown bear's body and placed in a small wooden temple alongside the other live sacrifices and corpses. The temple is set on fire and the commune members celebrate by mimicking the screams of those being burned alive. Dani initially sobs in horror, but gradually begins smiling with a distant look in her eye.

Cast
 
 Florence Pugh as Dani Ardor

Jack Reynor as Christian Hughes

William Jackson Harper as Josh

Vilhelm Blomgren as Pelle

Will Poulter as Mark

Ellora Torchia as Connie

Archie Madekwe as Simon

Henrik Norlén as Ulf

Gunnel Fred as Siv

Isabelle Grill as Maja

Agnes Rase as Dagny

Julia Ragnarsson as Inga

Mats Blomgren as Odd

Lars Väringer as Sten

Anna Åström as Karin

Hampus Hallberg as Ingemar

Liv Mjönes as Ulla

Louise Peterhoff as Hanna

Katarina Weidhagen as Ylva

Björn Andrésen as Dan

Tomas Engström as Jarl

Dag Andersson as Sven

Lennart R. Svensson as Mats

Anders Beckman as Arne

Rebecka Johnston as Ulrika

Tove Skeidsvoll as Majvor

Anders Back as Valentin

Anki Larsson as Irma

Levente Puczkó-Smith as Ruben

Gabriella Fón as Dani's mother

Zsolt Bojári as Dani's father

Klaudia Csányi as Terri Ardor


Production
In May 2018, it was announced Ari Aster would write and direct the film, with Lars Knudsen serving as producer. B-Reel Films, a Swedish company, produced the film alongside Square Peg, with A24 distributing.[4] According to Aster, he had been approached by B-Reel executives to helm a slasher film set in Sweden, an idea which he initially rejected as he felt he "had no way into the story."[5] Aster ultimately devised a plot in which the two central characters are experiencing relationship tensions verging on a breakup, and wrote the surrounding screenplay around this theme. He described the result as "a breakup movie dressed in the clothes of a folk horror film."[5]
In July 2018, Florence Pugh, Jack Reynor, Will Poulter, Vilhem Blomgren, William Jackson Harper, Ellora Torchia, and Archie Madekwe joined the cast.[6][7] Principal photography began on July 30, 2018, in Budapest, Hungary, and wrapped in October 2018.[8]

Release
Midsommar had a pre-release screening at the Alamo Drafthouse Cinema in New York City, on June 18, 2019.[9] The film was theatrically released in the United States on July 3, 2019.[10] It was released in Sweden on July 10, 2019.

Director's cut
Aster's original 171-minute cut of the film, which A24 asked Aster to trim down for a wide theatrical release, had its world premiere at the Film Society of Lincoln Center in New York City on August 20, 2019.[11] It was released in theaters nationwide on August 29, 2019 for one weekend only. The director's cut became an Apple TV exclusive on September 24, 2019.[12] The United Kingdom released the cut on Blu-ray and DVD on October 28, 2019.[13]

Home media
Midsommar was released on Digital HD on September 24, 2019, and on DVD and Blu-ray on October 8, 2019.[14]

Reception
Box office
Midsommar grossed $27.5 million in the United States and Canada, and $20.4 million in other territories, for a worldwide total of $47.9 million.[2]
In the United States and Canada, the film was projected to gross $8–10 million from 2,707 theaters over its first five days.[15] It made $3 million on its first day, including $1.1 million from Tuesday night previews, which Deadline Hollywood called a "smashing start."[16][17] It went on to debut to $10.9 million, finishing sixth at the box office; IndieWire said it was "just decent" given its estimated $8 million budget, but the film would likely find success in home media.[18][19] In its second weekend, the film dropped 44% to $3.7 million, finishing in eighth,[20] and then made $1.6 million in its third weekend, finishing in ninth.[21]

Critical response
On the review aggregator Rotten Tomatoes, the film holds an approval rating of 83% based on 377 reviews, with an average rating of 7.56/10. The website's critics consensus reads: "Ambitious, impressively crafted, and above all unsettling, Midsommar further proves writer-director Ari Aster is a horror auteur to be reckoned with."[22] On Metacritic, the film has a weighted average score of 72 out of 100, based on 54 critics, indicating "generally favorable reviews".[23] Audiences polled by CinemaScore gave the film a grade of "C+" on an A+ to F scale, while those at PostTrak gave it an average 3 out of 5 stars, with 50% saying they would definitely recommend it.[16]
John DeFore of The Hollywood Reporter described the film as the "horror equivalent of a destination wedding", and "more unsettling than frightening, [but] still a trip worth taking."[24] Writing for Variety, Andrew Barker noted that it is "neither the masterpiece nor the disaster that the film's most vocal viewers are bound to claim. Rather, it's an admirably strange, thematically muddled curiosity from a talented filmmaker who allows his ambitions to outpace his execution."[25] David Edelstein of Vulture praised Pugh's performance as "amazingly vivid" and noted that Aster "paces Midsommar more like an opera (Wagner, not Puccini) than a scare picture," but concluded that the film "doesn't jell because its impulses are so bifurcated. It's a parable of a woman's religious awakening—that's also a woman's fantasy of revenge against a man who didn't meet her emotional needs—that's also a male director's masochistic fantasy of emasculation at the hands of a matriarchal cult."[5]
Eric Kohn of IndieWire summarized the film as a "perverse breakup movie," adding that "Aster doesn't always sink the biggest surprises, but he excels at twisting the knife. After a deflowering that makes Ken Russell's The Devils look tame, Aster finds his way to a startling reality check."[26] Time Out's Joshua Rothkopf awarded the film a 5/5 star-rating, writing, "A savage yet evolved slice of Swedish folk-horror, Ari Aster's hallucinatory follow-up to Hereditary proves him a horror director with no peer."[27]
For The A.V. Club, A. A. Dowd stated that the film "rivals Hereditary in the cruel shock department", and labeled it a "B+ effort".[28] Writing for Inverse, Eric Francisco commented that the film feels "like a victory lap after Hereditary", and that Aster "takes his sweet time to lull viewers into his clutches ... But like how the characters experience time, its passage is a vague notion." He described the film as "a sharp portrayal of gaslighting".[29] Richard Brody of The New Yorker said that the film "is built on such a void of insight and experience, such a void of character and relationships, that even the first level of the house of narrative cards can't stand." He added, "In the end, the subject of Midsommar is as simple as it is regressive: lucky Americans, stay home."[30]
Nicolas Cage said that the film "was exciting" and said it had "Bergmanesque shots".[31] Jordan Peele said the film had "the most atrociously disturbing imagery I've ever seen on film" and "usurps The Wicker Man as the most iconic pagan movie to be referenced".[32]

Accolades



Award

Date of ceremony

Category

Recipient(s)

Result

Ref(s)



Gotham Independent Film Awards

December 2, 2019

Best Actress

Florence Pugh

Nominated

[33]



Best Screenplay

Ari Aster

Nominated



Hollywood Critics Association

January 9, 2020

Best Horror

Midsommar

Nominated

[34]



Independent Spirit Awards

February 8, 2020

Best Cinematography

Pawel Pogorzelski

Nominated





Santa Barbara International Film Festival

January 17, 2020

Virtuoso Award

Florence Pugh

Won

[35]



References

External links
 Official website 

Midsommar on IMDb

Midsommar at Metacritic

Midsommar at Rotten Tomatoes

   v
t
e
 Films directed by Ari Aster
Feature films 
 Hereditary (2018)

Midsommar (2019)


Short films 
 The Strange Thing About the Johnsons (2011)

Munchausen (2013)



  Film portal
United States portal
Speculative fiction/Horror portal
Hungary portal
Sweden portal





