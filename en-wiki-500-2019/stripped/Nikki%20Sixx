  American musician



 Nikki Sixx
 Sixx in September 2007
Background information
Birth nameFrank Carlton Serafino Feranna Jr.
Born (1958-12-11) December 11, 1958 (age 61)
OriginSan Jose, California, U.S.
Genres 
 Heavy metal

hard rock

glam metal

alternative metal


Occupation(s) 
 Musician

songwriter

author

fashion designer

radio host

photographer

record producer


Instruments 
 Bass


Years active1975–present
Labels 
 Eleven Seven

Elektra

Mötley

Sanctuary

Leathür

Warner Music Group

Beyond


Associated acts 
 Mötley Crüe

Sixx:A.M.

Brides of Destruction

58

London

Sister


Websitenikkisixx.net

Nikki Sixx (born Frank Carlton Serafino Feranna Jr.;[1][2] December 11, 1958) is an American musician, songwriter, radio host, and photographer, best known as the co-founder, bassist, and primary songwriter of the band Mötley Crüe.[1] Prior to forming Mötley Crüe, Sixx was a member of Sister before going on to form London[1] with his Sister bandmate Lizzie Grey.[3] In 2000, he formed side project group 58 with Dave Darling, Steve Gibb and Bucket Baker issuing one album, titled Diet for a New America, the same year[4] while, in 2002, he formed the hard rock supergroup Brides of Destruction with L.A. Guns guitarist Tracii Guns.[5] Formed in 2006, initially to record an audio accompaniment to Sixx's autobiography The Heroin Diaries: A Year in the Life of a Shattered Rock Star,[6] his side band Sixx:A.M. features songwriter, producer, and vocalist James Michael and guitarist DJ Ashba.[6]
Sixx has also worked with a number of artists and groups, co-writing and/or producing songs, such as Sex Pistols's guitarist Steve Jones, Lita Ford, Alice Cooper,[1] Meat Loaf,[7] Marion Raven,[8][9] Drowning Pool,[10] Saliva and The Last Vegas, among others.
Sixx launched the clothing line "Royal Underground" in 2006 with Kelly Gray, formerly the co-president and house model of St. John.[11] Initially the label concentrated on men's clothing[12] before expanding into women's[13] while in 2010, Premiere Radio Networks launched nationally syndicated Rock/alternative music radio programs "Sixx Sense" and "The Side Show Countdown" with both based in Dallas, Texas and hosted by Sixx and co-hosted by Jenn Marino.[14]

  Contents
 
1 Early life

2 Career
 
2.1 Early career, Sister, London (1975–1979)

2.2 Mötley Crüe (1981–2015)

2.3 58 (2000)

2.4 Brides of Destruction (2002–2004)

2.5 Sixx:A.M. (2006–2017)




3 Other work

4 Equipment
 
4.1 Signature basses

4.2 Other basses




5 Personal life

6 Radio shows

7 Running Wild in the Night

8 Discography
 
8.1 Production and songwriting credits




9 References

10 External links




Early life
Frank Carlton Serafino Feranna, Jr. was born on December 11, 1958 in San Jose, California.[1][15][16] He is of Italian ancestry on his father's side. Sixx was partially raised by his single mother, Deana Richards, and by his grandparents after his father left the family.[1] Feranna later moved in with his grandparents after his mother abandoned him. Feranna relocated several times while living with his grandparents.[17] Feranna's uncle, husband of Deana's sister Sharon, is Don Zimmerman, producer and president of Capitol Records. Feranna had one full biological sister, Lisa (born with Down syndrome; died circa 2000)[18] and has one (half) brother Rodney Anthony Feranna (born 1966) and a half-sister Ceci.[19]
Feranna grew up listening to Deep Purple, Harry Nilsson, The Beatles, The Rolling Stones, Elton John, Queen, Black Sabbath and later discovered T. Rex, David Bowie, and Slade.[20] While living in Jerome, Idaho, Feranna's youth years turned out to be a troubled one, as he became a teenage vandal, broke into neighbors' homes, shoplifted, and was expelled from school for selling drugs. His grandparents sent him to live with his mother, who had moved to Seattle.[21] Feranna lived there for a short time, and learned how to play the bass guitar having bought his first instrument with money gained from selling a guitar he had stolen.[1]

Career
Early career, Sister, London (1975–1979)
 Main articles: Sister (band) and London (heavy metal band)
At the age of 17, Feranna moved to Los Angeles, and began various jobs such as working at a liquor store[1] and selling vacuum cleaners over the phone[1] while he auditioned for bands. He eventually joined the band, Sister,[1] led by Blackie Lawless,[22] after answering an ad in The Recycler for a bass player.[23] Soon after recording a demo, Feranna was fired from Sister[23] along with bandmate Lizzie Grey.[3]
Feranna and Grey formed the band, London, soon afterwards, in 1978.[3][23] During this time, Feranna legally changed his name to Nikki Sixx.[1][23] After a number of lineup changes, London added former Mott the Hoople singer Nigel Benjamin to the group recording a 16-track demo in Burbank.[23] After the departure of Benjamin, along with the failure to find a replacement, Sixx departed London.[23] The group would go on to feature Sixx's former Sister bandmate Blackie Lawless (later of W.A.S.P.),[23] Izzy Stradlin (then of Hollywood Rose, later of Guns N' Roses) and drummer Fred Coury (later of Cinderella). In 2000, a number of the London demos recorded with Sixx were included on London Daze by Spiders & Snakes, led by former London guitarist Lizzie Grey.[3]

Mötley Crüe (1981–2015)
 Main article: Mötley Crüe
In 1981, Sixx founded Mötley Crüe alongside drummer Tommy Lee. They were later joined by guitarist Mick Mars through an ad in the local newspaper, and singer Vince Neil, with whom Lee had attended high school.[1][7] The band self-recorded their debut album, Too Fast for Love, which was subsequently released in November 1981 on the band's own Leathür Records label. After signing with Elektra Records, they re-released the same album.[1] The band then went on to record and release Shout at the Devil, raising the band to national fame.[1][7] They issued three more albums during the 80's, Theatre of Pain in 1985, Girls, Girls, Girls in 1987,[1][7] and Dr. Feelgood in 1989. The latter ended up being their most successful record,[1][7] staying in the charts for 114 weeks after its release.
During his time with Mötley Crüe, Sixx became addicted to heroin. He is quoted in The Heroin Diaries as saying: "Alcohol, acid, cocaine... they were just affairs. When I met heroin it was true love." He estimates he overdosed "about half a dozen times".[1][24] On December 23, 1987, Sixx overdosed on heroin and was reportedly declared clinically dead for two minutes before a paramedic revived him with two syringes full of adrenaline.[25]

      Nikki Sixx and Mick Mars performing onstage with Mötley Crüe, on June 14, 2005 in Glasgow, Scotland
After releasing the compilation album, Decade of Decadence, in 1991, Neil left the group, and was replaced by John Corabi, who formerly served with The Scream. They released one self titled album with Corabi, in 1994, before firing him in 1996. Afterwards, they reunited with Neil, with whom they released Generation Swine in 1997.[7]
Sixx had become controversial for an October 30, 1997 incident at Greensboro Coliseum, in which during a Mötley Crüe concert, he used racial epithets while goading the audience to physically attack a black security guard for repeatedly attacking a female fan.[26] In May 2001, Sixx addressed the issue, and claimed he had apologized to the victim of the incident.[27]
In 1999, Tommy Lee left the group to form Methods of Mayhem. He was replaced by former Ozzy Osbourne drummer, Randy Castillo, with whom they released the album, New Tattoo, in 2000.[7] The group went on hiatus soon after before reuniting in 2004, during which Sixx declared himself sober. A 2001 autobiography entitled The Dirt packaged the band as "the world's most notorious rock band". The book made the top ten on The New York Times Best Seller list and spent ten weeks there.[7]
In 2006, Mötley Crüe completed a reunion tour, featuring all four original members,[7] and embarked on a co-headlining tour with Aerosmith, called The Route of All Evil. In April 2008, the band announced the first Crüe Fest, a summer tour, that featured Sixx's side project Sixx:A.M., Buckcherry, Papa Roach and Trapt.[28] On June 24, 2008, Mötley Crüe released their ninth and final studio album, Saints of Los Angeles, with Sixx credited as either writer or co-writer on all tracks.[citation needed] The band officially retired in 2015.
Sixx wrote most of Mötley Crüe's material, including tracks such as "Live Wire", "Home Sweet Home", "Girls, Girls, Girls", "Kickstart My Heart", "Wild Side", "Hooligan's Holiday" and "Dr. Feelgood". In the 1990s, all four members began contributing to the material on the albums.[citation needed]

58 (2000)
 Main article: 58 (band)
In 2000, Sixx formed the internet based side project 58 with producer Dave Darling, guitarist Steve Gibb (formerly of Black Label Society and Crowbar) and drummer Bucket Baker.[4] They released one single, titled "Piece of Candy", and their debut album, Diet for a New America, also in 2000 through Sixx's Americoma label and Beyond Records.[4] The group did not tour, and was described by Sixx as "strictly an artistic thing."[27]

Brides of Destruction (2002–2004)
 Main article: Brides of Destruction
Brides of Destruction were formed by Sixx[7] and Tracii Guns[2] in Los Angeles 2002 initially with the name Cockstar[5][29] after Mötley Crüe went on hiatus and Guns left L.A. Guns. Sixx also invited former Beautiful Creatures guitarist DJ Ashba to join the group however he declined to focus on his solo band, ASHBA. Ashba would eventually join Sixx in Sixx:A.M..[30]
After a few lineup changes, that included Sixx's former Mötley Crüe bandmate John Corabi,[5] keyboardist Adam Hamilton[5] and drummer Kris Kohls of Adema,[5][31] the group was composed of Sixx, Guns, singer London LeGrand and drummer Scot Coogan formerly of Ednaswap and Annetenna.[5]
They were advised by radio programmers that the name Cockstar would not be announced on air.[29] They briefly adopted the moniker Motordog before settling on Brides of Destruction.[29][32][33]
They entered the studio with producer Stevo Bruno to begin recording what would become Here Come the Brides. The Brides played their first show opening for Mudvayne and Taproot on November 14, 2002 at the Ventura Theatre in California.[34][35]
After signing a deal with Sanctuary Records,[5][36][37] the group released Here Come the Brides in 2004, with the album debuting at number 92 on the Billboard 200[38] selling over 13,000 copies.[39] A tour of the US, Europe, including an appearance at Download Festival in the United Kingdom,[40] and Australia followed.
On October 25, 2004, it was announced that the group were to go on hiatus while Sixx reunited with Mötley Crüe for a reunion tour.[7][41] The group continued without Sixx, however, with Guns adding former Amen bassist Scott Sorry to the group as Sixx's replacement.[42] The second Brides of Destruction album, titled Runaway Brides, released in 2005 featured three songs co-written by Sixx during the Here Come the Brides sessions.[43]

Sixx:A.M. (2006–2017)
 Main article: Sixx:A.M.
Sixx formed his own group known as Sixx:A.M. in 2006, initially to record an audio accompaniment to his autobiography The Heroin Diaries: A Year in the Life of a Shattered Rock Star,[6] with friends producer/songwriter James Michael and guitarist DJ Ashba (Guns N' Roses, formerly of Beautiful Creatures and BulletBoys).[6][44] They recorded and released The Heroin Diaries Soundtrack in August 2007 through Eleven Seven.[6] The single, "Life Is Beautiful", received a high ratio of radio and video play[45] peaking at number 2 on the Mainstream Rock Tracks.

      Sixx (right) performing as part of Sixx:A.M. with James Michael in 2016.
The band made their live debut at the Crash Mansion on July 16, 2007. They performed five songs from the album, with former Beautiful Creatures drummer Glen Sobel filling in on the drums. On April 15, 2008, Sixx:A.M. announced they would be touring as part of Mötley Crüe's Crüe Fest.[28] The tour began on July 1, 2008, in West Palm Beach, Florida.[46] During Crüe Fest, Papa Roach drummer Tony Palermo served as a touring drummer for the band. A deluxe tour edition of The Heroin Diaries Soundtrack was released on November 25, 2008, which included a bonus live EP entitled Live Is Beautiful, which features recorded performances from the band's summer tour.
In April 2009, both Sixx and Michael confirmed that the band was in the studio, recording new material. Sixx added that the new material was "inspiring. it feels like we may have topped ourselves on this album coming up, and can't wait for you to hear what it sounds like."[47]
In 2010, the group continued recording the album with plans to release it by the late 2010/early 2011 with the group bringing in Paul R. Brown to shoot the video for the album's first single.[48][49] During an interview in July, Sixx stated that the album was almost finished.[50][51] This Is Gonna Hurt, the band's second studio album, was released on May 3, 2011. A third studio album, Modern Vintage, was released in 2014. Prayers for the Damned and Prayers for the Blessed were released in 2016.
The band went on hiatus in 2017, with other members DJ Ashba and James Michael forming a new band, Pyromantic.[52]

Other work
In 1989, Sixx was a featured guest artist on the album Fire and Gasoline by Steve Jones, formerly of the Sex Pistols.[1] Sixx co-wrote and performed on the song, "We're No Saints". In 1991, Sixx played bass on "Feed My Frankenstein" on Alice Cooper's Hey Stoopid album.[1] Sixx co-wrote the track "Die For You", along with Cooper and Mötley Crüe guitarist Mick Mars. In 2002, Sixx played on Butch Walkers first solo album "Left of Self Centered". In 2005, he collaborated with the Norwegian singer Marion Raven on two songs, "Heads Will Roll" and "Surfing the Sun", for Raven's debut album, Here I Am. A new version of "Heads Will Roll" appeared on Raven's 2006 EP Heads Will Roll and on her 2007 U.S. debut album, Set Me Free. In 2006, he was one of the songwriters for Meat Loaf's long-awaited album, Bat Out of Hell III: The Monster Is Loose.[citation needed]
In September 2007, Sixx released a book titled The Heroin Diaries: A Year in the Life of a Shattered Rock Star, a collection of his journal entries from 1986 and 1987 (when his heroin addiction was at its most dangerous). Written with British journalist Ian Gittins, it presents the present-day viewpoints of his bandmates, friends, ex-lovers, caretakers, business associates and family as they respond to specific passages.[53] The book debuted at #7 on The New York Times Best Seller list.[54] Along with Big & Rich (John Rich and Big Kenny Alphin), and James Otto, Sixx co-wrote "Ain't Gonna Stop" for Otto's 2008 Sunset Man CD on Warner Bros/Raybaw Records. In September 2011, Sixx is scheduled to appear on the live streaming video site backBEAT.[55]

Equipment
Signature basses
Sixx is most often seen playing Gibson Thunderbird basses. Between 2000 and 2003 Gibson produced the Nikki Sixx Signature Blackbird.[56] The Gibson Blackbird was for all intents and purposes a standard Thunderbird bass, but with a satin black finish, Iron Crosses on the fretboard instead of dots, an Iron Cross behind the classic Thunderbird logo, and Nikki Sixx's 'opti-grab' (a metal loop installed behind the bridge for hooking the little finger onto while playing). What also made this bass interesting was the lack of volume or tone controls, being replaced by a single on/off switch. Although subtle, this helped give this Blackbird more tone and a higher output. This model was discontinued in 2003, but has recently been put back in production as the Epiphone Nikki Sixx Blackbird.[57] Cosmetically the Epiphone Blackbird is identical to the Gibson original, but with a bolt-on single ply neck, solid mahogany body, different pickups and lower grade parts and manufacturing. The Epiphone model still kept the 'opti-grab,' designed and made first by his bass technician Tim Luzzi, and single on/off switch of the Gibson original. In 2008, Gibson announced a 'limited run' new Nikki Sixx signature bass. Like the original it features a neck through design made of mahogany and walnut, with maple 'wings' to form the body. Unlike the original 'Blackbird' bass, a clear 'satin black cherry' finish is given to the instrument, with red 'slash' X's on the 3rd, 5th, 7th and 12th frets. A mirror pickguard is also applied, with a red signature and two X's (6 x's on the whole bass) is also a new addition. Unlike the Gibson Blackbird, the new signature featured volume and tone controls, the 'opti-grab', and an on/off switch.[58]

Other basses
  This section of a biography of a living person does not include any references or sources. Please help by adding reliable sources. Contentious material about living people that is unsourced or poorly sourced must be removed immediately. Find sources: "Nikki Sixx" – news · newspapers · books · scholar · JSTOR  (October 2010) (Learn how and when to remove this template message)

His inspiration to use the Gibson Thunderbird came from Pete "Overend" Watts of Mott the Hoople and John Entwistle of The Who. His first Gibson Thunderbird was a white 1976 model. He would light it on fire with pyro gel during early Mötley Crüe shows, (when they were still a club band) and it finally just disintegrated. He used Fender Precision basses and Rickenbacker basses before he had his first Thunderbird.
Early on, he was sponsored by B.C. Rich, and used Mockingbird & Warlock basses. He used Hamer Firebird basses during the tour for Theatre of Pain, in either plain black or plain white, while some of them had finishes that suited his stage outfits. After that, he used Spector basses during Girls, Girls, Girls and Dr. Feelgood. These Spector basses were shaped like Thunderbirds, and usually are commonly called Spectorbirds. Sixx owned at least eight Spectorbirds. All eight had an opti-grab, designed and made by Tim Luzzi, 1 volume knob, P & J pickups, 24 frets and Spector bass "Crown" inlays. He used four during the tour for Girls, Girls, Girls, two black ones and one with a 101 Dalmatians finish, all of which had the Gibson Thunderbird Non-Reverse body type. One of the black basses had a large skull painting covering most of the body. He also used one in a buckeye burl finish with the reverse body style. It had an orange Harley-Davidson Crüe sticker where the Thunderbird logo usually is. These all had black hardware. For Dr. Feelgood  he used five Spectorbirds, two in sunburst and one in a natural finish. He also used a white one with a Non-Reverse style body, covered in small black stickers and a sticker saying Dancing on Glass. He also used a plain black Spectorbird with a reversed body style, which he smashed at the Make A Difference Foundation Moscow Music Peace Festival in Moscow.
During the 1990s, Sixx started using 12-string basses made in Japan by the Hiroshigi Kids Guitar Company. He owns at least five: a black one with red lettering and white binding, a black one with gold binding, a black one with white lettering and white binding, a red one with "Helter Skelter" written on it, and a green one. The red and green ones have dragon inlays on the body. He also used four- and five-string Epiphone Non-Reverse Thunderbirds for the Generation Swine tour and would usually smash one after his bass solo. He has also used Ernie Ball Music Man StingRay 5 basses, most notably while on tour with Brides of Destruction and the two newly recorded songs for the 1998 Mötley Crüe album, Greatest Hits.
He also has used Fender Precision Basses, particularly when smashing basses at the end of a set. They are usually black Squier Precision Basses with white pickguards. He previously used Ampeg amplifiers with Ampeg 8 x 10" loaded cabinets made with real wood, but had switched to Basson cabinets prior to their going out of business. The Basson cabinets were notoriously heavy (typically running 230–250 lbs), using medium density fiberboard covered with indoor-outdoor carpeting and loaded with Chinese Firestorm 1075 speakers (10"/75 oz magnets) and neoprene surrounds. Many of these cabinets were painted red with latex paint to match tour themes. Basson gave Sixx the cabinets in a marketing move to sell to metal-playing bassists, a very limited market. Basson went out of business in 2010. While recording The Heroin Diaries Soundtrack, he used a 1959 Fender Precision, which was amplified via 1964 Fender Bassman. Sixx also uses Audiotech Guitar Products Source Selector 1X6 Rack Mount Audio Switcher.

Personal life
  This section about a living person needs additional citations for verification. Please help by adding reliable sources. Contentious material about living persons that is unsourced or poorly sourced must be removed immediately, especially if potentially libelous or harmful. Find sources: "Nikki Sixx" – news · newspapers · books · scholar · JSTOR  (December 2010) (Learn how and when to remove this template message)

      Nikki Sixx and Courtney Bingham at the 'Call of Duty: Black Ops' Release Party in Santa Monica, California on November 4, 2010
Sixx was engaged to Denise "Vanity" Matthews in 1987.[59] In his autobiography, The Heroin Diaries: A Year in the Life of a Shattered Rock Star, Sixx described his toxic relationship with Matthews. "Vanity came and went during different periods of my addiction. She was a wild black chick who had sung with Prince: she'd also been his lover for a while. At the time I thought of Vanity as a disposable human being, like a used needle. Once its purpose was fulfilled it was ready for the trash, only to be dug up if you were really desperate…We became drug buddies: sometimes, you could even just about call us boyfriend and girlfriend. Vanity also taught me how to really freebase: the first time I based was with Tommy when Mötley just started and only a few times after that. So up until then, I’d been mostly snorting or injecting. But as soon as she showed me the real ins and outs of cooking up a good rock…it was love. Not her. The drug."[60]
From May 1989 to November 1996, Sixx was married to his first wife, Playboy Playmate Brandi Brandt;[1] they have three children: Gunner Nicholas Sixx (born January 25, 1991), Storm Brieann Sixx (born April 14, 1994), and Decker Nilsson Sixx (born May 23, 1995).
One month after the divorce from Brandt, Sixx married his second wife, another Playboy Playmate, actress Donna D'Errico.[1] Sixx and D'Errico have one daughter, Frankie-Jean Mary Sixx (born January 2, 2001). D'Errico has a son, Rhyan Jacob (born 1993), from a previous relationship. They separated shortly after their daughter's birth, and reconciled months later when Sixx completed rehab. They separated again on April 27, 2006 and divorced in June 2007, with D'Errico claiming irreconcilable differences.[61][62]
Sixx dated tattoo artist Kat Von D from 2008 to 2010.[63] A few months after their breakup, Sixx and Von D were spotted back together.[64] Sixx was featured on an episode of Von D's reality television show LA Ink in 2008, in which Von D gave him a tattoo of Mick Mars, lead guitarist of Mötley Crüe.[65] On August 25, 2010, Sixx issued a statement that their relationship had dissolved.[66] It was reported on October 19, 2010 that Nikki and Kat had gotten back together.[67] On October 27, 2010 Kat Von D confirmed to USA Today that indeed she and West Coast Choppers owner Jesse James were still together, debunking original reports that she and Sixx had reconciled.[68] On November 4, 2010 Sixx was spotted at the Call of Duty: Black Ops Launch Party in Santa Monica, California with Courtney Bingham, whom he has been dating ever since and they now live together.  On November 26, 2012, Nikki revealed to the public that he proposed to Courtney while vacationing in St. Barts. They were married on March 15, 2014. [69][70][71][72]
Bingham gave birth to their first child together, Ruby Sixx on July 27th, 2019. Sixx announced the birth through social media.[73]
Sixx practices Transcendental Meditation, as he considers it an important self-help technique.[74]

Radio shows
Launched on February 8, 2010,[75] Sixx Sense with Nikki Sixx broadcasts Monday through Friday from 7 p.m. to midnight local time on rock/alternative music stations. Each night, host Nikki Sixx discusses music and lifestyle topics as he gives listeners a backstage look at the world and mind of a rock star. Sixx was joined by co-host Kerri Kasem, from its first episode until March 28, 2014. On April 2, it was announced that radio personality Jenn Marino would be joining the show in Kasem's place. The show is based in Dallas, Texas in a studio in the Northpark Center.[76]
Starting on May 7, 2012, KEGL in Dallas/Fort Worth, Texas moved the show to mornings, making it the only station to carry the show in the mornings at 6 to 10 AM local time instead of the evening's time slot. The show is customized for the Dallas/Fort Worth listeners for broadcast in the mornings on KEGL. Sixx said that bringing Sixx Sense to mornings "has always been our goal. Who better to start your morning with than a rock star and a hot chick? It's a dream come true to have a morning show on one of America's best rock stations."[77] however, one year later, Sixx Sense returned to evenings at KEGL. In addition, recent episodes of "Sixx Sense" air 24/7 on its own iHeartRadio streaming page.[78]
The Side Show with Nikki Sixx is a two-hour original weekend program.[79] Airing Saturday or Sunday between 6 a.m. and midnight local time, Nikki Sixx will air top-charting songs, showcase new and emerging artists, and welcome guests from the worlds of music and entertainment.[76][79] In October 2017 Sixx announced  he would step down from Sixx Sense on December 31, 2017.[80]

Running Wild in the Night
With the formation of Sixx:A.M. and the release of The Heroin Diaries, Nikki Sixx teamed up with an already existing charity known as the Covenant House[81] and created his own branch called Running Wild in the Night.[45][82] In addition to partially funding the services the Covenant House provides on its own, Sixx's division also provides a creative arts and music program.[50][83] Sixx has negotiated with people in his industry to provide the program with musical instruments and software.[50][84]
A Portion of the profits from Sixx:A.M.’s album The Heroin Diaries Soundtrack and his autobiography, The Heroin Diaries: A Year in the Life of a Shattered Rock Star[45] is donated to help the Covenant House. He continues to auction off personal items to fund Running Wild in the Night. As of April 2009, he had raised over $100,000.[82]

Discography
 See also: Mötley Crüe discography



Title
Release
Label
Band



Too Fast for Love
1981
Elektra Records
Mötley Crüe



Shout at the Devil
1983



Theatre of Pain
1985



Girls, Girls, Girls
1987



Dr. Feelgood
1989



Mötley Crüe
1994



Generation Swine
1997



Diet for a New America

2000
Americoma/Beyond

58



New Tattoo
Mötley Records

Mötley Crüe



Here Come the Brides
2004
Sanctuary Records Group
Brides of Destruction



The Heroin Diaries Soundtrack
2007
Eleven Seven
Sixx:A.M.



Saints of Los Angeles
2008
Mötley Crüe



This Is Gonna Hurt
2011
Sixx:A.M.



7



Modern Vintage
2014



Prayers for the Damned, Vol. 1
2016



Prayers for the Blessed, Vol. 2


Production and songwriting credits



Year

Album title

Band

Record label

Credits



1988

Lita

Lita Ford

RCA

Co-writer "Falling in and Out of Love"



1989

Fire and Gasoline

Steve Jones

MCA

Co-writer "We're No Saints...."



1990

Hey Stoopid

Alice Cooper

Epic

Co-writer "Die for You"



2002

Back into Your System

Saliva

Island

Co-writer "Rest in Pieces"



2003

Couldn't Have Said It Better

Meat Loaf

Polydor/Sanctuary

Co-writer "Couldn't Have Said It Better", "Love You Out Loud" and "Man of Steel"



2004

ForThemAsses

OPM

Suburban Noize

Co-writer "Horny"



2005

Here I Am

Marion Raven

Atlantic

Co-writer "Heads Will Roll" and "Surfing the Sun"



2005

Runaway Brides

Brides of Destruction

Shrapnel

Co-writer "Criminal", "This Time Around" and "Blown Away"[43]



2006

Bat Out of Hell III: The Monster Is Loose

Meat Loaf

Virgin/Mercury

Co-writer "The Monster Is Loose"



2007

Set Me Free

Marion Raven

Eleven Seven/Warner Bros.

Co-writer "Set Me Free" and "Heads Will Roll"



2007

Full Circle

Drowning Pool

Eleven Seven

Co-writer and producer on "Reason I'm Alive"



2008

Sunset Man

James Otto

Warner Bros.

Co-writer "Ain't Gonna Stop"



2009

Whatever Gets You Off

The Last Vegas

Eleven Seven

Co-writer and producer on "I'm Bad", "Apologize" and "Cherry Red"



2010

Tattoos & Tequila

Vince Neil

Eleven Seven

Co-writer "Another Bad Day"


References

External links
 Official website

Nikki Sixx on IMDb

Official Sixx:A.M. website

Official Cruefest 2009 Tour website

Official Sixx Sense website

Mötley Crüe videos

   v
t
e
 Mötley Crüe
 
 Nikki Sixx

Mick Mars

Vince Neil

Tommy Lee

 John Corabi

Randy Castillo

Samantha Maloney


Studio albums 
 Too Fast for Love

Shout at the Devil

Theatre of Pain

Girls, Girls, Girls

Dr. Feelgood

Mötley Crüe

Generation Swine

New Tattoo

Saints of Los Angeles


Live albums 
 Live: Entertainment or Death

Carnival of Sins Live

The End: Live in Los Angeles


EPs 
 Raw Tracks

Quaternary


Compilations 
 Decade of Decadence 81-91

Greatest Hits (1998)

Supersonic and Demonic Relics

Loud as Fuck

Red, White & Crüe

Greatest Hits (2009)

The Dirt Soundtrack


Box sets 
 Music to Crash Your Car to: Vol. 1

Music to Crash Your Car to: Vol. 2


Video albums 
 Uncensored

Dr. Feelgood The Videos

Decade of Decadence

Behind the Music

Lewd, Crüed & Tattooed

Carnival of Sins Live

Crüe Fest


Singles 
 "Live Wire"

"Shout at the Devil"

"Helter Skelter"

"Looks That Kill"

"Too Young to Fall in Love"

"Smokin' in the Boys Room"

"Home Sweet Home"

"Girls, Girls, Girls"

"Wild Side"

"You're All I Need"

"Dr. Feelgood"

"Kickstart My Heart"

"Without You"

"Don't Go Away Mad (Just Go Away)"

"Same Ol' Situation (S.O.S.)"

"Primal Scream"

"Home Sweet Home '91"

"Anarchy in the U.K.

"Hooligan's Holiday"

"Misunderstood"

"Afraid"

"Beauty"

"Bitter Pill"

"Teaser"

"Hell on High Heels"

"New Tattoo"

"Treat Me Like the Dog I Am"

"If I Die Tomorrow"

"Sick Love Song"

"Saints of Los Angeles"

"Sex"

"All Bad Things Must End"


Tours 
 Monsters of Rock Tour 1984

Theatre of Pain Tour

Girls, Girls, Girls Tour

Dr. Feelgood World Tour '89–'90

Red, White & Crüe ... Better Live Than Dead

Route of All Evil Tour

Crüe Fest

Saints of Los Angeles Tour

Crüe Fest 2

Glam-A-Geddon

The Tour

Canadian Tour

Mötley Crüe Final Tour

The Stadium Tour


Related articles 
 Discography

Leathür Records

Mötley Records

The Dirt (film)

The Heroin Diaries: A Year in the Life of a Shattered Rock Star

Sister

London

Methods of Mayhem

58

Brides of Destruction

Sixx:A.M.

Rock Star Supernova

Nashville Outlaws: A Tribute to Mötley Crüe

Crüe Ball


 
  Category



   v
t
e
 Sixx:A.M.
 
 Nikki Sixx

DJ Ashba

James Michael


Studio albums 
 The Heroin Diaries Soundtrack

This Is Gonna Hurt

Modern Vintage

Prayers for the Damned

Prayers for the Blessed


Extended plays 
 X-Mas In Hell

Live Is Beautiful

7


Video albums 
 Crüe Fest


Singles 
 "Life Is Beautiful"

"Pray for Me"

"Tomorrow"

"Accidents Can Happen"

"Lies of the Beautiful People"

"This Is Gonna Hurt"

"Are You With Me"

"Gotta Get It Right"

"Stars"


Related articles 
 The Heroin Diaries: A Year in the Life of a Shattered Rock Star

Mötley Crüe

Brides of Destruction

58

Beautiful Creatures



   v
t
e
 Brides of Destruction
 
 Scot Coogan

Tracii Guns

London LeGrand

Nikki Sixx

 John Corabi

Ginger

Adam Hamilton

Scott Sorry


Studio albums 
 Here Come The Brides

Runaway Brides


Related 
 58

Adema

Amen

Annetenna

Ednaswap

L.A. Guns

Mötley Crüe

Sixx:A.M.

Sorry and the Sinatras

Souls of We

The Wildhearts

Guns N' Roses



   v
t
e
 London
 
 Nadir D'Priest

Brian West

Alan Krigger

 Lizzie Grey

Nikki Sixx

Blackie Lawless

Izzy Stradlin

Slash

Fred Coury


Studio albums 
 Non-Stop Rock

Don't Cry Wolf

Playa Del Rock


Compilation albums 
 The Metal Years


Related 
 London Daze

Sister

Mott the Hoople

Mötley Crüe

W.A.S.P.

Hollywood Rose

Guns N' Roses

Keel

Cinderella

Spiders & Snakes



 Authority control  
 BNF: cb14537333p (data)

GND: 136022782

ISNI: 0000 0003 6853 2786

LCCN: no2007115262

MusicBrainz: f84f1c8d-8b26-47c4-b1e2-76d812149047

NDL: 01141436

NKC: xx0087371

NLA: 35203212

SUDOC: 080757960

Trove: 863477

VIAF: 91759236

 WorldCat Identities: lccn-no2007115262







