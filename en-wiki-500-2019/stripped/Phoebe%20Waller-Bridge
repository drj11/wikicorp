  British actress and writer



 Phoebe Waller-Bridge
 Waller-Bridge in May 2018
Born (1985-07-14) 14 July 1985 (age 34)  London, England, United Kingdom
Occupation  Actress
writer
producer

Years active2009–present
Spouse(s) Conor Woodman (m. 2014; div. 2018)
RelativesIsobel Waller-Bridge (sister)

Phoebe Mary Waller-Bridge (born 14 July 1985) is an English actress, writer, and producer. She is best known as the creator, writer, and star of the BBC tragicomedy series Fleabag (2016–2019). She was also the head writer and executive producer for the first series of the BBC America thriller series Killing Eve (2018–present), which she adapted for television. Both Fleabag and Killing Eve have been highly acclaimed and named among the 100 greatest television series of the 21st century by The Guardian, with the former ranked at No. 8 and the latter at No. 30.[1]
For Fleabag, Waller-Bridge received the British Academy Television Award for Best Female Comedy Performance, three Primetime Emmy Awards (Outstanding Lead Actress in a Comedy Series, Outstanding Writing for a Comedy Series, and Outstanding Comedy Series),[2] and two Golden Globe Awards (Best Actress – Television Series Musical or Comedy and Best Television Series – Musical or Comedy).[3]
Waller-Bridge created, wrote and starred in the Channel 4 comedy-drama series Crashing (2016). She starred in the comedy series The Café (2011–2013) and the second series of crime drama Broadchurch (2015). She has appeared in films such as Albert Nobbs (2011), The Iron Lady (2011), Goodbye Christopher Robin (2017), and as L3-37 in the Star Wars film Solo: A Star Wars Story (2018). She also contributed to the screenplay of the upcoming James Bond film No Time to Die (2020).[4]

  Contents
 
1 Early life

2 Career

3 Personal life

4 Filmography

5 Works and publications

6 Awards

7 References

8 External links




Early life
Phoebe Mary Waller-Bridge was born in West London on 14 July 1985,[5] the daughter of Theresa Mary Waller-Bridge (née Clerke) and Michael Cyprian Waller-Bridge.[6] Her father founded the electronic trading platform Tradepoint, while her mother works for the Worshipful Company of Ironmongers.[7][8][9] The Waller-Bridge family were landed gentry of Cuckfield, Sussex.[10][11] On her father's side, she is also a descendant of The Rev. Sir Egerton Leigh, 2nd Baronet, Conservative MP for Mid Cheshire from 1873 to his death in 1876.[12][13] Her maternal grandfather was Sir John Edward Longueville Clerke, 12th baronet, of Hitcham, Buckinghamshire.[14] Waller-Bridge grew up in London's Ealing district,[15][16] and has two siblings: an older sister named Isobel Waller-Bridge, a composer with whom she has collaborated, and a younger brother named Jasper, a music manager.[17] Her parents are divorced.[18] She was educated at St Augustine's Priory, a Catholic independent school for girls,[19] followed by the independent sixth form college DLD College London in the Marylebone area of London.[20] She graduated from the Royal Academy of Dramatic Art in London.[21]

Career
In 2009, Waller-Bridge made her acting debut in the play Roaring Trade at Soho Theatre.[22] In 2013, Waller-Bridge appeared in one episode of Bad Education as "India". She appeared in the second series of drama Broadchurch in 2015.
In addition to acting, Waller-Bridge is a playwright. Her work includes the series of plays Good. Clean. Fun.[23] In 2016, Waller-Bridge wrote and starred in the Channel 4 sitcom Crashing and BBC Three's adaptation of Fleabag.[24][25]
After its initial release on BBC Three, Fleabag was broadcast on BBC Two from August 2016. It was picked up by the on-demand Amazon Video service and premiered in the United States in September 2016.[26][27] For her performance in the series she won the British Academy Television Award for Best Female Comedy Performance and was nominated for a Critics' Choice Television Award for Best Actress in a Comedy Series. Fleabag's second and final series aired in 2019. For the second series Waller-Bridge received Primetime Emmy Awards for Outstanding Lead Actress in a Comedy Series, Outstanding Writing for a Comedy Series, and Outstanding Comedy Series.[28][29]
Waller-Bridge is the co-artistic director, with Vicky Jones,[7] of DryWrite Theatre Company.[30][31][32] The two met and became friends while working on theatre productions.[33]
Waller-Bridge voiced and performed droid L3-37 in the Star Wars film Solo: A Star Wars Story (2018).[34][35]
Waller-Bridge wrote and produced the thriller television series Killing Eve based on novels by Luke Jennings.[36] The BBC America series stars Sandra Oh and Jodie Comer and premiered in April 2018.[37] For her work on the script, she received a nomination for a Primetime Emmy Award for Outstanding Writing for a Drama Series.
In March 2019, HBO ordered the series Run, which is executive-produced by Waller-Bridge and stars Domhnall Gleeson and Merritt Wever in lead roles.[38] Waller-Bridge also features in the series with a recurring role as the character Laurel.[39]
In 2019, Waller-Bridge co-wrote the screenplay for No Time to Die (2020), the 25th James Bond film, along with Neal Purvis and Robert Wade.[40][41]

Personal life
Waller-Bridge lives in the Shoreditch area of London. She married Irish presenter and documentary filmmaker Conor Woodman in 2014.[16] By 2017, they had separated and filed for divorce, which was finalised in 2018.[42] Since early 2018, she has been in a relationship with English-Irish playwright Martin McDonagh.[7] She describes herself as an atheist, although she says she "hopped around a bit from religion to religion" while growing up in London.[43]

Filmography

Phoebe Waller-Bridge film work


Year

Title

Role

Notes



2009
The Reward
Charlotte
Short film



2011
Beautiful Enough
Composer (voice)
Short film



Meconium
Lorna
Short film



Albert Nobbs
Viscountess Yarrell




The Iron Lady
Susie




2015
Man Up
Katie




2017
Goodbye Christopher Robin
Mary Brown




2018
Solo: A Star Wars Story
L3-37




2019
National Theatre Live: Fleabag
Fleabag
Theatrical event



2020
No Time to Die
N/A
Screenplay




Phoebe Waller-Bridge television work


Year

Title

Credited as

Role

Notes



Actress

Creator

Writer

Executive producer



2009
Doctors
Yes (1)
No
No
No
Katie Burbridge
Episode: "Chef's Secret"



2010
How Not to Live Your Life
Yes (1)
No
No
No
Felicity
Episode: "Don's Posh Weekend"



2011
The Night Watch
Yes
No
No
No
Lauren
Television film



2011–2013
The Café
Yes (13)
No
No
No
Chloe Astill




2013
Coming Up
Yes (1)
No
No
No
Karen
Episode: "Henry"



London Irish
Yes (1)
No
No
No
Steph
Episode: "Episode 2"



Bad Education
Yes (1)
No
No
No
India
Episode: "Drugs"



2014
Glue
Yes (2)
No
No
No
Bee Warwick




2014
Drifters
No
No
Yes (3)
No
N/A




2015
Broadchurch
Yes (8)
No
No
No
Abby Thompson




Flack
Yes
No
No
No
Eve
Television film



2016
Crashing
Yes
Yes
Yes
No
Lulu




2016–2019
Fleabag
Yes
Yes
Yes
Yes
Fleabag




2018–present
Killing Eve
No
Yes
Yes
Yes
N/A
Creator and Head writer for series one



2019
Saturday Night Live
Yes (1)
No
No
No
Herself (host)
Episode: "Phoebe Waller-Bridge/Taylor Swift"



2020
Run
Yes (3)
No
No
Yes
Laurel Halliday




Phoebe Waller-Bridge theatrework


Year

Title

Role

Venue

Ref.



Crazy Love

Billie

Paines Plough

[44]



2008

Twelfth Night

Viola

Sprite Productions

[45]



2009

Roaring Trade

Jess

Soho Theatre, London

[46]



2 May 1997

Sarah

The Bush Theatre, London

[46]



Rope

Leila Arden

Almeida Theatre, London

[46]



2010

Like a Fishbone

Intern

The Bush Theatre, London

[46]



Tribes

Ruth

Royal Court Theatre, London

[46]



2011

Hay Fever

Sorel Bliss

Noël Coward Theatre, London

[46][47]



2012

Mydidae

Marian

Soho Theatre, London   Trafalgar Studios, West End

[48]



2013–19

Fleabag

Fleabag

Underbelly, Cowgate Salisbury Playhouse SoHo Playhouse Wyndham's Theatre

[49]   [50]   [51][52]



2014

The One

Jo

Soho Theatre, London

[53]


Works and publications
 Waller-Bridge, Phoebe (1 January 2013). Fleabag. London: Nick Hern Books. ISBN 978-1-84-842364-0. OCLC 894546593.

Awards

Award nominations for Phoebe Waller-Bridge


Year

Award

Award category

Nominated work

Result



2014

The Offies (The Off West End Theatre Awards)

Best Female Performance[54]

Fleabag

Won



2014

The Offies (The Off West End Theatre Awards)

Most Promising New Playwright[54]

Fleabag

Won



2017

Royal Television Society Award

Breakthrough Star[55]

Fleabag

Won



2017

BAFTA TV Award

Best Female Performance in a Comedy[56]

Fleabag

Won



2019

BAFTA TV Craft Award

Best Writer – Drama[57]

Killing Eve

Nominated



2019

Britannia Award

British Artist of the Year[58]

N/A

Won



2020

Satellite Award[59]

Best Actress in a Musical or Comedy Series

Fleabag

Won



2020

Golden Globe Award[60]

Best Actress – Television Series Musical or Comedy

Fleabag

Won



2020

Critics' Choice Television Award[61]

Best Actress in a Comedy Series

Fleabag

Won



2020

Screen Actors Guild Award[62]

Outstanding Performance by a Female Actor in a Comedy Series

Fleabag

Won



2020

Laurence Olivier Award[63]

Best Actress

Fleabag

Pending



References

External links
 Phoebe Waller-Bridge on IMDb 

Phoebe Waller-Bridge's 'Fleabag' is NOT Autobiographical! on YouTube

  Awards for Phoebe Waller-Bridge
 
   v
t
e
 BAFTA TV Award for Best Female Comedy Performance
 
 Rebecca Front (2010)

Jo Brand (2011)

Jennifer Saunders (2012)

Olivia Colman (2013)

Katherine Parkinson (2014)

Jessica Hynes (2015)

Michaela Coel (2016)

Phoebe Waller-Bridge (2017)

Daisy May Cooper (2018)

Jessica Hynes (2019)



   v
t
e
 Britannia Awards
Excellence in Film 
 Albert R. Broccoli (1989)

Michael Caine (1990)

Peter Ustinov (1992)

Martin Scorsese (1993)

Anthony Hopkins (1995)

Bob Weinstein and Harvey Weinstein (1996)

Dustin Hoffman (1997)

John Travolta (1998)

Stanley Kubrick (1999)

Steven Spielberg (2000)

George Lucas (2002)

Hugh Grant (2003)

Tom Hanks (2004)

Tom Cruise (2005)

Clint Eastwood (2006)

Denzel Washington (2007)

Sean Penn (2008)

Robert De Niro (2009)

Jeff Bridges (2010)

Warren Beatty (2011)

Daniel Day-Lewis (2012)

George Clooney (2013)

Robert Downey Jr. (2014)

Meryl Streep (2015)

Jodie Foster (2016)

Matt Damon (2017)

Cate Blanchett (2018)

Jane Fonda (2019)


Excellence in Directing 
 Peter Weir (2003)

Jim Sheridan (2004)

Mike Newell (2005)

Anthony Minghella (2006)

Martin Campbell (2007)

Stephen Frears (2008)

Danny Boyle (2009)

Christopher Nolan (2010)

David Yates (2011)

Quentin Tarantino (2012)

Kathryn Bigelow (2013)

Mike Leigh (2014)

Sam Mendes (2015)

Ang Lee (2016)

Ava DuVernay (2017)

Steve McQueen (2018)

Jordan Peele (2019)


Worldwide Contribution to  Entertainment 
 Howard Stringer (2003)

Kirk Douglas (2009)

Ridley Scott & Tony Scott (2010)

John Lasseter (2011)

Will Wright (2012)

Ben Kingsley (2013)

Judi Dench (2014)

Harrison Ford (2015)

Samuel L. Jackson (2016)

Kenneth Branagh (2017)

Kevin Feige (2018)

Jackie Chan (2019)


British Artist of the Year 
 Rachel Weisz (2006)

Kate Winslet (2007)

Tilda Swinton (2008)

Emily Blunt (2009)

Michael Sheen (2010)

Helena Bonham Carter (2011)

Daniel Craig (2012)

Benedict Cumberbatch (2013)

Emma Watson (2014)

James Corden (2015)

Felicity Jones (2016)

Claire Foy (2017)

Emilia Clarke (2018)

Phoebe Waller-Bridge (2019)


Excellence in Comedy 
 Betty White (2010)

Ben Stiller (2011)

Trey Parker and Matt Stone (2012)

Sacha Baron Cohen (2013)

Julia Louis-Dreyfus (2014)

Amy Schumer (2015)

Ricky Gervais (2016)

Aziz Ansari (2017)

Jim Carrey (2018)

Steve Coogan (2019)


Excellence in Television 
 Aaron Spelling (1999)

 HBO Original Programming (2002)

Dick Van Dyke (2017)

Damian Lewis (2018)

Norman Lear (2019)


Humanitarian Award 
 Richard Curtis (2007)

Don Cheadle (2008)

Colin Firth (2009)

Idris Elba (2013)

Mark Ruffalo (2014)

Orlando Bloom (2015)

Ewan McGregor (2016)


Retired Awards 
 BBC (1999)

Tarsem Singh (1999)

Angela Lansbury (2003)

Helen Mirren (2004)

Elizabeth Taylor (2005)

Ronald Neame (2005)

Sidney Poitier (2006)

Bob Shaye and Michael Lynne (2007)



   v
t
e
 Critics' Choice Television Award for Best Actress in a Comedy Series
 
 Tina Fey (2011)

Zooey Deschanel / Amy Poehler (2012)

Julia Louis-Dreyfus (2013)

Julia Louis-Dreyfus (2014)

Amy Schumer (2015)

Rachel Bloom (2016)

Kate McKinnon (2016)

Rachel Brosnahan (2017)

Rachel Brosnahan (2018)

Phoebe Waller-Bridge (2019)



   v
t
e
 Primetime Emmy Award for Outstanding Lead Actress in a Comedy Series
 
 Gertrude Berg (1950)

Imogene Coca (1951)

Lucille Ball (1952)

Eve Arden (1953)

Lucille Ball (1955)

Nanette Fabray (1956)

Jane Wyatt (1957)

Jane Wyatt (1959)

Jane Wyatt (1960)

Barbara Stanwyck (1961)

Shirley Booth (1962)

Shirley Booth (1963)

Mary Tyler Moore (1964)

No Award (1965)

Mary Tyler Moore (1966)

Lucille Ball (1967)

Lucille Ball (1968)

Hope Lange (1969)

Hope Lange (1970)

Jean Stapleton (1971)

Jean Stapleton (1972)

Mary Tyler Moore (1973)

Mary Tyler Moore (1974)

Valerie Harper (1975)

Mary Tyler Moore (1976)

Bea Arthur (1977)

Jean Stapleton (1978)

Ruth Gordon (1979)

Cathryn Damon (1980)

Isabel Sanford (1981)

Carol Kane (1982)

Shelley Long (1983)

Jane Curtin (1984)

Jane Curtin (1985)

Betty White (1986)

Rue McClanahan (1987)

Bea Arthur (1988)

Candice Bergen (1989)

Candice Bergen (1990)

Kirstie Alley (1991)

Candice Bergen (1992)

Roseanne Barr (1993)

Candice Bergen (1994)

Candice Bergen (1995)

Helen Hunt (1996)

Helen Hunt (1997)

Helen Hunt (1998)

Helen Hunt (1999)

Patricia Heaton (2000)

Patricia Heaton (2001)

Jennifer Aniston (2002)

Debra Messing (2003)

Sarah Jessica Parker (2004)

Felicity Huffman (2005)

Julia Louis-Dreyfus (2006)

America Ferrera (2007)

Tina Fey (2008)

Toni Collette (2009)

Edie Falco (2010)

Melissa McCarthy (2011)

Julia Louis-Dreyfus (2012)

Julia Louis-Dreyfus (2013)

Julia Louis-Dreyfus (2014)

Julia Louis-Dreyfus (2015)

Julia Louis-Dreyfus (2016)

Julia Louis-Dreyfus (2017)

Rachel Brosnahan (2018)

Phoebe Waller-Bridge (2019)



   v
t
e
 Primetime Emmy Award for Outstanding Writing for a Comedy Series
1955–1959 
 James Allardice & Jack Douglas & Hal Kanter & Harry Winkler (1955)

Arnold M. Auerbach & Barry Blitzer & Vincent Bogert & Nat Hiken & Coleman Jacoby & Harvey Orkin & Arnold Rosen & Terry Ryan & Tony Webster (1956)

Billy Friedberg & Nat Hiken & Coleman Jacoby & Arnold Rosen & A.J. Russell & Terry Ryan & Phil Sharp & Tony Webster & Sydney Zelinka (1958)

George Balzer & Hal Goldman & Al Gordon & Sam Perrin (1959)


1960–1969 
 George Balzer & Hal Goldman & Al Gordon & Sam Perrin (1960)

Dave O'Brien & Martin Ragaway & Sherwood Schwartz & Al Schwartz & Red Skelton (1961)

Carl Reiner (1962)

Carl Reiner (1963)

No award (1964)

No award (1965)

Sam Denoff & Bill Persky for "Coast to Coast Big Mouth" (1966)

Buck Henry & Leonard B. Stern for "Ship of Spies: Parts 1 and 2" (1967)

Allan Burns & Chris Hayward for "The Coming Out Party" (1968)

No award (1969)


1970–1979 
 No award (1970)

James L. Brooks & Allan Burns for "Support Your Local Mother" (1971)

Burt Styler for "Edith's Problem" (1972)

Lee Kalcheim & Michael Ross & Bernie West for "The Bunkers and the Swingers" (1973)

Treva Silverman for "The Lou and Edie Story" (1974)

Stan Daniels & Ed. Weinberger for "Will Mary Richards Go to Jail?" (1975)

David Lloyd for "Chuckles Bites the Dust" (1976)

James L. Brooks & Allan Burns & Stan Daniels & Bob Ellison & David Lloyd & Ed. Weinberger for "The Last Show" (1977)

Harve Brosten & Barry Harman & Bob Schiller & Bob Weiskopf for "Cousin Liz" (1978)

No award (1979)


1980–1989 
 R.J. Colleary for "The Photographer" (1980)

Michael J. Leeson for "Tony's Sister and Jim" (1981)

Ken Estin for "Elegant Iggy" (1982)

Glen Charles and Les Charles for "Give Me a Ring Sometime" (1983)

David Angell for "Old Flames" (1984)

Ed. Weinberger & Michael J. Leeson for "Pilot" (The Cosby Show) (1985)

Barry Fanaro & Mort Nathan for "A Little Romance" (1986)

Gary David Goldberg & Alan Uger for "A, My Name is Alex" (1987)

Hugh Wilson for "The Bridge" (1988)

Diane English for "Pilot" (Murphy Brown) (1989)


1990–1999 
 Bob Brush for "Good-bye" (1990)

Gary Dontzig & Steven Peterman for "Jingle Hell, Jingle Hell, Jingle All the Way" (1991)

Elaine Pope & Larry Charles for "The Fix-Up" (1992)

Larry David for "The Contest" (1993)

David Angell & Peter Casey & David Lee for "The Good Son" (1994)

Chuck Ranberg & Anne Flett-Giordano for "An Affair to Forget" (1995)

Joe Keenan & Christopher Lloyd & Rob Greenberg & Jack Burditt & Chuck Ranberg & Anne Flett-Giordano & Linda Morris & Vic Rauseo for "Moon Dance" (1996)

Ellen DeGeneres & Mark Driscoll & Dava Savel & Tracy Newman & Jonathan Stark for "The Puppy Episode" (1997)

Peter Tolan & Garry Shandling for "Flip" (1998)

Jay Kogen for "Merry Christmas, Mrs. Moskowitz" (1999)


2000–2009 
 Linwood Boomer for "Pilot" (Malcolm in the Middle) (2000)

Alex Reid for "Bowling" (2001)

Larry Wilmore for "Pilot" (The Bernie Mac Show) (2002)

Tucker Cawley for "Baggage" (2003)

Mitchell Hurwitz for "Pilot" (Arrested Development) (2004)

Mitchell Hurwitz & Jim Vallely for "Righteous Brothers" (2005)

Greg Garcia for "Pilot" (My Name Is Earl) (2006)

Greg Daniels for "Gay Witch Hunt" (2007)

Tina Fey for "Cooter" (2008)

Matt Hubbard for "Reunion" (2009)


2010–present 
 Steven Levitan & Christopher Lloyd for "Pilot" (Modern Family) (2010)

Steven Levitan & Jeffrey Richman for "Caught in the Act" (2011)

Louis C.K. for "Pregnant" (2012)

Tina Fey & Tracey Wigfield for "Last Lunch" (2013)

Louis C.K. for "So Did the Fat Lady" (2014)

Simon Blackwell & Armando Iannucci & Tony Roche for "Election Night" (2015)

Aziz Ansari & Alan Yang for "Parents" (2016)

Aziz Ansari & Lena Waithe for "Thanksgiving" (2017)

Amy Sherman-Palladino for "Pilot (The Marvelous Mrs. Maisel)" (2018)

Phoebe Waller-Bridge for "Episode 1" (2019)



   v
t
e
 Golden Globe Award for Best Actress – Television Series Musical or Comedy
 
 Donna Reed (1962)

Inger Stevens (1963)

Mary Tyler Moore (1964)

Anne Francis (1965)

Marlo Thomas (1966)

Carol Burnett (1967)

Diahann Carroll (1968)

Carol Burnett / Julie Sommars (1969)

Mary Tyler Moore (1970)

Carol Burnett (1971)

Jean Stapleton (1972)

Cher / Jean Stapleton (1973)

Valerie Harper (1974)

Cloris Leachman (1975)

Carol Burnett (1976)

Carol Burnett (1977)

Linda Lavin (1978)

Linda Lavin (1979)

Katherine Helmond (1980)

Eileen Brennan (1981)

Debbie Allen (1982)

Joanna Cassidy (1983)

Shelley Long (1984)

Estelle Getty / Cybill Shepherd (1985)

Cybill Shepherd (1986)

Tracey Ullman (1987)

Candice Bergen (1988)

Jamie Lee Curtis (1989)

Kirstie Alley (1990)

Candice Bergen (1991)

Roseanne Barr (1992)

Helen Hunt (1993)

Helen Hunt (1994)

Cybill Shepherd (1995)

Helen Hunt (1996)

Calista Flockhart (1997)

Jenna Elfman (1998)

Sarah Jessica Parker (1999)

Sarah Jessica Parker (2000)

Sarah Jessica Parker (2001)

Jennifer Aniston (2002)

Sarah Jessica Parker (2003)

Teri Hatcher (2004)

Mary-Louise Parker (2005)

America Ferrera (2006)

Tina Fey (2007)

Tina Fey (2008)

Toni Collette (2009)

Laura Linney (2010)

Laura Dern (2011)

Lena Dunham (2012)

Amy Poehler (2013)

Gina Rodriguez (2014)

Rachel Bloom (2015)

Tracee Ellis Ross (2016)

Rachel Brosnahan (2017)

Rachel Brosnahan (2018)

Phoebe Waller-Bridge (2019)



   v
t
e
 Satellite Award for Best Actress – Television Series Musical or Comedy
 
 Jane Curtin (1996)

Tracey Ullman (1997)

Ellen DeGeneres (1998)

Illeana Douglas (1999)

Lisa Kudrow (2000)

Debra Messing (2001)

Debra Messing (2002)

Jane Kaczmarek (2003)

Portia de Rossi (2004)

Felicity Huffman / Mary-Louise Parker (2005)

Marcia Cross (2006)

America Ferrera (2007)

Tracey Ullman (2008)

Lea Michele (2009)

Laura Linney (2010)

Martha Plimpton (2011)

Kaley Cuoco (2012)

Taylor Schilling (2013)

Mindy Kaling (2014)

Taylor Schilling (2015)

Taylor Schilling (2016)

Niecy Nash (2017)

Issa Rae (2018)

Phoebe Waller-Bridge (2019)



   v
t
e
 Screen Actors Guild Award for Outstanding Performance by a Female Actor in a Comedy Series
 
 Helen Hunt (1994)

Christine Baranski (1995)

Julia Louis-Dreyfus (1996)

Julia Louis-Dreyfus (1997)

Tracey Ullman (1998)

Lisa Kudrow (1999)

Sarah Jessica Parker (2000)

Megan Mullally (2001)

Megan Mullally (2002)

Megan Mullally (2003)

Teri Hatcher (2004)

Felicity Huffman (2005)

America Ferrera (2006)

Tina Fey (2007)

Tina Fey (2008)

Tina Fey (2009)

Betty White (2010)

Betty White (2011)

Tina Fey (2012)

Julia Louis-Dreyfus (2013)

Uzo Aduba (2014)

Uzo Aduba (2015)

Julia Louis-Dreyfus (2016)

Julia Louis-Dreyfus (2017)

Rachel Brosnahan (2018)

Phoebe Waller-Bridge (2019)



   v
t
e
 TCA Award for Individual Achievement in Comedy
 
 David Hyde Pierce (1997)

David Hyde Pierce (1998)

Ray Romano (1999)

Jane Kaczmarek (2000)

Jane Kaczmarek (2001)

Bernie Mac (2002)

Jon Stewart (2003)

Ricky Gervais (2004)

Jon Stewart (2005)

Steve Carell (2006)

Alec Baldwin (2007)

Tina Fey (2008)

Jim Parsons (2009)

Jane Lynch (2010)

Ty Burrell / Nick Offerman (2011)

Louis C.K. (2012)

Louis C.K. (2013)

Julia Louis-Dreyfus (2014)

Amy Schumer (2015)

Rachel Bloom (2016)

Donald Glover (2017)

Rachel Brosnahan (2018)

Phoebe Waller-Bridge (2019)




 Authority control  
 BIBSYS: 1507616671479

GND: 1176033743

ISNI: 0000 0004 7476 8792

LCCN: nb2017003405

NTA: 421392347

VIAF: 142148752011441200549

 WorldCat Identities: lccn-nb2017003405







