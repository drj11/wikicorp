  This article is about the singer. For his self-titled album, see Shawn Mendes (album).
 Canadian singer, songwriter, and model



 Shawn Mendes
 Mendes at the 2019 American Music Awards
Born Shawn Peter Raul Mendes  (1998-08-08) August 8, 1998 (age 21)  Pickering, Ontario, Canada
Occupation 
 Singer

songwriter

record producer


Years active2013–present
AgentWilhelmina Models
AwardsFull list
Musical career
Genres 
 Pop

folk-pop

pop rock


Instruments 
 Vocals

guitar

piano


Labels 
 Island

Republic[1]

Universal Music Canada [2]


Associated acts 
 Teddy Geiger

Camila Cabello


Websiteshawnmendesofficial.com


Shawn Peter Raul Mendes (/ˈmɛndɛz/; born August 8, 1998) is a Canadian singer and songwriter. He gained a following in 2013, posting song covers on the video-sharing application Vine. The following year, he caught the attention of artist manager Andrew Gertler and Island Records A&R Ziggy Chareton, which led to him signing a deal with the record label. He has since released three studio albums, headlined three world tours, and received several awards.
Mendes released his self-titled debut EP (2014) and his debut studio album Handwritten (2015), whose single "Stitches" reached number one in the United Kingdom and the top 10 in the United States and Canada. He then released his second studio album Illuminate (2016), whose singles "Treat You Better" and "There's Nothing Holdin' Me Back" reached the top 10 in several countries. His self-titled third studio album (2018) was supported by the lead single "In My Blood". All three albums debuted atop the US Billboard 200, with the first one making Mendes one of five artists ever to debut at number one before the age of 18 and the third one making him the third-youngest artist to achieve three number one albums. In 2017, Mendes became the first artist to land three number-one singles on the Billboard Adult Contemporary chart before turning 20 years old. In 2018, he became the first artist to achieve four number-one singles on the Adult Pop Songs chart before the age of 20 as well. In 2019, he released the hit singles "If I Can't Have You" and "Señorita", with the latter peaking at number one on the US Billboard Hot 100.
Among his accolades, Mendes has won 13 SOCAN awards, 10 MTV Europe Music Awards, eight Juno Awards, eight iHeartRadio MMVAs, two American Music Awards, and received three Grammy Award nominations. In 2018, Time named Mendes as one of the 100 most influential people in the world on their annual list.

  Contents
 
1 Early life

2 Career
 
2.1 2013—2015: Handwritten

2.2 2016—2017: Illuminate

2.3 2018—present: Shawn Mendes




3 Artistry and musical influences

4 Brand endorsement and modelling

5 Philanthropy and supported causes

6 Personal life

7 Awards and nominations

8 Filmography

9 Discography

10 Tours
 
10.1 Headlining

10.2 Co-headlining

10.3 Supporting




11 References

12 External links




Early life
Mendes was born in the city of Pickering, Ontario, to Karen (née Rayment), a real estate agent, and Manuel Mendes, a businessman, who sells bar and restaurant supplies in Toronto. His father is Portuguese (from the Algarve),[3] while his mother is from England. He has a younger sister named Aaliyah.[4] He was raised in a religious family.[5]
Mendes was raised in Pickering where he attended Pine Ridge Secondary School.[6] At school, he played ice hockey and soccer, joined his high school glee club, and practised his stage presence in acting lessons (leading as Prince Charming at one point). He also auditioned for Disney Channel in Toronto.[7] Mendes graduated from high school in June 2016.[8]

Career
2013—2015: Handwritten
 Main article: Handwritten (Shawn Mendes album)
      Mendes at the Jingle Ball Tour 2014
Mendes learned to play guitar by watching YouTube tutorial videos at the age of 14 in 2012.[9] Less than a year later he started posting cover videos on YouTube. Mendes started attracting viewers after he posted a cover from Justin Bieber's "As Long as You Love Me" on the social video app Vine in 2013 and got 10,000 likes and as many followers the next day. After that he gained millions of views and followers in a few months, becoming well known for his six-second snippets of renditions of many popular songs.[10] Mendes saw Bieber as a career model at the time.[11] By August 2014, he was the third most-followed musician on Vine.[12] Artist manager Andrew Gertler discovered Mendes online in November 2013, bringing him to Island Records in January 2014. In April, he won Ryan Seacrest's "Best Cover Song" contest with "Say Something" by A Great Big World. He officially signed to Island in May 2014.
He released his first single "Life of the Party" on June 26, 2014.[13] He became the youngest to debut in the top 25 on the US Billboard Hot 100, making it to number 24 for the week ending July 12, 2014.[14] Prior to his signing, Mendes toured as a member of the Magcon Tour[15] alongside other young Viners with a large following on social media.[16] Mendes was also on a nationwide tour with Austin Mahone as an opening act. He released his debut major label EP in July.[17] The EP debuted and peaked at number five on Billboard 200, selling 48,000 copies in its first week.[18] He won a Teen Choice award in 2014 for Webstar in Music.[19] On September 5, 2014, "Oh Cecilia (Breaking My Heart)" featuring Mendes was released as the fifth single from The Vamps' debut album, Meet the Vamps.[20] On November 6, 2014, "Something Big" was released as the second single.[21]
On April 14, 2015, Mendes released his full-length album Handwritten, which debuted at number one on the Billboard 200 chart with 119,000 equivalent album units, selling 106,000 copies in its first week and was certified platinum.[22][23] Mendes then became the youngest artist to debut at number one since the release of Justin Bieber's My World 2.0.[24] The third single from the album, "Stitches", peaked at number four on the US Billboard Hot 100, becoming his first top 10 single in the US,[25] and became his first number one on the Adult Pop Songs[26] and Adult Contemporary charts.[27] The song later reached number one in the UK.[28] Also in 2015, Mendes opened for Taylor Swift during 1989 World Tour dates for North America and recorded "Believe" for the soundtrack of Disney Channel Original Movie Descendants.[29][30] In late 2015, Mendes and Camila Cabello, who was at the time a member of the group Fifth Harmony, released their collaborative single "I Know What You Did Last Summer". The song was included on Mendes' Handwritten Revisited reissue.[31]
Mendes was listed among Time's "25 Most Influential Teens of 2014", debuting in the list after being the youngest-ever artist to debut in the top 25 of Billboard Hot 100.[32] He was listed in Time's "The 30 Most Influential Teens of 2015", making the list after his debut album topped the Billboard 200 and his single "Stitches" made the top 10 in the US and other countries.[33]

2016—2017: Illuminate
 Main article: Illuminate (Shawn Mendes album)
      Mendes in 2017
On January 21, 2016, Mendes made his acting debut on The CW's The 100 third-season premiere.[34] He later announced his second world tour as a headliner, the Shawn Mendes World Tour, which started in March 2016[35] and sold out 38 shows in North America and Europe within minutes.[36]
Mendes released "Treat You Better", the lead single from his second studio album, in June 2016.[37] In the US, the single reached the top 10 on the Billboard Hot 100, became his second single to peak atop both the Adult Contemporary[27] and Adult Pop Songs[26] chart, and was certified triple platinum.[23] It also went top 10 in the UK. The album, Illuminate, was released on September 23, 2016 and debuted at number one on the US Billboard 200 with 145,000 equivalent album units, including 121,000 in pure album sales and was certified platinum.[23][38] It debuted atop the charts in Canada, becoming his second number one album in his home country. "Mercy" was released as the second single on August 18, 2016,[39] which entered the top 20 in the US and the UK and was certified double platinum.[23] Mendes released the live album Live at Madison Square Garden in December 2016.[40] He appeared as the musical guest on Saturday Night Live December 3, 2016.[41]
In April 2017, Mendes embarked on his Illuminate World Tour, which sold out arenas around the world such as Los Angeles' Staples Center and London's The O2 Arena.[42][43][44] He released the single "There's Nothing Holdin' Me Back" on April 20, 2017, included on his Illuminate deluxe edition.[45] The song was Mendes' third single to reach the top 10 in the US[46] and third single to reach number one on both the Adult Contemporary[27] and Adult Pop Songs[26] chart.[47] In August 2017, he became the first artist under 20 years old to have three number-one songs on the Billboard Adult Pop Songs chart.[48] In November 2017, Mendes became the first artist to have three number-one songs on the Billboard Adult Contemporary chart before turning 20 years old,[49] an unprecedented feat since the founding of the chart more than 50 years ago.
Mendes was listed among Time's The 30 Most Influential Teens of 2016[50] and made his first appearance on Forbes's 30 Under 30 2016: Music.[51] He topped Billboard's 21 Under 21 list in 2017, after his two albums topped Billboard 200 and his single "There's Nothing Holding Me Back" became his fifth top 20 on the Billboard Hot 100.[52]

2018—present: Shawn Mendes
 Main article: Shawn Mendes (album)
On March 22, 2018, Mendes released the lead single "In My Blood" from his upcoming third studio album,[53] followed up by the second single "Lost in Japan" on March 23.[54] "In My Blood" topped the Billboard Adult Pop Songs chart, making Mendes the first and only artist to have four number one singles in the chart before turning 20 years old.[26] "Youth" was released on May 3, featuring American singer Khalid.[55]
His self-titled studio album was released on May 25, 2018, to positive critical reviews, with particular praise towards his songwriting and artistic growth.[56] It debuted at number one in Canada, making it his third number one album in his home country. It debuted at number one on the US Billboard 200, making Mendes the third-youngest artist to collect three number-one albums.[57]

      Mendes performing at the concert honouring the 92nd birthday of Elizabeth II (April 2018)
To promote the album, Mendes embarked on his self-titled world tour in 2019.[58] Besides the tour, he performed at music festivals across Europe, North America, and South America. He performed at a televised concert honouring the 92nd birthday of Queen Elizabeth II on April 21, 2018.[59] He made TV show appearances on The Late Late Show with James Corden in June where he sang one of his latest singles each night for a week.[60] The tracks he performed live were "Nervous",[61] "Lost in Japan",[62] "Perfectly Wrong",[63] and the duet with Julia Michaels "Like to be You".[64] Mendes made an appearance on the late-night talk show The Tonight Show Starring Jimmy Fallon in October and performed Lost in Japan.[65] He, together with Fallon and the show's resident band The Roots, performed a special version of "Treat You Better" for the show's Classroom Instruments series.[66] He has also performed his latest singles on the iHeartRadio MMVAs in Canada on August 27, where he received eight nominations and won four awards.[67]
Mendes starred in a documentary directed by YouTube star Casey Neistat. The short film is part of YouTube's Artist Spotlight Story series, featuring an interview with Mendes and backstage and behind-the-scene footage of Mendes during one of his tours.[68] The trailer was released on YouTube on September 22 to officially announce the upcoming documentary.[69] The documentary, Shawn Mendes – Artist Spotlight Stories, was published on September 28.[70] Ahead of the official release day, Mendes and Neistat held a previewing show of the film where selected fans of Mendes were invited for the event.
The remixed version of "Lost in Japan", by Russian-German DJ Zedd, was released on September 27.[71] Mendes performed the remix version of the single during the 2018 American Music Award held in Los Angeles on October 9.[72] He was joined onstage by Zedd.[73] Mendes was listed on Billboard "21 Under 21 2018", topping the list for the second year in a row for his chart performance, having three consecutive number one albums.[74]

      Mendes performs "In My Blood" at the 2018 MTV Video Music Awards.
Mendes and Zac Brown Band were featured in an episode of the American TV program CMT Crossroads, a show that pairs a country musician with a musician from another genre. The episode was aired on October 24 and was taped a month before the scheduled airing date. Mendes and Zac Brown Band performed nine songs, where they sang parts of each other's songs and covered Michael Jackson's "Man in the Mirror". Parts of the dialogue between Mendes and Zac Brown Band, talking about music and experiences throughout their career, were shown in between the song performances.[75]
On November 1, Mendes was announced as one of the musical performers for the 2018 Victoria's Secret Fashion Show which was recorded in New York City in November and aired in December.[76] He released a three-song remix EP on December 21 entitled The Album (Remixes). The EP includes remixes of songs from his self-titled album such as "Where Were You in the Morning?" with Kaytranada, "Why" with Leon Bridges, and "Youth" with Jessie Reyez.[77]
On May 3, 2019, Mendes released the single "If I Can't Have You" along with its music video.[78] The single debuted at number two in the US Billboard Hot 100, becoming his highest charting single on the chart.[79] It also debuted in the top 10 in Australia and the UK, becoming his fifth top 10 single in both countries.[80][81] On July 19, he released the Gryffin remix of "If I Can't Have You".[82]
On June 21, 2019, he released “Señorita” with Cuban-American singer-songwriter Camila Cabello, along with the music video.[83] The song debuted at number 2 on the US Billboard Hot 100 chart and marks Mendes' and Cabello's second collaboration, following "I Know What You Did Last Summer" released in 2015.[84][85] On August 26, 2019, "Señorita" ascended to the number one position, making it Mendes' first chart-topper on the Hot 100.[86] The deluxe edition of Shawn Mendes was released on July 27, 2019, and includes the songs "If I Can't Have You" and "Señorita".[87]

Artistry and musical influences
Mendes has mainly been described as a pop and folk-pop singer.[88][89] Mendes has cited John Mayer, Ed Sheeran, Justin Timberlake, and Bruno Mars as his main musical influences.[90][91]  Growing up, Mendes listened to reggae music, Led Zeppelin, Garth Brooks, and country music thanks to his parents.[92] He expressed that his second studio album was influenced by Mayer's work[91] while his third album was inspired by Timberlake, Kings of Leon, Kanye West, and Daniel Caesar.[93] For Brittany Spanos of Rolling Stone, Mendes incorporates "catchy acoustic folk-pop tunes" in his catalogue,[89] while for Joe Coscarelli of The New York Times, "his soft, sometimes soulful pop-rock plays primarily to tweens and teenagers, but has also found traction on adult contemporary radio stations".[94] In an interview with Clash magazine, Mendes stated: "I want to create anthems for people. I want to create anthems for big moments in their lives...I don't want my music to play for a few months and then go away forever. And not only that, I want to do incredible things that make a difference too. I think it's not only about the music you release, it's about the things you do while you're making the music."[95]
Brand endorsement and modelling
Mendes signed with Wilhelmina Models in 2016.[96][97][98][99]
In June 2017, Mendes walked the runway during the "Emporio Armani Spring 2018" show held in Milan, Italy.[100] Mendes was wearing the Italian brand's new smartwatch, EA Connected, during the show. Ahead of his runway walk, the promotional video featuring Mendes was shown.[101]
On June 6, 2018, Mendes was announced as the ambassador for Emporio Armani's entire "Fall Winter 2018-2019" watch collection. In July 2018, photographs of Mendes wearing the new EA Connected smartwatches were published on social media.[102]
On February 16, 2019, Mendes announced that he was the latest brand ambassador for Calvin Klein's #MyCalvins campaign.[103] During the 61st Annual Grammy Awards, SmileDirectClub released an advertisement showcasing a campaign between the company and Mendes with some of the proceeds going to "organizations that seek to improve children's health as well as mental and emotional well-being."[104] Later that month, Emporio Armani released a new black-and-white advertisement for its touchscreen smartwatches featuring an instrumental version of "In My Blood" with Mendes boxing.[105]
In August 2019, Mendes announced a partnership with Canadian-based food chain, Tim Hortons, in which he features in a commercial and on beverage cups,[106] followed by a partnership with Roots Canada in September.[107]

Philanthropy and supported causes
In 2014, Mendes and DoSomething.org launched their campaign called "Notes from Shawn" where fans were encouraged to write positive notes and leave them in unexpected places. The campaign was inspired by the lyrics to his first single, "Life of the Party", and addressed low self-esteem, depression and awareness of self-harm.[108] They relaunched their campaign for the second year in a row in 2015, where the campaign was hashtagged online as NotesFromShawn. He partnered with BlendApp with the goal of raising up to $25,000, where $1 was raised for every signup and positive message shared on the application.[109] The campaign was relaunched for a third year in a row in 2016.[110]
He has also worked with Pencils of Promise, a non-profit organization that builds schools and helps raise the quality of education for developing countries, raising $25,000 to build a school in Ghana.[111]
In September 2017, after witnessing the devastation of the earthquake in Mexico City, Mendes created the Mexico Earthquake Relief Fund in conjunction with the American Red Cross and donated $100,000 towards relief efforts.[112]
In 2018, Mendes worked with Omaze to raise funds through donations to support the WE Schools program, a movement aiming to support youth through educational services and mentorship. Mendes encouraged his fans to help the cause while giving the donors a chance to attend his upcoming concert tour.[113]
In September 2018, Mendes took part in the annual Global Citizen festival held at Central Park, New York City.[114] He performed, alongside other artists such as Janelle Monáe, John Legend, and Janet Jackson, and raised awareness regarding the importance of education and children's lack of access to education around the world, particularly young women. Ahead of the event, he took to social media to reach out to Canada's Prime Minister Justin Trudeau, thanking the Prime Minister for leading the initiative for Leave No Girl Behind, a movement whose goal is to empower girls through workshops and programs,[115] and expressing his willingness to talk more about the project. He also encouraged his fans to support the movement. In response, the Prime Minister thanked Mendes and stated that "The more people we have fighting for girls' education, the better! Let's talk."[116]
In October 2018, Mendes, together with producer Teddy Geiger, released a cover of "Under Pressure" by the British rock band Queen and David Bowie.[117] The single was part of a series of covers of Queen's songs, released in celebration of Queen's biopic Bohemian Rhapsody.[118] The proceeds from the single were donated to the Mercury Phoenix Trust, an organization founded by Queen bandmembers after Freddie Mercury's death, which helps fight against HIV/AIDS.[119] Queen's manager, Jim Beach, expressed his gratitude to Mendes and Universal Music Group for helping the cause.[120]
On October 20, 2018, Mendes performed along with other artists such as Khalid, NF, Marshmello, Meghan Trainor, and Ella Mai at The Hollywood Bowl, Los Angeles for the "We Can Survive" event. The event was organized to raise funds for Young Survival Coalition, in an effort to support young women who were diagnosed with breast cancer.[121]
In August 2019, Mendes announced the launch of the Shawn Mendes Foundation, with the aim to "inspire and empower his fans and today’s youth to bring about positive change in the world and advocate for issues they care most about."[122] On 8th January, 2020, Mendes announced that both he and his Shawn Mendes Foundation will be donating an undisclosed sum of money to causes including Australia's Red Cross, the New South Wales Rural Fire Service, and South Australia Country Fire Service, to help ease the strain on those impacted by the devastating fires that are tearing through parts of the country.[123]

Personal life
Mendes has been open about his struggles with anxiety, which he disclosed publicly through "In My Blood", a track from his third studio album. He declared that he had been undergoing therapy in order to help him deal with the mental health condition, stating:

I spoke to a therapist a couple of times [...] Therapy is what works for you. Therapy is listening to music and running on the treadmill, therapy is going to dinner with your friends—it's something that distracts you, that helps you heal and so it just depends on what you think therapy is. I made a conscious effort to be more connected to the people in my life. I found I was closing myself off from everybody, thinking that would help me battle it then realizing the only way I was going to battle it was completely opening up and letting people in.[124]
With regard to speculation about his sexuality, Mendes stated, "First of all, I'm not gay. Second of all, it shouldn't make a difference if I was or if I wasn't. The focus should be on the music and not my sexuality."[125][126][127]
Since July 2019, he has been dating Cuban-American singer Camila Cabello. However, the relationship has garnered controversy, as both were accused of attempting to form a relationship for publicity,[128][129][130] but Mendes insisted it is "definitely not a publicity stunt".[131]

Awards and nominations
 Main article: List of awards and nominations received by Shawn Mendes
Mendes has received several nominations and awards. He won 13 Society of Composers, Authors and Music Publishers of Canada (SOCAN) awards, 11 MTV Europe Music Awards (EMA), eight iHeartRadio Much Music Video Awards (MMVA), eight Juno Awards, three BMI Awards, three American Music Awards, two MTV Video Music Awards, and the Allan Slaight Honour from Canada's Walk of Fame. He was nominated for three Grammy Awards.

Filmography

Films roles


Year
Title
Roles
Notes



2013

Metegol

Young Jake (voice)

English dub; direct to video



Television roles


Year
Title
Roles
Notes



2015

Yo quisiera

Himself

Episode: "Yo soy la bloguera de moda"



2016

The 100

Macallan

Episode: "Wanheda: Part 1"



Saturday Night Live

Himself

Episode: "Emma Stone/Shawn Mendes"



2018

Drop the Mic

Episode: "Odell Beckham Jr. vs. Shawn Mendes / Molly Ringwald vs. Jon Cryer"



2019

Saturday Night Live

Episode: "Adam Sandler/Shawn Mendes"



Discography
 Main articles: Shawn Mendes discography and songs
 Handwritten (2015)

Illuminate (2016)

Shawn Mendes (2018)

Tours
 
 
Biography portal

Canada portal

Pop music portal

Headlining
 #ShawnsFirstHeadlines (2014–2015)

Shawn Mendes World Tour (2016)

Illuminate World Tour (2017)

Shawn Mendes: The Tour (2019)

Co-headlining
 Jingle Ball Tour 2014 (with various artists) (2014)

Jingle Ball Tour 2015 (with various artists) (2015)

Jingle Ball Tour 2018 (with various artists) (2018)

Supporting
 Austin Mahone: Live on Tour (Austin Mahone) (North America) (2014)

The 1989 World Tour (Taylor Swift) (North America) (2015)

References

External links
  Shawn Mendesat Wikipedia's sister projects Media from Wikimedia Commons
            
Quotations from Wikiquote
            


 Official website

Shawn Mendes on IMDb

Shawn Mendes on Twitter

Shawn Mendes on Instagram

   v
t
e
 Shawn Mendes
 
 Awards and nominations

Discography

Songs

Live performances


Studio albums 
 Handwritten

Illuminate

Shawn Mendes


Live albums 
 Live at Madison Square Garden

MTV Unplugged


EPs 
 The Shawn Mendes EP


Singles 
 "Life of the Party"

"Something Big"

"Stitches"

"I Know What You Did Last Summer"

"Treat You Better"

"Mercy"

"There's Nothing Holdin' Me Back"

"In My Blood"

"Youth"

"Where Were You in the Morning?"

"Nervous"

"Lost in Japan"

"If I Can't Have You"

"Señorita"


Featured singles 
 "Oh Cecilia (Breaking My Heart)"

"Lover (Remix)"


Promotional singles 
 "Believe"

"Under Pressure"


Concert tours 
 Shawn's First Headlines

Shawn Mendes World Tour

Illuminate World Tour

Shawn Mendes: The Tour


 
  Category



 Authority control  
 BNF: cb17004182j (data)

GND: 1071955284

ISNI: 0000 0004 5300 2747

LCCN: no2014147012

MusicBrainz: b7d92248-97e3-4450-8057-6fe06738f735

NKC: xx0198586

NTA: 40722176X

SUDOC: 196898285

VIAF: 18144782949690386956

 WorldCat Identities: lccn-no2014147012







