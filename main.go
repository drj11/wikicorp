package main

// wikicorp

// This is the initial scraper.
// It takes as input a CSV file (filename given on the command line),
// where each row is a Wikipedia page.
// It uses the Wikipedia API to fetch the JSON version of each page,
// create one file per page in the `crawl/` directory.

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"time"
)

var wikip = flag.String("wiki", "en", "Which wikipedia to use; .wikipedia.org is appended")

var Pattern = "https://%s.wikipedia.org/w/api.php?action=parse&format=json&page=%s&redirects=true"

var Sleep = flag.Int("sleep", 1000, "time to sleep between GETs")

func main() {
	err := mainerr()
	if err != nil {
		log.Fatal(err)
	}
}

func mainerr() error {
	flag.Parse()
	file_name := flag.Arg(0)

	r, err := os.Open(file_name)
	if err != nil {
		return err
	}

	rows, err := csv.NewReader(r).ReadAll()
	if err != nil {
		return err
	}

	return Scrape(*wikip, rows)
}

// wiki gives the prefix of the xx.wikipedia.org server to use.
func Scrape(wiki string, rows [][]string) error {
	rows = rows[1:]
	for _, row := range rows {
		err := func() error {
			page := row[0]
			page_url := fmt.Sprintf(Pattern, wiki, url.QueryEscape(page))
			response, err := http.Get(page_url)
			if err != nil {
				return err
			}
			defer response.Body.Close()

			w, err := os.OpenFile(path.Join("crawl",
				url.PathEscape(page)),
				os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
			if err != nil {
				return err
			}
			defer w.Close()

			_, err = io.Copy(w, response.Body)
			if err != nil {
				return err
			}
			fmt.Println("Written", page)
			time.Sleep(time.Duration(*Sleep) * time.Millisecond)
			return nil
		}()
		if err != nil {
			return err
		}
	}
	return nil
}
