# part

`part` is a typographical alternative to split.

The principal is that unlike general purpose word splitting,
it is of some interest to retain words and their punctuation.
This way we can see samples of words and their adjacent
punctuation.

Following the terminology of https://www.unicode.org/reports/tr14/
the `part` tool partitions the text between the classes:
BK CR LF NL ZW SP


The following `egrep` identifies those character codes:

    egrep ';(BK|CR|LF|NL|ZW|SP) ' vendor/LineBreak.txt

However, I think we also need the BA characters that have
classes Zs or Cc:

    egrep ';BA.*# (Zs|Cc)' vendor/LineBreak.txt
