package main

import (
	"bufio"
	"flag"
	"io"
	"log"
	"os"
)

func main() {
	flag.Parse()

	for _, arg := range flag.Args() {
		err := Part(arg)
		if err != nil {
			log.Print(err)
		}
	}
}

func Part(filename string) error {
	var r io.ReadCloser
	var err error

	if filename == "-" {
		r = os.Stdin
	} else {
		r, err = os.Open(filename)
		if err != nil {
			return err
		}
		defer r.Close()
	}

	b := bufio.NewReader(r)
	out := os.Stdout

	// Whether a newline, '\n', has just been output.
	nl := false
	for {
		r, _, err := b.ReadRune()
		if err == io.EOF {
			break
		}
		switch r {
		case 0x9, 0xa, 0xb, 0xc, 0xd, ' ', 0x85,
			0x1680, 0x2000, 0x2001, 0x2002, 0x2003, 0x2004,
			0x2005, 0x2006,
			0x2008, 0x2009, 0x200A,
			0x200B, 0x2028, 0x2029,
			0x205F, 0x3000:
			if !nl {
				out.Write([]byte{'\n'})
				nl = true
			}
		default:
			out.Write([]byte(string(r)))
			nl = false
		}
	}
	return nil
}
