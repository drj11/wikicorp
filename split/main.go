package main

// Split a text into words.

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"unicode"
)

func main() {
	err := mainerr()
	if err != nil {
		log.Fatal(err)
	}
}

func mainerr() error {
	flag.Parse()

	for _, arg := range flag.Args() {
		r, err := OpenFile(arg)
		if err != nil {
			return err
		}
		defer r.Close()

		scanner := bufio.NewScanner(r)
		for scanner.Scan() {
			Split(os.Stdout, scanner.Text())
		}
		err = scanner.Err()
		if err != nil {
			return err
		}
	}

	return nil
}

func OpenFile(name string) (io.ReadCloser, error) {
	if name == "-" {
		return os.Stdin, nil
	}
	r, err := os.Open(name)
	return r, err
}

const (
	S_Letter  = 1
	S_Numeric = 4
	S_WB6     = 6
	S_WB12    = 12
	S_Any     = 99
)

func Split(w io.Writer, s string) {
	if s == "" {
		return
	}
	if len(s) <= 1 {
		fmt.Fprintf(w, "%s\n", s)
		return
	}

	BadRune := rune(-1)

	rs := []rune(s)
	rs = append(rs, BadRune, BadRune)

	state := 0x0
	tok_beg := 0
	i := 0
	r := rs[i]
	n := rs[i+1]

	Tok := func() {
		if tok_beg == i {
			return
		}
		fmt.Fprintf(w, "%s\n", string(rs[tok_beg:i]))
		tok_beg = i
	}

	// in each iteration, we either:
	// Tok() and return to state 0x0; or,
	// advance (and possibly change state).
	for {
		switch state {
		case 0x0:
			switch {
			case r == BadRune:
				return
			case unicode.IsNumber(r):
				state = S_Numeric
			case unicode.IsLetter(r):
				state = S_Letter
			default:
				state = S_Any
			}
		case S_Letter:
			switch {
			case unicode.IsLetter(r):
				// WB5
			case unicode.IsNumber(r):
				// WB9
				state = S_Numeric
			case (MidLetter(r) || MidNumLetQ(r)) && AHLetter(n):
				state = S_WB6
			default:
				Tok()
				state = 0x0
				continue
			}
		case S_WB6:
			switch {
			case AHLetter(r):
				state = S_Letter
			default:
				log.Fatal("WB6 NOT REACHED")
			}
		case S_Numeric:
			switch {
			case AHLetter(r):
				// WB10
				state = S_Letter
			case unicode.IsNumber(r):
				// WB8
			case (MidNum(r) || MidNumLetQ(r)) && unicode.IsNumber(n):
				state = S_WB12
			default:
				Tok()
				state = 0x0
				continue
			}
		case S_WB12:
			switch {
			case unicode.IsNumber(r):
				state = S_Numeric
			default:
				log.Fatal("WB12 NOT REACHED")
			}

		case S_Any:
			Tok()
			state = 0x0
			continue
		default:
			log.Fatal("NOT IMPLEMENTED State ", state)
		}
		i += 1
		r = n
		n = rs[i+1]
	}
}

func MidLetter(r rune) bool {
	switch r {
	case 0x003A, 0x00B7,
		0x0387, 0x055F, 0x05F4,
		0x2027, 0xFE13, 0xFE55, 0xFF1A:
		return true
	}
	return false
}

func MidNum(r rune) bool {
	// According to https://www.unicode.org/reports/tr14/
	// `Line_Break = Infix Numeric` corresponds to Class IS in
	// LineBreak.txt
	// Extracted from https://www.unicode.org/Public/UCD/latest/ucd/LineBreak.txt
	// on 2020-07-01.
	switch r {
	case 0x002C,
		// 0x002E,
		// 0x003A,
		0x003B,
		0x037E,
		0x0589,
		0x060C,
		0x060D,
		0x07F8,
		0x2044,
		0xFE10,
		// 0xFE13,
		0xFE14,
		0x066C, 0xFE50, 0xFE54, 0xFF0C, 0xFF1B:
		return true
	}
	return false
}

func AHLetter(r rune) bool {
	// :todo: add Hebrew
	return unicode.IsLetter(r)
}

func MidNumLetQ(r rune) bool {
	switch r {
	case 0x0027, 0x002E,
		0x2018, 0x2019, 0x2024,
		0xFE52, 0xFF07, 0xFF0E:
		return true
	}
	return false
}
