# Strip

`strip` is a program that recovers the HTML from the Wikipedia
JSON response that is stored in a file, and strips the HTML.
It yields a plain text file.

Only certain tags are kept, see source code.

In general for an HTML element,
the start- and end-tag are removed (along with attributes),
leaving only the text contents.


## Tags with replaced or added text

In order to insert whitespace that is implied,
certain tags result in additional text being output.

After a TD or TH a U+001F is output.

After a TR a \n is output.

Before a DIV, DL, UL or OL a U+2029 is output.

After a DD, DT, LI a \n is output

A BR tag (which conventionally has no text content) is replaced.
As per UAX #13
http://www.unicode.org/reports/tr13/tr13-9.html
it is replaced with LINE SEPARATOR U+2028.
This displays as unfound glyph (2028 in a box) on my Terminal,
but that hardly seems like my fault.

A HR tag is replaced with U+2029 U+2015 U+2029
(PARAGRAPH SEPARATOR, HORIZONTAL BAR, PARAGRAPH SEPARATOR)

## Wikipedia markup

Some Wikipedia markup is removed.

The `[edit]` link is removed by removing `span.mw-editsection`
elements.

Lists of notes, footnotes, and references, are removed by
removing `div.reflist` elements.

## Questions remaining

what to do with Q? (only appears in cite so far)

Should we denote sections somehow?

What to do with WBR?
