package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"golang.org/x/net/html"
	"io"
	"log"
	"os"
	"strings"
)

// Used to model and decode JSON from the Wikipedia API.
type WikiJSON struct {
	Parse struct {
		Title string
		Text  struct {
			Star string `json:"*"`
		}
	}
}

func main() {
	err := mainerr()
	if err != nil {
		log.Fatal(err)
	}
}
func mainerr() error {
	flag.Parse()

	file_name := flag.Arg(0)
	r, err := os.Open(file_name)
	if err != nil {
		return err
	}

	v := &WikiJSON{}

	json.NewDecoder(r).Decode(v)

	w, err := os.OpenFile(file_name+".html",
		os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		return nil
	}
	io.Copy(w, strings.NewReader(v.Parse.Text.Star))

	reader := strings.NewReader(v.Parse.Text.Star)

	doc, err := html.Parse(reader)
	if err != nil {
		return err
	}

	Strip(doc, os.Stdout)
	return nil
}

var KeepSlice = []string{
	"a",
	"abbr",
	"b",
	"bdi",
	"big",
	"blockquote",
	"body",
	"br",
	"caption",
	"center",
	"cite",
	"code",
	"dd",
	"del",
	"div",
	"dl",
	"dt",
	"em",
	"h2",
	"h3",
	"h4",
	"h5",
	"h6",
	"hr",
	"html",
	"i",
	"kbd",
	"label",
	"li",
	"ol",
	"p",
	"pre",
	"q",
	"s",
	"small",
	"source",
	"span",
	"strong",
	"sub",
	"sup",
	"table",
	"tbody",
	"td",
	"th",
	"time",
	"tr",
	"tt",
	"u",
	"ul",
	"var",
}

func SliceToMap(slice []string) map[string]struct{} {
	m := map[string]struct{}{}
	for _, s := range slice {
		m[s] = struct{}{}
	}
	return m
}

var Keep = SliceToMap(KeepSlice)

// Return true if and only if attribute slice
// contains an attribute with the key and where one of the
// space-delimited fields of that key's value matches val.
func InAttr(attrs []html.Attribute, key, val string) bool {
	for _, attr := range attrs {
		if attr.Key == key {
			for _, s := range strings.Fields(attr.Val) {
				if s == val {
					return true
				}
			}
		}
	}
	return false
}

func Strip(node *html.Node, w io.Writer) {
	switch node.Type {
	case html.ElementNode:
		_, keep := Keep[node.Data]
		if !keep {
			log.Println("REJECT", node.Data)
			return
		}

		// Reject DIV.reflist
		if node.Data == "div" &&
			InAttr(node.Attr, "class", "reflist") {
			log.Println("REJECT DIV.reflist")
			return
		}

		switch node.Data {
		case "span":
			// Reject <span class="mw-editsection" />
			if InAttr(node.Attr, "class", "mw-editsection") {
				log.Println("REJECT span mw-editsection")
				return
			}
		case "br":
			// BR and HR are converted using suggestions in
			// http://www.unicode.org/reports/tr13/tr13-9.html
			// Convert BR to LINE SEPARATOR U+2028
			fmt.Fprint(w, "\u2028")
			return
		case "hr":
			// Convert HR to horizontal line surrounded by
			// paragraph separators.
			fmt.Fprint(w, "\u2029\u2015\u2029")
			return
		case "div", "dl", "ol", "ul":
			// Before DIV, DL, OL and UL, put PARAGRAPH SEPARATOR.
			fmt.Fprint(w, "\u2029")
		}
	case html.TextNode:
		fmt.Fprint(w, node.Data)
	}

	for c := node.FirstChild; c != nil; c = c.NextSibling {
		Strip(c, w)
	}

	if node.Type == html.ElementNode {
		switch node.Data {
		case "td", "th":
			fmt.Fprint(w, "\u001F")
		case "dd", "dt", "tr", "li":
			fmt.Fprint(w, "\n")
		}
	}
}
