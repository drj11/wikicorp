# The non-alphabetic characters appearing in English words

David Jones
2020-07-02
Draft

This study uses the en-wiki-500-2019 corpus.
The tokens are the output of the an implementation of the
Unicode UAX #29 Word Breaking algorithm.

The token stream contains the tokens in the order that appear
in each Wikipedia page, considering pages in ASCIIbetical order.
Thus the same token can appear multiple times.

Consider tokens that contain an [:alpha:] and also
contain a non-[:alpha:].

70123 (13120 uniq)

of those 70K, 44885 contain «'», and of those 38281 end in «'s»
of those 70K, 306 end in «’s» (U+2019)
of those 70K, 24450 consist of only alpha, digits, and .
adding ' : , and ’ to that mix, leaves only 45 words (20 unique),
that contain anything other than :alpha:, 0-9, «.':,’»

## Primary findings

Apostrophe (U+0027) is the most common.
No surprises there.
Typographically it may be better to use
RIGHT SINGLE QUOTATION MARK U+2019 «’»,
and this is apparently also recommended by Unicode.
For the most common case of ’s,
apostrophe is used 125 times more frequently than
RIGHT SINGLE QUOTATION MARK.
While 0.8% is not very common, it is frequent enough that
downstream text processing systems ought to handle this case.

Full stop is used in words like "U.S." and "S.H.I.E.L.D.".

Numbers appear in words like 20th, 1950s, 3D, 4K,
but the most common is the disappointingly illiterate km2.

Comma appears almost exclusively in large monetary quantities
that use a letter as a currency symbol, such as 5,000,000R, and
large ordinals, such as 1,000.
And once in «O!RUL8,2?» an E.P. by BTS
(which this corpus fails to tokenise correctly).

Superscripts appear with low frequency (33 of the 70K),
but in contexts that are important to handle correctly.
For example, the typographically correct km², and
formulas like E = MC².

Colon (U+003A) appears mostly as Wikipedia artifacts,
but also occasionally in proper nouns:
ZE:A, Korean boy band;
Gen:Lock, an animated series;
Sixx:A.M., a hard rock band.


## Extremely secondary findings

U+2019 appears apparently incorrectly, in Blossom‘s and war‘s.

Some superscript 1 and 2 appears in
romanization of Mandarian, such as Tʻai²-chung¹,
a city in Taiwan.

Incidentally the mark that appears after the T
in Tʻai²-chung¹ that looks like a tick, is in fact
MODIFIER LETTER TURNED COMMA (U+02BB), which is a letter.
It is normally used as the
[ʻOkina](https://en.wikipedia.org/wiki/%CA%BBOkina) a phonemic
glottal stop used in some Polynesian languages.
So its appearance here may be a mistake
(I'm not familiar with Wade–Giles romanization).

Subscript 2 appears in CO₂, and the Proto-Indo-European tewtéh₂.

The Hebrew abbreviation א״י (Eretz Israel) appears once, and
the Hebrew word Shābhû‘ôth

Middle dot appears as help·info which is
apparently an artefact of Wikipedia and my stripping algorithm.

## END
